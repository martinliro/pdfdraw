/**
 * Contains PDFNetJS classes and functions
 */
export declare namespace PDFNet {
    /**
     * Converter is a utility class used to convert documents and files to PDF.
    Conversion of XPS, EMF and image files to PDF documents is performed internally.
    Other document formats are converted via native application and printing.
     */
    class Convert {
        /**
         * Method to create an OfficeToPDFOptions object
         * @returns A promise that resolves to a PDFNet.Convert.OfficeToPDFOptions.
         */
        static createOfficeToPDFOptions(): Promise<PDFNet.Convert.OfficeToPDFOptions>;
        /**
         * Method to create an XPSOutputOptions object
         * @returns A promise that resolves to a PDFNet.Convert.XPSOutputOptions.
         */
        static createXPSOutputOptions(): Promise<PDFNet.Convert.XPSOutputOptions>;
        /**
         * Method to create an XODOutputOptions object
         * @returns A promise that resolves to a PDFNet.Convert.XODOutputOptions.
         */
        static createXODOutputOptions(): Promise<PDFNet.Convert.XODOutputOptions>;
        /**
         * Method to create an TiffOutputOptions object
         * @returns A promise that resolves to a PDFNet.Convert.TiffOutputOptions.
         */
        static createTiffOutputOptions(): Promise<PDFNet.Convert.TiffOutputOptions>;
        /**
         * Method to create an HTMLOutputOptions object
         * @returns A promise that resolves to a PDFNet.Convert.HTMLOutputOptions.
         */
        static createHTMLOutputOptions(): Promise<PDFNet.Convert.HTMLOutputOptions>;
        /**
         * Method to create an EPUBOutputOptions object
         * @returns A promise that resolves to a PDFNet.Convert.EPUBOutputOptions.
         */
        static createEPUBOutputOptions(): Promise<PDFNet.Convert.EPUBOutputOptions>;
        /**
         * Method to create an SVGOutputOptions object
         * @returns A promise that resolves to a PDFNet.Convert.SVGOutputOptions.
         */
        static createSVGOutputOptions(): Promise<PDFNet.Convert.SVGOutputOptions>;
        /**
         * Method to create an CADConvertOptions object
         * @returns A promise that resolves to a PDFNet.Convert.CADConvertOptions.
         */
        static createCADConvertOptions(): Promise<PDFNet.Convert.CADConvertOptions>;
        /**
         * Convert the specified XPS document to PDF and append converted pages
         * to the specified PDF document.
         * @param in_pdfdoc - the PDFDoc to append to
         * @param in_filename - the path to the XPS document to convert
         */
        static fromXps(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string): Promise<void>;
        /**
         * Convert the specified XPS document contained in memory to PDF
         * and append converted pages to the specified PDF document.
         * @param in_pdfdoc - the PDFDoc to append to
         * @param buf - the buffer containing the xps document
         */
        static fromXpsMem(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<void>;
        /**
         * Convert the specified EMF to PDF and append converted pages to
         * to the specified PDF document.  EMF will be fitted to the page.
         * @param in_pdfdoc - the PDFDoc to append to
         * @param in_filename - the path to the EMF document to convert
         */
        static fromEmf(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string): Promise<void>;
        /**
         * Convert the Page to EMF and save to the specified path
         * @param in_page - the Page to convert to EMF
         * @param in_filename - the path to the EMF file to create
         */
        static pageToEmf(in_page: PDFNet.Page, in_filename: string): Promise<void>;
        /**
         * Convert the PDFDoc to EMF and save to the specified path
         * @param in_pdfdoc - the PDFDoc to convert to EMF
         * @param in_filename - the path to the EMF files to create, one file per page
         */
        static docToEmf(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string): Promise<void>;
        /**
         * Convert the specified plain text file to PDF and append converted
         * pages to the specified PDF document.
         * @param in_pdfdoc - the PDFDoc to append to
         * @param in_filename - the path to the plain text document to convert
         * @param [options] - the conversion options. The available options are:
         *
         * | Option Name             | Type    | Note                                                    |
         * |-------------------------|---------|---------------------------------------------------------|
         * | BytesPerBite            | Integer | In bytes. Use for streaming conversion only.            |
         * | FontFace                | String  | Set the font face used for the conversion.              |
         * | FontSize                | Integer | Set the font size used for the conversion.              |
         * | LineHeightMultiplier    | Double  | Set the line height multiplier used for the conversion. |
         * | MarginBottom            | Double  | In inches. Set the bottom margin of the page.           |
         * | MarginLeft              | Double  | In inches. Set the left margin of the page.             |
         * | MarginRight             | Double  | In inches. Set the right margin of the page.            |
         * | MarginTop               | Double  | In inches. Set the top margin of the page.              |
         * | PageHeight              | Double  | In inches. Set the page height.                         |
         * | PageWidth               | Double  | In inches. Set the page width.                          |
         * | UseSourceCodeFormatting | Boolean | Set whether to use mono font for the conversion.        |
         */
        static fromText(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string, options?: PDFNet.Obj): Promise<void>;
        static pageToSvg(in_page: PDFNet.Page, in_filename: string): Promise<void>;
        /**
         * Convert the Page to SVG and save to the specified path
         * @param in_page - the Page to convert to SVG
         * @param in_filename - the path to the SVG file to create
         * @param [options] - the conversion options
         */
        static pageToSvgWithOptions(in_page: PDFNet.Page, in_filename: string, options?: PDFNet.Obj | PDFNet.Convert.SVGOutputOptions): Promise<void>;
        static docToSvg(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string): Promise<void>;
        /**
         * Convert the PDFDoc to SVG and save to the specified path
         * @param in_pdfdoc - the PDFDoc to convert to SVG
         * @param in_filename - the path to the SVG files to create, one file per page
         * @param [options] - the conversion options
         */
        static docToSvgWithOptions(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string, options?: PDFNet.Obj | PDFNet.Convert.SVGOutputOptions): Promise<void>;
        /**
         * Convert the PDFDoc to XPS and save to the specified path
         * @param in_pdfdoc - the PDFDoc to convert to XPS
         * @param in_filename - the path to the document to create
         * @param [options] - the conversion options
         */
        static toXps(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string, options?: PDFNet.Obj | PDFNet.Convert.XPSOutputOptions): Promise<void>;
        /**
         * Convert the input file to XPS format and save to the specified path
         * @param in_inputFilename - the file to convert to XPS
         * @param in_outputFilename - the path to the XPS file to create
         * @param [options] - the conversion options
         */
        static fileToXps(in_inputFilename: string, in_outputFilename: string, options?: PDFNet.Obj | PDFNet.Convert.XPSOutputOptions): Promise<void>;
        /**
         * Convert the input file to XOD format and save to the specified path
         * @param in_inputFilename - the file to convert to XOD
         * @param in_outputFilename - the path to the XOD file to create
         * @param [options] - the conversion options
         */
        static fileToXod(in_inputFilename: string, in_outputFilename: string, options?: PDFNet.Obj | PDFNet.Convert.XODOutputOptions): Promise<void>;
        /**
         * Convert the input file to XOD format and save to the specified path
         * @param in_pdfdoc - the PDFDoc to convert to XOD
         * @param in_outputFilename - the path to the XOD file to create
         * @param [options] - the conversion options
         */
        static toXod(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_outputFilename: string, options?: PDFNet.Obj | PDFNet.Convert.XODOutputOptions): Promise<void>;
        /**
         * Generate a stream that incrementally converts the input file to XOD format.
         * @param in_filename - the file to convert to XOD
         * @param [options] - the conversion options
         * @returns A promise that resolves to a filter from which the file can be read incrementally.
         */
        static fileToXodStream(in_filename: string, options?: PDFNet.Obj | PDFNet.Convert.XODOutputOptions): Promise<PDFNet.Filter>;
        /**
         * Generate a stream that incrementally converts the input file to XOD format.
         * @param in_pdfdoc - the PDFDoc to convert to XOD
         * @param [options] - the conversion options
         * @returns A promise that resolves to a filter from which the file can be read incrementally.
         */
        static toXodStream(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, options?: PDFNet.Obj | PDFNet.Convert.XODOutputOptions): Promise<PDFNet.Filter>;
        /**
         * Create a DocumentConversion object suitable for converting a file to pdf.
         * Handles incoming files in .docx, .xlsx, pptx, .doc, .png, .jpg, .bmp, .gif, .jp2, .tif, .txt, .xml and .md format
         * This conversion will be performed entirely within PDFNet, and does not rely on any external functionality.
         *
         * This method allows for more control over the conversion process than the single call
         * ToPDF() interface. It does not perform any  conversion logic immediately, and can be expected
         * to return quickly. To perform the actual conversion, use the returned DocumentConversion object.
         * @param in_filename - the path to the source document.
         * @param [options] - the conversion options
         * @returns A promise that resolves to a DocumentConversion object which encapsulates this particular conversion.
         */
        static streamingPdfConversionWithPath(in_filename: string, options?: PDFNet.Obj | PDFNet.Convert.ConversionOptions): Promise<PDFNet.DocumentConversion>;
        /**
         * Create a DocumentConversion object suitable for converting a file to pdf and appending to the specified PDF document.
         * Handles incoming files in .docx, .xlsx, pptx, .doc, .png, .jpg, .bmp, .gif, .jp2, .tif, .txt, .xml and .md format
         * This conversion will be performed entirely within PDFNet, and does not rely on any external functionality.
         *
         * This method allows for more control over the conversion process than the single call
         * ToPDF() interface. It does not perform any  conversion logic immediately, and can be expected
         * to return quickly. To perform the actual conversion, use the returned DocumentConversion object.
         * @param in_pdfdoc - the conversion result will be appended to this pdf.
         * @param in_filename - the path to the source document.
         * @param [options] - the conversion options
         * @returns A promise that resolves to a DocumentConversion object which encapsulates this particular conversion.
         */
        static streamingPdfConversionWithPdfAndPath(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string, options?: PDFNet.Obj | PDFNet.Convert.ConversionOptions): Promise<PDFNet.DocumentConversion>;
        /**
         * Create a DocumentConversion object suitable for converting an office document (in .docx, .xlsx, pptx, or .doc format)
         * to pdf and appending to the specified PDF document.
         * This conversion will be performed entirely within PDFNet, and does not rely on Word
         * interop or any other external functionality.
         *
         * This method does not perform any  conversion logic and can be expected
         * to return quickly. To do the actual conversion, use the returned DocumentConversion object.
         * @param in_stream - the source document data.
         * @param [options] - the conversion options
         * @returns A promise that resolves to a DocumentConversion object which encapsulates this particular conversion.
         */
        static streamingPdfConversionWithFilter(in_stream: PDFNet.Filter, options?: PDFNet.Obj | PDFNet.Convert.ConversionOptions): Promise<PDFNet.DocumentConversion>;
        /**
         * Create a DocumentConversion object suitable for converting an office document (in .docx, .xlsx, pptx, or .doc format)
         * to pdf and appending to the specified PDF document.
         * This conversion will be performed entirely within PDFNet, and does not rely on Word
         * interop or any other external functionality.
         *
         * This method does not perform any  conversion logic and can be expected
         * to return quickly. To do the actual conversion, use the returned DocumentConversion object.
         * @param in_pdfdoc - the conversion result will be appended to this pdf.
         * @param in_stream - the source document data.
         * @param [options] - the conversion options
         * @returns A promise that resolves to a DocumentConversion object which encapsulates this particular conversion.
         */
        static streamingPdfConversionWithPdfAndFilter(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_stream: PDFNet.Filter, options?: PDFNet.Obj | PDFNet.Convert.ConversionOptions): Promise<PDFNet.DocumentConversion>;
        /**
         * convert the file or document to PDF and append to the specified PDF document
         * @param in_pdfdoc - the PDFDoc to append the converted document to. The
         * PDFDoc can then be converted to XPS, EMF or SVG using the other functions
         * in this class.
         * @param in_filename - the path to the document to be converted to pdf
         */
        static toPdf(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string): Promise<void>;
        static fromCAD(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_filename: string, opts?: PDFNet.Obj | PDFNet.Convert.CADConvertOptions): Promise<void>;
        static fromTiff(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_data: PDFNet.Filter): Promise<void>;
        /**
         * Utility function to determine if ToPdf or ToXps will require the PDFNet
         * printer to convert a specific external file to PDF.
         * @param in_filename - the path to the document to be checked
         * @returns A promise that resolves to true if ToPdf requires the printer to convert the file, false
         * otherwise.
         */
        static requiresPrinter(in_filename: string): Promise<boolean>;
        /**
         * Install the PDFNet printer. Installation can take a few seconds,
         * so it is recommended that you install the printer once as part of
         * your deployment process.  Duplicated installations will be quick since
         * the presence of the printer is checked before installation is attempted.
         * There is no need to uninstall the printer after conversions, it can be
         * left installed for later access.
         * @param [in_printerName] - the name of the printer to install and use for conversions.
         * If in_printerName is not provided then the name "PDFTron PDFNet" is used.
         */
        static printerInstall(in_printerName?: string): Promise<void>;
        /**
         * Uninstall all printers using the PDFNet printer driver.
         */
        static printerUninstall(): Promise<void>;
        /**
         * Get the name of the PDFNet printer installed in this process session.
         * @returns A promise that resolves to the Unicode name of the PDFNet printer
         */
        static printerGetPrinterName(): Promise<string>;
        static printerSetPrinterName(in_printerName?: string): Promise<void>;
        /**
         * Determine if the PDFNet printer is installed
         * @param [in_printerName] - the name of the printer to install and use for conversions.
         * If in_printerName is not provided then the name "PDFTron PDFNet" is used.
         * @returns A promise that resolves to true if the named printer is installed, false otherwise
         */
        static printerIsInstalled(in_printerName?: string): Promise<boolean>;
        /**
         * Configure how PDFNet prints documents.
         * @param print_mode - <pre>
         * PDFNet.Convert.PrinterMode = {
         * 	e_auto : 0
         * 	e_interop_only : 1
         * 	e_printer_only : 2
         * 	e_prefer_builtin_converter : 3
         * }
         * </pre>
         * set the print mode. Default is e_auto.
         */
        static printerSetMode(print_mode: number): Promise<void>;
        /**
         * Get the current mode for print jobs.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Convert.PrinterMode = {
         * 	e_auto : 0
         * 	e_interop_only : 1
         * 	e_printer_only : 2
         * 	e_prefer_builtin_converter : 3
         * }
         * </pre>
         * @returns A promise that resolves to the current print mode
         */
        static printerGetMode(): Promise<number>;
        /**
         * @returns A promise that resolves to an object of type: "string"
         */
        static pageToHtml(page: PDFNet.Page): Promise<string>;
        /**
         * Convert a file to HTML and save to the specified path
         * @param in_inputFilename - the file to convert to HTML
         * @param in_outputPath - the path to where generated content will be stored
         * @param [options] - the conversion options
         */
        static fileToHtml(in_inputFilename: string, in_outputPath: string, options?: PDFNet.Obj | PDFNet.Convert.HTMLOutputOptions): Promise<void>;
        /**
         * Convert the PDF to HTML and save to the specified path
         * @param in_pdfdoc - the PDF doc to convert to HTML
         * @param in_outputPath - the path to where generated content will be stored
         * @param [options] - the conversion options
         */
        static toHtml(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_outputPath: string, options?: PDFNet.Obj | PDFNet.Convert.HTMLOutputOptions): Promise<void>;
        /**
         * Convert a file to EPUB format and save to the specified path
         * @param in_inputFilename - the file to convert to EPUB
         * @param in_outputFilename - the path to the EPUB file to create
         * @param [html_options] - the conversion options
         * @param [epub_options] - the conversion options
         */
        static fileToEpub(in_inputFilename: string, in_outputFilename: string, html_options?: PDFNet.Obj | PDFNet.Convert.HTMLOutputOptions, epub_options?: PDFNet.Obj | PDFNet.Convert.EPUBOutputOptions): Promise<void>;
        /**
         * Convert the PDFDoc to EPUB format and save to the specified path
         * @param in_pdfdoc - the PDFDoc to convert to EPUB
         * @param in_outputFilename - the path to the EPUB file to create
         * @param [html_options] - the conversion options
         * @param [epub_options] - the conversion options
         */
        static toEpub(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_outputFilename: string, html_options?: PDFNet.Obj | PDFNet.Convert.HTMLOutputOptions, epub_options?: PDFNet.Obj | PDFNet.Convert.EPUBOutputOptions): Promise<void>;
        /**
         * Convert a file to multipage TIFF and save to the specified path
         * @param in_inputFilename - the file to convert to multipage TIFF
         * @param in_outputFilename - the path to the EPUB file to create
         * @param [options] - the conversion options
         */
        static fileToTiff(in_inputFilename: string, in_outputFilename: string, options?: PDFNet.Obj | PDFNet.Convert.TiffOutputOptions): Promise<void>;
        /**
         * Convert the PDF to multipage TIFF and save to the specified path
         * @param in_pdfdoc - the PDF doc to convert to multipage
         * @param in_outputFilename - the path to the EPUB file to create
         * @param [options] - the conversion options
         */
        static toTiff(in_pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_outputFilename: string, options?: PDFNet.Obj | PDFNet.Convert.TiffOutputOptions): Promise<void>;
        /**
         * Convert the an office document (in .docx, .xlsx, pptx, or .doc format) to pdf and append to the specified PDF document.
        This conversion is performed entirely within PDFNet, and does not rely on Word
        interop or any other external functionality.
         * @param in_filename - the path to the source document.
         * @param [options] - the conversion options
         * @returns A promise that resolves to a PDFDoc object
         */
        static officeToPdfWithPath(in_filename: string, options?: PDFNet.Obj): Promise<PDFNet.PDFDoc>;
        /**
         * Convert the an office document (in .docx, .xlsx, pptx, or .doc format) to pdf and append to the specified PDF document.
        This conversion is performed entirely within PDFNet, and does not rely on Word
        interop or any other external functionality.
         * @param in_stream - the source document data.
         * @param [options] - the conversion options
         * @returns A promise that resolves to a PDFDoc object
         */
        static officeToPdfWithFilter(in_stream: PDFNet.Filter, options?: PDFNet.Obj): Promise<PDFNet.PDFDoc>;
        /**
         * Convert the an office document (in .docx, .xlsx, pptx, or .doc format) to pdf.
         * @param input - Either a url from which to download the file or an ArrayBuffer containing the file data.
         * @param [options] - An object containing conversion options
         * @returns A promise that resolves to a "PDFDoc".
         */
        static office2PDF(input: string | ArrayBuffer, options?: PDFNet.Obj): Promise<PDFNet.PDFDoc>;
        /**
         * @param input - Either a url from which to download the file or an ArrayBuffer containing the file data.
         * @param [options] - An object containing conversion options
         * @returns A promise that resolves to an Uint8Array containing the resulting PDF data.
         */
        static office2PDFBuffer(input: string | ArrayBuffer, options?: PDFNet.Obj): Promise<Uint8Array>;
    }
    /**
     * The Optimizer class provides functionality for optimizing/shrinking
    output PDF files.
    
    'pdftron.PDF.Optimizer' is an optional PDFNet Add-On utility class that can be
    used to optimize PDF documents by reducing the file size, removing redundant
    information, and compressing data streams using the latest in image compression
    technology. PDF Optimizer can compress and shrink PDF file size with the
    following operations:
    - Remove duplicated fonts, images, ICC profiles, and any other data stream.
    - Optionally convert high-quality or print-ready PDF files to small, efficient and web-ready PDF.
    - Optionally down-sample large images to a given resolution.
    - Optionally compress or recompress PDF images using JBIG2 and JPEG2000 compression formats.
    - Compress uncompressed streams and remove unused PDF objects.
     */
    class Optimizer {
        /**
         * Method to create an ImageSettings object
         * @returns A promise that resolves to a PDFNet.Optimizer.ImageSettings.
         */
        static createImageSettings(): Promise<PDFNet.Optimizer.ImageSettings>;
        /**
         * Method to create an MonoImageSettings object
         * @returns A promise that resolves to a PDFNet.Optimizer.MonoImageSettings.
         */
        static createMonoImageSettings(): Promise<PDFNet.Optimizer.MonoImageSettings>;
        /**
         * Method to create an TextSettings object
         * @returns A promise that resolves to a PDFNet.Optimizer.TextSettings.
         */
        static createTextSettings(): Promise<PDFNet.Optimizer.TextSettings>;
        /**
         * Method to create an OptimizerSettings object
         * @returns A promise that resolves to a PDFNet.Optimizer.OptimizerSettings.
         */
        static createOptimizerSettings(): Promise<PDFNet.Optimizer.OptimizerSettings>;
        /**
         * Optimize the given document using the optimizers settings
         * @param doc - the document to optimize
         * @param [optimizerSettings] - the settings for the optimizer
         */
        static optimize(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, optimizerSettings?: any): Promise<void>;
    }
    /**
     * An abstract class containing utility functions for destroyable classes.
     */
    class Destroyable {
        /**
         * Take the ownership of this object, so that PDFNet.runWithCleanup won't destroy this object.
         */
        takeOwnership(): void;
        /**
         * Destructor
         */
        destroy(): Promise<void>;
    }
    /**
     * Method to create a RefreshOptions object
     * @returns A promise that resolves to a PDFNet.RefreshOptions.
     */
    function createRefreshOptions(): Promise<PDFNet.RefreshOptions>;
    /**
     * Options for PDFNet.PDFDoc.RefreshAnnotAppearances or PDFNet.Annot.refreshAppearanceRefreshOptions
     */
    class RefreshOptions {
        /**
         * Gets the value DrawBackgroundOnly from the options object
         * If true draw only the background and border, which can be useful when generating the rest of the annotation content elsewhere. Off by default.
         * @returns the current value for DrawBackgroundOnly.
         */
        getDrawBackgroundOnly(): boolean;
        /**
         * Sets the value for DrawBackgroundOnly in the options object
         * If true draw only the background and border, which can be useful when generating the rest of the annotation content elsewhere. Off by default.
         * @param value - the new value for DrawBackgroundOnly
         * @returns this object, for call chaining
         */
        setDrawBackgroundOnly(value: boolean): PDFNet.RefreshOptions;
        /**
         * Gets the value RefreshExisting from the options object
         * Whether we should refresh annotations with existing appearances. Defaults to false when used in PDFDoc.RefreshAnnotAppearances and true when used in Annot.RefreshAppearance.
         * @returns the current value for RefreshExisting.
         */
        getRefreshExisting(): boolean;
        /**
         * Sets the value for RefreshExisting in the options object
         * Whether we should refresh annotations with existing appearances. Defaults to false when used in PDFDoc.RefreshAnnotAppearances and true when used in Annot.RefreshAppearance.
         * @param value - the new value for RefreshExisting
         * @returns this object, for call chaining
         */
        setRefreshExisting(value: boolean): PDFNet.RefreshOptions;
        /**
         * Gets the value UseNonStandardRotation from the options object
         * Whether we should use rotation in the annotation even if it is not a multiple of 90. Off by default.
         * @returns the current value for UseNonStandardRotation.
         */
        getUseNonStandardRotation(): boolean;
        /**
         * Sets the value for UseNonStandardRotation in the options object
         * Whether we should use rotation in the annotation even if it is not a multiple of 90. Off by default.
         * @param value - the new value for UseNonStandardRotation.
         * @returns this object, for call chaining
         */
        setUseNonStandardRotation(value: boolean): PDFNet.RefreshOptions;
        /**
         * Gets the value UseRoundedCorners from the options object
         * Whether we should use the corner radii specified in Annot.BorderStyle. Off by default.
         * @returns the current value for UseRoundedCorners.
         */
        getUseRoundedCorners(): boolean;
        /**
         * Sets the value for UseRoundedCorners in the options object
         * Whether we should use the corner radii specified in Annot.BorderStyle. Off by default.
         * @param value - the new value for UseRoundedCorners.
         * @returns this object, for call chaining
         */
        setUseRoundedCorners(value: boolean): PDFNet.RefreshOptions;
    }
    /**
     * Method to create a DiffOptions object
     * @returns A promise that resolves to a PDFNet.DiffOptions.
     */
    function createDiffOptions(): Promise<PDFNet.DiffOptions>;
    /**
     * Options for PDFNet.PDFDoc.appendVisualDiff
     */
    class DiffOptions {
        /**
         * Gets the value AddGroupAnnots from the options object
         * Whether we should add an annot layer indicating the difference regions
         * @returns a bool, the current value for AddGroupAnnots.
         */
        getAddGroupAnnots(): boolean;
        /**
         * Sets the value for AddGroupAnnots in the options object
         * Whether we should add an annot layer indicating the difference regions
         * @param value - the new value for AddGroupAnnots
         * @returns this object, for call chaining
         */
        setAddGroupAnnots(value: boolean): PDFNet.DiffOptions;
        /**
         * Gets the value BlendMode from the options object
         * How the two colors should be blended.
         * @example
         * Return value:
         * <pre>
         * PDFNet.GState.BlendMode = {
         * 	e_bl_compatible : 0
         * 	e_bl_normal : 1
         * 	e_bl_multiply : 2
         * 	e_bl_screen : 3
         * 	e_bl_difference : 4
         * 	e_bl_darken : 5
         * 	e_bl_lighten : 6
         * 	e_bl_color_dodge : 7
         * 	e_bl_color_burn : 8
         * 	e_bl_exclusion : 9
         * 	e_bl_hard_light : 10
         * 	e_bl_overlay : 11
         * 	e_bl_soft_light : 12
         * 	e_bl_luminosity : 13
         * 	e_bl_hue : 14
         * 	e_bl_saturation : 15
         * 	e_bl_color : 16
         * }
         * </pre>
         * @returns the current value for BlendMode.
         */
        getBlendMode(): number;
        /**
         * Sets the value for BlendMode in the options object
         * How the two colors should be blended.
         * @param value - the new value for BlendMode
         * <pre>
         * PDFNet.GState.BlendMode = {
         * 	e_bl_compatible : 0
         * 	e_bl_normal : 1
         * 	e_bl_multiply : 2
         * 	e_bl_screen : 3
         * 	e_bl_difference : 4
         * 	e_bl_darken : 5
         * 	e_bl_lighten : 6
         * 	e_bl_color_dodge : 7
         * 	e_bl_color_burn : 8
         * 	e_bl_exclusion : 9
         * 	e_bl_hard_light : 10
         * 	e_bl_overlay : 11
         * 	e_bl_soft_light : 12
         * 	e_bl_luminosity : 13
         * 	e_bl_hue : 14
         * 	e_bl_saturation : 15
         * 	e_bl_color : 16
         * }
         * </pre>
         * @returns this object, for call chaining
         */
        setBlendMode(value: number): PDFNet.DiffOptions;
        /**
         * Gets the value ColorA from the options object
         * The difference color for the first page.
         * @returns a ColorPt, the current value for ColorA.
         */
        getColorA(): PDFNet.ColorPt;
        /**
         * Sets the value for ColorA in the options object
         * The difference color for the first page.
         * @param value - the new value for ColorA
         * @returns this object, for call chaining
         */
        setColorA(value: PDFNet.ColorPt): PDFNet.DiffOptions;
        /**
         * Gets the value ColorB from the options object
         * The difference color for the second page
         * @returns a ColorPt, the current value for ColorB.
         */
        getColorB(): PDFNet.ColorPt;
        /**
         * Sets the value for ColorB in the options object
         * The difference color for the second page
         * @param value - the new value for ColorB
         * @returns this object, for call chaining
         */
        setColorB(value: PDFNet.ColorPt): PDFNet.DiffOptions;
    }
    /**
     * Actions are typically what happens when a user clicks on a link or bookmark.
     *
     * Instead of simply jumping to a destination in the document, an annotation or
     * outline item can specify an action for the viewer application to perform, such
     * as launching an application, playing a sound, or changing an annotation's
     * appearance state.
     */
    class Action {
        /**
         * creates a new 'GoTo'action. GoTo action takes the user to the
         * specified Destination view located in the same document.
         * @param dest - A Destination for the new Action.
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createGoto(dest: PDFNet.Destination): Promise<PDFNet.Action>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createGotoWithKey(key: string, dest: PDFNet.Destination): Promise<PDFNet.Action>;
        /**
         * creates a new 'GoToR'action. A remote go-to action is similar to an
         * ordinary go-to action but jumps to a destination in another PDF file
         * instead of the current file.
         * @param file - The file referred to by the action.
         * @param page_num - A page number within the remote document. The first page is
         * numbered 0.
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createGotoRemote(file: PDFNet.FileSpec, page_num: number): Promise<PDFNet.Action>;
        /**
         * Creates a new 'GoToR'action. See the above method for details.
         * @param file - The file referred to by the action.
         * @param page_num - A page number within the remote document. The first page is
         * numbered 0.
         * @param new_window - A flag specifying whether to open the destination document
         * in a new window. If new_window is false, the destination document replaces
         * the current document in the same window, otherwise the viewer application
         * should behave in accordance with the current user preference.
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createGotoRemoteSetNewWindow(file: PDFNet.FileSpec, page_num: number, new_window: boolean): Promise<PDFNet.Action>;
        /**
         * Create a new URI action. The URI action is typically resolved by opening a URL in
         * the default web browser.
         * @param sdfdoc - The document in which to create the action.
         * @param uri - The uniform resource identifier to resolve, encoded in 7-bit ASCII.
         * A uniform resource identifier (URI) is a string that identifies (resolves to) a resource
         * on the Internet; typically a file that is the destination of a hypertext link, although
         * it can also resolve to a query or other entity. (URIs are described in Internet RFC 2396,
         * Uniform Resource Identifiers (URI).
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createURI(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, uri: string): Promise<PDFNet.Action>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createURIWithUString(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, current: string): Promise<PDFNet.Action>;
        /**
         * creates a new 'SubmitForm'action. A submit-form action transmits the names
         * and values of selected interactive form fields to a specified uniform
         * resource locator (URL), presumably the address of a Web server that will
         * process them and send back a response.
         * @param url - A URL file specification giving the uniform resource locator (URL)
         * of the script at the Web server that will process the submission.
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createSubmitForm(url: PDFNet.FileSpec): Promise<PDFNet.Action>;
        /**
         * Creates a new 'Launch' action. A launch action opens up a file using the
         * most appropriate program.
         * @param sdfdoc - the document in which to create the action
         * @param path - the full path of the file to be opened
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createLaunch(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, path: string): Promise<PDFNet.Action>;
        /**
         * Creates a new 'Show/Hide Field' action. A show/hide field action shows
         * or hide certain fields when it's invoked.
         * @param sdfdoc - the document in which to create the action
         * @param field_names_list - the list of fields to hide
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createHideField(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, field_names_list: string[]): Promise<PDFNet.Action>;
        /**
         * Creates a new 'Import Data' action. An import data action imports
         * form data from a FDF file into a PDF document.
         * @param sdfdoc - the document in which to create the action
         * @param path - the full path of the FDF file
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createImportData(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, path: string): Promise<PDFNet.Action>;
        /**
         * Creates a new 'Reset Form' action. A reset form action reset chosen
         * form fields to their default value.
         * @param sdfdoc - the document in which to create the action
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createResetForm(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<PDFNet.Action>;
        /**
         * Creates a new 'JavaScript' action. A javascript action executes a JavaScript
         * script when it's invoked.
         * @param sdfdoc - the document in which to create the action
         * @param script - the JavaScript script to be executed
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static createJavaScript(sdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, script: string): Promise<PDFNet.Action>;
        /**
         * A constructor. Creates an Action and initializes it using given Cos/SDF object.
         * @param [in_obj] - Pointer to the Cos/SDF object.
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        static create(in_obj?: PDFNet.Obj): Promise<PDFNet.Action>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        copy(): Promise<PDFNet.Action>;
        /**
         * Compares two Action objects for equality.
         * @param in_action - A reference to an existing Action object.
         * @returns A promise that resolves to true if the both Actions share the same underlying SDF/Cos object; otherwise false.
         */
        compare(in_action: PDFNet.Action): Promise<boolean>;
        /**
         * Indicates whether the Action is valid (non-null).
         * @returns A promise that resolves to true if this is a valid (non-null) Action; otherwise false.
         */
        isValid(): Promise<boolean>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Action.Type = {
         * 	e_GoTo : 0
         * 	e_GoToR : 1
         * 	e_GoToE : 2
         * 	e_Launch : 3
         * 	e_Thread : 4
         * 	e_URI : 5
         * 	e_Sound : 6
         * 	e_Movie : 7
         * 	e_Hide : 8
         * 	e_Named : 9
         * 	e_SubmitForm : 10
         * 	e_ResetForm : 11
         * 	e_ImportData : 12
         * 	e_JavaScript : 13
         * 	e_SetOCGState : 14
         * 	e_Rendition : 15
         * 	e_Trans : 16
         * 	e_GoTo3DView : 17
         * 	e_RichMediaExecute : 18
         * 	e_Unknown : 19
         * }
         * </pre>
         * @returns A promise that resolves to the type of this Action.
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the Action's Destination view.
         */
        getDest(): Promise<PDFNet.Destination>;
        /**
         * @returns A promise that resolves to the next action dictionary, an array of action dictionaries,
         * or NULL if there are no additional actions.
         *
         * Sequences of actions can be chained together. For example, the effect
         * of clicking a link annotation with the mouse might be to play a sound,
         * jump to a new page, and start up a movie. Note that the Next entry is
         * not restricted to a single action but may contain an array of actions,
         * each of which in turn may have a Next entry of its own. The actions may
         * thus form a tree instead of a simple linked list. Actions within each
         * Next array are executed in order, each followed in turn by any actions
         * specified in its Next entry, and so on recursively.
         */
        getNext(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @param flag - <pre>
         * PDFNet.Action.FormActionFlag = {
         * 	e_exclude : 0
         * 	e_include_no_value_fields : 1
         * 	e_export_format : 2
         * 	e_get_method : 3
         * 	e_submit_coordinates : 4
         * 	e_xfdf : 5
         * 	e_include_append_saves : 6
         * 	e_include_annotations : 7
         * 	e_submit_pdf : 8
         * 	e_canonical_format : 9
         * 	e_excl_non_user_annots : 10
         * 	e_excl_F_key : 11
         * 	e_embed_form : 13
         * }
         * </pre>
         * Action flag to get the value from.
         * @returns A promise that resolves to boolean value of the given action flag.
         */
        getFormActionFlag(flag: number): Promise<boolean>;
        /**
         * Set the value of a given field flag.
         * @param flag - <pre>
         * PDFNet.Action.FormActionFlag = {
         * 	e_exclude : 0
         * 	e_include_no_value_fields : 1
         * 	e_export_format : 2
         * 	e_get_method : 3
         * 	e_submit_coordinates : 4
         * 	e_xfdf : 5
         * 	e_include_append_saves : 6
         * 	e_include_annotations : 7
         * 	e_submit_pdf : 8
         * 	e_canonical_format : 9
         * 	e_excl_non_user_annots : 10
         * 	e_excl_F_key : 11
         * 	e_embed_form : 13
         * }
         * </pre>
         * Action flag to get the value from.
         * @param value - Boolean to set the value of the flag to.
         * Action flags are currently only used by submit and reset form actions.
         */
        setFormActionFlag(flag: number, value: boolean): Promise<void>;
        /**
         * Test if the action needs writeLock
         * @returns A promise that resolves to ture if needs, otherwise false
         */
        needsWriteLock(): Promise<boolean>;
        /**
         * Executes current action; this will only work for some action types that can be executed
         * only using the information contained in the action object or the associated PDF doc.
         * See also PDFViewCtrl::ExecuteAction()
         */
        execute(): Promise<void>;
        /**
         * Executes KeyStrokeAction, this shall be performed when the user modifies a character in a text field
         * or combo box or modifies the selecton in a scrollable list box. This action checks the added text for
         * validity and reject or modify it.
         * @param data - Data that contains previous text, added text,and added position information.
         * @returns A promise that resolves to an object of type: "PDFNet.KeyStrokeActionResult"
         */
        executeKeyStrokeAction(data: PDFNet.KeyStrokeEventData): Promise<PDFNet.KeyStrokeActionResult>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ActionParameter"
         */
        parameterCreateWithField(field: PDFNet.Field): Promise<PDFNet.ActionParameter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ActionParameter"
         */
        parameterCreateWithAnnot(annot: PDFNet.Annot): Promise<PDFNet.ActionParameter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ActionParameter"
         */
        parameterCreateWithPage(page: PDFNet.Page): Promise<PDFNet.ActionParameter>;
    }
    /**
     * ActionParameter is a class used to describe all the information needed to
     * execute an action. In most cases only the action itself is necessary since
     * the document can be inferred from associated SDF::Obj, however some actions
     * particularly JavaScript actions on Field or Annot objects need access to
     * the associated Field or Annot object.
     */
    class ActionParameter extends PDFNet.Destroyable {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.ActionParameter"
         */
        static create(action: PDFNet.Action): Promise<PDFNet.ActionParameter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Action"
         */
        getAction(): Promise<PDFNet.Action>;
    }
    /**
     * Annot is a base class for different types of annotations. For annotation
     * specific properties, please refer to derived classes.
     *
     * An annotation is an interactive object placed on a page, such as a text note, a link,
     * or an embedded file. PDF includes a wide variety of standard annotation types.
     * An annotation associates an object such as a widget, note, or movie with a location
     * on a page of a PDF document, or provides a means of interacting with the user
     * via the mouse and keyboard. For more details on PDF annotations please refer to
     * section 12.5 in the PDF Reference Manual and the documentation in derived classes.
     */
    class Annot {
        /**
         * Creates a new annotation of a given type, in the specified document.
         * The newly created annotation does not contain any properties specific
         * to a given annotation type, which means an invalid annotation object could be created.
         * It is therefore recommended to always create an annotation using type specific methods,
         * such as Annots::Line::Create() or Annots::FileAttachment::Create().
         * Users should only call Annot::Create() to create annotations of non-standard types
         * that are not recognized by PDFTron software (by using Annot::e_Unknown as a type).
         * @param doc - A document to which the annotation is added.
         * @param type - <pre>
         * PDFNet.Annot.Type = {
         * 	e_Text : 0
         * 	e_Link : 1
         * 	e_FreeText : 2
         * 	e_Line : 3
         * 	e_Square : 4
         * 	e_Circle : 5
         * 	e_Polygon : 6
         * 	e_Polyline : 7
         * 	e_Highlight : 8
         * 	e_Underline : 9
         * 	e_Squiggly : 10
         * 	e_StrikeOut : 11
         * 	e_Stamp : 12
         * 	e_Caret : 13
         * 	e_Ink : 14
         * 	e_Popup : 15
         * 	e_FileAttachment : 16
         * 	e_Sound : 17
         * 	e_Movie : 18
         * 	e_Widget : 19
         * 	e_Screen : 20
         * 	e_PrinterMark : 21
         * 	e_TrapNet : 22
         * 	e_Watermark : 23
         * 	e_3D : 24
         * 	e_Redact : 25
         * 	e_Projection : 26
         * 	e_RichMedia : 27
         * 	e_Unknown : 28
         * }
         * </pre>
         * Subtype of annotation to create.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @returns A promise that resolves to a newly created blank annotation for the given annotation type.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Create an annotation and initialize it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.Annot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.Annot>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Annot"
         */
        copy(): Promise<PDFNet.Annot>;
        /**
         * Compares two annotations for equality. The comparison will return true
         * only if both annotations share the same underlying SDF/Cos object.
         * @param d - Annotation to compare to
         * @returns A promise that resolves to an object of type: "boolean"
         */
        compare(d: PDFNet.Annot): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if this is a valid (non-null) annotation, false otherwise.
         * If the function returns false the underlying SDF/Cos object is null or is
         * not valid and the annotation object should be treated as a null object.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Annot.Type = {
         * 	e_Text : 0
         * 	e_Link : 1
         * 	e_FreeText : 2
         * 	e_Line : 3
         * 	e_Square : 4
         * 	e_Circle : 5
         * 	e_Polygon : 6
         * 	e_Polyline : 7
         * 	e_Highlight : 8
         * 	e_Underline : 9
         * 	e_Squiggly : 10
         * 	e_StrikeOut : 11
         * 	e_Stamp : 12
         * 	e_Caret : 13
         * 	e_Ink : 14
         * 	e_Popup : 15
         * 	e_FileAttachment : 16
         * 	e_Sound : 17
         * 	e_Movie : 18
         * 	e_Widget : 19
         * 	e_Screen : 20
         * 	e_PrinterMark : 21
         * 	e_TrapNet : 22
         * 	e_Watermark : 23
         * 	e_3D : 24
         * 	e_Redact : 25
         * 	e_Projection : 26
         * 	e_RichMedia : 27
         * 	e_Unknown : 28
         * }
         * </pre>
         * @returns A promise that resolves to the type of this annotation. Corresponds to the "Subtype" entry of annotation
         * dictionary, as per PDF Reference Manual section 12.5.2
         */
        getType(): Promise<number>;
        /**
         * Return true if this annotation is classified as a markup annotation.
         * @returns A promise that resolves to boolean value, true if this annotation is classified as a markup annotation.
         */
        isMarkup(): Promise<boolean>;
        /**
         * @returns A promise that resolves to annotation's bounding rectangle, specified in user space coordinates.
         *
         * The meaning of the rectangle depends on the annotation type. For Link and RubberStamp
         * annotations, the rectangle specifies the area containing the hyperlink area or stamp.
         * For Note annotations, the rectangle is describing the popup window when it's opened.
         * When it's closed, the icon is positioned at lower left corner.
         */
        getRect(): Promise<PDFNet.Rect>;
        /**
         * It is possible during viewing that GetRect does not return the most accurate bounding box
         * of what is actually rendered. This method calculates the bounding box, rather than relying
         * on what is specified in the PDF document. The bounding box is defined as the smallest
         * rectangle that includes all the visible content on the annotation.
         * @returns A promise that resolves to the bounding box for this annotation. The dimensions are specified in user space
         * coordinates.
         */
        getVisibleContentBox(): Promise<PDFNet.Rect>;
        /**
         * Sets the size and location of an annotation on its page.
         * @param pos - Annotation's bounding rectangle, specified in user space coordinates.
         *
         * The meaning of the rectangle depends on the annotation type. For Link and RubberStamp
         * annotations, the rectangle specifies the area containing the hyperlink area or stamp.
         * For Note annotations, the rectangle is describing the popup window when it's opened.
         * When it's closed, the icon is positioned at lower left corner.
         */
        setRect(pos: PDFNet.Rect): Promise<void>;
        /**
         * Scales the geometry of the annotation so that its appearance would now fit a new
         * rectangle on the page, in user units. Users still have to call RefreshAppearance() later
         * if they want a corresponding appearance stream to be generated for the new rectangle.
         * The main reason for not combining the two operations together is to be able to resize
         * annotations that do not have an appearance stream.
         * @param newrect - A reference to the new rectangle to which this annotation has to be resized.
         */
        resize(newrect: PDFNet.Rect): Promise<void>;
        /**
         * Sets the content of this annotation. (Optional).
         * @param contents - A reference to unicode string object with the text that will be associated with
         * this annotation. This is the text that annotation displays on user interaction, if the annotation
         * type supports it.
         */
        setContents(contents: string): Promise<void>;
        /**
         * Extract the content of this annotation. (Optional).
         * @returns A promise that resolves to  A unicode string object with the text that is associated with
         * this annotation. This is the text that annotation displays on user interaction,
         * if the annotation type supports it.
         */
        getContents(): Promise<string>;
        /**
         * Get the Action associated with the selected Annot Trigger event.
         * @param trigger - <pre>
         * PDFNet.Annot.EventType = {
         * 	e_action_trigger_activate : 0
         * 	e_action_trigger_annot_enter : 1
         * 	e_action_trigger_annot_exit : 2
         * 	e_action_trigger_annot_down : 3
         * 	e_action_trigger_annot_up : 4
         * 	e_action_trigger_annot_focus : 5
         * 	e_action_trigger_annot_blur : 6
         * 	e_action_trigger_annot_page_open : 7
         * 	e_action_trigger_annot_page_close : 8
         * 	e_action_trigger_annot_page_visible : 9
         * 	e_action_trigger_annot_page_invisible : 10
         * }
         * </pre>
         * the type of trigger event to get
         * @returns A promise that resolves to the Action Obj if present, otherwise NULL
         */
        getTriggerAction(trigger: number): Promise<PDFNet.Obj>;
        /**
         * Returns custom data associated with the given key.
         * @param key - The key for which to retrieve the associated data.
         * @returns A promise that resolves to the custom data string. If no data is available an empty string is returned.
         */
        getCustomData(key: string): Promise<string>;
        /**
         * Sets the custom data associated with the specified key.
         * @param key - The key under which to store this custom data
         * @param value - The custom data string to store
         */
        setCustomData(key: string, value: string): Promise<void>;
        /**
         * Deletes custom data associated with the given key.
         * @param key - The key for which to delete the associated data.
         */
        deleteCustomData(key: string): Promise<void>;
        /**
         * Gets the page the annotation is associated with.
         * @returns A promise that resolves to a Page object or null page object if the page reference is not available.
         * The page object returned is an indirect reference to the page object with which
         * this annotation is associated.
         * This entry shall be present in screen annotations associated with rendition actions.
         *
         * Optional. PDF 1.3 PDF 1.4 PDF 1.5 not used in FDF files.
         */
        getPage(): Promise<PDFNet.Page>;
        /**
         * sets the reference to a page the annotation is associated with.
         * (Optional PDF 1.3; not used in FDF files)
         * @param Page - The page object user wants the annotation to be associated with.
         */
        setPage(Page: PDFNet.Page): Promise<void>;
        /**
         * @returns A promise that resolves to the unique identifier for this annotation, or NULL if the identifier is not
         * available. The returned value is a String object and is the value of the "NM"
         * field, which was added as an optional attribute in PDF 1.4.
         */
        getUniqueID(): Promise<PDFNet.Obj>;
        /**
         * Gets an annotation's last modified date.
         * @returns A promise that resolves to the annotation's last modified time and date. If the annotation has no associated
         * date structure, the returned date is not valid (date.IsValid() returns false). Corresponds
         * to the "M" entry of the annotation dictionary.
         */
        getDate(): Promise<PDFNet.Date>;
        /**
         * Sets an annotation's last modified date.
         * @param date - The annotation's last modified time and date. Corresponds to the "M" entry of the
         * annotation dictionary.
         */
        setDate(date: PDFNet.Date): Promise<void>;
        /**
         * @param flag - <pre>
         * PDFNet.Annot.Flag = {
         * 	e_invisible : 0
         * 	e_hidden : 1
         * 	e_print : 2
         * 	e_no_zoom : 3
         * 	e_no_rotate : 4
         * 	e_no_view : 5
         * 	e_annot_read_only : 6
         * 	e_locked : 7
         * 	e_toggle_no_view : 8
         * 	e_locked_contents : 9
         * }
         * </pre>
         * The Flag property to query.
         * @returns A promise that resolves to the value of given Flag
         */
        getFlag(flag: number): Promise<boolean>;
        /**
         * sets the value of given Flag.
         * @param flag - <pre>
         * PDFNet.Annot.Flag = {
         * 	e_invisible : 0
         * 	e_hidden : 1
         * 	e_print : 2
         * 	e_no_zoom : 3
         * 	e_no_rotate : 4
         * 	e_no_view : 5
         * 	e_annot_read_only : 6
         * 	e_locked : 7
         * 	e_toggle_no_view : 8
         * 	e_locked_contents : 9
         * }
         * </pre>
         * The Flag property to modify.
         * @param value - The new value for the property.
         */
        setFlag(flag: number, value: boolean): Promise<void>;
        /**
         * Gets the annotation's appearance for the given combination of annotation
         * and appearance states.
         * @param [annot_state] - <pre>
         * PDFNet.Annot.State = {
         * 	e_normal : 0
         * 	e_rollover : 1
         * 	e_down : 2
         * }
         * </pre>
         * the annotation's appearance state, which selects the applicable
         * appearance stream from the appearance sub-dictionary. An annotation can define as many
         * as three separate appearances: The normal, rollover, and down appearance.
         * @param [app_state] - is an optional parameter specifying the appearance state
         * to look for (e.g. "Off", "On", etc). If appearance_state is NULL, the choice
         * between different appearance states is determined by the AS (Appearance State)
         * entry in the annotation dictionary.
         * @returns A promise that resolves to the appearance stream for this annotation, or NULL if the annotation
         * does not have an appearance for the given combination of annotation and
         * appearance states.
         */
        getAppearance(annot_state?: number, app_state?: string): Promise<PDFNet.Obj>;
        /**
         * Sets the annotation's appearance for the given combination of annotation
         * and appearance states.
         * (Optional; PDF 1.2)
         * @param app_stream - a content stream defining the new appearance.
         * @param [annot_state] - <pre>
         * PDFNet.Annot.State = {
         * 	e_normal : 0
         * 	e_rollover : 1
         * 	e_down : 2
         * }
         * </pre>
         * the annotation's appearance state, which selects the applicable
         * appearance stream from the appearance sub-dictionary. An annotation can define as many
         * as three separate appearances: The normal, rollover, and down appearance.
         * @param [app_state] - is an optional parameter specifying the appearance state
         * (e.g. "Off", "On", etc) under which the new appearance should be stored. If
         * appearance_state is NULL, the annotation will have only one annotation state.
         */
        setAppearance(app_stream: PDFNet.Obj, annot_state?: number, app_state?: string): Promise<void>;
        /**
         * Removes the annotation's appearance for the given combination of annotation
         * and appearance states.
         * @param [annot_state] - <pre>
         * PDFNet.Annot.State = {
         * 	e_normal : 0
         * 	e_rollover : 1
         * 	e_down : 2
         * }
         * </pre>
         * the annotation's appearance state, which selects the applicable
         * appearance stream from the appearance sub-dictionary. An annotation can define as many
         * as three separate appearances: The normal, rollover, and down appearance.
         * @param [app_state] - is an optional parameter specifying the appearance state
         * (e.g. "Off", "On", etc) under which the new appearance should be stored. If
         * appearance_state is NULL, the annotation will have only one annotation state.
         */
        removeAppearance(annot_state?: number, app_state?: string): Promise<void>;
        flatten(page: PDFNet.Page): Promise<void>;
        /**
         * Gets the annotation's active appearance state.
         * @returns A promise that resolves to the name of the active state.
         * The annotation's appearance state, which
         * selects the applicable appearance stream from an appearance subdictionary.
         */
        getActiveAppearanceState(): Promise<string>;
        setActiveAppearanceState(astate: string): Promise<void>;
        /**
         * Gets an annotation's color without any specified color space. It is generally recommended
         * to use getColorAsRGB(), getColorAsCMYK() or getColorAsGray() for more predictable behavior
         * @returns A promise that resolves to a ColorPt object containing an array of numbers in the
         * range 0.0 to 1.0.
         */
        getColor(): Promise<PDFNet.ColorPt>;
        /**
         * Gets an annotation's color in RGB color space.
         * @returns A promise that resolves to a ColorPt object containing an array of three numbers in the range 0.0 to 1.0,
         * representing an RGB colour used for the following purposes:
         *   The background of the annotation's icon when closed
         *   The title bar of the annotation's pop-up window
         *   The border of a link annotation
         *
         *  If the annotation does not specify an explicit color, a default color is returned.
         *  Text annotations return 'default yellow;' all others return black.
         */
        getColorAsRGB(): Promise<PDFNet.ColorPt>;
        /**
         * Returns the annotation's color in CMYK color space.
         * @returns A promise that resolves to a ColorPt object containing an array of four numbers in the range 0.0 to 1.0,
         * representing a CMYK color used for the following purposes:
         *   The background of the annotation's icon when closed
         *   The title bar of the annotation's pop-up window
         *   The border of a link annotation
         *
         * If the annotation does not specify an explicit color, a default color is returned.
         * Text annotations return 'default yellow;' all others return black.
         */
        getColorAsCMYK(): Promise<PDFNet.ColorPt>;
        /**
         * Returns the annotation's color in Gray color space.
         * @returns A promise that resolves to a ColorPt object containing a number in the range 0.0 to 1.0,
         * representing a Gray Scale color used for the following purposes:
         * The background of the annotation's icon when closed
         * The title bar of the annotation's pop-up window
         * The border of a link annotation
         * If the annotation does not specify an explicit color, black color is returned.
         */
        getColorAsGray(): Promise<PDFNet.ColorPt>;
        /**
         * Returns the color space the annotation's color is represented in.
         * @returns A promise that resolves to an integer that is either 1(for DeviceGray), 3(DeviceRGB), or 4(DeviceCMYK).
         * If the annotation has no color, i.e. is transparent, 0 will be returned.
         */
        getColorCompNum(): Promise<number>;
        setColorDefault(col: PDFNet.ColorPt): Promise<void>;
        /**
         * Sets an annotation's color.
         * (Optional; PDF 1.1)
         * @param col - A ColorPt object in RGB or Gray or CMYK color space representing the annotation's color.
         * The ColorPt contains an array of numbers in the range 0.0 to 1.0, representing a color used for the following purposes:
         *    The background of the annotation's icon when closed
         *     The title bar of the annotation's pop-up window
         *     The border of a link annotation
         *
         * The number of array elements determines the color space in which the color shall be defined:
         * 0 No color; transparent
         * 1 DeviceGray
         * 3 DeviceRGB
         * 4 DeviceCMYK
         * @param [numcomp] - The number of color components used to represent the color (i.e. 1, 3, 4).
         */
        setColor(col: PDFNet.ColorPt, numcomp?: number): Promise<void>;
        /**
         * Returns the struct parent of an annotation.
         * (Required if the annotation is a structural content item; PDF 1.3)
         * @returns A promise that resolves to an integer which is the integer key of the annotation's entry
         * in the structural parent tree.
         */
        getStructParent(): Promise<number>;
        /**
         * sets the struct parent of an annotation.
         * (Required if the annotation is a structural content item; PDF 1.3)
         * @param parkeyval - An integer which is the integer key of the
         * annotation's entry in the structural parent tree.
         */
        setStructParent(parkeyval: number): Promise<void>;
        /**
         * Returns optional content associated with this annotation.
         * @returns A promise that resolves to a SDF object corresponding to the group of optional properties.
         */
        getOptionalContent(): Promise<PDFNet.Obj>;
        /**
         * Associates optional content with this annotation. (Optional, PDF1.5).
         * @param content - A pointer to an SDF object corresponding to the optional content,
         * a PDF::OCG::Group or membership dictionary specifying the PDF::OCG::Group properties for
         * the annotation. Before the annotation is drawn, its visibility
         * shall be determined based on this entry as well as the annotation
         * flags specified in the Flag entry . If it is determined to be invisible,
         * the annotation shall be skipped, as if it were not in the document.
         */
        setOptionalContent(content: PDFNet.Obj): Promise<void>;
        /**
         * Regenerates the appearance stream for the annotation.
         * This method can be used to auto-generate the annotation appearance after creating
         * or modifying the annotation without providing an explicit appearance or
         * setting the "NeedAppearances" flag in the AcroForm dictionary.
         */
        refreshAppearance(): Promise<void>;
        /**
         * A version of RefreshAppearance allowing custom options to make slight tweaks in behaviour.
         * @param [options] - The RefreshOptions.
         */
        refreshAppearanceRefreshOptions(options?: PDFNet.RefreshOptions): Promise<void>;
        /**
         * Returns the rotation value of the annotation. The Rotation specifies the number of degrees by which the
         * annotation shall be rotated counterclockwise relative to the page.
         * The value shall be a multiple of 90.
         * @returns A promise that resolves to an integer representing the rotation value of the annotation.
         */
        getRotation(): Promise<number>;
        /**
         * Sets the rotation value of the annotation. The Rotation specifies the number of degrees by which the
         * annotation shall be rotated counterclockwise relative to the page.
         * The value shall be a multiple of 90.
         * (Optional)
         * @param angle - An integer representing the rotation value of the annotation.
         */
        setRotation(angle: number): Promise<void>;
        /**
         * Gets the border style for the annotation. Typically used for Link annotations.
         * @returns A promise that resolves to annotation's border style.
         */
        getBorderStyle(): Promise<PDFNet.AnnotBorderStyle>;
        /**
         * Sets the border style for the annotation.
         * @param bs - New border style for this annotation.
         * @param [oldStyleOnly] - PDF manual specifies two ways to add border information to an annotation object,
         * either through an array named 'Border' (PDF 1.0), or a dictionary called 'BS' (PDF 1.2) the latter
         * taking precedence over the former. However, if you want to create a border with rounded corners, you can only
         * do that using PDF 1.0 Border specification, in which case if you call SetBorderStyle() set the parameter
         * oldStyleOnly to true. This parameter has a default value of false in the API and does not need to be used otherwise.
         */
        setBorderStyle(bs: PDFNet.AnnotBorderStyle, oldStyleOnly?: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.AnnotBorderStyle.Style = {
         * 	e_solid : 0
         * 	e_dashed : 1
         * 	e_beveled : 2
         * 	e_inset : 3
         * 	e_underline : 4
         * }
         * </pre>
         */
        static getBorderStyleStyle(bs: PDFNet.AnnotBorderStyle): Promise<number>;
        static setBorderStyleStyle(bs: PDFNet.AnnotBorderStyle, bst: number): Promise<void>;
    }
    /**
     * AnnotBorderStyle structure specifies the characteristics of the annotation's border.
     * The border is specified as a rounded rectangle.
     */
    class AnnotBorderStyle extends PDFNet.Destroyable {
        /**
         * Constructor
         * @param s - <pre>
         * PDFNet.AnnotBorderStyle.Style = {
         * 	e_solid : 0
         * 	e_dashed : 1
         * 	e_beveled : 2
         * 	e_inset : 3
         * 	e_underline : 4
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.AnnotBorderStyle"
         */
        static create(s: number, b_width: number, b_hr: number, b_vr: number): Promise<PDFNet.AnnotBorderStyle>;
        /**
         * @param s - <pre>
         * PDFNet.AnnotBorderStyle.Style = {
         * 	e_solid : 0
         * 	e_dashed : 1
         * 	e_beveled : 2
         * 	e_inset : 3
         * 	e_underline : 4
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.AnnotBorderStyle"
         */
        static createWithDashPattern(s: number, b_width: number, b_hr: number, b_vr: number, buffer: any[]): Promise<PDFNet.AnnotBorderStyle>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.AnnotBorderStyle"
         */
        copy(): Promise<PDFNet.AnnotBorderStyle>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.AnnotBorderStyle.Style = {
         * 	e_solid : 0
         * 	e_dashed : 1
         * 	e_beveled : 2
         * 	e_inset : 3
         * 	e_underline : 4
         * }
         * </pre>
         * @returns A promise that resolves to the border style.
         */
        getStyle(): Promise<number>;
        /**
         * Sets the border style.
         * @param style - <pre>
         * PDFNet.AnnotBorderStyle.Style = {
         * 	e_solid : 0
         * 	e_dashed : 1
         * 	e_beveled : 2
         * 	e_inset : 3
         * 	e_underline : 4
         * }
         * </pre>
         */
        setStyle(style: number): Promise<void>;
        /**
         * @returns A promise that resolves to the border width.
         */
        getWidth(): Promise<number>;
        /**
         * Sets the border width
         * @param width - A number representing the width value to set the annotation to.
         */
        setWidth(width: number): Promise<void>;
        /**
         * @returns A promise that resolves to horizontal corner radius.
         */
        getHR(): Promise<number>;
        /**
         * Sets horizontal corner radius.
         * @param horizontal_radius - A number representing the value of the horizontal radius to set the annotation to.
         */
        setHR(horizontal_radius: number): Promise<void>;
        /**
         * @returns A promise that resolves to vertical corner radius.
         */
        getVR(): Promise<number>;
        /**
         * Sets vertical corner radius.
         * @param vertical_radius - A number representing the value of the vertical radius to set the annotation to.
         */
        setVR(vertical_radius: number): Promise<void>;
        /**
         * @returns A promise that resolves to the border dash pattern.
         */
        getDashPattern(): Promise<Float64Array>;
        /**
         * Comparison function.
         * Determines if parameter object is equal to current object.
         * @returns A promise that resolves to True if the two objects are equivalent, False otherwise
         */
        compare(b: PDFNet.AnnotBorderStyle): Promise<boolean>;
    }
    /**
     * [Missing documentation]
     */
    class AppearanceReferenceList {
    }
    /**
     * An application or plug-in extension that processes logical structure can attach
     * additional information, called attributes, to any structure element.
     * The attribute information is held in one or more attribute objects associated
     * with the structure element. An attribute object is a dictionary or stream
     * that includes an entry identifying the application or plug-in that owns the
     * attribute information. Other entries represent the attributes: the keys are
     * attribute names, and values are the corresponding attribute values.
     */
    class AttrObj {
        /**
         * Initialize a AttrObj using an existing low-level Cos/SDF object.
         * @param [dict] - a low-level (SDF/Cos) dictionary representing the attribute object.
         * @returns A promise that resolves to an object of type: "PDFNet.AttrObj"
         */
        static create(dict?: PDFNet.Obj): Promise<PDFNet.AttrObj>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.AttrObj"
         */
        copy(): Promise<PDFNet.AttrObj>;
        /**
         * @returns A promise that resolves to the name of the application or plug-in extension owning the
         * attribute data.
         */
        getOwner(): Promise<string>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * A %PDF document may optionally display a document outline on the screen, allowing
     * the user to navigate interactively from one part of the document to another.
     * The outline consists of a tree-structured hierarchy of Bookmarks (sometimes
     * called outline items), which serve as a 'visual table of contents' to display the
     * document's structure to the user.
     *
     * Each Bookmark has a title that appears on screen, and an Action that specifies
     * what happens when a user clicks on the Bookmark. The typical action for a
     * user-created Bookmark is to move to another location in the current document,
     * although any action (see PDF::Action) can be specified.
     *
     * Bookmark is a utility class used to simplify work with %PDF bookmarks (or
     * outlines; see section 8.2.2 'Document Outline' in %PDF Reference Manual for
     * more details).
     */
    class Bookmark {
        /**
         * Creates a new valid Bookmark with given title in the
         * specified document.
         * @param in_doc - The document in which a Bookmark is to be created.
         * @param in_title - The title string value of the new Bookmark.
         * @returns A promise that resolves to the new Bookmark.
         */
        static create(in_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, in_title: string): Promise<PDFNet.Bookmark>;
        /**
         * A constructor. Creates a Bookmark and initialize it using given Cos/SDF object.
         * @param in_bookmark_dict - Pointer to the Cos/SDF object (outline item dictionary).
         * @returns A promise that resolves to the new Bookmark.
         */
        static createFromObj(in_bookmark_dict: PDFNet.Obj): Promise<PDFNet.Bookmark>;
        /**
         * Copy Constructor. Creates a new Bookmark object equal to the existing Bookmark object.
         * @returns A promise that resolves to a new Bookmark object.
         */
        copy(): Promise<PDFNet.Bookmark>;
        /**
         * Compares two Bookmark objects for equality.
         * @param in_bookmark - A reference to an existing Bookmark object.
         * @returns A promise that resolves to true if the both Bookmarks share the same underlying SDF/Cos object; otherwise false.
         */
        compare(in_bookmark: PDFNet.Bookmark): Promise<boolean>;
        /**
         * Indicates whether the Bookmark is valid (non-null).
         * @returns A promise that resolves to true if this is a valid (non-null) Bookmark; otherwise false.
         */
        isValid(): Promise<boolean>;
        /**
         * Indicates whether the Bookmark has children.
         * @returns A promise that resolves to true if the Bookmark has children; otherwise false.
         */
        hasChildren(): Promise<boolean>;
        /**
         * Returns the Bookmark's next (right) sibling.
         * @returns A promise that resolves to the Bookmark's next (right) sibling.
         */
        getNext(): Promise<PDFNet.Bookmark>;
        /**
         * Returns the Bookmark's previous (left) sibling.
         * @returns A promise that resolves to the Bookmark's previous (left) sibling.
         */
        getPrev(): Promise<PDFNet.Bookmark>;
        /**
         * Returns the Bookmark's first child.
         * @returns A promise that resolves to the Bookmark's first child.
         */
        getFirstChild(): Promise<PDFNet.Bookmark>;
        /**
         * Returns the Bookmark's last child.
         * @returns A promise that resolves to the Bookmark's last child.
         */
        getLastChild(): Promise<PDFNet.Bookmark>;
        /**
         * Returns the Bookmark's parent Bookmark.
         * @returns A promise that resolves to the Bookmark's parent Bookmark.
         */
        getParent(): Promise<PDFNet.Bookmark>;
        /**
         * Returns the Bookmark specified by the given title string.
         * @param in_title - The title string value of the Bookmark to find.
         * @returns A promise that resolves to a Bookmark matching the title string value specified.
         */
        find(in_title: string): Promise<PDFNet.Bookmark>;
        /**
         * Adds a new Bookmark as the new last child of this Bookmark.
         * @param in_title - The title string value of the new Bookmark.
         * @returns A promise that resolves to the newly created child Bookmark.
         */
        addNewChild(in_title: string): Promise<PDFNet.Bookmark>;
        /**
         * adds the specified Bookmark as the new last child of this Bookmark.
         * @param in_bookmark - The Bookmark object to be added as a last child of this Bookmark.
         */
        addChild(in_bookmark: PDFNet.Bookmark): Promise<void>;
        /**
         * Adds a new Bookmark to the tree containing this Bookmark, as the
         * new right sibling.
         * @param in_title - The title string value of the new Bookmark.
         * @returns A promise that resolves to the newly created sibling Bookmark.
         */
        addNewNext(in_title: string): Promise<PDFNet.Bookmark>;
        /**
         * adds the specified Bookmark as the new right sibling to this Bookmark,
         * adjusting the tree containing this Bookmark appropriately.
         * @param in_bookmark - The Bookmark object to be added to this Bookmark.
         */
        addNext(in_bookmark: PDFNet.Bookmark): Promise<void>;
        /**
         * Adds a new Bookmark to the tree containing this Bookmark, as the
         * new left sibling.
         * @param in_title - The title string value of the new Bookmark.
         * @returns A promise that resolves to the newly created sibling Bookmark.
         */
        addNewPrev(in_title: string): Promise<PDFNet.Bookmark>;
        /**
         * adds the specified Bookmark as the new left sibling to this Bookmark,
         * adjusting the tree containing this Bookmark appropriately.
         * @param in_bookmark - The Bookmark object to be added to this Bookmark.
         */
        addPrev(in_bookmark: PDFNet.Bookmark): Promise<void>;
        /**
         * removes the Bookmark's subtree from the bookmark tree containing it.
         */
        delete(): Promise<void>;
        /**
         * unlinks this Bookmark from the bookmark tree that contains it, and
         * adjusts the tree appropriately.
         */
        unlink(): Promise<void>;
        /**
         * Returns the indentation level of the Bookmark in its containing tree.
         * @returns A promise that resolves to the indentation level of the Bookmark in its containing tree.
         */
        getIndent(): Promise<number>;
        /**
         * Indicates whether the Bookmark is open.
         * @returns A promise that resolves to true if this Bookmark is open; otherwise false.
         */
        isOpen(): Promise<boolean>;
        /**
         * Opens or closes the Bookmark.
         * @param in_open - Boolean value that contains the status.
         * If true, the Bookmark is opened. Otherwise the Bookmark is closed.
         */
        setOpen(in_open: boolean): Promise<void>;
        /**
         * Returns the number of opened bookmarks in this subtree.
         * @returns A promise that resolves to the number of opened bookmarks in this subtree (not including
         * this Bookmark). If the item is closed, a negative integer whose
         * absolute value specifies how many descendants would appear if the
         * item were reopened.
         */
        getOpenCount(): Promise<number>;
        /**
         * Returns the Bookmark's title string.
         * @returns A promise that resolves to the Bookmark's title string).
         */
        getTitle(): Promise<string>;
        /**
         * Returns the Bookmark's title string object.
         * @returns A promise that resolves to the Bookmark's title string object.
         */
        getTitleObj(): Promise<PDFNet.Obj>;
        /**
         * Sets the Bookmark's title string.
         * @param title - The new title string for the bookmark.
         */
        setTitle(title: string): Promise<void>;
        /**
         * Returns the Bookmark's action.
         * @returns A promise that resolves to the Bookmark's action.
         */
        getAction(): Promise<PDFNet.Action>;
        /**
         * sets the Bookmark's action.
         * @param in_action - The new Action for the Bookmark.
         */
        setAction(in_action: PDFNet.Action): Promise<void>;
        /**
         * removes the Bookmark's action.
         */
        removeAction(): Promise<void>;
        /**
         * Returns the Bookmark's flags.
         * @returns A promise that resolves to the flags of the Bookmark object.
         * Bit 1 (least-significant bit) indicates italic font whereas
         * bit 2 indicates bold font.
         * Therefore, 0 indicates normal, 1 is italic, 2 is bold, and 3 is bold-italic.
         */
        getFlags(): Promise<number>;
        /**
         * Sets the Bookmark's flags.
         * @param in_flags - The new bookmark flags.
         * Bit 1 (the least-significant bit) indicates italic font whereas
         * bit 2 indicates bold font.
         * Therefore, 0 indicates normal, 1 is italic, 2 is bold, and 3 is bold-italic.
         */
        setFlags(in_flags: number): Promise<void>;
        /**
         * Returns the Bookmark's RGB color value.
         * @returns A promise that resolves to an object of type: "Object"
         */
        getColor(): Promise<object>;
        /**
         * Sets the Bookmark's color value.
         * @param [in_r] - The red component of the color.
         * @param [in_g] - The green component of the color.
         * @param [in_b] - The blue component of the color.
         */
        setColor(in_r?: number, in_g?: number, in_b?: number): Promise<void>;
        /**
         * Returns the underlying SDF/Cos object.
         * @returns A promise that resolves to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * This class represents a Byte Range.
     */
    class ByteRange {
        constructor(m_offset?: number, m_size?: number);
        /**
         * Retrieves the start offset of the byte range.
         * @returns A promise that resolves to an unsigned integer.
         */
        getStartOffset(): Promise<number>;
        /**
         * Retrieves the end offset of the byte range.
         * @returns A promise that resolves to an unsigned integer.
         */
        getEndOffset(): Promise<number>;
        /**
         * Retrieves the size of the byte range.
         * @returns A promise that resolves to an unsigned integer.
         */
        getSize(): Promise<number>;
        m_offset: number;
        m_size: number;
    }
    /**
     * The class CADModule.
     * static interface to PDFTron SDKs CAD functionality
     */
    class CADModule {
        /**
         * Find out whether the CAD module is available (and licensed).
         * @returns A promise that resolves to returns true if CAD operations can be performed.
         */
        static isModuleAvailable(): Promise<boolean>;
    }
    /**
     * A Caret annotation (PDF 1.5) is a visual symbol that indicates
     * the presence of text edits.
     */
    class CaretAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates an Caret annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.CaretAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.CaretAnnot>;
        /**
         * creates an Caret annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Caret annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.CaretAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.CaretAnnot>;
        /**
         * Creates a new Caret annotation in the specified document.
         * @param doc - A document to which the Caret annotation is added.
         * @param pos - A rectangle specifying the Caret annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Caret annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.CaretAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the paragraph symbol associated with the caret.
         * @returns A promise that resolves to the name of the symbol. This can be either "P" (Use a new
         * paragraph symbol) or "None" (Don't use any symbol).
         * Default value: None.
         */
        getSymbol(): Promise<string>;
        /**
         * sets the caret symbol.
         * @param symbol - The name of the symbol. This can be either "P" (Use a new
         * paragraph symbol) or "None" (Don't use any symbol).
         * Default value: None.
         */
        setSymbol(symbol: string): Promise<void>;
    }
    /**
     * An object representing a check box used in a PDF Form.
     */
    class CheckBoxWidget extends PDFNet.WidgetAnnot {
        /**
         * Creates a new Check Box Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is to be added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param [field_name] - The name of the field for which to create a CheckBox widget.
         * @returns A promise that resolves to a newly created blank Check Box Widget annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field_name?: string): Promise<PDFNet.CheckBoxWidget>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Creates a new Check Box Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is to be added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param field - the field for which to create a CheckBox widget
         * @returns A promise that resolves to a newly created blank Check Box Widget annotation.
         */
        static createWithField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.CheckBoxWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.CheckBoxWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.CheckBoxWidget>;
        /**
         * Creates a Check Box Widget annotation and initialize it using given annotation object.
         *
         * <p>
         * <b> Note: </b>  The constructor does not copy any data, but is instead the logical
         * equivalent of a type cast.
         * @param annot - The annotation object to use.
         * @returns A promise that resolves to an object of type: "PDFNet.CheckBoxWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.CheckBoxWidget>;
        /**
         * Returns whether the checkbox is checked.
         * @returns A promise that resolves to  A boolean value indicating whether the checkbox is checked.
         */
        isChecked(): Promise<boolean>;
        /**
         * Check or uncheck the Check Box Widget
         * @param checked - If true, the annotation should be checked. Otherwise it should be unchecked.
         */
        setChecked(checked: boolean): Promise<void>;
    }
    /**
     * [Not Yet Documented]
     */
    class ChunkRenderer {
    }
    /**
     * A Circle annotation is a type of markup annotation that displays an ellipse on
     * the page. When opened, it can display a pop-up window containing the text of
     * the associated note. The ellipse may be inscribed and possibly padded within the
     * annotation rectangle defined by the annotation dictionary's Rect entry.
     */
    class CircleAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates an Circle annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.CircleAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.CircleAnnot>;
        /**
         * creates a Circle annotation and initializes it using given annotation object.
         * @param circle - Annot object used to initialize the Circle annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.CircleAnnot"
         */
        static createFromAnnot(circle: PDFNet.Annot): Promise<PDFNet.CircleAnnot>;
        /**
         * Creates a new Circle annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Circle annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.CircleAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        getInteriorColor(): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        getInteriorColorCompNum(): Promise<number>;
        setInteriorColorDefault(col: PDFNet.ColorPt): Promise<void>;
        setInteriorColor(col: PDFNet.ColorPt, numcomp: number): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Rect"
         */
        getContentRect(): Promise<PDFNet.Rect>;
        setContentRect(cr: PDFNet.Rect): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Rect"
         */
        getPadding(): Promise<PDFNet.Rect>;
        setPadding(cr: PDFNet.Rect): Promise<void>;
    }
    class ClassMap {
        /**
         * Initialize a ClassMap using an existing low-level Cos/SDF ClassMap object.
         * @param [dict] - a low-level (SDF/Cos) ClassMap dictionary.
         * @returns A promise that resolves to an object of type: "PDFNet.ClassMap"
         */
        static create(dict?: PDFNet.Obj): Promise<PDFNet.ClassMap>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.ClassMap"
         */
        copy(): Promise<PDFNet.ClassMap>;
        /**
         * @returns A promise that resolves to true if this is a valid (non-null) ClassMap, false otherwise.
         * If the function returns false the underlying SDF/Cos object is null or is
         * not valid and the ClassMap object should be treated as a null object.
         */
        isValid(): Promise<boolean>;
        /**
         * Returns the ClassMap dictionary.
         * @returns A promise that resolves to the object to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * ColorPt is an array of colorants (or tint values) representing a color point
     * in an associated color space.
     */
    class ColorPt extends PDFNet.Destroyable {
        /**
         * Constructor
         * @param [x] - initialized value of first color value (eg. red for rgb colorspace);
         * @param [y] - initialized value of second color value (eg. green for rgb colorspace);
         * @param [z] - initialized value of third color value (eg. blue for rgb colorspace);
         * @param [w] - initialized value of fourth color value (eg. when using CMYK);
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        static init(x?: number, y?: number, z?: number, w?: number): Promise<PDFNet.ColorPt>;
        /**
         * Comparison function.
         * Determines if parameter object is equal to current object.
         * @returns A promise that resolves to True if the two objects are equivalent, False otherwise
         */
        compare(right: PDFNet.ColorPt): Promise<boolean>;
        /**
         * A utility method to set the first 4 tint values. For example,
         * color.Set(red, green, blue) will initialize the ColorPt to given
         * tint values.
         * @param [x] - initialized value of first color value (eg. red for rgb colorspace);
         * @param [y] - initialized value of second color value (eg. green for rgb colorspace);
         * @param [z] - initialized value of third color value (eg. blue for rgb colorspace);
         * @param [w] - initialized value of fourth color value (eg. when using CMYK);
         */
        set(x?: number, y?: number, z?: number, w?: number): Promise<void>;
        /**
         * Sets a tint value at a given colorant index.
         * @param colorant_index - the color index. For example, for a color point
         * associated with a Gray color space the only allowed value for index
         * is 0. For a color point associated with a CMYK color space, the color_index
         * can range from 0 (cyan) to 4 (black).
         * @param colorant_value - The new tint value.
         *
         * For example, the following snippet will initialize the color point
         * to [red, green, blue]:
         * <pre>
         * color.SetColorantNum(3);
         * color.Set(0, red);
         * color.Set(1, green);
         * color.Set(2, blue);
         * </pre>
         *
         * The above code snippet is equivalent to the following line:
         *   color.Set(red, green, blue)
         */
        setByIndex(colorant_index: number, colorant_value: number): Promise<void>;
        /**
         * The number of colorants depends on the associated color space. To find
         * how many colorant are associated with a given color space use
         * color_space.GetComponentNum().
         *
         * For example, if you have a color point in the RGB color space you can
         * extract its colorants as follows:
         * <pre>
         * UInt8 rgb[3] = { UInt8(c.Get(0)*255), UInt8(c.Get(1)*255), UInt8(c.Get(2)*255) };
         * </pre>
         * @param colorant_index - number representing the index of the color space to get the tint from
         * @returns A promise that resolves to the tint value at a given colorant index.
         */
        get(colorant_index: number): Promise<number>;
        setColorantNum(num: number): Promise<void>;
    }
    /**
     * This abstract class is used to serve as a color space tag to identify the specific
     * color space of a Color object. It contains methods that transform colors in a specific
     * color space to/from several color space such as DeviceRGB and DeviceCMYK.
     *
     * For purposes of the methods in this class, colors are represented as arrays of color
     * components represented as doubles in a normalized range defined by each ColorSpace.
     * For many ColorSpaces (e.g. DeviceRGB), this range is 0.0 to 1.0. However, some ColorSpaces
     * have components whose values have a different range. Methods are provided to inquire per
     * component minimum and maximum normalized values.
     */
    class ColorSpace extends PDFNet.Destroyable {
        /**
         * Create a new DeviceGray ColorSpace object
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createDeviceGray(): Promise<PDFNet.ColorSpace>;
        /**
         * Create a new DeviceRGB ColorSpace object
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createDeviceRGB(): Promise<PDFNet.ColorSpace>;
        /**
         * Create a new DeviceCMYK ColorSpace object
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createDeviceCMYK(): Promise<PDFNet.ColorSpace>;
        /**
         * Create a new Pattern ColorSpace object
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createPattern(): Promise<PDFNet.ColorSpace>;
        /**
         * Create a ColorSpace from the given SDF/Cos object listed under ColorSpaces entry
         * in page Resource dictionary. If color_space dictionary is null, a non valid ColorSpace
         * object is created.
         * @param [color_space] - The Cos/SDF object to initialze the ColorSpace object with.
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static create(color_space?: PDFNet.Obj): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createICCFromFile(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, filepath: string): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createICCFromFilter(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, filter: PDFNet.Filter): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorSpace"
         */
        static createICCFromBuffer(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.ColorSpace>;
        /**
         * @param cs_type - <pre>
         * PDFNet.ColorSpace.Type = {
         * 	e_device_gray : 0
         * 	e_device_rgb : 1
         * 	e_device_cmyk : 2
         * 	e_cal_gray : 3
         * 	e_cal_rgb : 4
         * 	e_lab : 5
         * 	e_icc : 6
         * 	e_indexed : 7
         * 	e_pattern : 8
         * 	e_separation : 9
         * 	e_device_n : 10
         * 	e_null : 11
         * }
         * </pre>
         * @param cs_obj - Cos/SDF color space object.
         * @returns A promise that resolves to the number of components (tint components) used to represent color
         * point for this color space
         */
        static getComponentNumFromObj(cs_type: number, cs_obj: PDFNet.Obj): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ColorSpace.Type = {
         * 	e_device_gray : 0
         * 	e_device_rgb : 1
         * 	e_device_cmyk : 2
         * 	e_cal_gray : 3
         * 	e_cal_rgb : 4
         * 	e_lab : 5
         * 	e_icc : 6
         * 	e_indexed : 7
         * 	e_pattern : 8
         * 	e_separation : 9
         * 	e_device_n : 10
         * 	e_null : 11
         * }
         * </pre>
         * @param cs_obj - Cos/SDF color space object.
         * @returns A promise that resolves to the Type of a given SDF/Cos color space, or e_null for if
         * SDF object is not a valid color space
         */
        static getTypeFromObj(cs_obj: PDFNet.Obj): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ColorSpace.Type = {
         * 	e_device_gray : 0
         * 	e_device_rgb : 1
         * 	e_device_cmyk : 2
         * 	e_cal_gray : 3
         * 	e_cal_rgb : 4
         * 	e_lab : 5
         * 	e_icc : 6
         * 	e_indexed : 7
         * 	e_pattern : 8
         * 	e_separation : 9
         * 	e_device_n : 10
         * 	e_null : 11
         * }
         * </pre>
         * @returns A promise that resolves to the type of this color space
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the number of colorants (tint components) used to represent color
         *  point in this color space
         */
        getComponentNum(): Promise<number>;
        /**
         * Set color to the initial value used for this color space.
         * The initial value depends on the color space (see 4.5.7 in PDF Ref. Manual).
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        initColor(): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to an object of type: "Object"
         */
        initComponentRanges(num_comps: number): Promise<object>;
        /**
         * A convenience function used to convert color points from the current
         * color space to DeviceGray color space.
         * @param in_color - input color point in the current color space
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        convert2Gray(in_color: PDFNet.ColorPt): Promise<PDFNet.ColorPt>;
        /**
         * A convenience function used to convert color points from the current
         * color space to DeviceRGB color space.
         * @param in_color - input color point in the current color space
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        convert2RGB(in_color: PDFNet.ColorPt): Promise<PDFNet.ColorPt>;
        /**
         * A convenience function used to convert color points from the current
         * color space to DeviceCMYK color space.
         * @param in_color - input color point in the current color space
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        convert2CMYK(in_color: PDFNet.ColorPt): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to the alternate color space if it is available or NULL otherwise.
         * Color spaces that include alternate color space are e_separation, e_device_n,
         * and e_icc.
         */
        getAlternateColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to the base color space if this is an e_indexed or e_pattern with
         * associated base color space; NULL otherwise.
         */
        getBaseColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to the highest index for the color lookup table for Indexed color space.
         * Since the color table is indexed from zero to highval, the actual number of entries is
         * highval + 1. For color spaces other than indexed the method returns 0.
         */
        getHighVal(): Promise<number>;
        /**
         * @returns A promise that resolves to the color lookup table for Indexed color space. for color spaces other
         * than indexed the method returns NULL.
         */
        getLookupTable(): Promise<number>;
        /**
         * Get the base color given a component value (index) in Indexed color space.
         * @param color_idx - color value represented in the index color space
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        getBaseColor(color_idx: number): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to the function that transforms tint values into color component
         * values in the alternate color space.
         */
        getTintFunction(): Promise<PDFNet.Function>;
        /**
         * @returns A promise that resolves to true if Separation color space contains the colorant All.
         */
        isAll(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if Separation or DeviceN color space contains None colorants.
         * For DeviceN the function returns true only if component colorant names are all None.
         */
        isNone(): Promise<boolean>;
    }
    /**
     * An object representing a combo box used in a PDF Form.
     */
    class ComboBoxWidget extends PDFNet.WidgetAnnot {
        /**
         * Creates a new Combo Box Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param [field_name] - The name of the field for which to create a ComboBox widget
         * @returns A promise that resolves to a newly created blank Combo Box Widget annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field_name?: string): Promise<PDFNet.ComboBoxWidget>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Creates a new Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param field - The field for which to create a Text widget
         * @returns A promise that resolves to a newly created blank Widget annotation.
         */
        static createWithField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.ComboBoxWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ComboBoxWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.ComboBoxWidget>;
        /**
         * Creates a Combo Box Widget annotation and initialize it using given annotation object.
         *
         * <p>
         * <b> Note: </b>  The constructor does not copy any data, but is instead the logical
         * equivalent of a type cast.
         * @param annot - The annotation to use.
         * @returns A promise that resolves to an object of type: "PDFNet.ComboBoxWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.ComboBoxWidget>;
        /**
         * Adds an option to Combo Box widget.
         * @param value - The option to add
         */
        addOption(value: string): Promise<void>;
        addOptions(option_list: string[]): Promise<void>;
        /**
         * selects the given option in the Combo Box widget
         * @param value - The option to select
         */
        setSelectedOption(value: string): Promise<void>;
        /**
         * Retrieves the option selected in the ComboBox widget
         * @returns A promise that resolves to the option selected in the ComboBox widget
         */
        getSelectedOption(): Promise<string>;
        replaceOptions(option_list: string[]): Promise<void>;
        /**
         * Removes the option from Combo Box widget.
         * @param value - The option to remove
         */
        removeOption(value: string): Promise<void>;
    }
    /**
     * Content items are graphical objects that exist in the document independently
     * of the structure tree but are associated with structure elements.
     *
     * Content items are leaf nodes of the structure tree.
     */
    class ContentItem {
        constructor(o?: PDFNet.Obj, p?: PDFNet.Obj);
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.ContentItem"
         */
        copy(): Promise<PDFNet.ContentItem>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ContentItem.Type = {
         * 	e_MCR : 0
         * 	e_MCID : 1
         * 	e_OBJR : 2
         * 	e_Unknown : 3
         * }
         * </pre>
         * @returns A promise that resolves to the content item type.
         */
        getType(): Promise<number>;
        /**
         * find the parent structure element.
         * @returns A promise that resolves to an object of type: "PDFNet.SElement"
         */
        getParent(): Promise<PDFNet.SElement>;
        /**
         * the page on which the marked content is drawn, whether directly as part of
         * page content or indirectly by being in a Form XObject or annotation drawn
         * on that page.
         * @returns A promise that resolves to an object of type: "PDFNet.Page"
         */
        getPage(): Promise<PDFNet.Page>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to mcid (marked-content identifier).
         */
        getMCID(): Promise<number>;
        /**
         * @returns A promise that resolves to the stream object that contains the marked-content sequence.
         * The function will return a non-NULL object only if the marked-content
         * sequence resides in a content stream other than the content stream for the
         * page (e.g. in a form XObject).
         */
        getContainingStm(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to nULL or the PDF object owning the stream returned by
         * GetContainingStm() (e.g. the annotation to which an appearance stream
         * belongs).
         */
        getStmOwner(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the referenced object.
         */
        getRefObj(): Promise<PDFNet.Obj>;
        o: PDFNet.Obj;
        p: PDFNet.Obj;
    }
    /**
     * ContentReplacer is a utility class for replacing content (text and images)
     * in existing PDF (template) documents.
     *
     * Users can replace content in a PDF page using the following operations:
     * - Replace an image that exists in a target rectangle with a replacement image.
     * - Replace text that exists in a target rectangle with replacement text.
     * - Replace all instances of a specially marked string with replacement string.
     *
     * The following code replaces an image in a target region. This code also
     * replaces the text "[NAME]" and "[JOB_TITLE]" with "John Smith"
     * and "Software Developer" respectively. Notice the square braces ('[' and ']') on
     * the target strings in the original PDFDoc. These square braces are not included in
     * the actual function calls below, as they're implicitly added.
     *
     * <pre>
     * PDFDoc doc("../../TestFiles/BusinessCardTemplate.pdf");
     * doc.InitSecurityHandler();
     * ContentReplacer replacer;
     * Page page = doc.GetPage(1);
     * Image img = Image::Create(doc, "../../TestFiles/peppers.jpg");
     * replacer.AddImage(page.GetMediaBox(), img.GetSDFObj());
     * replacer.AddString("NAME", "John Smith");
     * replacer.AddString("JOB_TITLE", "Software Developer");
     * replacer.Process(page);
     * </pre>
     */
    class ContentReplacer extends PDFNet.Destroyable {
        /**
         * Create a new ContentReplacer object, to which replacement rules will be added.
         * The same object can be used to 'Process' multiple pages.
         * @returns A promise that resolves to an object of type: "PDFNet.ContentReplacer"
         */
        static create(): Promise<PDFNet.ContentReplacer>;
        /**
         * Replace the image that best fits into 'target_region' with 'replacement_image'.
         * @param target_region - The rectangle defining the area in which an image
         * that best fits the rectangle will be replaced by 'replacement_image'.
         * @param replacement_image - The 'SDF.Obj' of a 'PDF.Image' object.
         */
        addImage(target_region: PDFNet.Rect, replacement_image: PDFNet.Obj): Promise<void>;
        /**
         * All text inside 'target_region' will be deleted and replaced with 'replacement_text'.
         * @param target_region - The rectangle defining the area in which all text will
         * be replaced by 'replacement_text'.
         * @param replacement_text - The new text that will replace the existing text
         * in 'target_region'.
         */
        addText(target_region: PDFNet.Rect, replacement_text: string): Promise<void>;
        /**
         * Any text of the form "[template_text]" will be replaced by "replacement_text".
         * @param template_text - The text to remove.
         * @param replacement_text - The new text that will appear in place of 'template_text'.
         */
        addString(template_text: string, replacement_text: string): Promise<void>;
        /**
         * Change the delimiters from '[' and ']' to arbitary strings.
         * @param start_str - The starting delimiter string.
         * @param end_str - The ending delimiter string.
         */
        setMatchStrings(start_str: string, end_str: string): Promise<void>;
        /**
         * Apply the replacement instructions to the target page. Subsequent calls
         * to 'Process' can be made on other pages, and it will apply the same rules.
         * @param page - The page to apply the content replacement instructions to.
         */
        process(page: PDFNet.Page): Promise<void>;
    }
    /**
     * Experimental. Developer use only.
     */
    class ConversionMonitor extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        next(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        ready(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        progress(): Promise<number>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        filter(): Promise<PDFNet.Filter>;
    }
    /**
     * The Date class is a utility class used to simplify work with PDF date objects.
     *
     * PDF defines a standard date format, which closely follows international
     * standard ASN.1 (Abstract Syntax Notation One), A date is a string of the form
     * (D:YYYYMMDDHHmmSSOHH'mm'); See PDF Reference Manual for details.
     *
     * Date can be associated with a SDF/Cos date string using Date(Obj*) constructor
     * or later using Date::Attach(Obj*) or Date::Update(Obj*) methods.
     *
     * Date keeps a local date/time cache so it is necessary to call Date::Update()
     * method if the changes to the Date should be saved in the attached Cos/SDF string.
     */
    class Date {
        constructor(year?: number, month?: number, day?: number, hour?: number, minute?: number, second?: number, UT?: number, UT_hour?: number, UT_minutes?: number, mp_obj?: PDFNet.Obj);
        /**
         * Create a Date and initialize it using specified parameters.
         * The Date is not attached to any Cos/SDF object.
         * @param year - number representing the year to initialize the Date object to.
         * @param month - number representing the month to initialize the Date object to.
         * @param day - number representing the day to initialize the Date object to.
         * @param hour - number representing the hour to initialize the Date object to.
         * @param minute - number representing the minute to initialize the Date object to.
         * @param second - number representing the second to initialize the Date object to.
         * @returns A promise that resolves to an object of type: "PDFNet.Date"
         */
        static init(year: number, month: number, day: number, hour: number, minute: number, second: number): Promise<PDFNet.Date>;
        /**
         * Indicates whether the Date is valid (non-null).
         * @returns A promise that resolves to true if this is a valid (non-null) Date; otherwise false.
         */
        isValid(): Promise<boolean>;
        /**
         * attach the Cos/SDF object to the Date.
         * @param d - underlying Cos/SDF object. Must be an SDF::Str containing
         *  a PDF date object.
         */
        attach(d: PDFNet.Obj): Promise<void>;
        /**
         * Saves changes made to the Date object in the attached (or specified) SDF/Cos string.
         * @param [d] - an optional parameter indicating a SDF string that should be
         *  updated and attached to this Date. If parameter d is NULL or is omitted, update
         *  is performed on previously attached Cos/SDF date.
         * @returns A promise that resolves to true if the attached Cos/SDF string was successfully updated, false otherwise.
         */
        update(d?: PDFNet.Obj): Promise<boolean>;
        year: number;
        month: number;
        day: number;
        hour: number;
        minute: number;
        second: number;
        UT: number;
        UT_hour: number;
        UT_minutes: number;
        mp_obj: PDFNet.Obj;
    }
    /**
     * A destination defines a particular view of a document, consisting of the
     * following:
     *
     * - The page of the document to be displayed
     * - The location of the document window on that page
     * - The magnification (zoom) factor to use when displaying the page
     *
     * Destinations may be associated with Bookmarks, Annotations, and Remote Go-To Actions.
     *
     * Destination is a utility class used to simplify work with PDF Destinations;
     * Please refer to section 8.2.1 'Destinations' in PDF Reference Manual for details.
     */
    class Destination {
        /**
         * Create a new 'XYZ' Destination.
         *
         * The new Destination displays the page designated by 'page', with the
         * coordinates ('left', 'top') positioned at the top-left corner of the
         * window and the contents of the page magnified by the factor 'zoom'.
         * A null value for any of the parameters 'left', 'top', or 'zoom' specifies
         * that the current value of that parameter is to be retained unchanged.
         * A 'zoom' value of 0 has the same meaning as a null value.
         * the page within the window in the other dimension.
         * @param page - Page object to display
         * @param left - horizontal coordinate of the left edge of the window
         * @param top - vertical coordinate of the top edge of the window
         * @param zoom - amount to zoom the page by
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createXYZ(page: PDFNet.Page, left: number, top: number, zoom: number): Promise<PDFNet.Destination>;
        /**
         * Create a new 'Fit' Destination.
         *
         * The new Destination displays the page designated by 'page', with its contents
         * magnified just enough to fit the entire page within the window both
         * horizontally and vertically. If the required horizontal and vertical
         * magnification factors are different, use the smaller of the two, centering
         * the page within the window in the other dimension.
         * @param page - Page object to display
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFit(page: PDFNet.Page): Promise<PDFNet.Destination>;
        /**
         * Create a new 'FitH' Destination.
         *
         * The new Destination displays the page designated by 'page', with the
         * vertical coordinate 'top' positioned at the top edge of the window and
         * the contents of the page magnified just enough to fit the entire width
         * of the page within the window.
         * @param page - Page object to display
         * @param top - vertical coordinate of the top edge of the window
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFitH(page: PDFNet.Page, top: number): Promise<PDFNet.Destination>;
        /**
         * Create a new 'FitV' Destination.
         *
         * The new Destination displays the page designated by 'page', with the
         * horizontal coordinate 'left' positioned at the left edge of the window
         * and the contents of the page magnified just enough to fit the entire
         * height of the page within the window.
         * @param page - Page object to display
         * @param left - horizontal coordinate of the left edge of the window
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFitV(page: PDFNet.Page, left: number): Promise<PDFNet.Destination>;
        /**
         * Create a new 'FitR' Destination.
         *
         * The new Destination displays the page designated by 'page', with its
         * contents magnified just enough to fit the rectangle specified by the
         * coordinates 'left', 'bottom', 'right', and 'top' entirely within the
         * window both horizontally and vertically. If the required horizontal
         * and vertical magnification factors are different, use the smaller of
         * the two, centering the rectangle within the window in the other
         * dimension.
         * @param page - Page object to display
         * @param left - horizontal coordinate of the left edge of the window
         * @param bottom - vertical coordinate of the bottom edge of the window
         * @param right - horizontal coordinate of the right edge of the window
         * @param top - vertical coordinate of the top edge of the window
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFitR(page: PDFNet.Page, left: number, bottom: number, right: number, top: number): Promise<PDFNet.Destination>;
        /**
         * Create a new 'FitB' Destination.
         *
         * The new Destination displays the page designated by 'page', with its
         * contents magnified just enough to fit its bounding box entirely within
         * the window both horizontally and vertically. If the required
         * horizontal and vertical magnification factors are different, use the
         * smaller of the two, centering the bounding box within the window in
         * the other dimension.
         * @param page - Page object to display
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFitB(page: PDFNet.Page): Promise<PDFNet.Destination>;
        /**
         * Create a new 'FitBH' Destination.
         *
         * The new Destination displays the page designated by 'page', with
         * the vertical coordinate 'top' positioned at the top edge of the window
         * and the contents of the page magnified just enough to fit the entire
         * width of its bounding box within the window.
         * @param page - Page object to display
         * @param top - vertical coordinate of the top edge of the window
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFitBH(page: PDFNet.Page, top: number): Promise<PDFNet.Destination>;
        /**
         * Create a new 'FitBV' Destination.
         *
         * The new Destination displays Display the page designated by 'page',
         * with the horizontal coordinate 'left' positioned at the left edge of
         * the window and the contents of the page magnified just enough to fit
         * the entire height of its bounding box within the window.
         * @param page - Page object to display
         * @param left - horizontal coordinate of the left edge of the window
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static createFitBV(page: PDFNet.Page, left: number): Promise<PDFNet.Destination>;
        /**
         * Create a Destination and initialize it using given Cos/SDF object.
         * @param dest - a low-level (SDF/Cos) destination object. The low-level
         * destination can be either a named destination (i.e. a Name or a String)
         * or an explicit destination (i.e. an Array Obj). Please refer to section
         * 8.2.1 'Destinations' in PDF Reference Manual for more details.
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        static create(dest: PDFNet.Obj): Promise<PDFNet.Destination>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Destination"
         */
        copy(): Promise<PDFNet.Destination>;
        /**
         * @returns A promise that resolves to true if this is a valid Destination and can be resolved, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Destination.FitType = {
         * 	e_XYZ : 0
         * 	e_Fit : 1
         * 	e_FitH : 2
         * 	e_FitV : 3
         * 	e_FitR : 4
         * 	e_FitB : 5
         * 	e_FitBH : 6
         * 	e_FitBV : 7
         * }
         * </pre>
         * @returns A promise that resolves to destination's FitType.
         */
        getFitType(): Promise<number>;
        /**
         * @returns A promise that resolves to the Page that this destination refers to.
         */
        getPage(): Promise<PDFNet.Page>;
        /**
         * Modify the destination so that it refers to the new 'page' as the destination page.
         * @param page - The new page associated with this Destination.
         */
        setPage(page: PDFNet.Page): Promise<void>;
        /**
         * @returns A promise that resolves to the object to the underlying SDF/Cos object.
         * The returned SDF/Cos object is an explicit destination (i.e. the Obj is either
         * an array defining the destination, using the syntax shown in Table 8.2 in PDF
         * Reference Manual), or a dictionary with a 'D' entry whose value is such an
         * array. The latter form allows additional attributes to be associated with
         * the destination
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the explicit destination SDF/Cos object. This is always an Array
         * as shown in Table 8.2 in PDF Reference Manual.
         */
        getExplicitDestObj(): Promise<PDFNet.Obj>;
    }
    /**
     * DictIterator is used to traverse key/value pairs in a dictionary.
     * For example a DictIterator can be used to print out all the entries
     * in a given Obj dictionary as follows:
     * <pre>
     *  DictIterator itr = dict.GetDictIterator();
     *  while (itr.HasNext()) {
     *      Obj key = itr.Key();
     * 	    cout << key.GetName() << endl;
     *      Obj value = itr.Value();
     *      // ...
     *      itr.Next()
     *   }
     * </pre>
     */
    class DictIterator extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to true if the iterator can be successfully advanced to the
         * next element; false if the end collection is reached.
         */
        hasNext(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the key of the current dictionary entry.
         */
        key(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the value of the current dictionary entry.
         */
        value(): Promise<PDFNet.Obj>;
        /**
         * Advances the iterator to the next element of the collection.
         */
        next(): Promise<void>;
    }
    /**
     * [Missing documentation]
     */
    class DigestAlgorithm {
    }
    /**
     * A class representing a digital signature form field.
     */
    class DigitalSignatureField {
        constructor(mp_field_dict_obj?: PDFNet.Obj);
        /**
         * Returns whether the digital signature field has been cryptographically signed. Checks whether there is a digital signature dictionary in the field and whether it has a Contents entry. Must be called before using various digital signature dictionary-related functions. Does not check validity will return true even if a valid hash has not yet been generated (which will be the case after [Certify/Sign]OnNextSave[WithCustomHandler] has been called on the signature but even before Save is called on the document).
         * @returns A promise that resolves to a boolean value representing whether the digital signature field has a digital signature dictionary with a Contents entry.
         */
        hasCryptographicSignature(): Promise<boolean>;
        /**
         * Returns the SubFilter type of the digital signature. Specification says that one must check the SubFilter before using various getters. Must call HasCryptographicSignature first and use it to check whether the signature is signed.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DigitalSignatureField.SubFilterType = {
         * 	e_adbe_x509_rsa_sha1 : 0
         * 	e_adbe_pkcs7_detached : 1
         * 	e_adbe_pkcs7_sha1 : 2
         * 	e_ETSI_CAdES_detached : 3
         * 	e_ETSI_RFC3161 : 4
         * 	e_unknown : 5
         * 	e_absent : 6
         * }
         * </pre>
         * @returns A promise that resolves to an enumeration describing what the SubFilter of the digital signature is from within the digital signature dictionary.
         */
        getSubFilter(): Promise<number>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Returns the name of the signer of the signature from the digital signature dictionary. Must call HasCryptographicSignature first and use it to check whether the signature is signed.
         * @returns A promise that resolves to a unicode string containing the name of the signer from within the digital signature dictionary. Empty if Name entry not present.
         */
        getSignatureName(): Promise<string>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Returns the Location of the signature from the digital signature dictionary. Must call HasCryptographicSignature first and use it to check whether the signature is signed.
         * @returns A promise that resolves to a unicode string containing the signing location from within the digital signature dictionary. Empty if Location entry not present.
         */
        getLocation(): Promise<string>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Returns the Reason for the signature from the digital signature dictionary. Must call HasCryptographicSignature first and use it to check whether the signature is signed.
         * @returns A promise that resolves to a unicode string containing the reason for the signature from within the digital signature dictionary. Empty if Reason entry not present.
         */
        getReason(): Promise<string>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Returns the contact information of the signer from the digital signature dictionary. Must call HasCryptographicSignature first and use it to check whether the signature is signed.
         * @returns A promise that resolves to a unicode string containing the contact information of the signer from within the digital signature dictionary. Empty if ContactInfo entry not present.
         */
        getContactInfo(): Promise<string>;
        /**
         * Gets number of certificates in certificate chain (Cert entry of digital signature dictionary). Must call HasCryptographicSignature first and use it to check whether the signature is signed.
         * @returns A promise that resolves to an integer value the number of certificates in the Cert entry of the digital signature dictionary.
         */
        getCertCount(): Promise<number>;
        /**
         * Returns whether the field has a visible appearance. Can be called without checking HasCryptographicSignature first, since it operates on the surrounding Field dictionary, not the "V" entry (i.e. digital signature dictionary). Performs the zero-width+height check, the Hidden bit check, and the NoView bit check as described by the PDF 2.0 specification, section 12.7.5.5 "Signature fields".
         * @returns A promise that resolves to a boolean representing whether or not the signature field has a visible signature.
         */
        hasVisibleAppearance(): Promise<boolean>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Sets the ContactInfo entry in the digital signature dictionary. Must create a digital signature dictionary first using [Certify/Sign]OnNextSave[WithCustomHandler]. If this function is called on a digital signature field that has already been cryptographically signed with a valid hash, the hash will no longer be valid, so do not call Save (to sign/create the hash) until after you call this function, if you need to call this function in the first place. Essentially, call this function after [Certify/Sign]OnNextSave[WithCustomHandler] and before Save.
         * @param in_contact_info - - A string containing the ContactInfo to be set.
         */
        setContactInfo(in_contact_info: string): Promise<void>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Sets the Location entry in the digital signature dictionary. Must create a digital signature dictionary first using [Certify/Sign]OnNextSave[WithCustomHandler]. If this function is called on a digital signature field that has already been cryptographically signed with a valid hash, the hash will no longer be valid, so do not call Save (to sign/create the hash) until after you call this function, if you need to call this function in the first place. Essentially, call this function after [Certify/Sign]OnNextSave[WithCustomHandler] and before Save.
         * @param in_location - - A string containing the Location to be set.
         */
        setLocation(in_location: string): Promise<void>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp). Sets the Reason entry in the digital signature dictionary. Must create a digital signature dictionary first using [Certify/Sign]OnNextSave[WithCustomHandler]. If this function is called on a digital signature field that has already been cryptographically signed with a valid hash, the hash will no longer be valid, so do not call Save (to sign/create the hash) until after you call this function, if you need to call this function in the first place. Essentially, call this function after [Certify/Sign]OnNextSave[WithCustomHandler] and before Save.
         * @param in_reason - - A string containing the Reason to be set.
         */
        setReason(in_reason: string): Promise<void>;
        /**
         * Sets the document locking permission level for this digital signature field. Call only on unsigned signatures, otherwise a valid hash will be invalidated.
         * @param in_perms - <pre>
         * PDFNet.DigitalSignatureField.DocumentPermissions = {
         * 	e_no_changes_allowed : 1
         * 	e_formfilling_signing_allowed : 2
         * 	e_annotating_formfilling_signing_allowed : 3
         * 	e_unrestricted : 4
         * }
         * </pre>
         * -- An enumerated value representing the document locking permission level to set.
         */
        setDocumentPermissions(in_perms: number): Promise<void>;
        /**
         * Must be called to prepare a signature for signing, which is done afterwards by calling Save. Cannot sign two signatures during one save (throws). Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param in_pkcs12_keyfile_path - - The path to the PKCS #12 private keyfile to use to sign this digital signature.
         * @param in_password - - The password to use to parse the PKCS #12 keyfile.
         */
        signOnNextSave(in_pkcs12_keyfile_path: string, in_password: string): Promise<void>;
        /**
         * Must be called to prepare a signature for certification, which is done afterwards by calling Save. Throws if document already certified. Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param in_pkcs12_keyfile_path - - The path to the PKCS #12 private keyfile to use to certify this digital signature.
         * @param in_password - - The password to use to parse the PKCS #12 keyfile.
         */
        certifyOnNextSave(in_pkcs12_keyfile_path: string, in_password: string): Promise<void>;
        /**
         * Returns whether this digital signature field is locked against modifications by any digital signatures. Can be called when this field is unsigned.
         * @returns A promise that resolves to a boolean representing whether this digital signature field is locked against modifications by any digital signatures in the document.
         */
        isLockedByDigitalSignature(): Promise<boolean>;
        /**
         * If HasCryptographicSignature, returns most restrictive permissions found in any reference entries in this digital signature. Returns Lock-resident (i.e. tentative) permissions otherwise. Throws if invalid permission value is found.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DigitalSignatureField.DocumentPermissions = {
         * 	e_no_changes_allowed : 1
         * 	e_formfilling_signing_allowed : 2
         * 	e_annotating_formfilling_signing_allowed : 3
         * 	e_unrestricted : 4
         * }
         * </pre>
         * @returns A promise that resolves to an enumeration value representing the level of restrictions (potentially) placed on the document by this signature.
         */
        getDocumentPermissions(): Promise<number>;
        /**
         * Clears cryptographic signature, if present. Otherwise, does nothing. Do not need to call HasCryptographicSignature before calling this. After clearing, other signatures should still pass validation if saving after clearing was done incrementally. Clears the appearance as well.
         */
        clearSignature(): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.DigitalSignatureField"
         */
        static createFromField(d: PDFNet.Field): Promise<PDFNet.DigitalSignatureField>;
        /**
         * Should not be called when SubFilter is ETSI.RFC3161 (i.e. on a DocTimeStamp).
         * Returns the "M" entry from the digital signature dictionary, which represents the
         * signing date/time. Must call HasCryptographicSignature first and use it to check whether the
         * signature is signed.
         * @returns A promise that resolves to a PDF::Date object holding the signing date/time from within the digital signature dictionary. Returns a default-constructed PDF::Date if no date is present.
         */
        getSigningTime(): Promise<PDFNet.Date>;
        /**
         * Gets a certificate in the certificate chain (Cert entry) of the digital signature dictionary by index. Throws if Cert is not Array or String, throws if index is out of range and Cert is Array, throws if index is > 1 and Cert is string, otherwise retrieves the certificate.
         * @param in_index - - An integral index which must be greater than 0 and less than the cert count as retrieved using GetCertCount.
         * @returns A promise that resolves to a vector of bytes containing the certificate at the index. Returns empty vector if Cert is missing.
         */
        getCert(in_index: number): Promise<Uint8Array>;
        /**
         * Tentatively sets which fields are to be locked by this digital signature upon signing. It is not necessary to call HasCryptographicSignature before using this function. Throws if non-empty array of field names is passed along with FieldPermissions Action == e_lock_all.
         * @param in_action - <pre>
         * PDFNet.DigitalSignatureField.FieldPermissions = {
         * 	e_lock_all : 0
         * 	e_include : 1
         * 	e_exclude : 2
         * }
         * </pre>
         * -- An enumerated value representing which sort of field locking should be done. Options are All (lock all fields), Include (lock listed fields), and Exclude (lock all fields except listed fields).
         * @param [in_field_names_list] - - A list of field names; can be empty (and must be empty, if Action is set to All). Empty by default.
         */
        setFieldPermissions(in_action: number, in_field_names_list?: string[]): Promise<void>;
        /**
         * Must be called to prepare a signature for signing, which is done afterwards by calling Save. Cannot sign two signatures during one save (throws). Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param in_pkcs12_buffer - - A buffer of bytes containing the PKCS #12 private key certificate store to use to sign this digital signature.
         * @param in_password - - The password to use to parse the PKCS #12 buffer.
         */
        signOnNextSaveFromBuffer(in_pkcs12_buffer: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, in_password: string): Promise<void>;
        /**
         * Must be called to prepare a signature for signing, which is done afterwards by calling Save. Cannot sign two signatures during one save (throws). Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param in_signature_handler_id - - The unique id of the signature handler to use to sign this digital signature.
         */
        signOnNextSaveWithCustomHandler(in_signature_handler_id: number): Promise<void>;
        /**
         * Must be called to prepare a signature for certification, which is done afterwards by calling Save. Throws if document already certified. Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param in_pkcs12_buffer - - A buffer of bytes containing the PKCS #12 private key certificate store to use to certify this digital signature.
         * @param in_password - - The password to use to parse the PKCS #12 buffer.
         */
        certifyOnNextSaveFromBuffer(in_pkcs12_buffer: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, in_password: string): Promise<void>;
        /**
         * Must be called to prepare a signature for certification, which is done afterwards by calling Save. Throws if document already certified. Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param in_signature_handler_id - - The unique id of the signature handler to use to certify this digital signature.
         */
        certifyOnNextSaveWithCustomHandler(in_signature_handler_id: number): Promise<void>;
        /**
         * Retrieves the SDF Obj of the digital signature field.
         * @returns A promise that resolves to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * Returns the fully-qualified names of all fields locked by this signature using the field permissions feature. Retrieves from the digital signature dictionary if the form field HasCryptographicSignature. Otherwise, retrieves from the Lock entry of the digital signature form field. Result is invalidated by any field additions or removals. Does not take document permissions restrictions into account.
         * @returns A promise that resolves to a vector of UStrings representing the fully-qualified names of all fields locked by this signature.
         */
        getLockedFields(): Promise<string[]>;
        /**
         * Verifies this cryptographic digital signature in the manner specified by the VerificationOptions. EXPERIMENTAL. Digital signature verification is undergoing active development, but currently does not support a number of features. If we are missing a feature that is important to you, or if you have files that do not act as expected, please contact us using one of the following forms: https://www.pdftron.com/form/trial-support/ or https://www.pdftron.com/form/request/
         * @param in_opts - - The options specifying how to do the verification.
         * @returns A promise that resolves to a VerificationResult object containing various information about the verifiability of the cryptographic digital signature.
         */
        verify(in_opts: PDFNet.VerificationOptions): Promise<PDFNet.VerificationResult>;
        /**
         * Returns whether or not this signature is a certification.
         * @returns A promise that resolves to a boolean value representing whether or not this signature is a certification.
         */
        isCertification(): Promise<boolean>;
        /**
         * Returns the signing certificate. Must only be called on signed adbe.pkcs7.detached signatures.
         * @returns A promise that resolves to an X509Certificate object.
         */
        getSignerCertFromCMS(): Promise<PDFNet.X509Certificate>;
        /**
         * Retrieves the ranges of byte indices within the document over which this signature is intended to apply/be verifiable.
         * @returns A promise that resolves to a container of byte range objects
         */
        getByteRanges(): Promise<PDFNet.ByteRange[]>;
        getCertPathsFromCMS(index: number): Promise<void>;
        /**
         * Retrieves all constructible certificate paths from an adbe.pkcs7.detached digital signature. The signer
         * will always be returned if the signature is CMS-based and not corrupt. Must only be called on
         * signed adbe.pkcs7.detached signatures. The order of the certificates in each of the paths returned is as follows:
         * the signer will be first, and issuers come after it in order of the issuer of the previous certificate.
         * The default behaviour is to return a sub-path for each marginal issuer in a max-length path.
         * @returns A promise that resolves to a container of X509Certificate objects
         */
        getCertPathsFromCMSGetOutterVecSize(): Promise<number>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        enableLTVOfflineVerification(in_veri_res: PDFNet.VerificationResult): Promise<boolean>;
        /**
         * Must be called to prepare a secure PDF-embedded timestamp signature (RFC 3161
         * DocTimeStamp) for signing, which is done afterwards by calling Save on the
         * document with an e_incremental flag. Throws if document is locked by other
         * signatures,  if signature is already signed, or if another signature has already
         * been prepared for signing on the next save (because only one signing operation
         * can be done per incremental save). Default document permission level is
         * e_annotating_formfilling_signing_allowed.
         * @param in_timestamping_config - - Configuration options to store for timestamping.
         * These will include various items related to contacting a timestamping authority.
         * Incorrect configuration will result in document Save throwing an exception.
         * The usability of a combination of a TimestampingConfiguration and VerificationOptions
         * can be checked ahead of time to prevent exceptions by calling TestConfiguration on
         * TimestampingConfiguration and passing VerificationOptions.
         * @param in_timestamp_response_verification_options - - Options for the timestamp
         * response verification step (which is required by RFC 3161 to be done as part of
         * timestamping). These response verification options should include the root certificate
         * of the timestamp authority, so that the trust status of the timestamp signature
         * can be verified. The options that should be passed are the same ones that one expects
         * the timestamp to be verifiable with in the future (once it is embedded in the document),
         * except the response verification requires online revocation information whereas
         * the later verification may not (depending on whether LTV offline verification
         * information for the timestamp signature gets embedded into the document by that
         * time). The timestamp response verification step makes sure that (a) the
         * timestamp response has a success status, which is the only time that this is
         * verified in the entire workflow, which prevents embedding an unsuccessful
         * response; (b) that it digests the document correctly and is otherwise generally
         * verifiable; and (c) that the nonce is correct (which is the only time that this
         * is verifiable in the entire workflow) to prevent replay attacks (if it was not
         * requested in the TimestampingConfiguration that the nonce mechanism should be
         * disabled).
         */
        timestampOnNextSave(in_timestamping_config: PDFNet.TimestampingConfiguration, in_timestamp_response_verification_options: PDFNet.VerificationOptions): Promise<void>;
        /**
         * Sets the requested SubFilter value (which identifies a signature type) as the only one to use during future signing, overwriting all such previous settings. It is not necessary to call HasCryptographicSignature before calling this function. For example, this function can be used to switch to PAdES signing mode.
         * @param in_subfilter_type - <pre>
         * PDFNet.DigitalSignatureField.SubFilterType = {
         * 	e_adbe_x509_rsa_sha1 : 0
         * 	e_adbe_pkcs7_detached : 1
         * 	e_adbe_pkcs7_sha1 : 2
         * 	e_ETSI_CAdES_detached : 3
         * 	e_ETSI_RFC3161 : 4
         * 	e_unknown : 5
         * 	e_absent : 6
         * }
         * </pre>
         * -- The SubFilter type to set.
         * @param [in_make_mandatory] - - Whether to make usage of this SubFilter mandatory for future signing applications. Default value for this parameter is true.
         */
        useSubFilter(in_subfilter_type: number, in_make_mandatory?: boolean): Promise<void>;
        /**
         * Must be called to prepare a signature for certification, which is done afterwards by calling Save. Throws if document already certified.
        Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param url - The url to the PKCS #12 private keyfile to use to certify this digital signature.
         * @param in_password - - The password to use to parse the PKCS #12 keyfile.
         * @param [options] - Additional options
         * @param options.withCredentials - Whether to set the withCredentials property on the XMLHttpRequest
         * @param options.customHeaders - An object containing custom HTTP headers to be used when downloading the document
         */
        static certifyOnNextSaveFromURL(url: string, in_password: string, options?: {
            withCredentials: boolean;
            customHeaders: any;
        }): Promise<void>;
        /**
         * Must be called to prepare a signature for signing, which is done afterwards by calling Save. Cannot sign two signatures during one save (throws).
        Default document permission level is e_annotating_formfilling_signing_allowed. Throws if signature field already has a digital signature dictionary.
         * @param url - The url to the PKCS #12 private keyfile to use to sign this digital signature.
         * @param in_password - - The password to use to parse the PKCS #12 keyfile.
         * @param [options] - Additional options
         * @param options.withCredentials - Whether to set the withCredentials property on the XMLHttpRequest
         * @param options.customHeaders - An object containing custom HTTP headers to be used when downloading the document
         */
        static signOnNextSaveFromURL(url: string, in_password: string, options?: {
            withCredentials: boolean;
            customHeaders: any;
        }): Promise<void>;
        mp_field_dict_obj: PDFNet.Obj;
    }
    /**
     * The class DisallowedChange.
     * Data pertaining to a change detected in a document during a digital
     * signature modification permissions verification step, the change bein
     * g both made after the signature was signed, and disallowed by t
     * he signature's permissions settings.
     */
    class DisallowedChange extends PDFNet.Destroyable {
        /**
         * Returns the SDF object number of the indirect object associated with this DisallowedChange.
         * @returns A promise that resolves to an unsigned 32-bit integer value.
         */
        getObjNum(): Promise<number>;
        /**
         * Returns an enumeration value representing the semantic type of this disallowed change.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DisallowedChange.Type = {
         * 	e_form_filled : 0
         * 	e_digital_signature_signed : 1
         * 	e_page_template_instantiated : 2
         * 	e_annotation_created_or_updated_or_deleted : 3
         * 	e_other : 4
         * 	e_unknown : 5
         * }
         * </pre>
         * @returns A promise that resolves to an enumeration value of type: Type of DisallowedChange.
         */
        getType(): Promise<number>;
        /**
         * Returns a string value representing the semantic type of this disallowed change.
         * @returns A promise that resolves to a string.
         */
        getTypeAsString(): Promise<string>;
    }
    /**
     * The class DocSnapshot.
     * Represents a state of the document.
     */
    class DocSnapshot extends PDFNet.Destroyable {
        /**
         * Returns a hash that is unique to particular document states.
         * @returns A promise that resolves to a hash that is unique to particular document states.
         */
        getHash(): Promise<number>;
        /**
         * Returns whether this snapshot is valid.
         * @returns A promise that resolves to whether this snapshot is valid.
         */
        isValid(): Promise<boolean>;
        /**
         * Returns whether this snapshot's document state is equivalent to another.
         * @param snapshot - - the other snapshot with which to compare.
         * @returns A promise that resolves to whether this snapshot's document state is equivalent to another.
         */
        equals(snapshot: PDFNet.DocSnapshot): Promise<boolean>;
    }
    /**
     * The class DocumentConversion.
     * Encapsulates the conversion of a single document from one format to another.
     *
     * DocumentConversion instances are created through methods belonging to
     * the Convert class. See Convert.WordToPDFConversion for an example.
     */
    class DocumentConversion {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        getDoc(): Promise<PDFNet.PDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        isCancelled(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        getNumConvertedPages(): Promise<number>;
    }
    /**
     * Element is the abstract interface used to access graphical elements used to build the
     * display list.
     *
     * Just like many other classes in PDFNet (e.g. ColorSpace, Font, Annot, etc), Element
     * class follows the composite design pattern. This means that all Elements are
     * accessed through the same interface, but depending on the Element type (that can be
     * obtained using GetType()), only methods related to that type can be called.
     * For example, if GetType() returns e_image, it is illegal to call a method specific to
     * another Element type (i.e. a call to a text specific GetTextData() will throw an
     * Exception).
     */
    class Element {
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Element.Type = {
         * 	e_null : 0
         * 	e_path : 1
         * 	e_text_begin : 2
         * 	e_text : 3
         * 	e_text_new_line : 4
         * 	e_text_end : 5
         * 	e_image : 6
         * 	e_inline_image : 7
         * 	e_shading : 8
         * 	e_form : 9
         * 	e_group_begin : 10
         * 	e_group_end : 11
         * 	e_marked_content_begin : 12
         * 	e_marked_content_end : 13
         * 	e_marked_content_point : 14
         * }
         * </pre>
         * @returns A promise that resolves to the current element type.
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to gState of this Element
         */
        getGState(): Promise<PDFNet.GState>;
        /**
         * @returns A promise that resolves to current Transformation Matrix (CTM) that maps coordinates to the
         * initial user space.
         */
        getCTM(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to parent logical structure element (such as 'span' or 'paragraph').
         * If the Element is not associated with any structure element, the returned
         * SElement will not be valid (i.e. selem.IsValid() -> false).
         */
        getParentStructElement(): Promise<PDFNet.SElement>;
        /**
         * @returns A promise that resolves to marked Content Identifier (MCID) for this Element or
         * a negative number if the element is not assigned an identifier/MCID.
         *
         * Marked content identifier can be used to associate an Element with
         * logical structure element that refers to the Element.
         */
        getStructMCID(): Promise<number>;
        /**
         * @returns A promise that resolves to true if this element is visible in the optional-content
         * context (OCG::Context). The method considers the context's current OCMD stack,
         * the group ON-OFF states, the non-OC drawing status, the drawing and enumeration mode,
         * and the intent.
         *
         * When enumerating page content, OCG::Context can be passed as a parameter in
         * ElementReader.Begin() method. When using PDFDraw, PDFRasterizer, or PDFView class to
         * render PDF pages use PDFDraw::SetOCGContext() method to select an OC context.
         */
        isOCVisible(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the current path element is a clipping path and should be added
         * to clipping path stack.
         */
        isClippingPath(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the current path element should be stroked
         */
        isStroked(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the current path element should be filled
         */
        isFilled(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the current path should be filled using non-zero winding rule,
         * or false if the path should be filled using even-odd rule.
         *
         * According non-zero winding rule, you can determine whether a test point is inside or
         * outside a closed curve as follows: Draw a line from a test point to a point that
         * is distant from the curve. Count the number of times the curve crosses the test
         * line from left to right, and count the number of times the curve crosses the test
         * line from right to left. If those two numbers are the same, the test point is
         * outside the curve; otherwise, the test point is inside the curve.
         *
         * According to even-odd rule, you can determine whether a test point is inside
         * or outside a closed curve as follows: Draw a line from the test point to a point
         * that is distant from the curve. If that line crosses the curve an odd number of
         * times, the test point is inside the curve; otherwise, the test point is outside
         * the curve.
         */
        isWindingFill(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the current clip path is using non-zero winding rule, or false
         * for even-odd rule.
         */
        isClipWindingFill(): Promise<boolean>;
        /**
         * indicate whether the path is a clipping path or non-clipping path
         * @param clip - true to set path to clipping path. False for non-clipping path.
         */
        setPathClip(clip: boolean): Promise<void>;
        /**
         * indicate whether the path should be stroked
         * @param stroke - true to set path to be stroked. False for no stroke path.
         */
        setPathStroke(stroke: boolean): Promise<void>;
        /**
         * indicate whether the path should be filled
         * @param fill - true to set path to be filled. False for no fill path.
         */
        setPathFill(fill: boolean): Promise<void>;
        /**
         * sets path's fill rule.
         * @param winding_rule - if winding_rule is true path will be filled using non-zero
         * winding fill rule, otherwise even-odd fill will be used.
         */
        setWindingFill(winding_rule: boolean): Promise<void>;
        /**
         * sets clipping path's fill rule.
         * @param winding_rule - if winding_rule is true clipping should use non-zero
         * winding rule, or false for even-odd rule.
         */
        setClipWindingFill(winding_rule: boolean): Promise<void>;
        setPathTypes(in_seg_types: string, count: number): Promise<void>;
        /**
         * @returns A promise that resolves to the SDF object of the Image/Form object.
         */
        getXObject(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to a stream (filter) containing decoded image data
         */
        getImageData(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to the size of image data in bytes
         */
        getImageDataSize(): Promise<number>;
        /**
         * @returns A promise that resolves to the SDF object representing the color space in which image
         * are specified or NULL if the image is an image mask
         *
         * The returned color space may be any type of color space except Pattern.
         */
        getImageColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to the width of the image, in samples.
         */
        getImageWidth(): Promise<number>;
        /**
         * @returns A promise that resolves to the height of the image, in samples.
         */
        getImageHeight(): Promise<number>;
        /**
         * @returns A promise that resolves to decode array or NULL if the parameter is not specified. A decode object is an
         * array of numbers describing how to map image samples into the range of values
         * appropriate for the color space of the image. If ImageMask is true, the array must be
         * either [0 1] or [1 0]; otherwise, its length must be twice the number of color
         * components required by ColorSpace. Default value depends on the color space,
         * See Table 4.36 in PDF Ref. Manual.
         */
        getDecodeArray(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the number of bits used to represent each color component. Only a
         * single value may be specified; the number of bits is the same for all color
         * components. Valid values are 1, 2, 4, and 8.
         */
        getBitsPerComponent(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of color components per sample.
         */
        getComponentNum(): Promise<number>;
        /**
         * @returns A promise that resolves to a boolean indicating whether the inline image is to be treated as an image mask.
         */
        isImageMask(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a boolean indicating whether image interpolation is to be performed.
         */
        isImageInterpolate(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an image XObject defining an image mask to be applied to this image (See
         * 'Explicit Masking', 4.8.5), or an array specifying a range of colors
         * to be applied to it as a color key mask (See 'Color Key Masking').
         *
         * If IsImageMask() return true, this method will return NULL.
         */
        getMask(): Promise<PDFNet.Obj>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.RenderingIntent = {
         * 	e_absolute_colorimetric : 0
         * 	e_relative_colorimetric : 1
         * 	e_saturation : 2
         * 	e_perceptual : 3
         * }
         * </pre>
         * @returns A promise that resolves to the color rendering intent to be used in rendering the image.
         */
        getImageRenderingIntent(): Promise<number>;
        /**
         * @returns A promise that resolves to a pointer to Unicode string for this text Element. The
         * function maps character codes to Unicode array defined by Adobe
         * Glyph List (http://partners.adobe.com/asn/developer/type/glyphlist.txt).
         */
        getTextString(): Promise<string>;
        /**
         * @returns A promise that resolves to a reference to the current text matrix (Tm).
         */
        getTextMatrix(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to a CharIterator addressing the first CharData element in the text run.
         *
         * CharIterator points to CharData. CharData is a data structure that contains
         * the char_code number (used to retrieve glyph outlines, to map to Unicode, etc.),
         * character positioning information (x, y), and the number of bytes taken by the
         * character within the text buffer.
         */
        getCharIterator(): Promise<PDFNet.Iterator<number>>;
        /**
         * @returns A promise that resolves to the text advance distance in text space.
         *
         * The total sum of all of the advance values from rendering all of the characters
         * within this element, including the advance value on the glyphs, the effect of
         * properties such as 'char-spacing', 'word-spacing' and positioning adjustments
         * on 'TJ' elements.
         */
        getTextLength(): Promise<number>;
        /**
         * @returns A promise that resolves to the number used to adjust text matrix in horizontal direction when drawing
         * text. The number is expressed in thousandths of a unit of text space. The returned
         * number corresponds to a number value within TJ array. For 'Tj' text strings the
         * returned value is always 0.
         */
        getPosAdjustment(): Promise<number>;
        /**
         * returns the offset (out_x, out_y) to the start of the current line relative to
         * the beginning of the previous line.
         *
         * out_x and out_y are numbers expressed in unscaled text space units.
         * The returned numbers correspond to the arguments of 'Td' operator.
         * @returns A promise that resolves to an object of type: "Object"
         */
        getNewTextLineOffset(): Promise<object>;
        /**
         * @returns A promise that resolves to true if this element is directly associated with a text matrix
         * (that is Tm operator is just before this text element) or false if the text
         * matrix is default or is inherited from previous text elements.
         */
        hasTextMatrix(): Promise<boolean>;
        /**
         * set the text data for the current e_text Element.
         * @param buf_text_data - a pointer to a buffer containing text.
         */
        setTextData(buf_text_data: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<void>;
        /**
         * Sets the text matrix for a text element.
         * @param mtx - The new text matrix for this text element
         */
        setTextMatrix(mtx: PDFNet.Matrix2D): Promise<void>;
        /**
         * Sets the text matrix for a text element. This method accepts text
         * transformation matrix components directly.
         *
         * A transformation matrix in PDF is specified by six numbers, usually
         * in the form of an array containing six elements. In its most general
         * form, this array is denoted [a b c d h v]; it can represent any linear
         * transformation from one coordinate system to another. For more
         * information about PDF matrices please refer to section 4.2.2 'Common
         * Transformations' in PDF Reference Manual, and to documentation for
         * Matrix2D class.
         * @param a - horizontal 'scaling' component of the new text matrix.
         * @param b - 'rotation' component of the new text matrix.
         * @param c - 'rotation' component of the new text matrix.
         * @param d - vertical 'scaling' component of the new text matrix.
         * @param h - horizontal translation component of the new text matrix.
         * @param v - vertical translation component of the new text matrix.
         */
        setTextMatrixEntries(a: number, b: number, c: number, d: number, h: number, v: number): Promise<void>;
        /**
         * @param adjust - number to set the horizontal adjustment to
         */
        setPosAdjustment(adjust: number): Promise<void>;
        /**
         * Recompute the character positioning information (i.e. CharIterator-s) and
         * text length.
         *
         * Element objects caches text length and character positioning information.
         * If the user modifies the text data or graphics state the cached information
         * is not correct. UpdateTextMetrics() can be used to recalculate the correct
         * positioning and length information.
         */
        updateTextMetrics(): Promise<void>;
        /**
         * sets the offset (dx, dy) to the start of the current line relative to the beginning
         * of the previous line.
         * @param dx - horizontal offset to the start of the curret line
         * @param dy - vertical offset to the start of the current line
         */
        setNewTextLineOffset(dx: number, dy: number): Promise<void>;
        /**
         * @returns A promise that resolves to the SDF object of the Shading object.
         */
        getShading(): Promise<PDFNet.Shading>;
        /**
         * @returns A promise that resolves to a dictionary containing the property list or NULL if property
         * dictionary is not present.
         */
        getMCPropertyDict(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to a tag is a name object indicating the role or significance of
         * the marked content point/sequence.
         */
        getMCTag(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to a pointer to the internal text buffer for this text element.
         */
        getTextData(): Promise<number>;
        /**
         * Obtains the bounding box for a graphical element.
        
        Calculates the bounding box for a graphical element (i.e. an Element that belongs
        to one of following types: e_path, e_text, e_image, e_inline_image, e_shading e_form).
        The returned bounding box is guaranteed to encompass the Element, but is not guaranteed
        to be the smallest box that could contain the element. For example, for Bezier curves
        the bounding box will enclose all control points, not just the curve itself.
         * @returns A promise that resolves to a rectangle specifying the bounding box of Element (a rectangle that surrounds the
        entire element). The coordinates are represented in the default PDF page coordinate system
        and are using units called points ( 1 point = 1/72 inch =  2.54 /72 centimeter). The
        bounding box already accounts for the effects of current transformation matrix (CTM),
        text matrix, font size, and other properties in the graphics state. If this is a non-graphical
        element, the bounding box is undefined.
         */
        getBBox(): Promise<Rect>;
    }
    /**
     * ElementBuilder is used to build new PDF::Elements (e.g. image, text, path, etc)
     * from scratch. In conjunction with ElementWriter, ElementBuilder can be used to create
     * new page content.
     */
    class ElementBuilder extends PDFNet.Destroyable {
        /**
         * Constructor for an ElementBuilder object that can be used to build new PDF::Elements
         * (eg. image, text, path, etc.) from scratch. In conjunction with ElementWriter,
         * ElementBuilder can be used to create new page content.
         * @returns A promise that resolves to Returns an ElementBuilder object
         */
        static create(): Promise<PDFNet.ElementBuilder>;
        /**
         * The function sets the graphics state of this Element to the given value.
         * If 'gs' parameter is not specified or is NULL the function resets the
         * graphics state of this Element to the default graphics state (i.e. the
         * graphics state at the beginning of the display list).
         *
         * The function can be used in situations where the same ElementBuilder is used
         * to create content on several pages, XObjects, etc. If the graphics state is not
         * Reset() when moving to a new display list, the new Element will have the same
         * graphics state as the last Element in the previous display list (and this may
         * or may not be your intent).
         *
         * Another use of Reset(gs) is to make sure that two Elements have the graphics
         * state.
         * @param [gs] - GState (graphics state) object. If NULL or unspecified, resets graphics state to default.
         */
        reset(gs?: PDFNet.GState): Promise<void>;
        /**
         * Create a content image Element out of a given document Image.
         * @param img - the given image.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createImage(img: PDFNet.Image): Promise<PDFNet.Element>;
        /**
         * Create a content image Element out of a given document Image.
         * @param img - the given image.
         * @param mtx - the image transformation matrix.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createImageFromMatrix(img: PDFNet.Image, mtx: PDFNet.Matrix2D): Promise<PDFNet.Element>;
        /**
         * Create a content image Element out of a given document Image with
         * the lower left corner at (x, y), and scale factors (hscale, vscale).
         * @param img - the given image.
         * @param x - The horizontal x position to place the lower left corner of the image
         * @param y - The vertical x position to place the lower left corner of the image
         * @param hscale - The horizontal scale of the image
         * @param vscale - The vertical scale of the image
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createImageScaled(img: PDFNet.Image, x: number, y: number, hscale: number, vscale: number): Promise<PDFNet.Element>;
        /**
         * Create e_group_begin Element (i.e. 'q' operator in PDF content stream).
         * The function saves the current graphics state.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createGroupBegin(): Promise<PDFNet.Element>;
        /**
         * Create e_group_end Element (i.e. 'Q' operator in PDF content stream).
         * The function restores the previous graphics state.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createGroupEnd(): Promise<PDFNet.Element>;
        /**
         * @param sh - A Shading object. Shading objects represent a flat interface around
         * all PDF shading types (e_function_shading, e_axial_shading, etc.)
         * Create a shading Element.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createShading(sh: PDFNet.Shading): Promise<PDFNet.Element>;
        /**
         * Create a Form XObject Element.
         * @param form - a Form XObject content stream
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createFormFromStream(form: PDFNet.Obj): Promise<PDFNet.Element>;
        /**
         * Create a Form XObject Element using the content of the existing page.
         * This method assumes that the XObject will be used in the same
         * document as the given page. If you need to create the Form XObject
         * in a different document use CreateForm(Page, Doc) method.
         * @param page - A page used to create the Form XObject.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createFormFromPage(page: PDFNet.Page): Promise<PDFNet.Element>;
        /**
         * Create a Form XObject Element using the content of the existing page.
         * Unlike CreateForm(Page) method, you can use this method to create form
         * in another document.
         * @param page - A page used to create the Form XObject.
         * @param doc - Destination document for the Form XObject.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createFormFromDoc(page: PDFNet.Page, doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<PDFNet.Element>;
        /**
         * Start a text block ('BT' operator in PDF content stream).
         * The function installs the given font in the current graphics state.
         * @param font - font to set the text in the text block to
         * @param font_sz - size to set the text in the text block to
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createTextBeginWithFont(font: PDFNet.Font, font_sz: number): Promise<PDFNet.Element>;
        /**
         * Start a text block ('BT' operator in PDF content stream).
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createTextBegin(): Promise<PDFNet.Element>;
        /**
         * Ends a text block.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createTextEnd(): Promise<PDFNet.Element>;
        /**
         * Create a text run using the given font.
         * @param text_data - text to initialize the text run with
         * @param font - font of the text in the text run
         * @param font_sz - size of the text in the text run
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createTextRun(text_data: string, font: PDFNet.Font, font_sz: number): Promise<PDFNet.Element>;
        /**
         * Create a text run using the given font.
         * @param text_data - text to initialize the text run with
         * @param text_data_sz - size of the text run
         * @param font - font of the text in the text run
         * @param font_sz - size of the text in the text run
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createTextRunWithSize(text_data: string, text_data_sz: number, font: PDFNet.Font, font_sz: number): Promise<PDFNet.Element>;
        /**
         * Create a text run using the given font.
         * @param text_data - text to initialize the text run with. Uses unsigned characters.
         * @param font - font of the text in the text run
         * @param font_sz - size of the text in the text run
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createTextRunUnsigned(text_data: string, font: PDFNet.Font, font_sz: number): Promise<PDFNet.Element>;
        /**
         * Create a new text run.
         * @param text_data - text to initialize the text run with.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createNewTextRun(text_data: string): Promise<PDFNet.Element>;
        /**
         * Create a new text run.
         * @param text_data - text to initialize the text run with.
         * @param text_data_sz - size pf the text run.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createNewTextRunWithSize(text_data: string, text_data_sz: number): Promise<PDFNet.Element>;
        /**
         * Create a new text run.
         * @param text_data - text to initialize the text run with. Uses unsigned characters.
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createNewTextRunUnsigned(text_data: string): Promise<PDFNet.Element>;
        /**
         * Create a new text run from shaped text.
         * Shaped Text can be created with an approriate Font, using the Font::GetShapedText() method.
         * @param text_data - the shaped text data
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createShapedTextRun(text_data: PDFNet.ShapedText): Promise<PDFNet.Element>;
        /**
         * Create e_text_new_line Element (i.e. a Td operator in PDF content stream).
         * Move to the start of the next line, offset from the start of the current
         * line by (dx , dy). dx and dy are numbers expressed in unscaled text space
         * units.
         * @param dx - The horizontal x offset from the start of the current line
         * @param dy - The vertical y offset from the start of the current line
         * @returns A promise that resolves to the path Element
         */
        createTextNewLineWithOffset(dx: number, dy: number): Promise<PDFNet.Element>;
        /**
         * Create e_text_new_line Element (i.e. a T* operator in PDF content stream).
         * @returns A promise that resolves to the path Element
         */
        createTextNewLine(): Promise<PDFNet.Element>;
        /**
         * Create a path Element using the given path segment data
         * @param buf_points - A buffer/array containing data on the points in the path.
         * What each point represents is determined by the corresponding segment type.
         * @param buf_seg_types - A buffer/array containing data on the segment types.
         * Possible segment types are as follows:
         * <pre>
         * PDFNet.Element.PathSegmentType = {
         * 	e_moveto : 1,
         * 	e_lineto : 2,
         * 	e_cubicto : 3,
         * 	e_conicto : 4,
         * 	e_rect : 5,
         * 	e_closepath : 6
         * }
         * </pre>
         * @returns A promise that resolves to the path Element
         */
        createPath(buf_points: any[], buf_seg_types: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.Element>;
        /**
         * Create a rectangle path Element.
         * @param x - The horizontal coordinate of the lower left corner of the rectangle.
         * @param y - The vertical coordinate of the lower left corner of the rectangle.
         * @param width - The width of the rectangle.
         * @param height - The height of the rectangle.
         * @returns A promise that resolves to the path Element
         */
        createRect(x: number, y: number, width: number, height: number): Promise<PDFNet.Element>;
        /**
         * Create an ellipse (or circle, if width == height) path Element.
         * @param x - The horizontal x coordinate of the ellipse center.
         * @param y - The vertical y coordinate of the ellipse center.
         * @param width - The width of the ellipse rectangle.
         * @param height - The height of the ellipse rectangle.
         * @returns A promise that resolves to the path Element
         */
        createEllipse(x: number, y: number, width: number, height: number): Promise<PDFNet.Element>;
        /**
         * Starts building a new path Element that can contain an arbitrary sequence
         * of lines, curves, and rectangles.
         */
        pathBegin(): Promise<void>;
        /**
         * Finishes building of the path Element.
         * @returns A promise that resolves to the path Element
         */
        pathEnd(): Promise<PDFNet.Element>;
        /**
         * Add a rectangle to the current path as a complete subpath.
         * Setting the current point is not required before using this function.
         * @param x - The x coordinate of the lower left corner of the rectangle.
         * @param y - The y coordinate of the lower left corner of the rectangle.
         * @param width - The width of the rectangle.
         * @param height - The height of the rectangle.
         */
        rect(x: number, y: number, width: number, height: number): Promise<void>;
        /**
         * Add an ellipse (or circle, if rx == ry) to the current path as a complete subpath.
         * Setting the current point is not required before using this function.
         * @param x - The x coordinate of the ellipse center.
         * @param y - The y coordinate of the ellipse center.
         * @param width - The x radii of the ellipse.
         * @param height - The y radii of the ellipse.
         */
        ellipse(x: number, y: number, width: number, height: number): Promise<void>;
        /**
         * Set the current point.
         * @param x - The horizontal x component of the point
         * @param y - The vertical y component of the point
         */
        moveTo(x: number, y: number): Promise<void>;
        /**
         * Draw a line from the current point to the given point.
         * @param x - The horizontal x component of the goal point
         * @param y - The vertical y component of the goal point
         */
        lineTo(x: number, y: number): Promise<void>;
        /**
         * Draw a Bezier curve from the current point to the given point (x2, y2) using
         * (cx1, cy1) and (cx2, cy2) as control points.
         * @param cx1 - The x component of the first control point
         * @param cy1 - The y component of the first control point
         * @param cx2 - The x component of the second control point
         * @param cy2 - The y component of the second control point
         * @param x2 - The horizontal x component of the goal point
         * @param y2 - The vertical y component of the goal point
         */
        curveTo(cx1: number, cy1: number, cx2: number, cy2: number, x2: number, y2: number): Promise<void>;
        /**
         * Draw an arc with the specified parameters (lower left corner, width, height and angles).
         * @param x - The horizontal x coordinate of the lower left corner of the ellipse encompassing rectangle
         * @param y - The horizontal y coordinate of the lower left corner of the ellipse encompassing rectangle
         * @param width - overall width of the full ellipse (not considering the angular extents).
         * @param height - overall height of the full ellipse (not considering the angular extents).
         * @param start - starting angle of the arc in degrees
         * @param extent - angular extent of the arc in degrees
         */
        arcTo(x: number, y: number, width: number, height: number, start: number, extent: number): Promise<void>;
        /**
         * Draw an arc from the current point to the end point.
         * @param xr - x radius for the arc
         * @param yr - y radius for the arc
         * @param rx - x-axis rotation in radians
         * @param isLargeArc - indicates if smaller or larger arc is chosen
         * 		1 one of the two larger arc sweeps is chosen
         * 		0 one of the two smaller arc sweeps is chosen
         * @param sweep - direction in which arc is drawn (1 clockwise, 0 counterclockwise)
         * @param endX - x coordinate of end point
         * @param endY - y coordinate of end point
         */
        arcTo2(xr: number, yr: number, rx: number, isLargeArc: boolean, sweep: boolean, endX: number, endY: number): Promise<void>;
        /**
         * Closes the current subpath.
         */
        closePath(): Promise<void>;
        /**
         * Create a new Unicode text run.
         * @param text_data - the Unicode text
         * @returns A promise that resolves to an object of type: "PDFNet.Element"
         */
        createUnicodeTextRun(text_data: string): Promise<PDFNet.Element>;
    }
    /**
     * ElementReader can be used to parse and process content streams. ElementReader provides a
     * convenient interface used to traverse the Element display list of a page. The display list
     * representing graphical elements (such as text-runs, paths, images, shadings, forms, etc) is
     * accessed using the intrinsic iterator. ElementReader automatically concatenates page contents
     * spanning multiple streams and provides a mechanism to parse contents of sub-display lists
     * (e.g. forms XObjects and Type3 fonts).
     *
     * For a full sample, please refer to ElementReader and ElementReaderAdvTest sample projects.
     */
    class ElementReader extends PDFNet.Destroyable {
        /**
         * Constructor for an ElementReader object that can be used to parse and process
         * content streams.
         * @returns A promise that resolves to Returns an ElementReader object
         */
        static create(): Promise<PDFNet.ElementReader>;
        /**
         * begin processing a page.
         * @param page - A page to start processing.
         * @param [ctx] - An optional parameter used to specify the Optional Content (OC)
         * Context that should be used when processing the page. When the OCG::Context is specified,
         * Element::IsOCVisible() will return 'true' or 'false' depending on the visibility of the
         * current Optional Content Group (OCG) and the states of flags in the given context.
         */
        beginOnPage(page: PDFNet.Page, ctx?: PDFNet.OCGContext): Promise<void>;
        /**
         * begin processing given content stream. The content stream may be
         * a Form XObject, Type3 glyph stream, pattern stream or any other content stream.
         * @param content_stream - A stream object representing the content stream (usually
         * a Form XObject).
         * @param [resource_dict] - An optional '/Resource' dictionary parameter.
         * If content stream refers to named resources that are not present in
         * the local Resource dictionary, the names are looked up in the supplied
         * resource dictionary.
         * @param [ctx] - An optional parameter used to specify the Optional Content (OC)
         * Context that should be used when processing the page. When the OCG::Context is specified,
         * Element::IsOCVisible() will return 'true' or 'false' depending on the visibility of the
         * current Optional Content Group (OCG) and the states of flags in the given context.
         */
        begin(content_stream: PDFNet.Obj, resource_dict?: PDFNet.Obj, ctx?: PDFNet.OCGContext): Promise<void>;
        /**
         * @param res - resource dictionary for finding images, fonts, etc.
         */
        appendResource(res: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to a page Element or a 'NULL' element if the end of current-display list was
         * reached. You may use GetType() to determine the type of the returned Element.
         */
        next(): Promise<PDFNet.Element>;
        /**
         * @returns A promise that resolves to the current Element or a 'NULL' Element. The current element is the one
         * returned in the last call to Next().
         */
        current(): Promise<PDFNet.Element>;
        /**
         * when the current element is a form XObject you have the option to skip form
         * processing (by not calling FormBegin()) or to open the form stream and
         * continue Element traversal into the form.
         *
         * To open a form XObject display list use FormBegin() method. The Next() returned
         * Element will be the first Element in the form XObject display list. Subsequent calls to Next()
         * will traverse form's display list until NULL is returned. At any point you can
         * close the form sub-list using ElementReader::End() method. After the form display
         * list is closed (using End()) the processing will return to the parent display list
         * at the point where it left off before entering the form XObject.
         */
        formBegin(): Promise<void>;
        /**
         * a method used to spawn the sub-display list representing the tiling pattern
         * of the current element in the ElementReader. You can call this method at any
         * point as long as the current element is valid.
         * @param fill_pattern - If true, the filling pattern of the current element will
         * be spawned; otherwise, the stroking pattern of the current element will be
         * spawned. Note that the graphics state will be inherited from the parent content
         * stream (the content stream in which the pattern is defined as a resource) automatically.
         * @param [reset_ctm_tfm] - An optional parameter used to indicate whether the pattern's
         * display list should set its initial CTM and transformation matrices to identity matrix.
         * In general, we should leave it to be false.
         *
         * To open a tiling pattern sub-display list use PatternBegin(pattern) method.
         * The Next() returned Element will be the first Element in the pattern display list.
         * Subsequent calls to Next() will traverse pattern's display list until NULL is
         * encountered. At any point you can close the pattern sub-list using
         * ElementReader::End() method. After the pattern display list is closed,
         * the processing will return to the parent display list at the point where
         * pattern display list was spawned.
         */
        patternBegin(fill_pattern: boolean, reset_ctm_tfm?: boolean): Promise<void>;
        /**
         * Close the current display list.
         *
         * If the current display list is a sub-list created using FormBegin(), PatternBegin(),
         * or Type3FontBegin() methods, the function will end the sub-list and will return
         * processing to the parent display list at the point where it left off before
         * entering the sub-list.
         * @returns A promise that resolves to true if the closed display list is a sub-list or false if it is a root
         * display list.
         */
        end(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an iterator to the beginning of the list containing identifiers of modified
         * graphics state attributes since the last call to ClearChangeList(). The list can
         * be consulted to determine which graphics states were modified between two
         * Elements. Attributes are ordered in the same way as they are set in the content
         * stream. Duplicate attributes are eliminated.
         */
        getChangesIterator(): Promise<PDFNet.Iterator<number>>;
        /**
         * @param attrib - <pre>
         * PDFNet.GState.Attribute = {
         * 	e_transform : 0
         * 	e_rendering_intent : 1
         * 	e_stroke_cs : 2
         * 	e_stroke_color : 3
         * 	e_fill_cs : 4
         * 	e_fill_color : 5
         * 	e_line_width : 6
         * 	e_line_cap : 7
         * 	e_line_join : 8
         * 	e_flatness : 9
         * 	e_miter_limit : 10
         * 	e_dash_pattern : 11
         * 	e_char_spacing : 12
         * 	e_word_spacing : 13
         * 	e_horizontal_scale : 14
         * 	e_leading : 15
         * 	e_font : 16
         * 	e_font_size : 17
         * 	e_text_render_mode : 18
         * 	e_text_rise : 19
         * 	e_text_knockout : 20
         * 	e_text_pos_offset : 21
         * 	e_blend_mode : 22
         * 	e_opacity_fill : 23
         * 	e_opacity_stroke : 24
         * 	e_alpha_is_shape : 25
         * 	e_soft_mask : 26
         * 	e_smoothnes : 27
         * 	e_auto_stoke_adjust : 28
         * 	e_stroke_overprint : 29
         * 	e_fill_overprint : 30
         * 	e_overprint_mode : 31
         * 	e_transfer_funct : 32
         * 	e_BG_funct : 33
         * 	e_UCR_funct : 34
         * 	e_halftone : 35
         * 	e_null : 36
         * }
         * </pre>
         * the GState attribute to test if it has been changed
         * ClearChangeList().
         * @returns A promise that resolves to true if given GState attribute was changed since the last call to
         */
        isChanged(attrib: number): Promise<boolean>;
        /**
         * Clear the list containing identifiers of modified graphics state attributes.
         * The list of modified attributes is then accumulated during a subsequent call(s)
         * to ElementReader.Next().
         */
        clearChangeList(): Promise<void>;
        /**
         * @param name - string of the name of the SDF/Cos object to get
         * @returns A promise that resolves to sDF/Cos object matching the specified name in the current resource
         * dictionary. For 'Page' the name is looked up in the page's /Resources/<Class>
         * dictionary. For Form XObjects, Patterns, and Type3 fonts that have a content
         * stream within page content stream the specified resource is first looked-up in the
         * resource dictionary of the inner stream. If the resource is not found, the name is
         * looked up in the outer content stream's resource dictionary. The function returns
         * NULL if the resource was not found.
         */
        getFont(name: string): Promise<PDFNet.Obj>;
        /**
         * @param name - string of the name of the SDF/Cos object to get
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getXObject(name: string): Promise<PDFNet.Obj>;
        /**
         * @param name - string of the name of the SDF/Cos object to get
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getShading(name: string): Promise<PDFNet.Obj>;
        /**
         * @param name - string of the name of the SDF/Cos object to get
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getColorSpace(name: string): Promise<PDFNet.Obj>;
        /**
         * @param name - string of the name of the SDF/Cos object to get
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getPattern(name: string): Promise<PDFNet.Obj>;
        /**
         * @param name - string of the name of the SDF/Cos object to get
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getExtGState(name: string): Promise<PDFNet.Obj>;
    }
    /**
     * ElementWriter can be used to assemble and write new content to a page, Form XObject,
     * Type3 Glyph stream, pattern stream, or any other content stream.
     */
    class ElementWriter extends PDFNet.Destroyable {
        /**
         * Constructor for an ElementWriter object that can be used to assemble and write
         * new content to a page, Form XObject, pattern stream or any other content stream.
         * @returns A promise that resolves to Returns an ElementWriter object
         */
        static create(): Promise<PDFNet.ElementWriter>;
        /**
         * begin writing to the given page.
         *
         * By default, new content will be appended to the page, as foreground graphics.
         * It is possible to add new page content as background graphics by setting the
         * second parameter in begin method to 'true' (e.g. writer.Begin(page, true)).
         * @param page - The page to write content.
         * @param [placement] - <pre>
         * PDFNet.ElementWriter.WriteMode = {
         * 	e_underlay : 0
         * 	e_overlay : 1
         * 	e_replacement : 2
         * }
         * </pre>
         * An optional flag indicating whether the new content should
         * be added as a foreground or background layer to the existing page. By default, the new
         * content will appear on top of the existing graphics.
         * @param [page_coord_sys] - An optional flag used to select the target coordinate system
         * if true (default), the coordinates are relative to the lower-left corner of the page,
         * otherwise the coordinates are defined in PDF user coordinate system (which may,
         * or may not coincide with the page coordinates).
         * @param [compress] - An optional flag indicating whether the page content stream
         * should be compressed. This may be useful for debugging content streams. Also
         * some applications need to do a clear text search on strings in the PDF files.
         * By default, all content streams are compressed.
         * @param [resources] - the resource dictionary in which to store resources for the final page.
         * By default, a new resource dictionary will be created.
         */
        beginOnPage(page: PDFNet.Page, placement?: number, page_coord_sys?: boolean, compress?: boolean, resources?: PDFNet.Obj): Promise<void>;
        /**
         * begin writing an Element sequence to a new stream. Use this function to write
         * Elements to a content stream other than the page. For example, you can create
         * Form XObjects (See Section '4.9 Form XObjects' in PDF Reference for more details)
         * pattern streams, Type3 font glyph streams, etc.
         * @param doc - A low-level SDF/Cos document that will contain the new stream. You can
         * access low-level document using PDFDoc::GetSDFDoc() or Obj::GetDoc() methods.
         * @param [compress] - An optional flag indicating whether the page content stream
         * should be compressed. This may be useful for debugging content streams. Also
         * some applications need to do a clear text search on strings in the PDF files.
         * By default, all content streams are compressed.
         */
        begin(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, compress?: boolean): Promise<void>;
        /**
         * begin writing an Element sequence to a stream. Use this function to write
         * Elements to a content stream which will replace an existing content stream in an
         * object passed as a parameter.
         * @param stream_obj_to_update - A low-level SDF stream object that will contain the new stream.
         * Old stream inside that object will be discarded.
         * @param [compress] - An optional flag indicating whether the content stream
         * should be compressed. This may be useful for debugging content streams. Also
         * some applications need to do a clear text search on strings in the PDF files.
         * By default, all content streams are compressed.
         * @param [resources] - the resource dictionary in which to store resources for the final page.
         * By default, a new resource dictionary will be created.
         */
        beginOnObj(stream_obj_to_update: PDFNet.Obj, compress?: boolean, resources?: PDFNet.Obj): Promise<void>;
        /**
         * Finish writing to a page
         * @returns A promise that resolves to a low-level stream object that was used to store Elements.
         */
        end(): Promise<PDFNet.Obj>;
        /**
         * Writes the Element to the content stream.
         * @param element - The element to write to the content stream.
         */
        writeElement(element: PDFNet.Element): Promise<void>;
        /**
         * A utility function that surrounds the given Element with a graphics state
         * Save/Restore Element (i.e. in PDF content stream represented as 'q element Q').
         *
         * The function is equivalent to calling WriteElement three times:
         * 	 WriteElement(eSave);
         * 	 WriteElement(element);
         * 	 WriteElement(eRestore);
         *
         * where eSave is 'e_group_begin' and eRestore is 'e_group_end' Element
         *
         * The function is useful when XObjects such as Images and Forms are drawn on
         * the page.
         * @param element - Element object to enact function on.
         */
        writePlacedElement(element: PDFNet.Element): Promise<void>;
        /**
         * the Flush method flushes all pending Element writing operations.
         * This method is typically only required to be called when intermixing
         * direct content writing (i.e. WriteBuffer/WriteString) with Element writing.
         */
        flush(): Promise<void>;
        writeBuffer(data: string, data_sz: number): Promise<void>;
        /**
         * Writes an arbitrary string to the content stream.
         * Serves the same purpose as WriteBuffer().
         * @param str - String to write to the content stream.
         */
        writeString(str: string): Promise<void>;
        setDefaultGState(r: PDFNet.ElementReader): Promise<void>;
        /**
         * Write only the graphics state changes applied to this element and skip writing the element itself.
         * This is especially useful when rewriting page content, but with the intention to skip certain elements.
         * @param element - The element for which to write graphics state changes.
         */
        writeGStateChanges(element: PDFNet.Element): Promise<void>;
    }
    /**
     * This class represents the result of verifying a secure embedded
     * timestamp digital signature.
     */
    class EmbeddedTimestampVerificationResult extends PDFNet.Destroyable {
        /**
         * Retrieves the main verification status. The main status is determined based on the other statuses.
         * @returns A promise that resolves to a boolean representing whether or not the verification operation was completely successful.
         */
        getVerificationStatus(): Promise<boolean>;
        /**
         * Retrieves the result condition associated with the CMS signed digest verification step.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.DigestStatus = {
         * 	e_digest_invalid : 0
         * 	e_digest_verified : 1
         * 	e_digest_verification_disabled : 2
         * 	e_weak_digest_algorithm_but_digest_verifiable : 3
         * 	e_no_digest_status : 4
         * 	e_unsupported_encoding : 5
         * }
         * </pre>
         * @returns A promise that resolves to a DigestStatus-type enumeration value.
         */
        getCMSDigestStatus(): Promise<number>;
        /**
         * Retrieves the result condition associated with the message imprint digest verification step.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.DigestStatus = {
         * 	e_digest_invalid : 0
         * 	e_digest_verified : 1
         * 	e_digest_verification_disabled : 2
         * 	e_weak_digest_algorithm_but_digest_verifiable : 3
         * 	e_no_digest_status : 4
         * 	e_unsupported_encoding : 5
         * }
         * </pre>
         * @returns A promise that resolves to a DigestStatus-type enumeration value.
         */
        getMessageImprintDigestStatus(): Promise<number>;
        /**
         * Retrieves the result condition associated with the trust verification step.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.TrustStatus = {
         * 	e_trust_verified : 0
         * 	e_untrusted : 1
         * 	e_trust_verification_disabled : 2
         * 	e_no_trust_status : 3
         * }
         * </pre>
         * @returns A promise that resolves to a TrustStatus-type enumeration value.
         */
        getTrustStatus(): Promise<number>;
        /**
         * Retrieves the result condition associated with the CMS signed digest verification step, as a descriptive string.
         * @returns A promise that resolves to a string
         */
        getCMSDigestStatusAsString(): Promise<string>;
        /**
         * Retrieves the result condition associated with the message imprint digest verification step, as a descriptive string.
         * @returns A promise that resolves to a string
         */
        getMessageImprintDigestStatusAsString(): Promise<string>;
        /**
         * Retrieves the result condition associated with the trust verification step, as a descriptive string.
         * @returns A promise that resolves to a string
         */
        getTrustStatusAsString(): Promise<string>;
        /**
         * Returns whether there is a detailed TrustVerificationResult in this EmbeddedTimestampVerificationResult.
         * @returns A promise that resolves to a boolean
         */
        hasTrustVerificationResult(): Promise<boolean>;
        /**
         * Retrieves the detailed result associated with the trust step of the verification operation that returned this EmbeddedTimestampVerificationResult,
         * if such a detailed trust result is available. Must call HasTrustVerificationResult first and check for a true result.
         * @returns A promise that resolves to a TrustVerificationResult object.
         */
        getTrustVerificationResult(): Promise<PDFNet.TrustVerificationResult>;
        /**
         * Retrieves an enumeration value representing the digest algorithm used to sign the timestamp token.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DigestAlgorithm.Type = {
         * 	e_SHA1 : 0
         * 	e_SHA256 : 1
         * 	e_SHA384 : 2
         * 	e_SHA512 : 3
         * 	e_RIPEMD160 : 4
         * 	e_unknown_digest_algorithm : 5
         * }
         * </pre>
         * @returns A promise that resolves to a DigestAlgorithm enumeration value.
         */
        getCMSSignatureDigestAlgorithm(): Promise<number>;
        /**
         * Retrieves an enumeration value representing the digest algorithm used inside the message imprint field of the timestamp to digest the main signature value.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DigestAlgorithm.Type = {
         * 	e_SHA1 : 0
         * 	e_SHA256 : 1
         * 	e_SHA384 : 2
         * 	e_SHA512 : 3
         * 	e_RIPEMD160 : 4
         * 	e_unknown_digest_algorithm : 5
         * }
         * </pre>
         * @returns A promise that resolves to a DigestAlgorithm enumeration value.
         */
        getMessageImprintDigestAlgorithm(): Promise<number>;
        /**
         * Retrieves reports about unsupported features encountered during verification of the timestamp.
         * Current possible values:
         * 	"GeneralizedTime format with length <number greater than 15>",
         * 	"unsupported digest algorithm"
         * @returns A promise that resolves to a container of strings representing unsupported features encountered during verification of the timestamp
         */
        getUnsupportedFeatures(): Promise<string[]>;
    }
    /**
     * FDFDoc is a class representing Forms Data Format (FDF) documents.
     * FDF is typically used when submitting form data to a server, receiving
     * the response, and incorporating it into the interactive form. It can also
     * be used to export form data to stand-alone files that can be stored, transmitted
     * electronically, and imported back into the corresponding PDF interactive form.
     * In addition, beginning in PDF 1.3, FDF can be used to define a container for
     * annotations that are separate from the PDF document to which they apply.
     */
    class FDFDoc extends PDFNet.Destroyable {
        /**
         * Default constructor that creates an empty new document.
         * @returns A promise that resolves to an object of type: "PDFNet.FDFDoc"
         */
        static create(): Promise<PDFNet.FDFDoc>;
        /**
         * Open an existing FDF document
         * @param filepath - pathname to the file.
         * @returns A promise that resolves to an object of type: "PDFNet.FDFDoc"
         */
        static createFromFilePath(filepath: string): Promise<PDFNet.FDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.FDFDoc"
         */
        static createFromUFilePath(filepath: string): Promise<PDFNet.FDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.FDFDoc"
         */
        static createFromStream(stream: PDFNet.Filter): Promise<PDFNet.FDFDoc>;
        /**
         * Open a SDF/Cos document from a memory buffer.
         * @param buf - a memory buffer containing the serialized document
         * @returns A promise that resolves to an object of type: "PDFNet.FDFDoc"
         */
        static createFromMemoryBuffer(buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.FDFDoc>;
        /**
         * @returns A promise that resolves to true if document was modified, false otherwise
         */
        isModified(): Promise<boolean>;
        /**
         * Saves the document to a file.
         *
         * If a full save is requested to the original path, the file is saved to a file
         * system-determined temporary file, the old file is deleted, and the temporary file
         * is renamed to path.
         *
         * A full save with remove unused or linearization option may re-arrange object in
         * the cross reference table. Therefore all pointers and references to document objects
         * and resources should be re acquired in order to continue document editing.
         *
         * In order to use incremental save the specified path must match original path and
         * e_incremental flag bit should be set.
         * @param path - The full path name to which the file is saved.
         */
        save(path: string): Promise<void>;
        /**
         * Saves the document to a memory buffer.
         * @returns A promise that resolves to an object of type: "Uint8Array"
         */
        saveMemoryBuffer(): Promise<Uint8Array>;
        /**
         * @returns A promise that resolves to A dictionary representing the Cos root of the document (document's trailer)
         */
        getTrailer(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to A dictionary representing the Cos root of the document (/Root entry
         * within the trailer dictionary)
         */
        getRoot(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the FDF dictionary located in "/Root" or NULL if dictionary is not present.
         */
        getFDF(): Promise<PDFNet.Obj>;
        /**
         * Get the PDF document file that this FDF file was exported from or is intended
         * to be imported into.
         * @returns A promise that resolves to a String with the PDF document file name.
         */
        getPDFFileName(): Promise<string>;
        /**
         * Set the PDF document file that this FDF file was exported from or is intended
         * to be imported into.
         * @param filepath - pathname to the file.
         */
        setPDFFileName(filepath: string): Promise<void>;
        /**
         * Get the ID entry from "/Root/FDF" dictionary.
         * @returns A promise that resolves to An object representing the ID entry in "/Root/FDF" dictionary.
         */
        getID(): Promise<PDFNet.Obj>;
        /**
         * Set the ID entry in "/Root/FDF" dictionary.
         * @param id - ID array object.
         */
        setID(id: PDFNet.Obj): Promise<void>;
        /**
         * An interactive form (sometimes referred to as an AcroForm) is a
         * collection of fields for gathering information interactively from
         * the user. A FDF document may contain any number of fields appearing
         * on any combination of pages, all of which make up a single, global
         * interactive form spanning the entire document.
         *
         * The following methods are used to access and manipulate Interactive form
         * fields (sometimes referred to as AcroForms).
         * @returns A promise that resolves to an iterator to the first FDFField in the document.
         */
        getFieldIteratorBegin(): Promise<PDFNet.Iterator<PDFNet.FDFField>>;
        /**
         * An interactive form (sometimes referred to as an AcroForm) is a
         * collection of fields for gathering information interactively from
         * the user. A FDF document may contain any number of fields appearing
         * on any combination of pages, all of which make up a single, global
         * interactive form spanning the entire document.
         *
         * The following methods are used to access and manipulate Interactive form
         * fields (sometimes referred to as AcroForms).
         * @param field_name - String representing the name of the FDFField to get.
         * @returns A promise that resolves to an iterator to the FDFField in the document.
         */
        getFieldIterator(field_name: string): Promise<PDFNet.Iterator<PDFNet.FDFField>>;
        /**
         * @param field_name - a string representing the fully qualified name of
         * the field (e.g. "employee.name.first").
         * @returns A promise that resolves to a FDFField associated with the given field_name or invalid
         * field (null) if the field is not found.
         */
        getField(field_name: string): Promise<PDFNet.FDFField>;
        /**
         * @param type - <pre>
         * PDFNet.Field.Type = {
         * 	e_button : 0
         * 	e_check : 1
         * 	e_radio : 2
         * 	e_text : 3
         * 	e_choice : 4
         * 	e_signature : 5
         * 	e_null : 6
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.FDFField"
         */
        fieldCreate(field_name: string, type: number, field_value?: PDFNet.Obj): Promise<PDFNet.FDFField>;
        /**
         * @param type - <pre>
         * PDFNet.Field.Type = {
         * 	e_button : 0
         * 	e_check : 1
         * 	e_radio : 2
         * 	e_text : 3
         * 	e_choice : 4
         * 	e_signature : 5
         * 	e_null : 6
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.FDFField"
         */
        fieldCreateFromString(field_name: string, type: number, field_value: string): Promise<PDFNet.FDFField>;
        /**
         * @returns A promise that resolves to document's SDF/Cos document
         */
        getSDFDoc(): Promise<PDFNet.SDFDoc>;
        /**
         * Create a new FDFDoc from XFDF input. Input can be either a XFDF file path, or the XFDF data itself.
         * @param file_name - string containing either the file path to a XFDF file, or the XML buffer containing the XFDF.
         * @returns A promise that resolves to a new FDFDoc.
         */
        static createFromXFDF(file_name: string): Promise<PDFNet.FDFDoc>;
        saveAsXFDF(file_name: string): Promise<void>;
        /**
         * Export FDF file as a XFDF string
         * @returns A promise that resolves to a UString containing the XFDF representation of the FDF file
         */
        saveAsXFDFAsString(): Promise<string>;
        /**
         * Merge the annotations from XFDF file into FDF file
         * @param command_file - string containing the xml command file path or xml string of the command
         * @param [permitted_user] - optional user name of the permitted user
         */
        mergeAnnots(command_file: string, permitted_user?: string): Promise<void>;
    }
    /**
     * FDFField is an interactive form field in an FDF document
     */
    class FDFField {
        constructor(mp_leaf_node?: PDFNet.Obj, mp_root_array?: PDFNet.Obj);
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.FDFField"
         */
        static create(field_dict?: PDFNet.Obj, fdf_dict?: PDFNet.Obj): Promise<PDFNet.FDFField>;
        /**
         * @returns A promise that resolves to the value of the Field (the value of its /V key) or NULL if the
         * value is not specified.
         * The format of field's value varies depending on the field type.
         */
        getValue(): Promise<PDFNet.Obj>;
        /**
         * sets the value of the FDFField (the value of the field's /V key).
         * @param value - the value to set the FDFField to
         */
        setValue(value: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to a string representing the fully qualified name of the field
         * (e.g. "employee.name.first").
         */
        getName(): Promise<string>;
        /**
         * @returns A promise that resolves to a string representing the partial name of the field (e.g.
         * "first" when "employee.name.first" is fully qualified name).
         */
        getPartialName(): Promise<string>;
        /**
         * @returns A promise that resolves to the object to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * The function returns the specified attribute.
         * @param attrib - name of the attribute to find
         * @returns A promise that resolves to return the attribute value if the given attribute name
         * was found or a NULL object if the given attribute name was not found.
         */
        findAttribute(attrib: string): Promise<PDFNet.Obj>;
        mp_leaf_node: PDFNet.Obj;
        mp_root_array: PDFNet.Obj;
    }
    /**
     * An interactive form (sometimes referred to as an AcroForm) is a
     * collection of fields for gathering information interactively from
     * the user. A PDF document may contain any number of Fields appearing
     * on any combination of pages, all of which make up a single, global
     * interactive form spanning the entire document.
     *
     * PDFNet fully supports reading, writing, and editing PDF forms and
     * provides many utility methods so that work with forms is simple and
     * efficient. Using PDFNet forms API arbitrary subsets of form fields
     * can be imported or exported from the document, new forms can be
     * created from scratch, and the appearance of existing forms can be
     * modified.
     *
     * In PDFNet Fields are accessed through FieldIterator-s. The list of
     * all Fields present in the document can be traversed as follows:
     *
     * <pre>
     * FieldIterator itr = pdfdoc.GetFieldIterator();
     * for(; itr.HasNext(); itr.Next()) {
     *   Field field = itr.Current();
     *   Console.WriteLine("Field name: {0}", field.GetName());
     *  }
     * </pre>
     *
     * For a full sample, please refer to 'InteractiveForms' sample project.
     *
     * To search field by name use FieldFind method. For example:
     * <pre>
     * FieldIterator itr = pdfdoc.FieldFind("name");
     * if (itr.HasNext()) {
     *   Console.WriteLine("Field name: {0}", itr.Current().GetName());
     * }
     * else { ...field was not found... }
     * </pre>
     *
     * If a given field name was not found or if the end of the field list
     * was reached the iterator HasNext() will return false.
     *
     * If you have a valid iterator you can access the Field using Current() method. For example:
     * Field field = itr.Current();
     *
     * Using Flatten(...) method it is possible to merge field
     * appearances with the page content. Form 'flattening' refers to the
     * operation that changes active form fields into a static area that is
     * part of the PDF document, just like the other text and images in
     * the document. A completely flattened PDF form does not have any
     * widget annotations or interactive fields.
     */
    class Field {
        constructor(leaf_node?: PDFNet.Obj, builder?: PDFNet.ElementBuilder);
        /**
         * construct a PDF::Field from a SDF dictionary representing a terminal field node.
         * @param field_dict - the SDF dictionary to construct the field from.
         * @returns A promise that resolves to an object of type: "PDFNet.Field"
         */
        static create(field_dict: PDFNet.Obj): Promise<PDFNet.Field>;
        /**
         * @returns A promise that resolves to whether this is a valid (non-null) Field. If the
         * function returns false the underlying SDF/Cos object is null and
         * the Field object should be treated as null as well.
         */
        isValid(): Promise<boolean>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Field.Type = {
         * 	e_button : 0
         * 	e_check : 1
         * 	e_radio : 2
         * 	e_text : 3
         * 	e_choice : 4
         * 	e_signature : 5
         * 	e_null : 6
         * }
         * </pre>
         * @returns A promise that resolves to the field's value, whose type/format varies depending on the field type.
         * See the descriptions of individual field types for further information.
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the value of the Field (the value of its /V key) or NULL if the
         * value is not specified.
         *
         * The format of field's value varies depending on the field type.
         */
        getValue(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to a string of the value of the Field (the value of its /V key) or
         * NULL if the value is not specified.
         *
         * The format of field's value varies depending on the field type.
         */
        getValueAsString(): Promise<string>;
        /**
         * @returns A promise that resolves to a string of the default value to which the field reverts when a reset-form
         * action is executed or NULL if the default value is not specified.
         *
         * The format of field's value varies depending on the field type.
         */
        getDefaultValueAsString(): Promise<string>;
        /**
         * sets the value of the field (i.e. the value of the field's /V key).
         * The format of field's value varies depending on the field type.
         * @param value - the new field value.
         * @returns A promise that resolves to an object of type: "PDFNet.ViewChangeCollection"
         */
        setValueAsString(value: string): Promise<PDFNet.ViewChangeCollection>;
        /**
         * Sets the value of the field (i.e. the value of the field's /V key).
         * The format of field's value varies depending on the field type.
         * @param value - the new field value.
         * @returns A promise that resolves to an object of type: "PDFNet.ViewChangeCollection"
         */
        setValue(value: PDFNet.Obj): Promise<PDFNet.ViewChangeCollection>;
        /**
         * sets the value of a check-box or radio-button field.
         * @param value - If true, the filed will be set to 'True', if false the field will
         * be set to 'False'.
         * @returns A promise that resolves to an object of type: "PDFNet.ViewChangeCollection"
         */
        setValueAsBool(value: boolean): Promise<PDFNet.ViewChangeCollection>;
        /**
         * Get the Action associated with the selected Field Trigger event.
         * @param trigger - <pre>
         * PDFNet.Field.EventType = {
         * 	e_action_trigger_keystroke : 13
         * 	e_action_trigger_format : 14
         * 	e_action_trigger_validate : 15
         * 	e_action_trigger_calculate : 16
         * }
         * </pre>
         * the type of trigger event to get
         * @returns A promise that resolves to the Action Obj if present, otherwise NULL
         */
        getTriggerAction(trigger: number): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to field value as a boolean.
         */
        getValueAsBool(): Promise<boolean>;
        /**
         * regenerates the appearance stream for the Widget Annotation containing
         * variable text. Call this method if you modified field's value and would
         * like to update field's appearance.
         */
        refreshAppearance(): Promise<void>;
        /**
         * removes any appearances associated with the field.
         */
        eraseAppearance(): Promise<void>;
        /**
         * @returns A promise that resolves to the default value to which the field reverts when a reset-form action
         * is executed or NULL if the default value is not specified.
         *
         * The format of field's value varies depending on the field type.
         */
        getDefaultValue(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to a string representing the fully qualified name of the field
         * (e.g. "employee.name.first").
         */
        getName(): Promise<string>;
        /**
         * @returns A promise that resolves to a string representing the partial name of the field (e.g.
         * "first" when "employee.name.first" is fully qualified name).
         */
        getPartialName(): Promise<string>;
        /**
         * modifies the field name.
         * @param field_name - a string representing the fully qualified name of
         * the field (e.g. "employee.name.first").
         */
        rename(field_name: string): Promise<void>;
        /**
         * @returns A promise that resolves to true if this Field is a Widget Annotation
         *
         * Determines whether or not this Field is an Annotation.
         */
        isAnnot(): Promise<boolean>;
        /**
         * Sets the signature handler to use for adding a signature to this field. If the signature handler is not found
         * in PDFDoc's signature handlers list, this field will not be signed. To add signature handlers, use PDFDoc.AddSignatureHandler
         * method.
         *
         * If a signature handler is already assigned to this field and this method is called once again, the associate signature
         * handler for this field will be updated with the new handler.
         * @param signature_handler_id - The unique id of the SignatureHandler to use for adding signature in this field.
         * @returns A promise that resolves to the signature dictionary created using the SignatureHandler, or NULL pointer if the signature handler is not found.
         */
        useSignatureHandler(signature_handler_id: number): Promise<PDFNet.Obj>;
        /**
         * @param flag - <pre>
         * PDFNet.Field.Flag = {
         * 	e_read_only : 0
         * 	e_required : 1
         * 	e_no_export : 2
         * 	e_pushbutton_flag : 3
         * 	e_radio_flag : 4
         * 	e_toggle_to_off : 5
         * 	e_radios_in_unison : 6
         * 	e_multiline : 7
         * 	e_password : 8
         * 	e_file_select : 9
         * 	e_no_spellcheck : 10
         * 	e_no_scroll : 11
         * 	e_comb : 12
         * 	e_rich_text : 13
         * 	e_combo : 14
         * 	e_edit : 15
         * 	e_sort : 16
         * 	e_multiselect : 17
         * 	e_commit_on_sel_change : 18
         * }
         * </pre>
         * @returns A promise that resolves to the value of given field flag
         */
        getFlag(flag: number): Promise<boolean>;
        /**
         * @param flag - <pre>
         * PDFNet.Field.Flag = {
         * 	e_read_only : 0
         * 	e_required : 1
         * 	e_no_export : 2
         * 	e_pushbutton_flag : 3
         * 	e_radio_flag : 4
         * 	e_toggle_to_off : 5
         * 	e_radios_in_unison : 6
         * 	e_multiline : 7
         * 	e_password : 8
         * 	e_file_select : 9
         * 	e_no_spellcheck : 10
         * 	e_no_scroll : 11
         * 	e_comb : 12
         * 	e_rich_text : 13
         * 	e_combo : 14
         * 	e_edit : 15
         * 	e_sort : 16
         * 	e_multiselect : 17
         * 	e_commit_on_sel_change : 18
         * }
         * </pre>
         */
        setFlag(flag: number, value: boolean): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Field.TextJustification = {
         * 	e_left_justified : 0
         * 	e_centered : 1
         * 	e_right_justified : 2
         * }
         * </pre>
         * @returns A promise that resolves to the form of quadding (justification) to be used in displaying
         * the text fields.
         */
        getJustification(): Promise<number>;
        /**
         * sets the justification to be used in displaying the text field.
         * @param j - <pre>
         * PDFNet.Field.TextJustification = {
         * 	e_left_justified : 0
         * 	e_centered : 1
         * 	e_right_justified : 2
         * }
         * </pre>
         * enum representing justification to set the text field to, options are e_left_justified, e_centered and e_right_justified
         */
        setJustification(j: number): Promise<void>;
        /**
         * sets the maximum length of the field's text, in characters.
         * @param max_len - maximum length of a field's text.
         */
        setMaxLen(max_len: number): Promise<void>;
        /**
         * @returns A promise that resolves to the maximum length of the field's text, in characters, or a
         * negative number if the length is not limited.
         */
        getMaxLen(): Promise<number>;
        /**
         * @returns A promise that resolves to the default graphics state that should be used in formatting the
         * text. The state corresponds to /DA entry in the field dictionary.
         */
        getDefaultAppearance(): Promise<PDFNet.GState>;
        /**
         * @returns A promise that resolves to the rectangle that should be refreshed after changing a field.
         */
        getUpdateRect(): Promise<PDFNet.Rect>;
        /**
         * Flatten/Merge existing form field appearances with the page content and
         * remove widget annotation.
         *
         * Form 'flattening' refers to the operation that changes active form fields
         * into a static area that is part of the PDF document, just like the other
         * text and images in the document. A completely flattened PDF form does not
         * have any widget annotations or interactive fields.
         * @param page - page object to flatten
         */
        flatten(page: PDFNet.Page): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        findInheritedAttribute(attrib: string): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * Returns the total number of options in a list or combo box.
         * @returns A promise that resolves to an object of type: "number"
         */
        getOptCount(): Promise<number>;
        /**
         * @param index - index position of the option to retrieve.
         * @returns A promise that resolves to the string of the option at the givent index.
         */
        getOpt(index: number): Promise<string>;
        /**
         * Returns whether modifying this field would invalidate a digital signature in the document.
         * @returns A promise that resolves to whether modifying this field would invalidate a digital signature in the document
         */
        isLockedByDigitalSignature(): Promise<boolean>;
        leaf_node: PDFNet.Obj;
        builder: PDFNet.ElementBuilder;
    }
    /**
     * A file attachment annotation contains a reference to a file, which may be
     * embedded in the PDF document.
     */
    class FileAttachmentAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates an FileAttachment annotation and initializes it using given Cos/SDF object.
         * d Cos/SDF object used to initialize the FileAttachment annotation
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.FileAttachmentAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.FileAttachmentAnnot>;
        /**
         * The function saves the data referenced by this File Attachment to an
         * external file.
         *
         * If the file is embedded, the function saves the embedded file.
         * If the file is not embedded, the function will copy the external file.
         * If the file is not embedded and the external file can't be found, the function
         * returns false.
         * @param [save_as] - An optional parameter indicating the filepath and filename
         * where the data should be saved. If this parameter is not specified the function
         * will attempt to save the file using FileSpec.GetFilePath().
         * @returns A promise that resolves to true is the file was saved successfully, false otherwise.
         */
        export(save_as?: string): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Annot"
         */
        createFromAnnot(): Promise<PDFNet.Annot>;
        /**
         * Creates a file attachment annotation.
         *
         * A file attachment annotation contains a reference to a file, which typically
         * is embedded in the PDF file.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, in user space coordinates.
         * @param fs - a file specification object used to initialize the file attachment annotation.
         * @param [icon_name] - <pre>
         * PDFNet.FileAttachmentAnnot.Icon = {
         * 	e_Graph : 0
         * 	e_PushPin : 1
         * 	e_Paperclip : 2
         * 	e_Tag : 3
         * 	e_Unknown : 4
         * }
         * </pre>
         * The name of an icon to be used in displaying the annotation, default is PushPin.
         * @returns A promise that resolves to a new file attachment annotation.
         */
        static createWithFileSpec(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, fs: PDFNet.FileSpec, icon_name?: number): Promise<PDFNet.FileAttachmentAnnot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.FileAttachmentAnnot"
         */
        static createDefault(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, path: string): Promise<PDFNet.FileAttachmentAnnot>;
        /**
         * @returns A promise that resolves to the file specification that contains a file reference or the embedded file data stream.
         */
        getFileSpec(): Promise<PDFNet.FileSpec>;
        /**
         * sets the file specification.
         * @param file - The file specification to associate with this annotation..
         * The file specification contains a file reference or the embedded file data stream.
         */
        setFileSpec(file: PDFNet.FileSpec): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.FileAttachmentAnnot.Icon = {
         * 	e_Graph : 0
         * 	e_PushPin : 1
         * 	e_Paperclip : 2
         * 	e_Tag : 3
         * 	e_Unknown : 4
         * }
         * </pre>
         * @returns A promise that resolves to the type the associated icon style.
         */
        getIcon(): Promise<number>;
        /**
         * sets the icon style associated with FileAttachment annotation.
         * (Optional)
         * @param [type] - <pre>
         * PDFNet.FileAttachmentAnnot.Icon = {
         * 	e_Graph : 0
         * 	e_PushPin : 1
         * 	e_Paperclip : 2
         * 	e_Tag : 3
         * 	e_Unknown : 4
         * }
         * </pre>
         * icon style.
         */
        setIcon(type?: number): Promise<void>;
        /**
         * Returns the name of the icon associated with the FileAttachment annotation.
         * @returns A promise that resolves to a string denoting the name of the icon.
         */
        getIconName(): Promise<string>;
        /**
         * sets the name of the icon associated with the FileAttachment annotation.
         * (Optional)
         * @param iname - A string.denoting the name of the icon.
         */
        setIconName(iname: string): Promise<void>;
    }
    /**
     * FileSpec corresponds to the PDF file specification object.
     *
     * A PDF file can refer to the contents of another file by using a file specification,
     * which can take either of the following forms:
     *
     * - A simple file specification gives just the name of the target file in
     *   a standard format, independent of the naming conventions of any particular file system.
     *
     * - A full file specification includes information related to one or more specific file
     *   systems.
     *
     * - A URL reference.
     *
     * Although the file designated by a file specification is normally external to the
     * PDF file referring to it, it is also possible to embed the file allowing its contents
     * to be stored or transmitted along with the PDF file. However, embedding a file does not
     * change the presumption that it is external to (or separate from) the PDF file.
     *
     * For more details on file specifications, please refer to Section 3.10, 'File Specifications'
     * in the PDF Reference Manual.
     */
    class FileSpec {
        /**
         * Creates a file specification for the given file. By default, the specified
         * file is embedded in PDF.
         * @param doc - A document to which the FileSpec should be added. To obtain
         * SDFDoc from PDFDoc use PDFDoc::GetSDFDoc() or Obj::GetDoc().
         * @param path - The path to convert into a file specification.
         * @param [embed] - A flag indicating whether to embed specified in the PDF.
         * By default, all files are embedded.
         * @returns A promise that resolves to newly created FileSpec object.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, path: string, embed?: boolean): Promise<PDFNet.FileSpec>;
        /**
         * Creates a URL file specification.
         * @param doc - A document to which the FileSpec should be added. To obtain
         * SDFDoc from PDFDoc use PDFDoc::GetSDFDoc() or Obj::GetDoc().
         * @param url - A uniform resource locator (URL) of the form defined in
         * Internet RFC 1738, Uniform Resource Locators Specification.
         * @returns A promise that resolves to newly created FileSpec object.
         */
        static createURL(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, url: string): Promise<PDFNet.FileSpec>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.FileSpec"
         */
        static createFromObj(f: PDFNet.Obj): Promise<PDFNet.FileSpec>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.FileSpec"
         */
        copy(): Promise<PDFNet.FileSpec>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        compare(d: PDFNet.FileSpec): Promise<boolean>;
        /**
         * @returns A promise that resolves to whether this is a valid (non-null) FileSpec. If the
         * function returns false the underlying SDF/Cos object is null or is not valid
         * and the FileSpec object should be treated as null as well.
         */
        isValid(): Promise<boolean>;
        /**
         * The function saves the data referenced by this FileSpec to an external file.
         * @param [save_as] - An optional parameter indicating the filepath and filename
         * where the data should be saved. If this parameter is not specified, the function
         * will attempt to save the file using FileSpec.GetFilePath().
         *
         * If the file is embedded, the function saves the embedded file.
         * If the file is not embedded, the function will copy the external file.
         * If the file is not embedded and the external file can't be found, the function
         * returns false.
         * @returns A promise that resolves to true is the file was saved successfully, false otherwise.
         */
        export(save_as?: string): Promise<boolean>;
        /**
         * The function returns data referenced by this FileSpec.
         * @returns A promise that resolves to a stream (filter) containing file data.
         * If the file is embedded, the function returns a stream to the embedded file.
         * If the file is not embedded, the function will return a stream to the external file.
         * If the file is not embedded and the external file can't be found, the function
         * returns NULL.
         */
        getFileData(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to the file path for this file specification.
         *
         * If the FileSpec is a dictionary, a corresponding platform specific path
         * is returned (DOS, Mac, or Unix). Otherwise the function returns the path represented
         * in the form described in Section 3.10.1, 'File Specification Strings,' or , if the
         * file system is URL, as a uniform resource locator (URL). If the FileSpec is not
         * valid, an empty string is returned.
         */
        getFilePath(): Promise<string>;
        setDesc(desc: string): Promise<void>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * Provides a generic view of a sequence of bytes.
     *
     * A Filter is the abstract base class of all filters. A filter is an abstraction of
     * a sequence of bytes, such as a file, an input/output device, an inter-process communication
     * pipe, or a TCP/IP socket. The Filter class and its derived classes provide a generic view
     * of these different types of input and output, isolating the programmer from the specific
     * details of the operating system and the underlying devices.
     *
     * Besides providing access to input/output sources Filters can be also to transform the data
     * (e.g. to compress the data stream, to normalize the image data, to encrypt data, etc).
     * Filters can also be attached to each other to form pipelines. For example, a filter used to
     * open an image data file can be attached to a filter that decompresses the data, which is
     * attached to another filter that will normalize the image data.
     *
     * Depending on the underlying data source or repository, filters might support only some of
     * these capabilities. An application can query a stream for its capabilities by using the
     * IsInputFilter() and CanSeek() properties.
     */
    class Filter extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        createASCII85Encode(line_width: number, buf_sz: number): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createMappedFileFromUString(filename: string): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createMemoryFilter(buf_sz: number, is_input: boolean): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createImage2RGBFromElement(elem: PDFNet.Element): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createImage2RGBFromObj(obj: PDFNet.Obj): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createImage2RGB(img: PDFNet.Image): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createImage2RGBAFromElement(elem: PDFNet.Element, premultiply: boolean): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createImage2RGBAFromObj(obj: PDFNet.Obj, premultiply: boolean): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        static createImage2RGBA(img: PDFNet.Image, premultiply: boolean): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        mappedFileFileSize(): Promise<number>;
        /**
         * Comparison function.
         * Determines if parameter object is equal to current object.
         * @returns A promise that resolves to True if the two objects are equivalent, False otherwise
         */
        mappedFileCompare(mf2: PDFNet.Filter): Promise<boolean>;
        attachFilter(attach_filter: PDFNet.Filter): Promise<void>;
        /**
         * Release the ownership of the attached filter. After the attached filter is
         * released this filter points to NULL filter.
         * @returns A promise that resolves to Previously attached filter.
         */
        releaseAttachedFilter(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to returns attached Filter or a NULL filter if no filter is attached.
         */
        getAttachedFilter(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to returns the first filter in the chain (usually a file filter)
         */
        getSourceFilter(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to descriptive name of the filter.
         */
        getName(): Promise<string>;
        /**
         * @returns A promise that resolves to string representing the name of corresponding decode filter as
         * it should appear in document (e.g. both ASCIIHexDecode and ASCIIHexEncode
         * should return ASCIIHexDecode).
         */
        getDecodeName(): Promise<string>;
        /**
         * @returns A promise that resolves to beginning of the buffer of Size() bytes that can be used to
         * read or write data.
         */
        begin(): Promise<number>;
        /**
         * @returns A promise that resolves to the size of buffer returned by Begin(). If the Size() returns 0
         * end of data has been reached.
         */
        size(): Promise<number>;
        /**
         * Moves the Begin() pointer num_bytes forward.
         * @param num_bytes - number of bytes to consume. num_bytes must be less than or
         * equal to Size().
         */
        consume(num_bytes: number): Promise<void>;
        /**
         * @returns A promise that resolves to the number of bytes consumed since opening the filter or
         *   the last Seek operation
         */
        count(): Promise<number>;
        /**
         * Sets a new counting point for the current filter. All subsequent Consume()
         * operations will increment this counter.
         *
         * Make sure that the output filter is flushed before using SetCount().
         * @param new_count - number to set the counting point of the filter to.
         * @returns A promise that resolves to the value of previous counter
         */
        setCount(new_count: number): Promise<number>;
        /**
         * The functions specifies the length of the data stream. The default
         * implementation doesn't do anything. For some derived filters such
         * as file segment filter it may be useful to override this function
         * in order to limit the stream length.
         * @param bytes - the length of stream in bytes
         */
        setStreamLength(bytes: number): Promise<void>;
        /**
         * Forces any data remaining in the buffer to be written to input or
         * output filter.
         */
        flush(): Promise<void>;
        /**
         * Forces any data remaining in the filter chain to the source or destination.
         */
        flushAll(): Promise<void>;
        /**
         * @returns A promise that resolves to boolean indicating whether this is an input filter.
         */
        isInputFilter(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the stream supports seeking; otherwise, false.
         * default is to return false.
         */
        canSeek(): Promise<boolean>;
        /**
         * Writes the entire filter, starting at current position, to
         * 	specified filepath.  Should only be called on an input filter.
         * @param path - the output filepath.
         * @param append - 'true' to append to existing file contents, 'false' to overwrite.
         */
        writeToFile(path: string, append: boolean): Promise<void>;
        /**
         * When overridden in a derived class, sets the position within the current stream.
         * @param offset - A byte offset relative to origin. If offset is negative,
         *  the new position will precede the position specified by origin by the number
         *  of bytes specified by offset. If offset is zero, the new position will be the
         *  position specified by origin. If offset is positive, the new position will follow
         *  the position specified by origin by the number of bytes specified by offset.
         * @param origin - <pre>
         * PDFNet.Filter.ReferencePos = {
         * 	e_begin : 0
         * 	e_end : 2
         * 	e_cur : 1
         * }
         * </pre>
         * A value of type ReferencePos indicating the reference point used
         * to obtain the new position
         */
        seek(offset: number, origin: number): Promise<void>;
        /**
         * Reports the current read position in the stream relative to the stream origin.
         * @returns A promise that resolves to The current position in the stream
         */
        tell(): Promise<number>;
        /**
         * Create Filter iterator. Filter iterator similar to a regular filter. However,
         * there can be only one owner of the attached filter.
         * @returns A promise that resolves to an object of type: "PDFNet.Filter"
         */
        createInputIterator(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to the file path to the underlying file stream.
         * Default implementation returns empty string.
         */
        getFilePath(): Promise<string>;
        /**
         * @returns A promise that resolves to a pointer to the beginning of the buffer. Use method Count() to
         * determine the number of bytes written to or read from MemoryFilter.
         */
        memoryFilterGetBuffer(): Promise<number>;
        memoryFilterSetAsInputFilter(): Promise<void>;
        memoryFilterReset(): Promise<void>;
        /**
         * @param buf - A memory buffer containing the underlying data.
         * @returns A promise that resolves to a "Filter" created from the buffer
         */
        static createFromMemory(buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.Filter>;
        /**
         * @param url - A url from which the data can be downloaded.
         * @param [options] - Additional options
         * @param options.withCredentials - Whether to set the withCredentials property on the XMLHttpRequest
         * @param options.customHeaders - An object containing custom HTTP headers to be used when downloading the document
         * @returns A promise that resolves to a "Filter" created from the url
         */
        static createURLFilter(url: string, options?: {
            withCredentials: boolean;
            customHeaders: any;
        }): Promise<PDFNet.Filter>;
        /**
         * Constructor for Flate encoder.
        FlateEncode filter can be used to compress any data stream
        using Flate (i.e. ZIP) compression method.
         * @param [input_filter = PDFNet.Filter("0")] - the input data stream
         * @param [compression_level = -1] - compression_level must be a number between 0 and 9: 1 gives best speed,
        9 gives best compression, 0 gives no compression at all (the input data is simply
        copied a block at a time), -1 requests a default compromise between speed
        and compression (currently equivalent to level 6).
         * @param [buf_sz = 256] - filter buffer size (in bytes).
         * @returns A promise that resolves to an object of type: "Filter" (generated documentation)
         */
        static createFlateEncode(input_filter?: PDFNet.Filter, compression_level?: number, buf_sz?: number): Promise<PDFNet.Filter>;
    }
    /**
     * FilterReader is a utility class providing a convenient way to read data
     * from an input filter (using Filter directly is not very intuitive).
     *
     * For example:
     * <pre>
     * StdFile file("my_stream.txt", StdFile::e_read_mode);
     * FilterReader reader(file);
     * while (reader.Read(...)) ...
     * </pre>
     */
    class FilterReader extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.FilterReader"
         */
        static create(filter: PDFNet.Filter): Promise<PDFNet.FilterReader>;
        /**
         * Attaches a filter to the this FilterReader.
         * @param filter - filter object to attach
         */
        attachFilter(filter: PDFNet.Filter): Promise<void>;
        /**
         * @returns A promise that resolves to The attached Filter or a NULL filter if no filter is attached.
         */
        getAttachedFilter(): Promise<PDFNet.Filter>;
        /**
         * Sets the position within the current stream.
         * @param offset - A byte offset relative to origin. If offset is negative,
         *  the new position will precede the position specified by origin by the number
         *  of bytes specified by offset. If offset is zero, the new position will be the
         *  position specified by origin. If offset is positive, the new position will follow
         *  the position specified by origin by the number of bytes specified by offset.
         * @param origin - <pre>
         * PDFNet.Filter.ReferencePos = {
         * 	e_begin : 0
         * 	e_end : 2
         * 	e_cur : 1
         * }
         * </pre>
         * A value of type ReferencePos indicating the reference point used
         * to obtain the new position
         */
        seek(offset: number, origin: number): Promise<void>;
        /**
         * Reports the current read position in the stream relative to the stream origin.
         * @returns A promise that resolves to The current position in the stream
         */
        tell(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of bytes consumed since opening the filter or
         * since the last Seek operation.
         */
        count(): Promise<number>;
        /**
         * Forces any data remaining in the buffer to be written to input or output filter.
         */
        flush(): Promise<void>;
        /**
         * Forces any data remaining in the filter chain to the source or destination.
         */
        flushAll(): Promise<void>;
        /**
         * @returns A promise that resolves to the next character from the stream or EOF (-1) if the end of file is reached.
         */
        get(): Promise<number>;
        /**
         * @returns A promise that resolves to the next character without extracting it from the stream or
         * 			 or EOF (-1) if the end of file is reached.
         */
        peek(): Promise<number>;
        /**
         * Read data from the attached filter
         * @param buf_size - The maximum amount to read
         * @returns A promise that resolves to a Uint8Array containing the data that was read.
                    the promise will resolve to null if no data was remaining.
         */
        read(buf_size: number): Promise<Uint8Array>;
        /**
         * Read all the remaining data to a buffer from the attached filter
         * @returns A promise that resolves to a Uint8Array containing the data that was read.
                    the promise will resolve to null if no data was remaining.
         */
        readAllIntoBuffer(): Promise<Uint8Array>;
    }
    /**
     * FilterWriter is a utility class providing a convenient way to write data
     * to an output filter (using Filter directly is not very intuitive).
     *
     * For example:
     * <pre>
     * StdFile outfile("file.dat", StdFile::e_write_mode);
     * FilterWriter fwriter(outfile);
     * fwriter.WriteBuffer(buf, buf_sz);
     * fwriter.Flush();
     * </pre>
     */
    class FilterWriter extends PDFNet.Destroyable {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.FilterWriter"
         */
        static create(filter: PDFNet.Filter): Promise<PDFNet.FilterWriter>;
        /**
         * Attaches a filter to the this FilterWriter.
         * @param filter - filter object to attach
         */
        attachFilter(filter: PDFNet.Filter): Promise<void>;
        /**
         * @returns A promise that resolves to The attached Filter or a NULL filter if no filter is attached.
         */
        getAttachedFilter(): Promise<PDFNet.Filter>;
        /**
         * Sets the position within the current stream.
         * @param offset - A byte offset relative to origin. If offset is negative,
         *  the new position will precede the position specified by origin by the number
         *  of bytes specified by offset. If offset is zero, the new position will be the
         *  position specified by origin. If offset is positive, the new position will follow
         *  the position specified by origin by the number of bytes specified by offset.
         * @param origin - <pre>
         * PDFNet.Filter.ReferencePos = {
         * 	e_begin : 0
         * 	e_end : 2
         * 	e_cur : 1
         * }
         * </pre>
         * A value of type ReferencePos indicating the reference point used
         * to obtain the new position
         */
        seek(offset: number, origin: number): Promise<void>;
        /**
         * Reports the current read position in the stream relative to the stream origin.
         * @returns A promise that resolves to The current position in the stream
         */
        tell(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of bytes consumed since opening the filter or
         * since the last Seek operation.
         */
        count(): Promise<number>;
        /**
         * Forces any data remaining in the buffer to be written to input or output filter.
         */
        flush(): Promise<void>;
        /**
         * Forces any data remaining in the filter chain to the source or destination.
         */
        flushAll(): Promise<void>;
        /**
         * Write a single character to the output stream.
         * @param ch - An unsigned character to write to the output stream.
         */
        writeUChar(ch: number): Promise<void>;
        /**
         * Write an integer to the output stream.
         * @param num - An integer to write to the output stream.
         */
        writeInt16(num: number): Promise<void>;
        /**
         * Write an integer to the output stream.
         * @param num - An integer to write to the output stream.
         */
        writeUInt16(num: number): Promise<void>;
        /**
         * Write an integer to the output stream.
         * @param num - An integer to write to the output stream.
         */
        writeInt32(num: number): Promise<void>;
        /**
         * Write an integer to the output stream.
         * @param num - An integer to write to the output stream.
         */
        writeUInt32(num: number): Promise<void>;
        /**
         * Write an integer to the output stream.
         * @param num - An integer to write to the output stream.
         */
        writeInt64(num: number): Promise<void>;
        /**
         * Write an integer to the output stream.
         * @param num - An integer to write to the output stream.
         */
        writeUInt64(num: number): Promise<void>;
        /**
         * Write a string to the output stream.
         * @param str - A string to write to the output stream.
         */
        writeString(str: string): Promise<void>;
        /**
         * Write the entire input stream to the output stream (i.e. to this FilterWriter).
         * @param reader - A FilterReader attached to an input stream.
         */
        writeFilter(reader: PDFNet.FilterReader): Promise<void>;
        /**
         * Write out a null terminated 'line' followed by a end of line character
         * default end of line character is carriage return.
         * @param line - string to write out.
         * @param [eol] - end of line character. Defaults to carriage return (0x0D).
         */
        writeLine(line: string, eol?: number): Promise<void>;
        /**
         * @param buf - buffer object to write out.
         * @returns A promise that resolves to returns the number of bytes actually written to a stream. This number may
         *   less than buf_size if the stream is corrupted.
         */
        writeBuffer(buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<number>;
    }
    /**
     * Flattener is a optional PDFNet add-on that can be used to simplify and optimize
     * existing PDF's to render faster on devices with lower memory and speeds.
     *
     * PDF documents can frequently contain very complex page description (e.g.
     * thousands of paths, different shadings, color spaces, blend modes, large images
     * etc.) that may not be suitable for interactive viewing on mobile devices.
     * Flattener can be used to speed-up PDF rendering on mobile devices and on the Web
     * by simplifying page content (e.g. flattening complex graphics into images) while
     * maintaining vector text whenever possible.
     *
     * By using the FlattenMode::e_simple option each page in the PDF will be
     * reduced to a single background image, with the remaining text over top in vector
     * format. Some text may still get flattened, in particular any text that is clipped,
     * or underneath, other content that will be flattened.
     *
     * On the other hand the FlattenMode::e_fast will not flatten simple content, such
     * as simple straight lines, nor will it flatten Type3 fonts.
     */
    class Flattener extends PDFNet.Destroyable {
        /**
         * Flattener constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Flattener"
         */
        static create(): Promise<PDFNet.Flattener>;
        /**
         * The output resolution, from 1 to 1000, in Dots Per Inch (DPI) at which to
         * render elements which cannot be directly converted.
         * the default value is 150 Dots Per Inch
         * @param dpi - the resolution in Dots Per Inch
         */
        setDPI(dpi: number): Promise<void>;
        /**
         * Used to control how precise or relaxed text flattening is. When some text is
         * preserved (not flattened to image) the visual appearance of the document may be altered.
         * @param threshold - <pre>
         * PDFNet.Flattener.Threshold = {
         * 	e_very_strict : 0
         * 	e_strict : 1
         * 	e_default : 2
         * 	e_keep_most : 3
         * 	e_keep_all : 4
         * }
         * </pre>
         * the threshold setting to use.
         */
        setThreshold(threshold: number): Promise<void>;
        /**
         * Specifies the maximum image size in pixels.
         * @param max_pixels - the maximum number of pixels an image can have.
         */
        setMaximumImagePixels(max_pixels: number): Promise<void>;
        /**
         * Specifies whether to leave images in existing compression, or as JPEG.
         * @param jpg - if true PDF will contain all JPEG images.
         */
        setPreferJPG(jpg: boolean): Promise<void>;
        /**
         * Specifies the compression quality to use when generating JPEG images.
         * @param quality - the JPEG compression quality, from 0(highest compression) to 100(best quality).
         */
        setJPGQuality(quality: number): Promise<void>;
        /**
         * Enable or disable path hinting.
         * @param hinting - if true path hinting is enabled. Path hinting is used to slightly
         * adjust paths in order to avoid or alleviate artifacts of hair line cracks between
         * certain graphical elements. This option is turned on by default.
         */
        setPathHinting(hinting: boolean): Promise<void>;
        /**
         * Process each page in the PDF, flattening content that matches the mode criteria.
         * @param doc - the document to flatten.
         * @param mode - <pre>
         * PDFNet.Flattener.Mode = {
         * 	e_simple : 0
         * 	e_fast : 1
         * }
         * </pre>
         * indicates the criteria for which elements are flattened.
         */
        process(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, mode: number): Promise<void>;
        /**
         * Process the given page, flattening content that matches the mode criteria.
         * @param page - the page to flatten.
         * @param mode - <pre>
         * PDFNet.Flattener.Mode = {
         * 	e_simple : 0
         * 	e_fast : 1
         * }
         * </pre>
         * indicates the criteria for which elements are flattened.
         */
        processPage(page: PDFNet.Page, mode: number): Promise<void>;
    }
    /**
     * A font that is used to draw text on a page. It corresponds to a Font Resource
     * in a PDF file. More than one page may reference the same Font object.
     * A Font has a number of attributes, including an array of widths, the character
     * encoding, and the font's resource name.
     *
     * PDF document can contain several different types of fonts and Font class
     * represents a single, flat interface around all PDF font types.
     *
     * There are two main classes of fonts in PDF: simple and composite fonts.
     *
     * Simple fonts are Type1, TrueType, and Type3 fonts. All simple fonts have the
     * following properties:<BR><BR>
     *  - Glyphs in the font are selected by single-byte character codes
     *    obtained from a string that is shown by the text-showing operators.
     *    Logically, these codes index into a table of 256 glyphs; the mapping
     *    from codes to glyphs is called the font's encoding. Each font program
     *    has a built-in encoding. Under some circumstances, the encoding can
     *    be altered by means described in Section 5.5.5 "Character Encoding" in
     *    PDF Reference Manual.<BR><BR>
     *
     *  - Each glyph has a single set of metrics. Therefore simple fonts support
     *    only horizontal writing mode.<BR><BR>
     *
     * A composite font is one whose glyphs are obtained from a font like object
     * called a CIDFont (e.g. CIDType0Font and CIDType0Font). A composite font is
     * represented by a font dictionary whose Subtype value is Type0. The Type 0 font
     * is known as the root font, while its associated CIDFont is called its descendant.
     * CID-keyed fonts provide a convenient and efficient method for defining
     * multiple-byte character encodings and fonts with a large number of glyphs.
     * These capabilities provide great flexibility for representing text in writing
     * systems for languages with large character sets, such as Chinese, Japanese,
     * and Korean (CJK).
     */
    class Font extends PDFNet.Destroyable {
        /**
         * create a PDF::Font object from an existing SDF font object that is embedded
         * in the document. If font_dict is null, a non valid font is created.
         * @param [font_dict] - The Cos/SDF object to create the Font object with.
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createFromObj(font_dict?: PDFNet.Obj): Promise<PDFNet.Font>;
        /**
         * Constructor
         * @param doc - Document on which to create the font on.
         * @param type - <pre>
         * PDFNet.Font.StandardType1Font = {
         * 	e_times_roman : 0
         * 	e_times_bold : 1
         * 	e_times_italic : 2
         * 	e_times_bold_italic : 3
         * 	e_helvetica : 4
         * 	e_helvetica_bold : 5
         * 	e_helvetica_oblique : 6
         * 	e_helvetica_bold_oblique : 7
         * 	e_courier : 8
         * 	e_courier_bold : 9
         * 	e_courier_oblique : 10
         * 	e_courier_bold_oblique : 11
         * 	e_symbol : 12
         * 	e_zapf_dingbats : 13
         * 	e_null : 14
         * }
         * </pre>
         * The type of font to create.
         * <pre>
         * PDFNet.Font.StandardType1Font = {
         * 	e_times_roman : 0
         * 	e_times_bold : 1
         * 	e_times_italic : 2
         * 	e_times_bold_italic : 3
         * 	e_helvetica : 4
         * 	e_helvetica_bold : 5
         * 	e_helvetica_oblique : 6
         * 	e_helvetica_bold_oblique : 7
         * 	e_courier : 8
         * 	e_courier_bold : 9
         * 	e_courier_oblique : 10
         * 	e_courier_bold_oblique : 11
         * 	e_symbol : 12
         * 	e_zapf_dingbats : 13
         * 	e_null : 14
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "Font" (generated documentation)
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number): Promise<PDFNet.Font>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createFromFontDescriptor(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, from: PDFNet.Font, char_set: string): Promise<PDFNet.Font>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createFromName(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, name: string, char_set: string): Promise<PDFNet.Font>;
        /**
         * @param type - <pre>
         * PDFNet.Font.StandardType1Font = {
         * 	e_times_roman : 0
         * 	e_times_bold : 1
         * 	e_times_italic : 2
         * 	e_times_bold_italic : 3
         * 	e_helvetica : 4
         * 	e_helvetica_bold : 5
         * 	e_helvetica_oblique : 6
         * 	e_helvetica_bold_oblique : 7
         * 	e_courier : 8
         * 	e_courier_bold : 9
         * 	e_courier_oblique : 10
         * 	e_courier_bold_oblique : 11
         * 	e_symbol : 12
         * 	e_zapf_dingbats : 13
         * 	e_null : 14
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createAndEmbed(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number): Promise<PDFNet.Font>;
        /**
         * Embed an external TrueType font in the document as a Simple font.
         * @param doc - Document in which the external font should be embedded.
         * @param font_path - Path to the external font file.
         * @param [embed] - A boolean indicating whether the font should be embedded or
         * not. For accurate font reproduction set the embed flag to 'true'.
         * @param [subset] - A boolean indicating whether the embedded font should
         * be subsetted.
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createTrueTypeFont(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, font_path: string, embed?: boolean, subset?: boolean): Promise<PDFNet.Font>;
        /**
         * Embed an external TrueType font in the document as a CID font.
         * By default the function selects "Identity-H" encoding that maps 2-byte
         * character codes ranging from 0 to 65,535 to the same Unicode value.
         * Other predefined encodings are listed in Table 5.15 'Predefined CMap names'
         * in PDF Reference Manual.
         * @param doc - document in which the external font should be embedded.
         * @param font_path - path to the external font file.
         * @param [embed] - a boolean indicating whether the font should be embedded or
         * not. For accurate font reproduction set the embed flag to 'true'.
         * @param [subset] - a boolean indicating whether the embedded font should
         * be subsetted
         * @param [encoding] - <pre>
         * PDFNet.Font.Encoding = {
         * 	e_IdentityH : 0
         * 	e_Indices : 1
         * }
         * </pre>
         * the encoding type either e_IdentityH (default)
         * or e_Indices (to write glyph indices rather than unicode)
         * @param [ttc_font_index] - if a TrueTypeCollection (TTC) font is loaded this
         * parameter controls which font is actually picked
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createCIDTrueTypeFont(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, font_path: string, embed?: boolean, subset?: boolean, encoding?: number, ttc_font_index?: number): Promise<PDFNet.Font>;
        /**
         * Embed an external Type1 font in the document.
         * @param doc - document in which the external font should be embedded.
         * @param font_path - path to the external font file.
         * @param [embed] - a boolean indicating whether the font should be embedded or
         * not. For accurate font reproduction set the embed flag to 'true'.
         * @returns A promise that resolves to an object of type: "PDFNet.Font"
         */
        static createType1Font(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, font_path: string, embed?: boolean): Promise<PDFNet.Font>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Font.Type = {
         * 	e_Type1 : 0
         * 	e_TrueType : 1
         * 	e_MMType1 : 2
         * 	e_Type3 : 3
         * 	e_Type0 : 4
         * 	e_CIDType0 : 5
         * 	e_CIDType2 : 6
         * }
         * </pre>
         * @returns A promise that resolves to font Type
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to true for non-CID based fonts such as Type1, TrueType, and Type3
         *
         * All simple fonts have the following properties:<BR><BR>
         *
         * Glyphs in the font are selected by single-byte character codes
         *   obtained from a string that is shown by the text-showing operators.
         *   Logically, these codes index into a table of 256 glyphs; the mapping
         *   from codes to glyphs is called the font's encoding. Each font program
         *   has a built-in encoding. Under some circumstances, the encoding can
         *   be altered by means described in Section 5.5.5 "Character Encoding" in
         *   PDF Reference Manual.<BR><BR>
         *
         * Each glyph has a single set of metrics. Therefore simple fonts support
         *   only horizontal writing mode.<BR><BR>
         */
        isSimple(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.Font.Type = {
         * 	e_Type1 : 0
         * 	e_TrueType : 1
         * 	e_MMType1 : 2
         * 	e_Type3 : 3
         * 	e_Type0 : 4
         * 	e_CIDType0 : 5
         * 	e_CIDType2 : 6
         * }
         * </pre>
         */
        static getTypeFromObj(font_dict: PDFNet.Obj): Promise<number>;
        /**
         * @returns A promise that resolves to a SDF/Cos object of this Font.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to a SDF/Cos object representing FontDescriptor or NULL is FontDescriptor
         * is not present.
         */
        getDescriptor(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the name of a font. The behavior depends on the font type;
         * for a Type 3 font it gets the value of the Name key in a PDF Font resource.
         * For other types it gets the value of the BaseFont key in a PDF font resource.
         */
        getName(): Promise<string>;
        /**
         * @returns A promise that resolves to the face's family name. This is an ASCII string, usually in English,
         * which describes the typeface's family (like 'Times New Roman', 'Bodoni', 'Garamond',
         * etc). This is a least common denominator used to list fonts.
         */
        getFamilyName(): Promise<string>;
        /**
         * @returns A promise that resolves to true if all glyphs have the same width
         */
        isFixedWidth(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if glyphs have serifs
         */
        isSerif(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if font contains characters outside the Adobe standard Latin character set.
         */
        isSymbolic(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if glyphs have dominant vertical strokes that are slanted.
         */
        isItalic(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if font contains no lowercase letters
         */
        isAllCap(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if bold glyphs should be painted with extra pixels at very small text sizes.
         */
        isForceBold(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the font uses horizontal writing mode, false for vertical writing mode.
         */
        isHorizontalMode(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        getWidth(char_code: number): Promise<number>;
        /**
         * @returns A promise that resolves to the maximal advance width, in font units, for all glyphs in this face.
         */
        getMaxWidth(): Promise<number>;
        /**
         * @returns A promise that resolves to the default width to use for character codes whose widths are
         * not specified in a font dictionary's Widths array.
         */
        getMissingWidth(): Promise<number>;
        /**
         * GetCharCodeIterator represents an iterator interface used to traverse
         * a list of char codes for which there is a glyph outline in the embedded font.
         * @returns A promise that resolves to an object of type: "PDFNet.Iterator<number>"
         */
        getCharCodeIterator(): Promise<PDFNet.Iterator<number>>;
        /**
         * Creates a set of positioned glyphs corresponding to the visual representation
         * of the provided text string.
         *
         * The shaped text will take into  account any advanced positioning and
         * substitution features provided by an underylying embedded font file.
         * For example, these features could include kerning, ligatures, and diacritic
         * positioning. Typically the resulting shaped text would be fed into
         * ElementBuilder.CreateShapedTextRun()
         * @param text_to_shape - the string to be shaped.
         * @returns A promise that resolves to a ShapedText object representing the result of the shaping operation.
         */
        getShapedText(text_to_shape: string): Promise<PDFNet.ShapedText>;
        /**
         * @returns A promise that resolves to the font's encoding array (the mapping of character codes to glyphs).
         * The array contains 256 pointers. If a pointer is not NULL, it points to a
         * C string containing the name of the glyph for the code point corresponding
         * to the index. If it is NULL, then the name of the glyph is unchanged from
         * that specified by the font's built-in encoding.
         *
         * For a Type 3 font, all glyph names will be present in the encoding array,
         * and NULL entries correspond to un-encoded code points.
         */
        getEncoding(): Promise<string>;
        /**
         * Tests whether or not the specified font is stored as a font file in a stream
         * embedded in the PDF file.
         * @returns A promise that resolves to true if the font is embedded in the file, false otherwise.
         */
        isEmbedded(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the PostScript font name for the embedded font. If the embedded font
         * name is not available the function returns the empty string .
         */
        getEmbeddedFontName(): Promise<string>;
        /**
         * @returns A promise that resolves to the stream object of the embedded font or NULL if there if the
         * font is not embedded.
         */
        getEmbeddedFont(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the size of decoded buffer containing embedded font data or 0
         * if this information is not known in advance.
         */
        getEmbeddedFontBufSize(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of font units per EM square for this face. This is
         * typically 2048 for TrueType fonts, 1000 for Type1 fonts
         */
        getUnitsPerEm(): Promise<number>;
        /**
         * @returns A promise that resolves to a rectangle expressed in the glyph coordinate system, specifying the
         * font bounding box. This is the smallest rectangle enclosing the shape that would
         * result if all of the glyphs of the font were placed with their origins coincident
         * and then filled.
         */
        getBBox(): Promise<PDFNet.Rect>;
        /**
         * The face's ascender is the vertical distance from the baseline to the topmost
         * point of any glyph in the face. This field's value is a positive number, expressed
         * in the glyph coordinate system. For all font types except Type 3, the units of
         * glyph space are one-thousandth of a unit of text space. Some font designs use
         * a value different from 'bbox.yMax'.
         * @returns A promise that resolves to an object of type: "number"
         */
        getAscent(): Promise<number>;
        /**
         * The face's descender is the vertical distance from the baseline to the bottommost
         * point of any glyph in the face. This field's value is a negative number expressed
         * in the glyph coordinate system. For all font types except Type 3, the units of
         * glyph space are one-thousandth of a unit of text space. Some font designs use
         * a value different from 'bbox.yMin'.
         * @returns A promise that resolves to an object of type: "number"
         */
        getDescent(): Promise<number>;
        /**
         * @returns A promise that resolves to font::e_null if the font is not a standard Type1 font or some
         * other StandardType1Font value for a standard Type1 font.
         */
        getStandardType1FontType(): Promise<number>;
        /**
         * @returns A promise that resolves to true if the embedded font is represented as CFF (Compact Font Format).
         */
        isCFF(): Promise<boolean>;
        /**
         * @returns A promise that resolves to type3 font matrix, mapping glyph space to text space
         * A common practice is to define glyphs in terms of a 1000-unit
         * glyph coordinate system, in which case the font matrix is [0.001 0 0 0.001 0 0].
         */
        getType3FontMatrix(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getType3GlyphStream(char_code: number): Promise<PDFNet.Obj>;
        /**
         * @param char_code - character to query for vertical advance
         * @returns A promise that resolves to vertical advance. vertical advance is a displacement vector for vertical
         * writing mode (i.e. writing mode 1); its horizontal component is always 0.
         */
        getVerticalAdvance(char_code: number): Promise<object>;
        /**
         * @returns A promise that resolves to descendant CIDFont.
         */
        getDescendant(): Promise<PDFNet.Font>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        mapToCID(char_code: number): Promise<number>;
    }
    /**
     * A FreeText annotation (PDF 1.3) displays text directly on the page.
     * Unlike an ordinary Text annotation, a FreeText annotation has no
     * open or closed state; The content of the FreeText annotation is always
     * visible instead of being displayed in a popup window.
     */
    class FreeTextAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates a FreeText annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.FreeTextAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.FreeTextAnnot>;
        /**
         * creates a FreeText annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the FreeText annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.FreeTextAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.FreeTextAnnot>;
        /**
         * Creates a new FreeText annotation in the specified document.
         * @param doc - A document to which the FreeText annotation is added.
         * @param pos - A rectangle specifying the FreeText annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank FreeText annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.FreeTextAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the default appearance of the FreeText annotation.
         * @returns A promise that resolves to a string representing the default appearance of the annotation.
         */
        getDefaultAppearance(): Promise<string>;
        /**
         * sets the default appearance of the FreeText annotation.
         * @param app_str - A string representing the default appearance of the annotation.
         */
        setDefaultAppearance(app_str: string): Promise<void>;
        /**
         * Returns the quading format of the FreeText annotation.
         * (PDF 1.4)
         * @returns A promise that resolves to a int (code) indicating the quading format of the FreeText annotation.
         */
        getQuaddingFormat(): Promise<number>;
        /**
         * Sets the quading format of the FreeText annotation.
         * (Optional; PDF 1.4)
         * @param format - A int code indicating the quading format of the FreeText annotation.
         * Default value: 0 (left-justified).
         */
        setQuaddingFormat(format: number): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "Object"
         */
        getCalloutLinePoints(): Promise<object>;
        /**
         * sets the callout line points of the FreeText annotation.
         * (Optional; meaningful only if IT is FreeTextCallout; PDF 1.6)
         * @param p1 - The target point. (where the ending style is used)
         * @param p2 - The knee point.
         * @param p3 - The ending point.
         */
        setCalloutLinePoints(p1: PDFNet.Point, p2: PDFNet.Point, p3: PDFNet.Point): Promise<void>;
        /**
         * sets the callout line points of the FreeText annotation.
         * (Optional; meaningful only if IT is FreeTextCallout; PDF 1.6)
         * @param p1 - The target point. (where the ending style is used)
         * @param p2 - The ending point.
         */
        setCalloutLinePointsTwo(p1: PDFNet.Point, p2: PDFNet.Point): Promise<void>;
        /**
         * Returns Intent name of the FreeText annotation.
         * (PDF 1.4)
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.FreeTextAnnot.IntentName = {
         * 	e_FreeText : 0
         * 	e_FreeTextCallout : 1
         * 	e_FreeTextTypeWriter : 2
         * 	e_Unknown : 3
         * }
         * </pre>
         * @returns A promise that resolves to the intent name of the annotation as
         * an entry from the enum "IntentName".
         */
        getIntentName(): Promise<number>;
        /**
         * Sets the Intent name of the FreeText annotation.
         * (Optional; PDF 1.4)
         * @param [mode] - <pre>
         * PDFNet.FreeTextAnnot.IntentName = {
         * 	e_FreeText : 0
         * 	e_FreeTextCallout : 1
         * 	e_FreeTextTypeWriter : 2
         * 	e_Unknown : 3
         * }
         * </pre>
         * The intent name of the annotation as
         * an entry from the enum "IntentName".
         */
        setIntentName(mode?: number): Promise<void>;
        setIntentNameDefault(): Promise<void>;
        /**
         * Returns the ending style of the callout line of the FreeText Annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         * @returns A promise that resolves to the ending style represented as one of the entries of the enum "EndingStyle"
         */
        getEndingStyle(): Promise<number>;
        /**
         * Sets the ending style of the callout line of the FreeText Annotation.
         * (Optional; meaningful only if CL is present; PDF 1.6)
         * @param style - <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         * The ending style represented using one of the
         * entries of the enum "EndingStyle"
         */
        setEndingStyle(style: number): Promise<void>;
        /**
         * sets the ending style of the callout line of the FreeText Annotation.
         * (Optional; meaningful only if CL is present; PDF 1.6)
         * @param est - The ending style represented using a string.
         */
        setEndingStyleName(est: string): Promise<void>;
        /**
         * sets the text color of the FreeText Annotation.
         * @param color - ColorPt object representing the color.
         * @param col_comp - number of colorant components in ColorPt object.
         */
        setTextColor(color: PDFNet.ColorPt, col_comp: number): Promise<void>;
        /**
         * returns the text color of the FreeText Annotation.
         * @returns A promise that resolves to an object of type: "Object"
         */
        getTextColor(): Promise<object>;
        /**
         * sets the line and border color of the FreeText Annotation.
         * @param color - ColorPt object representing the color.
         * @param col_comp - number of colorant components in ColorPt object.
         */
        setLineColor(color: PDFNet.ColorPt, col_comp: number): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "Object"
         */
        getLineColor(): Promise<object>;
        /**
         * Sets the default appearance font size. A value of zero specifies
         * that the font size should should adjust so that the text uses
         * as much of the FreeText bounding box as possible.
         * @param font_size - Set the default font size. A value of zero means
         * auto resize font.
         */
        setFontSize(font_size: number): Promise<void>;
        /**
         * Get the default appearance font size. To get the actual font
         * size used, call RefreshAppearance and then use ElementReader
         * on the content stream of this annotation.
         * @returns A promise that resolves to the default font size, where a value of zero indicates
         * auto sizing.
         */
        getFontSize(): Promise<number>;
    }
    /**
     * Although PDF is not a programming language it provides several types of function
     * object that represent parameterized classes of functions, including mathematical
     * formulas and sampled representations with arbitrary resolution. Functions are used
     * in various ways in PDF, including device-dependent rasterization information for
     * high-quality printing (halftone spot functions and transfer functions), color
     * transform functions for certain color spaces, and specification of colors as a
     * function of position for smooth shadings. Functions in PDF represent static,
     * self-contained numerical transformations.<br><br>
     *
     * PDF::Function represents a single, flat interface around all PDF function types.
     */
    class Function extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Function"
         */
        static create(funct_dict?: PDFNet.Obj): Promise<PDFNet.Function>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Function.Type = {
         * 	e_sampled : 0
         * 	e_exponential : 2
         * 	e_stitching : 3
         * 	e_postscript : 4
         * }
         * </pre>
         * @returns A promise that resolves to the function type
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of input components required by the function
         */
        getInputCardinality(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of output components returned by the function
         */
        getOutputCardinality(): Promise<number>;
        eval(inval: number, outval: number): Promise<void>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * GState is a class that keeps track of a number of style attributes used to visually
     * define graphical Elements. Each PDF::Element has an associated GState that can be used to
     * query or set various graphics properties.
     */
    class GState {
        /**
         * @returns A promise that resolves to the transformation matrix for this element.
         */
        getTransform(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to color space used for stroking
         */
        getStrokeColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to color space used for filling
         */
        getFillColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to a color value/point represented in the current stroke color space
         */
        getStrokeColor(): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to the SDF pattern object of currently selected PatternColorSpace used for stroking.
         */
        getStrokePattern(): Promise<PDFNet.PatternColor>;
        /**
         * @returns A promise that resolves to a color value/point represented in the current fill color space
         */
        getFillColor(): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to the pattern color of currently selected pattern color space used for filling.
         */
        getFillPattern(): Promise<PDFNet.PatternColor>;
        /**
         * @returns A promise that resolves to current value of flatness tolerance
         *
         * Flatness is a number in the range 0 to 100; a value of 0 specifies the output
         * device's default flatness tolerance.
         *
         * The flatness tolerance controls the maximum permitted distance in device pixels
         * between the mathematically correct path and an approximation constructed from
         * straight line segments.
         */
        getFlatness(): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.LineCap = {
         * 	e_butt_cap : 0
         * 	e_round_cap : 1
         * 	e_square_cap : 2
         * }
         * </pre>
         * @returns A promise that resolves to currently selected LineCap style
         *
         * The line cap style specifies the shape to be used at the ends of open sub-paths
         * (and dashes, if any) when they are stroked.
         */
        getLineCap(): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.LineJoin = {
         * 	e_miter_join : 0
         * 	e_round_join : 1
         * 	e_bevel_join : 2
         * }
         * </pre>
         * @returns A promise that resolves to currently selected LineJoin style
         *
         * The line join style specifies the shape to be used at the corners of paths that
         * are stroked.
         */
        getLineJoin(): Promise<number>;
        /**
         * @returns A promise that resolves to the thickness of the line used to stroke a path.
         */
        getLineWidth(): Promise<number>;
        /**
         * @returns A promise that resolves to current value of miter limit.
         *
         * The miter limit imposes a maximum on the ratio of the miter length to the
         * line width. When the limit is exceeded, the join is converted from a miter
         * to a bevel.
         */
        getMiterLimit(): Promise<number>;
        /**
         * @returns A promise that resolves to the phase of the currently selected dash pattern. dash phase is expressed in
         * user space units.
         */
        getPhase(): Promise<number>;
        /**
         * @returns A promise that resolves to currently selected character spacing.
         *
         * The character spacing parameter is a number specified in unscaled text space
         * units. When the glyph for each character in the string is rendered, the character
         * spacing is added to the horizontal or vertical component of the glyph's displacement,
         * depending on the writing mode. See Section 5.2.1 in PDF Reference Manual for details.
         */
        getCharSpacing(): Promise<number>;
        /**
         * @returns A promise that resolves to currently selected word spacing
         *
         * Word spacing works the same way as character spacing, but applies only to the
         * space character (char code 32). See Section 5.2.2 in PDF Reference Manual for details.
         */
        getWordSpacing(): Promise<number>;
        /**
         * @returns A promise that resolves to currently selected horizontal scale
         *
         * The horizontal scaling parameter adjusts the width of glyphs by stretching
         * or compressing them in the horizontal direction. Its value is specified as
         * a percentage of the normal width of the glyphs, with 100 being the normal width.
         * The scaling always applies to the horizontal coordinate in text space, independently
         * of the writing mode. See Section 5.2.3 in PDF Reference Manual for details.
         */
        getHorizontalScale(): Promise<number>;
        /**
         * @returns A promise that resolves to currently selected leading parameter
         *
         * The leading parameter is measured in unscaled text space units. It specifies
         * the vertical distance between the baselines of adjacent lines of text.
         * See Section 5.2.4 in PDF Reference Manual for details.
         */
        getLeading(): Promise<number>;
        /**
         * @returns A promise that resolves to currently selected font
         */
        getFont(): Promise<PDFNet.Font>;
        /**
         * @returns A promise that resolves to the font size
         */
        getFontSize(): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.TextRenderingMode = {
         * 	e_fill_text : 0
         * 	e_stroke_text : 1
         * 	e_fill_stroke_text : 2
         * 	e_invisible_text : 3
         * 	e_fill_clip_text : 4
         * 	e_stroke_clip_text : 5
         * 	e_fill_stroke_clip_text : 6
         * 	e_clip_text : 7
         * }
         * </pre>
         * @returns A promise that resolves to current text rendering mode.
         *
         * The text rendering mode determines whether showing text causes glyph outlines to
         * be stroked, filled, used as a clipping boundary, or some combination of the three.
         * See Section 5.2.5 in PDF Reference Manual for details..
         */
        getTextRenderMode(): Promise<number>;
        /**
         * @returns A promise that resolves to current value of text rise
         *
         * Text rise specifies the distance, in unscaled text space units, to move the
         * baseline up or down from its default location. Positive values of text rise
         * move the baseline up
         */
        getTextRise(): Promise<number>;
        /**
         * @returns A promise that resolves to a boolean flag that determines the text element is considered
         * elementary objects for purposes of color compositing in the transparent imaging
         * model.
         */
        isTextKnockout(): Promise<boolean>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.RenderingIntent = {
         * 	e_absolute_colorimetric : 0
         * 	e_relative_colorimetric : 1
         * 	e_saturation : 2
         * 	e_perceptual : 3
         * }
         * </pre>
         * @returns A promise that resolves to the color intent to be used for rendering the Element
         */
        getRenderingIntent(): Promise<number>;
        /**
         * A utility function that maps a string representing a rendering intent to
         * RenderingIntent type.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.RenderingIntent = {
         * 	e_absolute_colorimetric : 0
         * 	e_relative_colorimetric : 1
         * 	e_saturation : 2
         * 	e_perceptual : 3
         * }
         * </pre>
         * @param name - string that represents the rendering intent to get.
         * @returns A promise that resolves to the color rendering intent type matching the specified string
         */
        static getRenderingIntentType(name: string): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.BlendMode = {
         * 	e_bl_compatible : 0
         * 	e_bl_normal : 1
         * 	e_bl_multiply : 2
         * 	e_bl_screen : 3
         * 	e_bl_difference : 4
         * 	e_bl_darken : 5
         * 	e_bl_lighten : 6
         * 	e_bl_color_dodge : 7
         * 	e_bl_color_burn : 8
         * 	e_bl_exclusion : 9
         * 	e_bl_hard_light : 10
         * 	e_bl_overlay : 11
         * 	e_bl_soft_light : 12
         * 	e_bl_luminosity : 13
         * 	e_bl_hue : 14
         * 	e_bl_saturation : 15
         * 	e_bl_color : 16
         * }
         * </pre>
         * @returns A promise that resolves to the current blend mode to be used in the transparent imaging model.
         * Corresponds to the /BM key within the ExtGState's dictionary.
         */
        getBlendMode(): Promise<number>;
        /**
         * @returns A promise that resolves to the opacity value for painting operations other than stroking.
         * Returns the value of the /ca key in the ExtGState dictionary. If the value is not
         * found, the default value of 1 is returned.
         */
        getFillOpacity(): Promise<number>;
        /**
         * @returns A promise that resolves to opacity value for stroke painting operations for paths and glyph outlines.
         * Returns the value of the /CA key in the ExtGState dictionary. If the value is not
         * found, the default value of 1 is returned.
         */
        getStrokeOpacity(): Promise<number>;
        /**
         * @returns A promise that resolves to the alpha source flag ('alpha is shape'), specifying whether the
         * current soft mask and alpha constant are to be interpreted as shape values
         * (true) or opacity values (false).
         */
        getAISFlag(): Promise<boolean>;
        /**
         * @returns A promise that resolves to associated soft mask. NULL if the soft mask is not selected or
         * SDF dictionary representing the soft mask otherwise.
         */
        getSoftMask(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the soft mask transform. This is the transformation matrix at the moment the soft
         * mask is established in the graphics state with the gs operator. This information is only
         * relevant when applying the soft mask that may be specified in the graphics state to the
         * current element.
         */
        getSoftMaskTransform(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to whether overprint is enabled for stroke painting operations.
         * Corresponds to the /OP key within the ExtGState's dictionary.
         */
        getStrokeOverprint(): Promise<boolean>;
        /**
         * @returns A promise that resolves to whether overprint is enabled for fill painting operations.
         * Corresponds to the /op key within the ExtGState's dictionary.
         */
        getFillOverprint(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the overprint mode used by this graphics state.
         * Corresponds to the /OPM key within the ExtGState's dictionary.
         */
        getOverprintMode(): Promise<number>;
        /**
         * @returns A promise that resolves to a flag specifying whether stroke adjustment is enabled in the graphics
         * state. Corresponds to the /SA key within the ExtGState's dictionary.
         */
        getAutoStrokeAdjust(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the smoothness tolerance used to control the quality of smooth
         * shading. Corresponds to the /SM key within the ExtGState's dictionary.
         * The allowable error (or tolerance) is expressed as a fraction of the range
         * of the color component, from 0.0 to 1.0.
         */
        getSmoothnessTolerance(): Promise<number>;
        /**
         * @returns A promise that resolves to currently selected transfer function (NULL by default) used during
         * color conversion process. A transfer function adjusts the values of color
         * components to compensate for nonlinear response in an output device and in
         * the human eye. Corresponds to the /TR key within the ExtGState's dictionary.
         */
        getTransferFunct(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to currently selected black-generation function (NULL by default) used
         * during conversion between DeviceRGB and DeviceCMYK. Corresponds to the /BG key
         * within the ExtGState's dictionary.
         */
        getBlackGenFunct(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to currently selected undercolor-removal function (NULL by default) used
         * during conversion between DeviceRGB and DeviceCMYK. Corresponds to the /UCR key
         * within the ExtGState's dictionary.
         */
        getUCRFunct(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to currently selected halftone dictionary or stream (NULL by default).
         * Corresponds to the /HT key within the ExtGState's dictionary.
         * Halftoning is a process by which continuous-tone colors are approximated on an
         * output device that can achieve only a limited number of discrete colors.
         */
        getHalftone(): Promise<PDFNet.Obj>;
        /**
         * Set the transformation matrix associated with this element.
         * @param mtx - The new transformation for this text element.
         */
        setTransformMatrix(mtx: PDFNet.Matrix2D): Promise<void>;
        /**
         * Set the transformation matrix associated with this element.
         *
         * A transformation matrix in PDF is specified by six numbers, usually
         * in the form of an array containing six elements. In its most general
         * form, this array is denoted [a b c d h v]; it can represent any linear
         * transformation from one coordinate system to another. For more
         * information about PDF matrices please refer to section 4.2.2 'Common
         * Transformations' in PDF Reference Manual, and to documentation for
         * pdftron::Common::Matrix2D class.
         * @param a - horizontal 'scaling' component of the new text matrix.
         * @param b - 'rotation' component of the new text matrix.
         * @param c - 'rotation' component of the new text matrix.
         * @param d - vertical 'scaling' component of the new text matrix.
         * @param h - horizontal translation component of the new text matrix.
         * @param v - vertical translation component of the new text matrix.
         */
        setTransform(a: number, b: number, c: number, d: number, h: number, v: number): Promise<void>;
        /**
         * Concatenate the given matrix to the transformation matrix of this element.
         * @param mtx - Matrix2D object to concatenate the current matrix with.
         */
        concatMatrix(mtx: PDFNet.Matrix2D): Promise<void>;
        /**
         * Concatenate the given matrix expressed in its values to the transformation
         * matrix of this element.
         * @param a - horizontal 'scaling' component of the new text matrix.
         * @param b - 'rotation' component of the new text matrix.
         * @param c - 'rotation' component of the new text matrix.
         * @param d - vertical 'scaling' component of the new text matrix.
         * @param h - horizontal translation component of the new text matrix.
         * @param v - vertical translation component of the new text matrix.
         */
        concat(a: number, b: number, c: number, d: number, h: number, v: number): Promise<void>;
        /**
         * Sets the color space used for stroking operations
         * @param cs - ColorSpace object to use for stroking operations
         */
        setStrokeColorSpace(cs: PDFNet.ColorSpace): Promise<void>;
        /**
         * Sets the color space used for filling operations
         * @param cs - ColorSpace object to use for filling operations
         */
        setFillColorSpace(cs: PDFNet.ColorSpace): Promise<void>;
        /**
         * Sets the color value/point used for stroking operations.
         * @param c - is the color used for stroking operations
         */
        setStrokeColorWithColorPt(c: PDFNet.ColorPt): Promise<void>;
        /**
         * Set the stroke color to the given tiling pattern.
         * @param pattern - SDF pattern object.
         */
        setStrokeColorWithPattern(pattern: PDFNet.PatternColor): Promise<void>;
        /**
         * Set the stroke color to the given uncolored tiling pattern.
         * @param pattern - pattern (PatternType = 1 and PaintType = 2) object.
         * @param c - is a color in the pattern's underlying color space.
         */
        setStrokeColor(pattern: PDFNet.PatternColor, c: PDFNet.ColorPt): Promise<void>;
        /**
         * Sets the color value/point used for filling operations.
         * @param c - the color used for filling operations
         * The color value must be represented in the currently selected color space used
         * for filling.
         */
        setFillColorWithColorPt(c: PDFNet.ColorPt): Promise<void>;
        /**
         * Set the fill color to the given tiling pattern.
         * @param pattern - New pattern color.
         */
        setFillColorWithPattern(pattern: PDFNet.PatternColor): Promise<void>;
        /**
         * Set the fill color to the given uncolored tiling pattern.
         * @param pattern - PatternColor (PatternType = 1 and PaintType = 2) object.
         * @param c - is a color in the pattern's underlying color space.
         */
        setFillColor(pattern: PDFNet.PatternColor, c: PDFNet.ColorPt): Promise<void>;
        /**
         * Sets the value of flatness tolerance.
         * @param flatness - is a number in the range 0 to 100; a value of 0 specifies the output
         * device's default flatness tolerance.
         *
         * The flatness tolerance controls the maximum permitted distance in device pixels
         * between the mathematically correct path and an approximation constructed from
         * straight line segments.
         */
        setFlatness(flatness: number): Promise<void>;
        /**
         * Sets LineCap style property.
         *
         * The line cap style specifies the shape to be used at the ends of open subpaths
         * (and dashes, if any) when they are stroked.
         * @param cap - <pre>
         * PDFNet.GState.LineCap = {
         * 	e_butt_cap : 0
         * 	e_round_cap : 1
         * 	e_square_cap : 2
         * }
         * </pre>
         */
        setLineCap(cap: number): Promise<void>;
        /**
         * Sets LineJoin style property.
         *
         * The line join style specifies the shape to be used at the corners of paths that
         * are stroked.
         * @param join - <pre>
         * PDFNet.GState.LineJoin = {
         * 	e_miter_join : 0
         * 	e_round_join : 1
         * 	e_bevel_join : 2
         * }
         * </pre>
         */
        setLineJoin(join: number): Promise<void>;
        /**
         * Sets the thickness of the line used to stroke a path.
         * @param width - a non-negative number expressed in user space units.
         * A line width of 0 denotes the thinnest line that can be rendered at device
         * resolution: 1 device pixel wide.
         */
        setLineWidth(width: number): Promise<void>;
        /**
         * Sets miter limit.
         * @param miter_limit - A number that imposes a maximum on the ratio of the miter
         * length to the line width. When the limit is exceeded, the join is converted
         * from a miter to a bevel.
         */
        setMiterLimit(miter_limit: number): Promise<void>;
        setDashPattern(buf_dash_array: any[], phase: number): Promise<void>;
        /**
         * Sets character spacing.
         * @param char_spacing - a number specified in unscaled text space units. When the
         * glyph for each character in the string is rendered, the character spacing is
         * added to the horizontal or vertical component of the glyph's displacement,
         * depending on the writing mode. See Section 5.2.1 in PDF Reference Manual for details.
         */
        setCharSpacing(char_spacing: number): Promise<void>;
        /**
         * Sets word spacing.
         * @param word_spacing - a number specified in unscaled text space units.
         * Word spacing works the same way as character spacing, but applies only to the
         * space character (char code 32). See Section 5.2.2 in PDF Reference Manual for details.
         */
        setWordSpacing(word_spacing: number): Promise<void>;
        /**
         * Sets horizontal scale.
         * The horizontal scaling parameter adjusts the width of glyphs by stretching
         * or compressing them in the horizontal direction. Its value is specified as
         * a percentage of the normal width of the glyphs, with 100 being the normal width.
         * The scaling always applies to the horizontal coordinate in text space, independently
         * of the writing mode. See Section 5.2.3 in PDF Reference Manual for details.
         * @param hscale - value to set horizontal scale to.
         */
        setHorizontalScale(hscale: number): Promise<void>;
        /**
         * Sets the leading parameter.
         *
         * The leading parameter is measured in unscaled text space units. It specifies
         * the vertical distance between the baselines of adjacent lines of text.
         * See Section 5.2.4 in PDF Reference Manual for details.
         * @param leading - number representing vertical distance between lines of text
         */
        setLeading(leading: number): Promise<void>;
        /**
         * Sets the font and font size used to draw text.
         * @param font - Font to draw the text with
         * @param font_sz - size of the font to draw the text with
         */
        setFont(font: PDFNet.Font, font_sz: number): Promise<void>;
        /**
         * Sets text rendering mode.
         * The text rendering mode determines whether showing text causes glyph outlines to
         * be stroked, filled, used as a clipping boundary, or some combination of the three.
         * See Section 5.2.5 in PDF Reference Manual for details..
         * @param rmode - <pre>
         * PDFNet.GState.TextRenderingMode = {
         * 	e_fill_text : 0
         * 	e_stroke_text : 1
         * 	e_fill_stroke_text : 2
         * 	e_invisible_text : 3
         * 	e_fill_clip_text : 4
         * 	e_stroke_clip_text : 5
         * 	e_fill_stroke_clip_text : 6
         * 	e_clip_text : 7
         * }
         * </pre>
         * the mode that text will rendered with.
         * rmode can take the following values:
         * PDFNet.GState.TextRenderingMode = {
         * 	e_fill_text : 0,
         * 	e_stroke_text : 1,
         * 	e_fill_stroke_text : 2,
         * 	e_invisible_text : 3,
         * 	e_fill_clip_text : 4,
         * 	e_stroke_clip_text : 5,
         * 	e_fill_stroke_clip_text : 6,
         * 	e_clip_text : 7
         * }
         */
        setTextRenderMode(rmode: number): Promise<void>;
        /**
         * Sets text rise.
         * Text rise specifies the distance, in unscaled text space units, to move the
         * baseline up or down from its default location. Positive values of text rise
         * move the baseline up
         * @param rise - distance to move baseline up. Negative values move baseline down.
         */
        setTextRise(rise: number): Promise<void>;
        /**
         * Mark the object as elementary for purposes of color compositing in the
         * transparent imaging model.
         * @param knockout - Whether an object is elementary or not.
         */
        setTextKnockout(knockout: boolean): Promise<void>;
        /**
         * Sets the color intent to be used for rendering the Element.
         * @param intent - <pre>
         * PDFNet.GState.RenderingIntent = {
         * 	e_absolute_colorimetric : 0
         * 	e_relative_colorimetric : 1
         * 	e_saturation : 2
         * 	e_perceptual : 3
         * }
         * </pre>
         */
        setRenderingIntent(intent: number): Promise<void>;
        /**
         * Sets the current blend mode to be used in the transparent imaging model.
         * Corresponds to the /BM key within the ExtGState's dictionary.
         * @param BM - <pre>
         * PDFNet.GState.BlendMode = {
         * 	e_bl_compatible : 0
         * 	e_bl_normal : 1
         * 	e_bl_multiply : 2
         * 	e_bl_screen : 3
         * 	e_bl_difference : 4
         * 	e_bl_darken : 5
         * 	e_bl_lighten : 6
         * 	e_bl_color_dodge : 7
         * 	e_bl_color_burn : 8
         * 	e_bl_exclusion : 9
         * 	e_bl_hard_light : 10
         * 	e_bl_overlay : 11
         * 	e_bl_soft_light : 12
         * 	e_bl_luminosity : 13
         * 	e_bl_hue : 14
         * 	e_bl_saturation : 15
         * 	e_bl_color : 16
         * }
         * </pre>
         * New blending mode type.
         *
         * <pre>
         * // C#
         * gs.SetBlendMode(GState.BlendMode.e_lighten);
         *
         * // C++
         * gs->SetBlendMode(GState::e_lighten);
         * </pre>
         */
        setBlendMode(BM: number): Promise<void>;
        /**
         * Sets the opacity value for painting operations other than stroking.
         * Corresponds to the value of the /ca key in the ExtGState dictionary.
         * @param ca - value to set fill opacity to
         */
        setFillOpacity(ca: number): Promise<void>;
        /**
         * Sets opacity value for stroke painting operations for paths and glyph outlines.
         * Corresponds to the value of the /CA key in the ExtGState dictionary.
         * @param ca - value to set stroke opacity to
         */
        setStrokeOpacity(ca: number): Promise<void>;
        /**
         * specifies if the alpha is to be interpreted as a shape or opacity mask.
         * The alpha source flag ('alpha is shape'), specifies whether the
         * current soft mask and alpha constant are to be interpreted as shape values
         * (true) or opacity values (false).
         * @param AIS - true for interpretation as shape values or false for opacity values
         */
        setAISFlag(AIS: boolean): Promise<void>;
        /**
         * Sets the soft mask of the extended graphics state.
         * Corresponds to the /SMask key within the ExtGState's dictionary.
         * @param SM - SDF/Cos black-generation function or name
         */
        setSoftMask(SM: PDFNet.Obj): Promise<void>;
        /**
         * specifies if overprint is enabled for stroke operations. Corresponds to the /OP
         * key within the ExtGState's dictionary.
         * @param OP - true to enable overprint for stroke, false to disable.
         */
        setStrokeOverprint(OP: boolean): Promise<void>;
        /**
         * specifies if overprint is enabled for fill operations. Corresponds to the /op
         * key within the ExtGState's dictionary.
         * @param op - true to enable overprint for fill, false to disable.
         */
        setFillOverprint(op: boolean): Promise<void>;
        /**
         * Sets the overprint mode. Corresponds to the /OPM key within the ExtGState's
         * dictionary.
         * @param OPM - overprint mode.
         */
        setOverprintMode(OPM: number): Promise<void>;
        /**
         * Specify whether to apply automatic stroke adjustment.
         * Corresponds to the /SA key within the ExtGState's dictionary.
         * @param SA - if true automatic stroke adjustment will be applied.
         */
        setAutoStrokeAdjust(SA: boolean): Promise<void>;
        setSmoothnessTolerance(SM: number): Promise<void>;
        /**
         * Sets black-generation function used during conversion between DeviceRGB
         * and DeviceCMYK. Corresponds to the /BG key within the ExtGState's
         * dictionary.
         * @param BG - SDF/Cos black-generation function or name
         */
        setBlackGenFunct(BG: PDFNet.Obj): Promise<void>;
        /**
         * Sets undercolor-removal function used during conversion between DeviceRGB
         * and DeviceCMYK. Corresponds to the /UCR key within the ExtGState's
         * dictionary.
         * @param UCR - SDF/Cos undercolor-removal function or name
         */
        setUCRFunct(UCR: PDFNet.Obj): Promise<void>;
        /**
         * Sets transfer function used during color conversion process. A transfer
         * function adjusts the values of color components to compensate for nonlinear
         * response in an output device and in the human eye. Corresponds to the /TR key
         * within the ExtGState's dictionary.
         * @param TR - SDF/Cos transfer function, array, or name
         */
        setTransferFunct(TR: PDFNet.Obj): Promise<void>;
        /**
         * @param HT - SDF/Cos halftone dictionary, stream, or name
         * @returns A promise that resolves to currently selected halftone dictionary or stream (NULL by default).
         * Corresponds to the /HT key within the ExtGState's dictionary.
         * Halftoning is a process by which continuous-tone colors are approximated on an
         * output device that can achieve only a limited number of discrete colors.
         */
        setHalftone(HT: PDFNet.Obj): Promise<void>;
    }
    /**
     * The class GeometryCollection.
     * A Preprocessed PDF geometry collection
     */
    class GeometryCollection extends PDFNet.Destroyable {
        /**
         * return the point within the collection which is closest to the queried point. All values are in the page coordinate space.
         * @param x - - the x coordinate to snap, in page coordinates.
         * @param y - - the y coordinate to snap, in page coordinates.
         * @param mode - - a combination of flags from the SnappingMode enumeration.
         * @returns A promise that resolves to a point within the collection, closest to the queried point. If the collection is empty, the queried point will be returned unchanged.
         */
        snapToNearest(x: number, y: number, mode: number): Promise<PDFNet.Point>;
        /**
         * return the point within the collection which is closest to the queried point. All values are in the page coordinate space.
         * @param x - - the x coordinate to snap.
         * @param y - - the y coordinate to snap.
         * @param dpi - - the resolution of the rendered page, in pixels per inch.
         * @param mode - - a combination of flags from the SnappingMode enumeration.
         * @returns A promise that resolves to a point within the collection, closest to the queried point. If the collection is empty, the queried point will be returned unchanged.
         */
        snapToNearestPixel(x: number, y: number, dpi: number, mode: number): Promise<PDFNet.Point>;
    }
    /**
     * 'pdftron.PDF.HTML2PDF' is an optional PDFNet Add-On utility class that can be
     * used to convert HTML web pages into PDF documents by using an external
     * module (html2pdf).
     *
     * The html2pdf modules can be downloaded from http: www.pdftron.com/pdfnet/downloads.html.
     *
     * Users can convert HTML pages to PDF using the following operations:
     * - Simple one line static method to convert a single web page to PDF.
     * - Convert HTML pages from URL or string, plus optional table of contents, in user defined order.
     * - Optionally configure settings for proxy, images, java script, and more for each HTML page.
     * - Optionally configure the PDF output, including page size, margins, orientation, and more.
     * - Optionally add table of contents, including setting the depth and appearance.
     *
     * The following code converts a single webpage to pdf
     *
     * <pre>
     * using namespace pdftron;
     * using namespace PDF;
     *
     * PDFDoc pdfdoc;
     * if ( HTML2PDF::Convert(pdfdoc, "http://www.gutenberg.org/wiki/Main_Page") )
     * 		pdfdoc.Save(outputFile, SDF::SDFDoc::e_remove_unused, NULL);
     * </pre>
     *
     * The following code demonstrates how to convert multiple web pages into one pdf,
     * excluding the background, and with lowered image quality to save space.
     *
     * <pre>
     * using namespace pdftron;
     * using namespace PDF;
     *
     * HTML2PDF converter;
     * converter.SetImageQuality(25);
     *
     * HTML2PDF::WebPageSettings settings;
     * settings.SetPrintBackground(false);
     *
     * converter.InsertFromURL("http://www.gutenberg.org/wiki/Main_Page", settings);
     *
     * PDFDoc pdfdoc;
     * if ( converter.Convert(pdfdoc) )
     * 		pdfdoc.Save(outputFile, SDF::SDFDoc::e_remove_unused, NULL);
     * </pre>
     */
    class HTML2PDF extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        static staticConvert(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, url: string): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        static staticConvert2(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, url: string, settings: PDFNet.HTML2PDF.WebPageSettings): Promise<boolean>;
        /**
         * Default constructor.
         * @returns A promise that resolves to an object of type: "PDFNet.HTML2PDF"
         */
        static create(): Promise<PDFNet.HTML2PDF>;
        /**
         * Add a web page to be converted. A single URL typically
         * results in many PDF pages.
         * @param url - HTML page, or relative path to local HTML page
         */
        insertFromUrl(url: string): Promise<void>;
        /**
         * Add a web page to be converted. A single URL typically
         * results in many PDF pages.
         * @param url - HTML page, or relative path to local HTML page
         * @param settings - How the web page should be loaded and converted
         */
        insertFromUrl2(url: string, settings: PDFNet.HTML2PDF.WebPageSettings): Promise<void>;
        /**
         * Convert HTML encoded in string.
         * @param html - String containing HTML code.
         */
        insertFromHtmlString(html: string): Promise<void>;
        /**
         * Convert HTML encoded in string.
         * @param html - String containing HTML code.
         * @param settings - How the HTML content described in html is loaded.
         */
        insertFromHtmlString2(html: string, settings: PDFNet.HTML2PDF.WebPageSettings): Promise<void>;
        /**
         * Add a table of contents to the produced PDF.
         */
        insertTOC(): Promise<void>;
        /**
         * Add a table of contents to the produced PDF.
         * @param settings - Settings for the table of contents.
         */
        insertTOC2(settings: PDFNet.HTML2PDF.TOCSettings): Promise<void>;
        /**
         * Convert HTML documents and append the results
         * to doc.
         * @param doc - Target PDF to which converted HTML pages will
         * be appended to.
         * @returns A promise that resolves to true if successful, otherwise false. Use
         */
        convert(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<boolean>;
        /**
         * Return the largest HTTP error code encountered during conversion
         * @returns A promise that resolves to the largest HTTP code greater then or equal to 300 encountered during loading
         * of any of the supplied objects, if no such error code is found 0 is returned.
         */
        getHttpErrorCode(): Promise<number>;
        /**
         * Get results of conversion, including errors and warnings, in human readable form.
         * @returns A promise that resolves to string containing results of conversion.
         */
        getLog(): Promise<string>;
        /**
         * Set paper size of output PDF
         * @param size - <pre>
         * PDFNet.PrinterMode.PaperSize = {
         * 	e_custom : 0
         * 	e_letter : 1
         * 	e_letter_small : 2
         * 	e_tabloid : 3
         * 	e_ledger : 4
         * 	e_legal : 5
         * 	e_statement : 6
         * 	e_executive : 7
         * 	e_a3 : 8
         * 	e_a4 : 9
         * 	e_a4_mall : 10
         * 	e_a5 : 11
         * 	e_b4_jis : 12
         * 	e_b5_jis : 13
         * 	e_folio : 14
         * 	e_quarto : 15
         * 	e_10x14 : 16
         * 	e_11x17 : 17
         * 	e_note : 18
         * 	e_envelope_9 : 19
         * 	e_envelope_10 : 20
         * 	e_envelope_11 : 21
         * 	e_envelope_12 : 22
         * 	e_envelope_14 : 23
         * 	e_c_size_sheet : 24
         * 	e_d_size_sheet : 25
         * 	e_e_size_sheet : 26
         * 	e_envelope_dl : 27
         * 	e_envelope_c5 : 28
         * 	e_envelope_c3 : 29
         * 	e_envelope_c4 : 30
         * 	e_envelope_c6 : 31
         * 	e_envelope_c65 : 32
         * 	e_envelope_b4 : 33
         * 	e_envelope_b5 : 34
         * 	e_envelope_b6 : 35
         * 	e_envelope_italy : 36
         * 	e_envelope_monarch : 37
         * 	e_6_3_quarters_envelope : 38
         * 	e_us_std_fanfold : 39
         * 	e_german_std_fanfold : 40
         * 	e_german_legal_fanfold : 41
         * 	e_b4_iso : 42
         * 	e_japanese_postcard : 43
         * 	e_9x11 : 44
         * 	e_10x11 : 45
         * 	e_15x11 : 46
         * 	e_envelope_invite : 47
         * 	e_reserved_48 : 48
         * 	e_reserved_49 : 49
         * 	e_letter_extra : 50
         * 	e_legal_extra : 51
         * 	e_tabloid_extra : 52
         * 	e_a4_extra : 53
         * 	e_letter_transverse : 54
         * 	e_a4_transverse : 55
         * 	e_letter_extra_transverse : 56
         * 	e_supera_supera_a4 : 57
         * 	e_Superb_Superb_a3 : 58
         * 	e_letter_plus : 59
         * 	e_a4_plus : 60
         * 	e_a5_transverse : 61
         * 	e_b5_jis_transverse : 62
         * 	e_a3_extra : 63
         * 	e_a5_extra : 64
         * 	e_b5_iso_extra : 65
         * 	e_a2 : 66
         * 	e_a3_transverse : 67
         * 	e_a3_extra_transverse : 68
         * 	e_japanese_double_postcard : 69
         * 	e_a6 : 70
         * 	e_japanese_envelope_kaku_2 : 71
         * 	e_japanese_envelope_kaku_3 : 72
         * 	e_japanese_envelope_chou_3 : 73
         * 	e_japanese_envelope_chou_4 : 74
         * 	e_letter_rotated : 75
         * 	e_a3_rotated : 76
         * 	e_a4_rotated : 77
         * 	e_a5_rotated : 78
         * 	e_b4_jis_rotated : 79
         * 	e_b5_jis_rotated : 80
         * 	e_japanese_postcard_rotated : 81
         * 	e_double_japanese_postcard_rotated : 82
         * 	e_a6_rotated : 83
         * 	e_japanese_envelope_kaku_2_rotated : 84
         * 	e_japanese_envelope_kaku_3_rotated : 85
         * 	e_japanese_envelope_chou_3_rotated : 86
         * 	e_japanese_envelope_chou_4_rotated : 87
         * 	e_b6_jis : 88
         * 	e_b6_jis_rotated : 89
         * 	e_12x11 : 90
         * 	e_japanese_envelope_you_4 : 91
         * 	e_japanese_envelope_you_4_rotated : 92
         * 	e_PrinterMode_prc_16k : 93
         * 	e_prc_32k : 94
         * 	e_prc_32k_big : 95
         * 	e_prc_envelop_1 : 96
         * 	e_prc_envelop_2 : 97
         * 	e_prc_envelop_3 : 98
         * 	e_prc_envelop_4 : 99
         * 	e_prc_envelop_5 : 100
         * 	e_prc_envelop_6 : 101
         * 	e_prc_envelop_7 : 102
         * 	e_prc_envelop_8 : 103
         * 	e_prc_envelop_9 : 104
         * 	e_prc_envelop_10 : 105
         * 	e_prc_16k_rotated : 106
         * 	e_prc_32k_rotated : 107
         * 	e_prc_32k_big__rotated : 108
         * 	e_prc_envelop_1_rotated : 109
         * 	e_prc_envelop_2_rotated : 110
         * 	e_prc_envelop_3_rotated : 111
         * 	e_prc_envelop_4_rotated : 112
         * 	e_prc_envelop_5_rotated : 113
         * 	e_prc_envelop_6_rotated : 114
         * 	e_prc_envelop_7_rotated : 115
         * 	e_prc_envelop_8_rotated : 116
         * 	e_prc_envelop_9_rotated : 117
         * 	e_prc_envelop_10_rotated : 118
         * }
         * </pre>
         * Paper size to use for produced PDF.
         */
        setPaperSize(size: number): Promise<void>;
        /**
         * Manually set the paper dimensions of the produced PDF.
         * @param width - Width of the page, e.g. "4cm".
         * @param height - Height of the page, eg. "12in".
         */
        setPaperSize2(width: string, height: string): Promise<void>;
        /**
         * Set page orientation for output PDF.
         * @param enable - If true generated PDF pages will be orientated to
         * landscape, otherwise orientation will be portrait.
         */
        setLandscape(enable: boolean): Promise<void>;
        /**
         * Change the DPI explicitly for the output PDF.
         * @param dpi - Dots per inch, e.g. 80.
         */
        setDPI(dpi: number): Promise<void>;
        /**
         * Add bookmarks to the PDF.
         * @param enable - If true bookmarks will be generated for the
         * produced PDF.
         * @param [depth] - Maximum depth of the outline (e.g. 4).
         */
        setOutline(enable: boolean, depth?: number): Promise<void>;
        /**
         * Save outline to a xml file.
         * @param xml_file - Path of where xml data representing outline
         * of produced PDF should be saved to.
         */
        dumpOutline(xml_file: string): Promise<void>;
        /**
         * Use loss less compression to create PDF.
         * @param enable - If true loss less compression will be used to
         * create PDF.
         */
        setPDFCompression(enable: boolean): Promise<void>;
        /**
         * Set margins of generated PDF.
         * @param top - Size of the top margin, e.g. "2cm".
         * @param bottom - Size of the bottom margin, e.g. "2cm".
         * @param left - Size of the left margin, e.g. "2cm".
         * @param right - Size of the right margin, e.g. "2cm".
         */
        setMargins(top: string, bottom: string, left: string, right: string): Promise<void>;
        /**
         * Maximum DPI to use for images in the generated PDF.
         * @param dpi - Maximum dpi of images in produced PDF, e.g. 80.
         */
        setImageDPI(dpi: number): Promise<void>;
        /**
         * JpEG compression factor to use when generating PDF.
         * @param quality - Compression factor, e.g. 92.
         */
        setImageQuality(quality: number): Promise<void>;
        /**
         * Path of file used for loading and storing cookies.
         * @param path - Path to file used for loading and storing cookies.
         */
        setCookieJar(path: string): Promise<void>;
        /**
         * Display HTML to PDF conversion progress, warnings, and errors, to stdout.
         * @param quiet - If false, progress information is sent to stdout during conversion.
         */
        setQuiet(quiet: boolean): Promise<void>;
        /**
         * Set the only location that PDFNet will look for the html2pdf module.
         * @param path - A folder or file path. If non-empty, PDFNet will only
         * look in path for the html2pdf module, otherwise it will search in
         * the default locations for the module.
         */
        static setModulePath(path: string): Promise<void>;
    }
    namespace HTML2PDF {
        /**
         * Proxy settings to be used when loading content from web pages.
         */
        class Proxy extends PDFNet.Destroyable {
            /**
             * Constructor
             * @returns A promise that resolves to an object of type: "PDFNet.HTML2PDF.Proxy"
             */
            static create(): Promise<PDFNet.HTML2PDF.Proxy>;
            /**
             * @param type - <pre>
             * PDFNet.HTML2PDF.Proxy.Type = {
             * 	e_default : 0
             * 	e_none : 1
             * 	e_http : 2
             * 	e_socks5 : 3
             * }
             * </pre>
             */
            setType(type: number): Promise<void>;
            /**
             * Set the port number to use
             * @param port - A valid port number, e.g. 3128.
             */
            setPort(port: number): Promise<void>;
            /**
             * Set the proxy host to use.
             * @param host - String defining the host name, e.g. "myserver" or "www.xxx.yyy.zzz"
             */
            setHost(host: string): Promise<void>;
            /**
             * Set the username to use when logging into the proxy
             * @param username - The login name, e.g. "elbarto".
             */
            setUsername(username: string): Promise<void>;
            /**
             * Set the password to use when logging into the proxy with username
             * @param password - The password to use, e.g. "bart".
             */
            setPassword(password: string): Promise<void>;
        }
        /**
         * Settings for table of contents.
         */
        class TOCSettings extends PDFNet.Destroyable {
            /**
             * Constructor
             * @returns A promise that resolves to an object of type: "PDFNet.HTML2PDF.TOCSettings"
             */
            static create(): Promise<PDFNet.HTML2PDF.TOCSettings>;
            /**
             * Use a dotted line when creating TOC.
             * @param enable - Table of contents will use dotted lines.
             */
            setDottedLines(enable: boolean): Promise<void>;
            /**
             * Create links from TOC to actual content.
             * @param enable - Entries in table of contents will
             * link to section in the PDF.
             */
            setLinks(enable: boolean): Promise<void>;
            /**
             * Caption text to be used with TOC.
             * @param caption - Text that will appear with the table of contents.
             */
            setCaptionText(caption: string): Promise<void>;
            /**
             * Indentation used for every TOC level...
             * @param indentation - How much to indent each level, e.g. "2"
             */
            setLevelIndentation(indentation: number): Promise<void>;
            /**
             * How much to shrink font for every level, e.g. 0.8
             * @param shrink - Rate at which lower level entries will appear smaller
             */
            setTextSizeShrink(shrink: number): Promise<void>;
            /**
             * xsl style sheet used to convert outline XML into a
             * table of content.
             * @param style_sheet - Path to xsl style sheet to be used to generate
             * this table of contents.
             */
            setXsl(style_sheet: string): Promise<void>;
        }
        /**
         * Settings that control how a web page is opened and converted to PDF.
         */
        class WebPageSettings extends PDFNet.Destroyable {
            /**
             * Constructor
             * @returns A promise that resolves to an object of type: "PDFNet.HTML2PDF.WebPageSettings"
             */
            static create(): Promise<PDFNet.HTML2PDF.WebPageSettings>;
            /**
             * Print the background of this web page.
             * @param background - If true background is printed.
             */
            setPrintBackground(background: boolean): Promise<void>;
            /**
             * Print the images of this web page.
             * @param load - If true images are printed.
             */
            setLoadImages(load: boolean): Promise<void>;
            /**
             * Allow javascript from this web page to be run.
             * @param enable - If true javascript's are allowed.
             */
            setAllowJavaScript(enable: boolean): Promise<void>;
            /**
             * Allow intelligent shrinking to fit more content per page.
             * @param enable - If true intelligent shrinking is enabled and
             * the pixel/dpi ratio is non constant.
             */
            setSmartShrinking(enable: boolean): Promise<void>;
            /**
             * Set the smallest font size allowed, e.g 9.
             * @param size - No fonts will appear smaller than this.
             */
            setMinimumFontSize(size: number): Promise<void>;
            /**
             * Default encoding to be used when not specified by the web page.
             * @param encoding - Default encoding, e.g. utf-8 or iso-8859-1.
             */
            setDefaultEncoding(encoding: string): Promise<void>;
            /**
             * Url or path to user specified style sheet.
             * @param url - URL or file path to user style sheet to be used
             * with this web page.
             */
            setUserStyleSheet(url: string): Promise<void>;
            /**
             * Allow Netscape and flash plugins from this web page to
             * be run. Enabling will have limited success.
             * @param enable - If true Netscape & flash plugins will be run.
             */
            setAllowPlugins(enable: boolean): Promise<void>;
            /**
             * Controls how content will be printed from this web page.
             * @param print - If true the print media type will be used, otherwise
             * the screen media type will be used to print content.
             */
            setPrintMediaType(print: boolean): Promise<void>;
            /**
             * Add sections from this web page to the outline and
             * table of contents.
             * @param include - If true PDF pages created from this web
             * page will show up in the outline, and table of contents,
             * otherwise, produced PDF pages will be excluded.
             */
            setIncludeInOutline(include: boolean): Promise<void>;
            /**
             * HtTP authentication username to use when logging into the website.
             * @param username - The login name to use with the server, e.g. "bart".
             */
            setUsername(username: string): Promise<void>;
            /**
             * HtTP authentication password to use when logging into the website.
             * @param password - The login password to use with the server, e.g. "elbarto".
             */
            setPassword(password: string): Promise<void>;
            /**
             * Amount of time to wait for a web page to start printing after
             * it's completed loading. Converter will wait a maximum of msec milliseconds
             * for javascript to call window.print().
             * @param msec - Maximum wait time in milliseconds, e.g. 1200.
             */
            setJavaScriptDelay(msec: number): Promise<void>;
            /**
             * Zoom factor to use when loading object.
             * @param zoom - How much to magnify the web content by, e.g. 2.2.
             */
            setZoom(zoom: number): Promise<void>;
            /**
             * Allow local and piped files access to other local files.
             * @param block - If true local files will be inaccessible.
             */
            setBlockLocalFileAccess(block: boolean): Promise<void>;
            /**
             * Stop slow running javascript's.
             * @param stop - If true, slow running javascript's will be stopped.
             */
            setStopSlowScripts(stop: boolean): Promise<void>;
            /**
             * Forward javascript warnings and errors to output.
             * @param forward - If true javascript errors and warnings will be forwarded
             * to stdout and the log.
             */
            setDebugJavaScriptOutput(forward: boolean): Promise<void>;
            /**
             * @param val - <pre>
             * PDFNet.HTML2PDF.WebPageSettings.ErrorHandling = {
             * 	e_abort : 0
             * 	e_skip : 1
             * 	e_ignore : 2
             * }
             * </pre>
             */
            setLoadErrorHandling(val: number): Promise<void>;
            /**
             * Convert external links in HTML document to external
             * PDF links.
             * @param convert - If true PDF pages produced from this web page
             * can have external links.
             */
            setExternalLinks(convert: boolean): Promise<void>;
            /**
             * Convert internal links in HTML document into PDF references.
             * @param convert - If true PDF pages produced from this web page
             * will have links to other PDF pages.
             */
            setInternalLinks(convert: boolean): Promise<void>;
            /**
             * Turn HTML forms into PDF forms.
             * @param forms - If true PDF pages produced from this web page
             * will have PDF forms for any HTML forms the web page has.
             */
            setProduceForms(forms: boolean): Promise<void>;
            /**
             * Use this proxy to load content from this web page.
             * @param proxy - Contains settings for proxy
             */
            setProxy(proxy: PDFNet.HTML2PDF.Proxy): Promise<void>;
        }
        namespace WebPageSettings {
            enum ErrorHandling {
                e_abort,
                e_skip,
                e_ignore
            }
        }
        namespace Proxy {
            enum Type {
                e_default,
                e_none,
                e_http,
                e_socks5
            }
        }
    }
    /**
     * A Highlight annotation covers a word or a group of contiguous words with partially transparent
     * color.
     */
    class HighlightAnnot extends PDFNet.TextMarkupAnnot {
        /**
         * creates a Highlight annotation and initializes it using given Cos/SDF object.
         * @param d - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.HighlightAnnot"
         */
        static createFromObj(d: PDFNet.Obj): Promise<PDFNet.HighlightAnnot>;
        /**
         * creates a Highlight annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Highlight annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.HighlightAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.HighlightAnnot>;
        /**
         * Creates a new Highlight annotation in the specified document.
         * @param doc - A document to which the Highlight annotation is added.
         * @param pos - A rectangle specifying the Highlight annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Highlight annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.HighlightAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
    }
    /**
     * Highlights is used to store the necessary information and perform certain
     * tasks in accordance with Adobe's Highlight standard, whose details can be
     * found at:
     *
     * http://partners.adobe.com/public/developer/en/pdf/HighlightFileFormat.pdf
     *
     * In a nutshell, the Highlights class maintains a set of highlights.
     * Each highlight contains three pieces of information:
     *
     * page: the number of the page this Highlight is on;
     * position: the start position (text offset) of this Highlight;
     * length: the length of this Highlight.
     *
     * Possible use case scenarios for Highlights include:
     *  - Load a Highlight file (in XML format) and highlight the corresponding
     *    texts in the viewer (e.g., if the viewer is implemented using PDFViewCtrl,
     *    it can be achieved simply by calling PDFViewCtrl::SelectByHighlights()
     *    method);
     *  - Save the Highlight information (e.g., constructed by the TextSearch
     *    class) to an XML file for external uses.
     *
     * Note:
     *  - The Highlights class does not maintain the corresponding PDF document for
     *    its highlights. It is the user's responsibility to match them up.
     *  - The Highlights class ensures that each highlight it maintains is
     *    unique (no two highlights have the same page, position and length values).
     *  - The current implementation of Highlights only supports the 'characters'
     *    encoding for 'units' as described in the format; the 'words' encoding is
     *    not supported at this point.
     *
     *
     * For a sample code, please take a look at the TextSearchTest sample project.
     */
    class Highlights extends PDFNet.Destroyable {
        /**
         * Constructor and destructor.
         * @returns A promise that resolves to an object of type: "PDFNet.Highlights"
         */
        static create(): Promise<PDFNet.Highlights>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Highlights"
         */
        copyCtor(): Promise<PDFNet.Highlights>;
        /**
         * Add highlights.
         * @param hlts - the Highlights instance containing the highlights to be added.
         */
        add(hlts: PDFNet.Highlights): Promise<void>;
        /**
         * Load the Highlight information from a file. Note that the
         * pre-existing Highlight information is discarded.
         * @param file_name - the name of the file to load from.
         */
        load(file_name: string): Promise<void>;
        /**
         * Save the current Highlight information in the class to a file.
         * @param file_name - the name of the file to save to.
         */
        save(file_name: string): Promise<void>;
        /**
         * Clear the current Highlight information in the class.
         */
        clear(): Promise<void>;
        /**
         * Rewind the internal pointer to the first highlight.
         * @param doc - the PDF document to which the highlights correspond.
         */
        begin(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<void>;
        /**
         * Query if there is any subsequent highlight after the current highlight.
         * @returns A promise that resolves to an object of type: "boolean"
         */
        hasNext(): Promise<boolean>;
        /**
         * Move the current highlight to the next highlight.
         */
        next(): Promise<void>;
        /**
         * Get the page number of the current highlight.
         * @returns A promise that resolves to an object of type: "number"
         */
        getCurrentPageNumber(): Promise<number>;
        /**
         * Retrieves an array of Quads (each quad contains 4 points) representing the regions that were
        highlighted.
         * @returns A promise that resolves to an object of type: "number" (generated documentation)
         */
        getCurrentQuads(): Promise<PDFNet.QuadPoint[]>;
    }
    /**
     * Image class provides common methods for working with PDF images.
     */
    class Image {
        /**
         * Create and embed an Image from an external file taking into account specified
         * compression hints.
         *
         * By default the function will either pass-through data preserving the original
         * compression or will compress data using Flate compression. It is possible to
         * fine tune compression or to select a different compression algorithm using
         * 'encoder_hints' object.
         * @param doc - A document to which the image should be added. To obtain
         * SDF::Doc from PDFDoc use PDFDoc::GetSDFDoc() or Obj::GetDoc().
         * @param filename - The name of the image file. Currently supported formats are
         * JPEG, PNG, GIF, TIFF, BMP, EMF, and WMF. Other raster formats can be embedded by
         * decompressing image data and using other versions of Image::Create(...) method.
         * @param [encoder_hints] - An optional SDF::Obj containing a hint (or an SDF::Array of
         * hints) that could be used to select a specific compression method and compression
         * parameters. For a concrete example of how to create encoder hints, please take a look
         * at JBIG2Test and AddImage sample projects. The image encoder accepts the following
         * hints:
         *
         *  /JBIG2; SDF::Name("JBIG2"), An SDF::Name Object with value equal to "JBIG2". If the
         * 	image is monochrome (i.e. bpc == 1), the encoder will compress the image using JBIG2Decode
         * 	filter.
         * 	Note that JBIG2 compression is not recommended for use on scanned text/financial documents or equivalent
         *  since its lossless nature can lead to similar looking numbers or characters being replaced.
         *
         *  [/JBIG2 /Threshold 0.6 /SharePages 50] Compress a monochrome image using lossy JBIG2Decode
         *  compression with the given image threshold and by sharing segments from a specified number
         *  of pages. The threshold is a floating point number in the range from 0.4 to 0.9. Increasing the threshold
         *  value will increase image quality, but may increase the file size. The default value
         *  for threshold is 0.85. "SharePages" parameter can be used to specify the maximum number of
         *  pages sharing a common 'JBIG2Globals' segment stream. Increasing the value of this parameter
         *  improves compression ratio at the expense of memory usage.
         *
         * 	- [/CCITT] Compress a monochrome (i.e. bpc == 1) image using CCITT Group 4 compression. This algorithm typically produces
         * 				larger output than JBIG2, but is lossless. This makes it much more suitable for scanned text documents.
         * 				CCITT is the best option for more general monochrome compression use cases, since JBIG2 has potential to change image content.
         *
         *  [/JPEG] Use JPEG compression with default compression.
         *  [/JPEG /Quality 60] Use JPEG compression with given quality setting. The "Quality"
         * 	value is expressed on the 0..100 scale.
         *
         *  [/JP2] or [/JPEG2000] Use JPEG2000 compression to compress a RGB or a grayscale image
         *
         *  [/Flate] Use Flate compression with maximum compression at the expense of
         * 	speed.
         *
         *  [/Flate /Level 9] Use Flate compression using specified compression level.
         * 	Compression "Level" must be a number between 0 and 9: 1 gives best speed,
         * 	9 gives best compression, 0 gives no compression at all (the input data is simply
         * 	copied a block at a time).
         *
         *  /RAW or [/RAW] The encoder will not use any compression method and the image
         * 	will be stored in the raw format.
         * @returns A promise that resolves to pDF::Image object representing the embedded image.
         */
        static createFromFile(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, filename: string, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * Create and embed an Image. Embed the raw image data taking into account
         * specified compression hints.
         *
         * By default the function will compress all images using Flate compression.
         * It is possible to fine tune compression or to select a different compression
         * algorithm using 'encoder_hints' object.
         * @param doc - A document to which the image should be added. The 'Doc' object
         * can be obtained using Obj::GetDoc() or PDFDoc::GetSDFDoc().
         * @param buf - The stream or buffer containing image data. The image data must
         * not be compressed and must follow PDF format for sample representation (please refer
         * to section 4.8.2 'Sample Representation' in PDF Reference Manual for details).
         * @param width - The width of the image, in samples.
         * @param height - The height of the image, in samples.
         * @param bpc - The number of bits used to represent each color component.
         * @param color_space - The color space in which image samples are represented.
         * @param [encoder_hints] - An optional parameter that can be used to fine tune
         * compression or to select a different compression algorithm. See Image::Create()
         * for details.
         * @returns A promise that resolves to pDF::Image object representing the embedded image.
         */
        static createFromMemory(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, width: number, height: number, bpc: number, color_space: PDFNet.ColorSpace, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createFromMemory2(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createFromStream(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, image_data: PDFNet.FilterReader, width: number, height: number, bpc: number, color_space: PDFNet.ColorSpace, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createFromStream2(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, image_data: PDFNet.Filter, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * Create and embed an ImageMask. Embed the raw image data taking into account
         * specified compression hints. The ImageMask can be used as a stencil mask for
         * painting in the current color or as an explicit mask specifying which areas of
         * the image to paint and which to mask out. One of the most important uses of
         * stencil masking is for painting character glyphs represented as bitmaps.
         * @param doc - A document to which the image should be added. The 'Doc' object
         * can be obtained using Obj::GetDoc() or PDFDoc::GetSDFDoc().
         * @param buf - The stream or buffer containing image data stored in 1 bit per
         * sample format. The image data must not be compressed and must follow PDF format for
         * sample representation (please refer to section 4.8.2 'Sample Representation' in PDF
         * Reference Manual for details).
         * @param width - The width of the image, in samples.
         * @param height - The height of the image, in samples.
         * @param [encoder_hints] - An optional parameter that can be used to fine tune
         * compression or to select a different compression algorithm. See Image::Create()
         * for details.
         * @returns A promise that resolves to pDF::Image object representing the embedded ImageMask.
         */
        static createImageMask(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, width: number, height: number, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createImageMaskFromStream(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, image_data: PDFNet.FilterReader, width: number, height: number, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * create and embed a Soft Mask. Embed the raw image data taking into account
         * specified compression hints.
         * A soft-mask image (see "Soft-Mask Images" in PDF Reference Manual) is
         * used as a source of mask shape or mask opacity values in the transparent
         * imaging model.
         * @param doc - A document to which the image should be added. The 'Doc' object
         * can be obtained using Obj::GetDoc() or PDFDoc::GetSDFDoc().
         * @param buf - The stream or buffer containing image data represented in
         * DeviceGray color space (i.e. one component per sample). The image data must not
         * be compressed and must follow PDF format for sample representation (please refer
         * to section 4.8.2 'Sample Representation' in PDF Reference Manual for details).
         * @param width - The width of the image, in samples.
         * @param height - The height of the image, in samples.
         * @param bpc - The number of bits used to represent each color component.
         * @param [encoder_hints] - An optional parameter that can be used to fine tune
         * compression or to select a different compression algorithm. See Image::Create()
         * for details.
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createSoftMask(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, width: number, height: number, bpc: number, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createSoftMaskFromStream(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, image_data: PDFNet.FilterReader, width: number, height: number, bpc: number, encoder_hints?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * @param input_format - <pre>
         * PDFNet.Image.InputFilter = {
         * 	e_none : 0
         * 	e_jpeg : 1
         * 	e_jp2 : 2
         * 	e_flate : 3
         * 	e_g3 : 4
         * 	e_g4 : 5
         * 	e_ascii_hex : 6
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createDirectFromMemory(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, width: number, height: number, bpc: number, color_space: PDFNet.ColorSpace, input_format: number): Promise<PDFNet.Image>;
        /**
         * @param input_format - <pre>
         * PDFNet.Image.InputFilter = {
         * 	e_none : 0
         * 	e_jpeg : 1
         * 	e_jp2 : 2
         * 	e_flate : 3
         * 	e_g3 : 4
         * 	e_g4 : 5
         * 	e_ascii_hex : 6
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createDirectFromStream(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, image_data: PDFNet.FilterReader, width: number, height: number, bpc: number, color_space: PDFNet.ColorSpace, input_format: number): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        static createFromObj(image_xobject?: PDFNet.Obj): Promise<PDFNet.Image>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Image"
         */
        copy(): Promise<PDFNet.Image>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to whether this is a valid raster image. If the function returns false the
         * underlying SDF/Cos object is not a valid raster image and this Image object should
         * be treated as null.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a stream (filter) containing decoded image data
         */
        getImageData(): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to the size of image data in bytes
         */
        getImageDataSize(): Promise<number>;
        /**
         * @returns A promise that resolves to the SDF object representing the color space in which image
         * samples are specified or NULL if:
         *  the image is an image mask
         *  or is compressed using JPXDecode with missing ColorSpace entry in image dictionary.
         *
         * The returned color space may be any type of color space except Pattern.
         */
        getImageColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to the width of the image, in samples.
         */
        getImageWidth(): Promise<number>;
        /**
         * @returns A promise that resolves to the height of the image, in samples.
         */
        getImageHeight(): Promise<number>;
        /**
         * @returns A promise that resolves to decode array or NULL if the parameter is not specified. A decode object is an
         * array of numbers describing how to map image samples into the range of values
         * appropriate for the images color space . If ImageMask is true, the array must be
         * either [0 1] or [1 0]; otherwise, its length must be twice the number of color
         * components required by ColorSpace. Default value depends on the color space,
         * See Table 4.36 in PDF Ref. Manual.
         */
        getDecodeArray(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the number of bits used to represent each color component. Only a
         * single value may be specified; the number of bits is the same for all color
         * components. Valid values are 1, 2, 4, 8, and 16.
         */
        getBitsPerComponent(): Promise<number>;
        /**
         * @returns A promise that resolves to the number of color components per sample.
         */
        getComponentNum(): Promise<number>;
        /**
         * @returns A promise that resolves to a boolean indicating whether the inline image is to be treated as an image mask.
         */
        isImageMask(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a boolean indicating whether image interpolation is to be performed.
         */
        isImageInterpolate(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an image XObject defining an image mask to be applied to this image (See
         * 'Explicit Masking', 4.8.5), or an array specifying a range of colors
         * to be applied to it as a color key mask (See 'Color Key Masking').
         *
         * If IsImageMask() return true, this method will return NULL.
         */
        getMask(): Promise<PDFNet.Obj>;
        /**
         * set an Explicit Image Mask.
         * @param image_mask - An Image object which serves as an explicit mask for the
         * base (this) image. The base image and the image mask need not have the
         * same resolution (Width and Height values), but since all images are defined on
         * the unit square in user space, their boundaries on the page will coincide; that is,
         * they will overlay each other. The image mask indicates which places on the page
         * are to be painted and which are to be masked out (left unchanged). Unmasked areas
         * are painted with the corresponding portions of the base image; masked areas
         * are not.
         */
        setMask(image_mask: PDFNet.Image): Promise<void>;
        /**
         * set a Color Key Mask.
         * @param mask - is an Cos/SDF array specifying a range of colors to be masked
         * out. Samples in the image that fall within this range are not painted, allowing
         * the existing background to show through. The effect is similar to that of the
         * video technique known as chroma-key. For details of the array format please
         * refer to section 4.8.5 'Color Key Masking' in PDF Reference Manual.
         */
        setMaskWithObj(mask: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to an image XObject defining a Soft Mask to be applied to this image
         * (See section 7.5.4 'Soft-Mask Images' in PDF Reference Manual), or NULL
         * if the image does not have the soft mask.
         */
        getSoftMask(): Promise<PDFNet.Obj>;
        /**
         * set a Soft Mask.
         * @param soft_mask - is a subsidiary Image object defining a soft-mask image
         * (See section 7.5.4 'Soft-Mask Images' in PDF Reference Manual) to be used
         * as a source of mask shape or mask opacity values in the transparent imaging
         * model. The alpha source parameter in the graphics state determines whether
         * the mask values are interpreted as shape or opacity.
         */
        setSoftMask(soft_mask: PDFNet.Image): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.GState.RenderingIntent = {
         * 	e_absolute_colorimetric : 0
         * 	e_relative_colorimetric : 1
         * 	e_saturation : 2
         * 	e_perceptual : 3
         * }
         * </pre>
         * @returns A promise that resolves to the color rendering intent to be used in rendering the image.
         */
        getImageRenderingIntent(): Promise<number>;
        /**
         * Saves this image to a file.
         *
         * The output image format (TIFF, JPEG, or PNG) will be
         * automatically selected based on the properties of the embedded
         * image. For example, if the embedded image is using CCITT Fax
         * compression, the output format will be TIFF. Similarly, if the
         * embedded image is using JPEG compression the output format will
         * be JPEG. If your application needs to explicitly control output
         * image format you may want to use ExportAsTiff() or ExportAsPng().
         * @param filename - string that specifies the path name for
         * the saved image. The filename should not include the extension
         * which will be appended to the filename string based on the output
         * format.
         * @returns A promise that resolves to the number indicating the selected image format:
         * (0 PNG, 1 TIF, 2 JPEG).
         */
        export(filename: string): Promise<number>;
        /**
         * Saves this image to the output stream.
         * (0 PNG, 1 TIF, 2 JPEG).
         * @param writer - A pointer to FilterWriter used to write to the
         * output stream. If the parameter is null, nothing will be written
         * to the output stream, but the function returns the format identifier.
         * @returns A promise that resolves to the number indicating the selected image format:
         */
        exportFromStream(writer: PDFNet.FilterWriter): Promise<number>;
        /**
         * Saves this image to a TIFF file.
         * @param filename - string that specifies the path name for
         * the saved image. The filename should include the file extension
         */
        exportAsTiff(filename: string): Promise<void>;
        /**
         * Saves this image to a TIFF output stream.
         * @param writer - FilterWriter used to write to the output stream.
         */
        exportAsTiffFromStream(writer: PDFNet.FilterWriter): Promise<void>;
        /**
         * Saves this image to a PNG file.
         * @param filename - string that specifies the path name for
         * the saved image. The filename should include the file extension
         */
        exportAsPng(filename: string): Promise<void>;
        /**
         * Saves this image to a PNG output stream.
         * @param writer - FilterWriter used to write to the output stream.
         */
        exportAsPngFromStream(writer: PDFNet.FilterWriter): Promise<void>;
        /**
         * This function will fully download the image url as a memory buffer and embed it in the supplied PDFDoc.
         * @param doc - the PDF document in which to embed the image
         * @param url - The image url to download and embed.
         * @param [options] - Additional options
         * @param options.withCredentials - Whether to set the withCredentials property on the XMLHttpRequest
         * @param options.customHeaders - An object containing custom HTTP headers to be used when downloading the document
         * @returns A promise that resolves to an object of type: "Image"
         */
        static createFromURL(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, url: string, encoder_hints?: number, options?: {
            withCredentials: boolean;
            customHeaders: any;
        }): Promise<PDFNet.Image>;
    }
    /**
     * An ink annotation (PDF 1.3) represents a freehand "scribble" composed
     * of one or more disjoint paths. When opened, it shall display a pop-up
     * window containing the text of the associated note.
     */
    class InkAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates an Ink annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.InkAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.InkAnnot>;
        /**
         * creates an Ink annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Ink annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.InkAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.InkAnnot>;
        /**
         * Creates a new Ink annotation in the specified document.
         * @param doc - A document to which the Ink annotation is added.
         * @param pos - A rectangle specifying the Ink annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Ink annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.InkAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns number of paths in the annotation.
         * @returns A promise that resolves to an integer representing the number of paths in the 'InkList'
         * entry of the annotation dictionary.
         */
        getPathCount(): Promise<number>;
        /**
         * Returns number of points in a certain stroked path in the InkList.
         * @param pathindex - path index for each the point count is returned. Index starts at 0.
         * @returns A promise that resolves to an integer representing the number of points in the stroked path of the Ink list.
         */
        getPointCount(pathindex: number): Promise<number>;
        /**
         * Returns the specific point in a given path.
         * @param pathindex - path index for each the point is returned. Index starts at 0.
         * @param pointindex - index of point in the path. Index starts at 0.
         * @returns A promise that resolves to a Point object for specified path and point index.
         */
        getPoint(pathindex: number, pointindex: number): Promise<PDFNet.Point>;
        /**
         * sets the specific point in the Ink List, adding it if needed.
         * @param pathindex - An unsigned integer indicating the index of the path.
         * @param pointindex - An unsigned integer indicating the index of the point
         * within the stroked path indicated by the parameter "pathindex".
         * @param pt - A Point object that is to be assigned.
         */
        setPoint(pathindex: number, pointindex: number, pt: PDFNet.Point): Promise<void>;
        /**
         * Erase a rectangle area formed by pt1pt2 with width
         * @param pt1 - A point object that is one end of the eraser segment
         * @param pt2 - A point object that is the other end of the eraser segment
         * @param width - The half width of the eraser
         * @returns A promise that resolves to whether an ink stroke was erased
         */
        erase(pt1: PDFNet.Point, pt2: PDFNet.Point, width: number): Promise<boolean>;
        /**
         * Retrieves whether the Ink will draw like a highlighter.
         * @returns A promise that resolves to true if the Ink will draw like a highlighter. (multiply blend mode)
         * If false it will draw in normal mode. (normal blend mode)
         */
        getHighlightIntent(): Promise<boolean>;
        /**
         * Enables or disables the Ink drawing like a highlighter.
         * @param highlight - true if the Ink will draw like a highlighter. (multiply blend mode)
         * If false it will draw in normal mode. (normal blend mode)
         */
        setHighlightIntent(highlight: boolean): Promise<void>;
    }
    /**
     * Supports a simple iteration over a generic collection.
     */
    class Iterator<T> extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to true if the iterator can be successfully advanced to the
         * next element; false if the iterator is no longer valid.
         */
        hasNext(): Promise<boolean>;
        /**
         * Advances the iterator to the next element of the collection.
         */
        next(): Promise<void>;
        /**
         * @returns A promise that resolves to an object which this iterator points to.
         */
        current(): Promise<T>;
    }
    /**
     * [Missing documentation]
     */
    class KeyStrokeActionResult extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "string"
         */
        getText(): Promise<string>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.KeyStrokeActionResult"
         */
        copy(): Promise<PDFNet.KeyStrokeActionResult>;
    }
    /**
     * [Missing documentation]
     */
    class KeyStrokeEventData extends PDFNet.Destroyable {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.KeyStrokeEventData"
         */
        static create(field_name: string, current: string, change: string, selection_start: number, selection_end: number): Promise<PDFNet.KeyStrokeEventData>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.KeyStrokeEventData"
         */
        copy(): Promise<PDFNet.KeyStrokeEventData>;
    }
    /**
     * A line annotation (PDF 1.3) is used to display a single straight
     * line on the page. When opened, it should display a pop-up window containing
     * the text of the associated note.
     */
    class LineAnnot extends PDFNet.MarkupAnnot {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.LineAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.LineAnnot>;
        /**
         * creates a Line annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Line annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.LineAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.LineAnnot>;
        /**
         * Creates a new Line annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Line annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.LineAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the coordinates of the start of a line.
         * @returns A promise that resolves to a point specifying the coordinates of the start of the line.
         */
        getStartPoint(): Promise<PDFNet.Point>;
        /**
         * sets the coordinates of the start of a line.
         * @param sp - A point specifying the coordinates of the end of the line.
         */
        setStartPoint(sp: PDFNet.Point): Promise<void>;
        /**
         * Returns the coordinates of the end of a line.
         * @returns A promise that resolves to a point specifying the coordinates of the end of the line.
         */
        getEndPoint(): Promise<PDFNet.Point>;
        /**
         * sets the coordinates of the end of a line.
         * @param ep - A point specifying the coordinates of the end of the line.
         */
        setEndPoint(ep: PDFNet.Point): Promise<void>;
        /**
         * Returns the ending style that applies to the first point of the line.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         * @returns A promise that resolves to a enum value from the "EndingStyle".
         */
        getStartStyle(): Promise<number>;
        /**
         * sets the ending style that applies to the first point of the line.
         * (Optional; PDF 1.4.)
         * @param ss - <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         * A enum value from the "EndingStyle".
         */
        setStartStyle(ss: number): Promise<void>;
        /**
         * Returns the ending style  that applies to the second point of the line.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         * @returns A promise that resolves to a enum value from the "EndingStyle".
         */
        getEndStyle(): Promise<number>;
        /**
         * sets the ending style  that applies to the second point of the line.
         * (Optional; PDF 1.4)
         * @param es - <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         * A enum value from the "EndingStyle".
         */
        setEndStyle(es: number): Promise<void>;
        /**
         * Returns the leader line length of a line.
         * @returns A promise that resolves to a number denoting the length of the leader line in default user space units.
         */
        getLeaderLineLength(): Promise<number>;
        /**
         * Sets the leader line length of a line.
         * (PDF 1.6)
         * @param length - A number denoting the length of the leader line in default user space units.
         */
        setLeaderLineLength(length: number): Promise<void>;
        /**
         * Returns the leader line extension length of a line.
         * @returns A promise that resolves to a number denoting the length of the leader line extension in default user space units.
         */
        getLeaderLineExtensionLength(): Promise<number>;
        /**
         * Sets the leader line extension length of a line.
         * (PDF 1.6)
         * @param length - A number denoting the length of the leader line extension in default user space units.
         */
        setLeaderLineExtensionLength(length: number): Promise<void>;
        /**
         * Returns true if caption is to be shown, otherwise false.
         * @returns A promise that resolves to a boolean value indicating whether the caption will be shown.
         */
        getShowCaption(): Promise<boolean>;
        /**
         * Sets the option of whether to show caption.
         * @param showCaption - A boolean value indicating whether the caption will be shown.
         */
        setShowCaption(showCaption: boolean): Promise<void>;
        /**
         * Returns the intent type of the line.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.LineAnnot.IntentType = {
         * 	e_LineArrow : 0
         * 	e_LineDimension : 1
         * 	e_null : 2
         * }
         * </pre>
         * @returns A promise that resolves to an intent type value from the "IntentType" enum.
         */
        getIntentType(): Promise<number>;
        /**
         * sets the intent type of the line.
         * (For PDF 1.6)
         * @param it - <pre>
         * PDFNet.LineAnnot.IntentType = {
         * 	e_LineArrow : 0
         * 	e_LineDimension : 1
         * 	e_null : 2
         * }
         * </pre>
         * An intent type value from the "IntentType" enum.
         */
        setIntentType(it: number): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.LineAnnot.CapPos = {
         * 	e_Inline : 0
         * 	e_Top : 1
         * }
         * </pre>
         */
        getCapPos(): Promise<number>;
        /**
         * @param it - <pre>
         * PDFNet.LineAnnot.CapPos = {
         * 	e_Inline : 0
         * 	e_Top : 1
         * }
         * </pre>
         */
        setCapPos(it: number): Promise<void>;
        /**
         * Returns the leader line offset length of a line.
         * @returns A promise that resolves to a number denoting the length of the leader line offset in default user space units.
         */
        getLeaderLineOffset(): Promise<number>;
        /**
         * sets the leader line offset length of a line.
         * (PDF 1.7)
         * @param length - A number denoting the length of the leader line offset in default user space units.
         */
        setLeaderLineOffset(length: number): Promise<void>;
        /**
         * Returns the horizontal offset of the caption.
         * @returns A promise that resolves to a number denoting the horizontal offset of the caption in default user space units.
         */
        getTextHOffset(): Promise<number>;
        /**
         * sets the horizontal offset of the caption.
         * (For PDF 1.7 )
         * @param offset - A number denoting the horizontal offset of the caption in default user space units.
         */
        setTextHOffset(offset: number): Promise<void>;
        /**
         * Returns the vertical offset of the caption.
         * @returns A promise that resolves to a number denoting the vertical offset of the caption in default user space units.
         */
        getTextVOffset(): Promise<number>;
        /**
         * sets the vertical offset of the caption.
         * (For PDF 1.7 )
         * @param offset - A number denoting the vertical offset of the caption in default user space units.
         */
        setTextVOffset(offset: number): Promise<void>;
    }
    /**
     * A link annotation represents either a hypertext link to a destination elsewhere in the document
     * or an action to be performed.
     */
    class LinkAnnot extends PDFNet.Annot {
        /**
         * creates a Link annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.LinkAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.LinkAnnot>;
        /**
         * creates a Link annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Link annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.LinkAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.LinkAnnot>;
        /**
         * Creates a new Link annotation in the specified document.
         * @param doc - A document to which the Link annotation is added.
         * @param pos - A rectangle specifying the Link annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Link annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.LinkAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * removes this annotation's action.
         */
        removeAction(): Promise<void>;
        /**
         * Returns the Action of the Link Annotation.
         * @returns A promise that resolves to an Action object of the Link annotation.
         */
        getAction(): Promise<PDFNet.Action>;
        /**
         * sets the Action of the Link Annotation.
         * (Optional; PDF 1.1 )
         * @param action - An Action object that shall be associated with this Link annotation.
         */
        setAction(action: PDFNet.Action): Promise<void>;
        /**
         * Returns the highlighting mode of this Link annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.LinkAnnot.HighlightingMode = {
         * 	e_none : 0
         * 	e_invert : 1
         * 	e_outline : 2
         * 	e_push : 3
         * }
         * </pre>
         * @returns A promise that resolves to the highLighting mode as a value of the enum "HighlightingMode".
         */
        getHighlightingMode(): Promise<number>;
        /**
         * Sets the highlighting mode for this Link annotation.
         * (Optional; PDF 1.2 )
         * @param value - <pre>
         * PDFNet.LinkAnnot.HighlightingMode = {
         * 	e_none : 0
         * 	e_invert : 1
         * 	e_outline : 2
         * 	e_push : 3
         * }
         * </pre>
         * the mode as a value of the enum "HighlightingMode".
         */
        setHighlightingMode(value: number): Promise<void>;
        /**
         * Returns the number of QuadPoints in the 'QuadPoints' array of the Link annotation.
         * @returns A promise that resolves to the number of QuadPoints.
         */
        getQuadPointCount(): Promise<number>;
        /**
         * Returns the QuadPoint located at a certain index of the QuadPoint array of the Link
         * annotation.
         * @param idx - The index of the QuadPoint, starts at zero and should be less than the return value of GetQuadPointCount().
         * @returns A promise that resolves to the QuadPoint located at a certain index of the QuadPoint array of the Link annotation.
         */
        getQuadPoint(idx: number): Promise<PDFNet.QuadPoint>;
        /**
         * set the QuadPoint to be located at a certain index of the QuadPoint array of the Link annotation.
         * (Optional; PDF 1.6 )
         * @param idx - The index of the QuadPoint, starts at zero and should be less than the return value of GetQuadPointCount().
         * @param qp - The QuadPoint to be stored in the annotation.
         */
        setQuadPoint(idx: number, qp: PDFNet.QuadPoint): Promise<void>;
    }
    /**
     * An object representing a List Box used in a PDF Form.
     */
    class ListBoxWidget extends PDFNet.WidgetAnnot {
        /**
         * Creates a new List Box Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param [field_name] - The name of the field for which to create a List Box Widget
         * @returns A promise that resolves to a newly created blank List Box Widget annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field_name?: string): Promise<PDFNet.ListBoxWidget>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Creates a new List Box Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param field - the field for which to create a List Box Widget
         * @returns A promise that resolves to a newly created blank List Box Widget annotation.
         */
        static createWithField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.ListBoxWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ListBoxWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.ListBoxWidget>;
        /**
         * Creates a List Box Widget annotation and initialize it using given annotation object.
         *
         * <p>
         * <b> Note: </b>  The constructor does not copy any data, but is instead the logical
         * equivalent of a type cast.
         * @param annot - The annotation to use.
         * @returns A promise that resolves to an object of type: "PDFNet.ListBoxWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.ListBoxWidget>;
        /**
         * Adds option to List Box Widget.
         * @param value - The option to add
         */
        addOption(value: string): Promise<void>;
        addOptions(option_list: string[]): Promise<void>;
        setSelectedOptions(option_list: string[]): Promise<void>;
        replaceOptions(option_list: string[]): Promise<void>;
        /**
         * Removes the option from List Box widget.
         * @param value - The option to remove.
         */
        removeOption(value: string): Promise<void>;
    }
    /**
     * Markup is a base class for a number of annotations types that
     * are used to mark up PDF documents. These annotations have text that
     * appears as part of the annotation and may be displayed in other ways by a
     * conforming reader, such as in a Comments pane.
     * Markup annotations may be divided into the following groups:
     * - Free text annotations display text directly on the page.
     *   The annotation's Contents entry specifies the displayed text.
     * - Most other markup annotations have an associated pop-up window
     *   that may contain text. The annotation's Contents entry specifies
     *   the text that shall be displayed when the pop-up window is opened.
     *   These include text, line, square, circle, polygon, polyline,
     *   highlight,underline, squiggly-underline, strikeout, rubber stamp,
     *   caret, ink, and file attachment annotations.
     * - Sound annotations do not have a pop-up window but may also have
     *   associated text specified by the Contents entry.
     * - A subset of markup annotations are intended to markup text of a
     *   document (e.g. highlight, strikeout, jagged, underline) and they
     *   are derived from TextMarkup base class.
     */
    class MarkupAnnot extends PDFNet.Annot {
        /**
         * creates a markup annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.MarkupAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.MarkupAnnot>;
        /**
         * creates a markup annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Markup annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.MarkupAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.MarkupAnnot>;
        /**
         * Returns the title of the markup annotation.
         * @returns A promise that resolves to a string representing the title of the markup annotation,
         * or null is the title is not specified.
         */
        getTitle(): Promise<string>;
        /**
         * sets the title of the markup annotation.
         * (Optional; PDF 1.1)
         * @param title - A string.
         */
        setTitle(title: string): Promise<void>;
        /**
         * sets the title of the markup annotation.
         * (Optional; PDF 1.1)
         * @param title - A string.
         */
        setTitleUString(title: string): Promise<void>;
        /**
         * Returns the Popup object associated with this markup annotation.
         * @returns A promise that resolves to a Popup object that is associated with this markup annotation.
         */
        getPopup(): Promise<PDFNet.Annot>;
        /**
         * Sets the Popup object associated with this markup annotation.
         * (Optional; PDF 1.3 )
         * @param ppup - A Popup object that is associated with this markup annotation.
         */
        setPopup(ppup: PDFNet.Annot): Promise<void>;
        /**
         * @returns A promise that resolves to the opacity value.
         * Default value: 1.0.
         */
        getOpacity(): Promise<number>;
        /**
         * Sets the opacity value for the annotation.
         * (Optional; PDF 1.4 )
         * @param op - A number indicating the Markup annotation's opacity value.
         * Default value: 1.0.
         */
        setOpacity(op: number): Promise<void>;
        /**
         * Returns the subject of the Markup annotation.
         * (PDF 1.5)
         * @returns A promise that resolves to a string representing the subject of the Markup annotation.
         */
        getSubject(): Promise<string>;
        /**
         * Sets subject of the Markup annotation.
         * (Optional; PDF 1.5 )
         * @param contents - A string representing the subject of the Markup annotation.
         */
        setSubject(contents: string): Promise<void>;
        /**
         * @returns A promise that resolves to the creation date for the markup annotation.
         */
        getCreationDates(): Promise<PDFNet.Date>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.MarkupAnnot.BorderEffect = {
         * 	e_None : 0
         * 	e_Cloudy : 1
         * }
         * </pre>
         * @returns A promise that resolves to the border effect of the markup annotation.
         * Default value: e_None.
         *
         * Beginning with PDF 1.5, some annotations (square, circle, and polygon) may have a 'BE' entry,
         * which is a border effect dictionary that specifies an effect that shall be applied to the border
         * of the annotations. Beginning with PDF 1.6, the free text annotation may also have a BE entry.
         */
        getBorderEffect(): Promise<number>;
        /**
         * Sets the border effect of the markup annotation. (Optional; PDF 1.5 )
         *
         * Beginning with PDF 1.5, some annotations (square, circle, and polygon) may have a 'BE' entry,
         * which is a border effect dictionary that specifies an effect that shall be applied to the border
         * of the annotations. Beginning with PDF 1.6, the free text annotation may also have a BE entry.
         * @param [effect] - <pre>
         * PDFNet.MarkupAnnot.BorderEffect = {
         * 	e_None : 0
         * 	e_Cloudy : 1
         * }
         * </pre>
         * An entry from the enum "BorderEffect" that
         * represents the border effect of the Markup annotation.
         * Default value: e_None.
         */
        setBorderEffect(effect?: number): Promise<void>;
        /**
         * @returns A promise that resolves to a number describing the intensity of the border effect, in the range 0 to 2.
         *
         * Beginning with PDF 1.5, some annotations (square, circle, and polygon) may have a 'BE' entry,
         * which is a border effect dictionary that specifies an effect that shall be applied to the border
         * of the annotations. Beginning with PDF 1.6, the free text annotation may also have a BE entry.
         */
        getBorderEffectIntensity(): Promise<number>;
        /**
         * sets the border effect intensity of the markup annotation.
         * (Optional; valid only if Border effect is Cloudy)
         *
         * Beginning with PDF 1.5, some annotations (square, circle, and polygon) may have a 'BE' entry,
         * which is a border effect dictionary that specifies an effect that shall be applied to the border
         * of the annotations. Beginning with PDF 1.6, the free text annotation may also have a BE entry.
         * @param [intensity] - A number describing the intensity of the border effect, in the
         * range 0 (which is default) to 2.
         */
        setBorderEffectIntensity(intensity?: number): Promise<void>;
        /**
         * Sets the creation date for the markup annotation.
         * (Optional; PDF 1.5 )
         * @param dt - A Date object indicating the date the markup annotation is created.
         */
        setCreationDates(dt: PDFNet.Date): Promise<void>;
        /**
         * Returns the interior color of the annotation.
         * @returns A promise that resolves to a ColorPt object that denotes the color of the annotation.
         */
        getInteriorColor(): Promise<PDFNet.ColorPt>;
        /**
         * Returns the number indicating the interior color space of the annotation.
         * @returns A promise that resolves to an integer indicating the number of channels forming the color space.
         * 3 corresponds to RGB, 4 corresponds to CMYK, and 1 corresponds to Gray.
         * If the interior is transparent, the return value is 0.
         */
        getInteriorColorCompNum(): Promise<number>;
        setInteriorColorRGB(col: PDFNet.ColorPt): Promise<void>;
        /**
         * Sets the interior color of the Markup annotation.
         * @param c - A ColorPt object that denotes the color of the Markup annotation.
         * @param CompNum - An integer indicating the number of channels forming the
         * color space used. It also defines the length of the array to be allocated
         * for storing the entries of c.
         */
        setInteriorColor(c: PDFNet.ColorPt, CompNum: number): Promise<void>;
        /**
         * Returns the inner bounding rectangle of the Markup annotation.
         * @returns A promise that resolves to a rectangle specifying the region where content should be displayed.
         */
        getContentRect(): Promise<PDFNet.Rect>;
        /**
         * sets the inner bounding rectangle of the Markup annotation. (Optional)
         * @param cr - A Rect struct to be assign to the 'RD' entry of the annotation dictionary.
         */
        setContentRect(cr: PDFNet.Rect): Promise<void>;
        /**
         * Returns the rectangle difference between overall annotation rectangle and content rectangle.
         * @returns A promise that resolves to a set of four numbers represented as a Rect struct.
         */
        getPadding(): Promise<PDFNet.Rect>;
        /**
         * sets the rectangle difference between overall annotation rectangle and content rectangle. (Optional)
         * @param rd - A set of four numbers represented as a Rect struct
         */
        setPadding(rd: PDFNet.Rect): Promise<void>;
        /**
         * Rotates the appearance of the Markup annotation.
         * @param angle - The new rotation
         */
        rotateAppearance(angle: number): Promise<void>;
    }
    /**
     * 2D Matrix
     *
     * A Matrix2D object represents a 3x3 matrix that, in turn, represents an affine transformation.
     * A Matrix2D object stores only six of the nine numbers in a 3x3 matrix because all 3x3
     * matrices that represent affine transformations have the same third column (0, 0, 1).
     *
     * Affine transformations include rotating, scaling, reflecting, shearing, and translating.
     * In PDFNet, the Matrix2D class provides the foundation for performing affine transformations
     * on vector drawings, images, and text.
     *
     * A transformation matrix specifies the relationship between two coordinate spaces.
     * By modifying a transformation matrix, objects can be scaled, rotated, translated,
     * or transformed in other ways.
     *
     * A transformation matrix in PDF is specified by six numbers, usually in the form
     * of an array containing six elements. In its most general form, this array is denoted
     * [a b c d h v]; The following table lists the arrays that specify the most common
     * transformations:
     *
     *   - Translations are specified as [1 0 0 1 tx ty], where tx and ty are the distances
     *     to translate the origin of the coordinate system in the horizontal and vertical
     *     dimensions, respectively.
     *
     *   - Scaling is obtained by [sx 0 0 sy 0 0]. This scales the coordinates so that 1
     *     unit in the horizontal and vertical dimensions of the new coordinate system is
     *     the same size as sx and sy units, respectively, in the previous coordinate system.
     *
     *   - Rotations are produced by [cos(A) sin(A) -sin(A) cos(A) 0 0], which has the effect
     *     of rotating the coordinate system axes by an angle 'A' counterclockwise.
     *
     *   - Skew is specified by [1 tan(A) tan(B) 1 0 0], which skews the x axis by an angle
     *     A and the y axis by an angle B.
     *
     *  Matrix2D elements are positioned as follows :
     * 			| m_a m_b 0 |
     * 			| m_c m_d 0 |
     * 			| m_h m_v 1 |
     *
     * A single Matrix2D object can store a single transformation or a sequence of transformations.
     * The latter is called a composite transformation. The matrix of a composite transformation is
     * obtained by multiplying (concatenating) the matrices of the individual transformations.
     * Because matrix multiplication is not commutative-the order in which matrices are multiplied
     * is significant. For example, if you first rotate, then scale, then translate, you get a
     * different result than if you first translate, then rotate, then scale.
     *
     * --------------------
     * Since Matrix2D is a struct, it can be created manually by calling "new PDFNet.Matrix2D(m_a, m_b, m_c, m_d, m_h, m_v)"
     * eg. var myfoo = new PDFNet.Matrix2D(1,0,0,1,0,0);
     *
     * Default values for a Matrix2D struct are:
     * m_a = 0
     * m_b = 0
     * m_c = 0
     * m_d = 0
     * m_h = 0
     * m_v = 0
     */
    class Matrix2D {
        constructor(m_a?: number, m_b?: number, m_c?: number, m_d?: number, m_h?: number, m_v?: number);
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Matrix2D"
         */
        copy(): Promise<PDFNet.Matrix2D>;
        /**
         * The Set method sets the elements of this matrix.
         * @param a - the matrix element in the first row, first column.
         * @param b - the matrix element in the first row, second column.
         * @param c - the matrix element in the second row, first column.
         * @param d - the matrix element in the second row, second column.
         * @param h - the matrix element in the third row, first column.
         * @param v - the matrix element in the third row, second column.
         */
        set(a: number, b: number, c: number, d: number, h: number, v: number): Promise<void>;
        /**
         * the Concat method updates this matrix with the product of itself and another matrix
         * specified through an argument list.
         * @param a - the matrix element in the first row, first column.
         * @param b - the matrix element in the first row, second column.
         * @param c - the matrix element in the second row, first column.
         * @param d - the matrix element in the second row, second column.
         * @param h - the matrix element in the third row, first column.
         * @param v - the matrix element in the third row, second column.
         */
        concat(a: number, b: number, c: number, d: number, h: number, v: number): Promise<void>;
        /**
         * The equality operator determines whether the elements of this matrix are equal to
         * the elements of another matrix.
         * @param m2 - A Matrix object that is compared with this Matrix object.
         * @returns A promise that resolves to a boolean regarding whether two matrices are the same.
         */
        equals(m2: PDFNet.Matrix2D): Promise<boolean>;
        /**
         * @returns A promise that resolves to if this matrix is invertible, the Inverse method returns its inverse matrix.
         */
        inverse(): Promise<PDFNet.Matrix2D>;
        /**
         * Updates this matrix with the product of itself and a
         * translation matrix (i.e. it is equivalent to this.m_h += h; this.m_v += v).
         * @param h - the horizontal component of the translation.
         * @param v - the vertical component of the translation.
         */
        translate(h: number, v: number): Promise<void>;
        /**
         * Updates this matrix to the concatenation of a translation matrix and the original matrix.
         * M' = T(h, v) * M. It is equivalent to this.m_h += h; this.m_v += v.
         * @param h - the horizontal component of the translation.
         * @param v - the vertical component of the translation.
         */
        preTranslate(h: number, v: number): Promise<void>;
        /**
         * Updates this matrix by concatenating a translation matrix.
         * M' = M * T(h, v). It is equivalent to this.Concat(1,0,0,1,h,v).
         * @param h - the horizontal component of the translation.
         * @param v - the vertical component of the translation.
         */
        postTranslate(h: number, v: number): Promise<void>;
        /**
         * The Scale method updates this matrix with the product of itself and a scaling matrix.
         * @param h - the horizontal scale factor.
         * @param v - the vertical scale factor
         */
        scale(h: number, v: number): Promise<void>;
        /**
         * Create zero matrix (0 0 0 0 0 0)
         * @returns A promise that resolves to an object of type: "PDFNet.Matrix2D"
         */
        static createZeroMatrix(): Promise<PDFNet.Matrix2D>;
        /**
         * Create identity matrix (1 0 0 1 0 0)
         * @returns A promise that resolves to an object of type: "PDFNet.Matrix2D"
         */
        static createIdentityMatrix(): Promise<PDFNet.Matrix2D>;
        /**
         * @param angle - the angle of rotation in radians.
         * Positive values specify clockwise rotation.
         * @returns A promise that resolves to a rotation matrix for a given angle.
         */
        static createRotationMatrix(angle: number): Promise<PDFNet.Matrix2D>;
        /**
         * Creates and initializes a Matrix object based on six numbers that define an
        affine transformation.
         * @param [a = 0] - the matrix element in the first row, first column.
         * @param [b = 0] - the matrix element in the first row, second column.
         * @param [c = 0] - the matrix element in the second row, first column.
         * @param [d = 0] - the matrix element in the second row, second column.
         * @param [h = 0] - the matrix element in the third row, first column.
         * @param [v = 0] - the matrix element in the third row, second column.
         * @returns A promise that resolves to an object of type: "Matrix2D".
         */
        static create(a?: number, b?: number, c?: number, d?: number, h?: number, v?: number): Promise<PDFNet.Matrix2D>;
        /**
         * Transform/multiply the point (x, y) using this matrix
         * @param x - x-coordinate of point to transform
         * @param y - y-coordinate of point to transform
         * @returns A promise that resolves to a javascript object that contains an x value and y value
         */
        mult(x: number, y: number): Promise<PDFNet.Obj>;
        m_a: number;
        m_b: number;
        m_c: number;
        m_d: number;
        m_h: number;
        m_v: number;
    }
    /**
     * A movie annotation contains animated graphics and sound to be
     * presented on the computer screen and through the speakers. When the
     * annotation is activated, the movie is played.
     */
    class MovieAnnot extends PDFNet.Annot {
        /**
         * creates a Movie annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.MovieAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.MovieAnnot>;
        /**
         * creates a Movie annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Movie annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.MovieAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.MovieAnnot>;
        /**
         * Creates a new Movie annotation in the specified document.
         * @param doc - A document to which the Movie annotation is added.
         * @param pos - A rectangle specifying the Movie annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Movie annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.MovieAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the title of the Movie Annotation.
         * @returns A promise that resolves to a string representing the title of the Movie Annotation.
         */
        getTitle(): Promise<string>;
        /**
         * sets the title of the Movie Annotation.
         * (Optional)
         * @param title - A string representing the title of the Movie Annotation.
         */
        setTitle(title: string): Promise<void>;
        /**
         * Returns the option of whether the Movie is to be played.
         * @returns A promise that resolves to a boolean value indicating if the movie is to be played.
         */
        isToBePlayed(): Promise<boolean>;
        /**
         * sets the option of whether the Movie is to be played.
         * (Optional)
         * @param [isplay] - A boolean value telling if the movie is to be played.
         * Default value: true.
         */
        setToBePlayed(isplay?: boolean): Promise<void>;
    }
    /**
     * A NameTree is a common data structure in PDF. See section 3.8.5 'Name Trees'
     * in PDF Reference Manual for more details.
     *
     * A name tree serves a similar purpose to a dictionary - associating keys and
     * values - but by different means. NameTrees allow efficient storage of very
     * large association collections (string/Obj* maps). A NameTree can have many
     * more entries than a SDF/Cos dictionary can.
     *
     * NameTree-s use SDF/Cos-style strings (not null-terminated C strings), which
     * may use Unicode encoding etc.
     *
     * <pre>
     *   PDFDoc doc("../Data/PDFReference.pdf");
     *   NameTree dests = NameTree::Find(*doc.GetSDFDoc(), "Dests");
     *   if (dests.IsValid()) {
     *     // Traversing the NameTree
     *     UString key;
     *     for (DictIterator i = dests.GetIterator(); i.HasNext(); i.Next())
     *        i.Key().GetAsPDFText(key); // ...
     *   }
     * </pre>
     */
    class NameTree {
        /**
         * Retrieves the NameTree inside the '/Root/Names' dictionary with the
         * specified key name, or creates it if it does not exist.
         * @param doc - The document in which the name tree is created.
         * @param name - The name of the NameTree to create.
         * @returns A promise that resolves to the newly created NameTree for the doc or an existing tree with
         * the same key name.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, name: string): Promise<PDFNet.NameTree>;
        /**
         * Retrieves a name tree, with the given key name, from the '/Root/Names'
         * dictionary of the doc.
         * @param doc - The document in which to search for the name.
         * @param name - The name of the name tree to find.
         * @returns A promise that resolves to the requested NameTree. If the requested NameTree exists
         * NameTree.IsValid() will return true, and false otherwise.
         */
        static find(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, name: string): Promise<PDFNet.NameTree>;
        /**
         * Create a high level NameTree wrapper around an existing SDF/Cos NameTree.
         * This does not copy the object.
         * @param name_tree - SDF/Cos root of the NameTree object.
         * @returns A promise that resolves to an object of type: "PDFNet.NameTree"
         */
        static createFromObj(name_tree: PDFNet.Obj): Promise<PDFNet.NameTree>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.NameTree"
         */
        copy(): Promise<PDFNet.NameTree>;
        /**
         * @returns A promise that resolves to whether this is a valid (non-null) NameTree. If the
         * function returns false the underlying SDF/Cos object is null and
         * the NameTree object should be treated as null as well.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.DictIterator"
         */
        getIterator(key: string): Promise<PDFNet.DictIterator>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getValue(key: string): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an iterator that addresses the first element in the NameTree.
         * The iterator can be used to traverse all entries stored in the NameTree.
         *
         * <pre>
         *  UString key;
         *  for (NameTreeIterator i = dests.GetIterator(); i.HasNext(); i.Next()) {
         *    i.GetKey().GetAsPDFText(key); // ...
         *  }
         * </pre>
         */
        getIteratorBegin(): Promise<PDFNet.DictIterator>;
        put(key: string, value: PDFNet.Obj): Promise<void>;
        eraseKey(key: string): Promise<void>;
        /**
         * Removes the NameTree entry pointed by the iterator.
         * @param pos - ditionary iterator object that points to the NameTree entry to be removed.
         */
        erase(pos: PDFNet.DictIterator): Promise<void>;
        /**
         * @returns A promise that resolves to the object to the underlying SDF/Cos object. If the NameTree.IsValid()
         * returns false the SDF/Cos object is NULL.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * A NumberTree is a common data structure in PDF. See section 3.8.6 'Number Trees'
     * in PDF Reference Manual for more details.
     *
     * A number tree serves a similar purpose to a dictionary - associating keys and
     * values - but by different means. NumberTrees allow efficient storage of very
     * large association collections (number/Obj* maps). A NumberTree can have many
     * more entries than a SDF/Cos dictionary can.
     *
     * Sample code:
     * <pre>
     *   PDFDoc doc("../Data/test.pdf");
     *   NumberTree labels(doc.GetRoot().Get("PageLabels").Value());
     *   if (labels.IsValid()) {
     *     // Traversing the NumberTree
     *     for (NumberTreeIterator i = labels.GetIterator(); i.HasNext(); i.Next())
     *         cout << "Key: " << i.Key().GetNumber() << endl;
     *   }
     * </pre>
     */
    class NumberTree {
        /**
         * Create a high level NumberTree wrapper around an existing SDF/Cos NumberTree.
         * This does not copy the object.
         * @param number_tree - SDF/Cos root of the NumberTree object.
         * @returns A promise that resolves to an object of type: "PDFNet.NumberTree"
         */
        static create(number_tree: PDFNet.Obj): Promise<PDFNet.NumberTree>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.NumberTree"
         */
        copy(): Promise<PDFNet.NumberTree>;
        /**
         * @returns A promise that resolves to whether this is a valid (non-null) NumberTree. If the
         * function returns false the underlying SDF/Cos object is null and
         * the NumberTree object should be treated as null as well.
         */
        isValid(): Promise<boolean>;
        /**
         * Search for the specified key in the NumberTree.
         * @param key - the number representing the key to be found.
         *
         * Sample code:
         *   NumberTreeIterator i = dests.Find(5);
         *   if (i.HasNext()) {
         *     cout << "Key: " << i.GetKey()->GetNumber() << endl;
         *     cout << "Value: " << i.GetValue()->GetType() << endl;
         *   }
         * @returns A promise that resolves to if the key is present the function returns a NumberTreeIterator the points
         * to the given Key/Value pair. If the key is not found the function returns End()
         * (a non-valid) iterator.
         */
        getIterator(key: number): Promise<PDFNet.DictIterator>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getValue(key: number): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an iterator to the first key/value pair (i.e. NNTreeData) in
         * the document. You can use the Next method on the returned iterator to
         * traverse all entries stored under the NumberTree.
         *
         * Sample code:
         * 	 for (NumberTreeIterator i = dests.GetIterator(); i.HasNext(); i.Next())
         * 	   cout << "Key: " << i.GetKey().GetNumber() << endl;
         */
        getIteratorBegin(): Promise<PDFNet.DictIterator>;
        /**
         * Puts a new entry in the name tree. If an entry with this number
         * is already in the tree, it is replaced.
         * @param key - A number representing the key of the new entry.
         * @param value - the value associated with the key. It can be any SDF::Obj.
         */
        put(key: number, value: PDFNet.Obj): Promise<void>;
        /**
         * Removes the specified object from the tree. Does nothing if no object
         * with that number exists.
         * @param key - A number representing the key of the entry to be removed.
         */
        eraseKey(key: number): Promise<void>;
        /**
         * Removes the NumberTree entry pointed by the iterator.
         * @param pos - dictionary iterator object that points to the NumberTree entry to be removed
         */
        erase(pos: PDFNet.DictIterator): Promise<void>;
        /**
         * @returns A promise that resolves to the object to the underlying SDF/Cos object. If the NumberTree.IsValid()
         * returns false the SDF/Cos object is NULL.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * optional-content group
     */
    class OCG {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.OCG"
         */
        static create(pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, name: string): Promise<PDFNet.OCG>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.OCG"
         */
        static createFromObj(ocg_dict: PDFNet.Obj): Promise<PDFNet.OCG>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.OCG"
         */
        copy(): Promise<PDFNet.OCG>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to true if this is a valid (non-null) OCG, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the name of this optional-content group (OCG).
         */
        getName(): Promise<string>;
        /**
         * sets the name of this optional-content group (OCG).
         * @param value - The new name.
         */
        setName(value: string): Promise<void>;
        /**
         * @returns A promise that resolves to oCG intent.
         * An intent is a name object (or an array of name objects) broadly describing the
         * intended use, which can be either "View" or "Design". A group's content is
         * considered to be optional (that is, the group's state is considered in its
         * visibility) if any intent in its list matches an intent of the context. The
         * intent list of the context is usually set from the intent list of the document
         * configuration.
         */
        getIntent(): Promise<PDFNet.Obj>;
        /**
         * Sets the Intent entry in an optional-content group's SDF/Cos dictionary.
         * For more information, see GetIntent().
         * @param value - The new Intent entry value (a name object or an array of name objects).
         */
        setIntent(value: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to true if this group is associated with a Usage dictionary, false otherwise.
         */
        hasUsage(): Promise<boolean>;
        /**
         * @param key - The usage key in the usage dictionary entry. The possible key values are:
         * CreatorInfo, Language, Export, Zoom, Print, View, User, PageElement.
         * @returns A promise that resolves to the usage information associated with the given key in the Usage dictionary
         * for the group, or a NULL if the entry is not present. A Usage dictionary entry
         * provides more specific intended usage information than an intent entry.
         */
        getUsage(key: string): Promise<PDFNet.Obj>;
        /**
         * @param ctx - The context for which to get the group's state.
         * @returns A promise that resolves to true if this OCG object is in the ON state in a given context,
         * false otherwise.
         */
        getCurrentState(ctx: PDFNet.OCGContext): Promise<boolean>;
        /**
         * sets the current ON-OFF state of the optional-content group (OCG) object in a given context.
         * @param ctx - The context for which to set the group's state.
         * @param state - The new state.
         */
        setCurrentState(ctx: PDFNet.OCGContext, state: boolean): Promise<void>;
        /**
         * @param cfg - The configuration for which to get the group's initial state.
         * @returns A promise that resolves to the initial state (ON or OFF) of the optional-content group
         * (OCG) object in a given configuration.
         */
        getInitialState(cfg: PDFNet.OCGConfig): Promise<boolean>;
        /**
         * sets the initial state (ON or OFF) of the optional-content group (OCG)
         * object in a given configuration.
         * @param cfg - The configuration for which to set the group's initial state.
         * @param state - The new initial state, true if the state is ON, false if it is OFF.
         */
        setInitialState(cfg: PDFNet.OCGConfig, state: boolean): Promise<void>;
        /**
         * @param cfg - The OC configuration.
         * @returns A promise that resolves to true if this OCG is locked in a given configuration, false otherwise.
         * The on/off state of a locked OCG cannot be toggled by the user through the user interface.
         */
        isLocked(cfg: PDFNet.OCGConfig): Promise<boolean>;
        /**
         * Sets the locked state of an OCG in a given configuration. The on/off state of a
         * locked OCG cannot be toggled by the user through the user interface.
         * @param cfg - IN/OUT The optional-content configuration.
         * @param state - true if the OCG should be locked, false otherwise.
         */
        setLocked(cfg: PDFNet.OCGConfig, state: boolean): Promise<void>;
    }
    /**
     * The OCGConfig object represents an optional-content (OC) configuration
     * structure (see section 4.10.3 in PDF Reference), used to persist a set
     * of visibility states and other optional-content information in a PDF file
     * A document has a default configuration, saved in the D entry in the
     * 'OCProperties' dictionary (that can be obtained using [PDFDoc].getOCConfig()),
     * and can have a list of other configurations, saved as an array in the
     * 'Configs' entry in the OCProperties dictionary.
     * <p>
     * Configurations are typically used to initialize the OCG ON-OFF states for
     * an optional-content context (OCGContext). The OCG order in the configuration
     * is the order in which the groups appear in the Layers panel of a PDF viewer.
     * The configuration can also define a set of mutually exclusive OCGs, called a
     * radio button group.
     * </p>
     */
    class OCGConfig {
        /**
         * Creates a new optional-content configuration from an existing SDF/Cos object.
         * @param dict - SDF object to create optional-content configuration from.
         * @returns A promise that resolves to an object of type: "PDFNet.OCGConfig"
         */
        static createFromObj(dict: PDFNet.Obj): Promise<PDFNet.OCGConfig>;
        /**
         * Creates a new optional-content configuration object in the document.
         * @param pdfdoc - The document in which the new OCG::Config will be created.
         * @param default_config - If true, the configuration will be set as the
         * default OCG configuration (i.e. listed as a D entry under 'OCProperties'
         * dictionary).
         * @returns A promise that resolves to the newly created configuration object.
         */
        static create(pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, default_config: boolean): Promise<PDFNet.OCGConfig>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.OCGConfig"
         */
        copy(): Promise<PDFNet.OCGConfig>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the Obj array that specifies the order of optional content (OC) groups
         * in this configuration or NULL if the configuration does not contain any OCGs.
         * The order of OCGs in the array is used to represent the order in which the
         * group names are displayed in the Layers panel of a PDF viewer application.
         * For more information, please refer to Section 4.10.3 in the PDF Reference.
         */
        getOrder(): Promise<PDFNet.Obj>;
        /**
         * Sets the user interface display order of optional-content groups (OCGs) in
         * this configuration. This is the order in which the group names are displayed
         * in the Layers panel of a PDF viewer.
         * @param value - The SDF/Cos object containing the OCG order array.
         * For more information, please refer to section 4.10.3 in the PDF Reference.
         */
        setOrder(value: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to the name of an optional-content configuration (suitable for
         * presentation in a user interface).
         */
        getName(): Promise<string>;
        /**
         * sets the name of an optional-content configuration (suitable for presentation
         * in a user interface). The method stores the specified string as the Name entry
         * in the configuration's SDF/Cos dictionary.
         * @param value - The new name string.
         */
        setName(value: string): Promise<void>;
        /**
         * @returns A promise that resolves to the creator property of an optional-content configuration. The string
         * is used to represent the name of the application or feature that created this
         * configuration.
         */
        getCreator(): Promise<string>;
        /**
         * sets the creator property of an optional-content configuration. Stores the
         * specified string as the Creator entry in the configuration's SDF/Cos dictionary.
         * @param value - The new creator string.
         */
        setCreator(value: string): Promise<void>;
        /**
         * @returns A promise that resolves to the base initialization state. This state is used to initialize the states
         * of all the OCGs in a document when this configuration is applied. The value of this entry
         * must be one of the following names:
         *  <li>ON: The states of all groups are turned ON. </li>
         *  <li>OFF: The states of all groups are turned OFF.</li>
         *  <li>Unchanged: The states of all groups are left unchanged.</li>
         *
         * <p> After base initialization, the contents of the ON and OFF arrays are processed, overriding
         * the state of the groups included in the arrays.</p>
         */
        getInitBaseState(): Promise<string>;
        /**
         * sets the base initialization state. For more info, please see GetInitBaseState().
         * @param [value] - new base state ("ON", "OFF", or "Unchanged").
         */
        setInitBaseState(value?: string): Promise<void>;
        /**
         * @returns A promise that resolves to the "ON" initialization array from the configuration dictionary or
         * NULL if the array is not present. The returned object is an array of optional
         * content groups whose state should be set to ON when this configuration is applied.
         * Note: If the BaseState entry is ON, this entry is redundant.
         */
        getInitOnStates(): Promise<PDFNet.Obj>;
        /**
         * sets the 'ON' initialization array in the configuration dictionary.
         * @param value - the initialization array to be used
         */
        setInitOnStates(value: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to the "OFF" initialization array from the configuration dictionary or
         * NULL if the array is not present. The returned object is an array of optional
         * content groups whose state should be set to OFF when this configuration is applied.
         * Note: If the BaseState entry is OFF, this entry is redundant.
         */
        getInitOffStates(): Promise<PDFNet.Obj>;
        /**
         * sets the 'OFF' initialization array in the configuration dictionary.
         * @param value - the initialization array to be used.
         */
        setInitOffStates(value: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to oCG configuration intent. An intent is a name object (or an array of name
         * objects) broadly describing the intended use, which can be either "View" or "Design".
         * A group's content is considered to be optional (that is, the group's state is considered
         * in its visibility) if any intent in its list matches an intent of the context. The
         * intent list of the context is usually set from the intent list of the document
         * configuration. If the configuration has no Intent entry, the default value of
         * "View" is used.
         */
        getIntent(): Promise<PDFNet.Obj>;
        /**
         * Sets the Intent entry in an optional-content configuration's SDF/Cos dictionary.
         * For more information, see GetIntent().
         * @param value - The new Intent entry value (a name object or an array of name objects).
         */
        setIntent(value: PDFNet.Obj): Promise<void>;
        /**
         * @returns A promise that resolves to the list of locked OCGs or NULL if there are no locked OCGs.
         * The on/off state of a locked OCG cannot be toggled by the user through the
         * user interface.
         */
        getLockedOCGs(): Promise<PDFNet.Obj>;
        /**
         * sets the array of locked OCGs. The on/off state of a locked OCG cannot be
         * toggled by the user through the user interface.
         * @param value - An SDF/Cos array of OCG objects to be locked in this
         * configuration, or an empty array if the configuration should not contain
         * locked OCGs. The default is the empty array.
         */
        setLockedOCGs(value: PDFNet.Obj): Promise<void>;
    }
    /**
     * <p>
     * The OCGContext object represents an optional-content context in a document, within which
     * document objects such as words or annotations are visible or hidden. The context keeps track
     * of the ON-OFF states of all of the optional-content groups (OCGs) in a document. Content is
     * or is not visible with respect to the OCG states stored in a specific context. Unlike other
     * objects in OCG namespace, the OCGContext does not correspond to any explicit PDF structure.
     * </p><p>
     * Each PDFView has a default context (PDF::GetOCGContext()) that it uses for on-screen drawing
     * and that determines the default state for any drawing. The context has flags that control
     * whether to draw content that is marked as optional, and whether to draw content that is not
     * marked as optional.
     * </p><p>
     * When enumerating page content, OCGContext can be passed as a parameter in ElementReader.Begin()
     * method. When using PDFDraw, PDFRasterizer, or PDFView class to render PDF pages use
     *  ( PDFDraw::SetOCGContext() method to select an OC context.
     * </p><p>
     * There can be more than one Context object, representing different combinations of OCG states.
     * You can change the states of OCGs within any context. You can build contexts with your own
     * combination of OCG states, and issue drawing or enumeration commands using that context.
     * For example, you can pass an optional-content context to ElementReader.Begin(). You can save
     * the resulting state information as part of the configuration (e.g. using Config::SetInit methods),
     * but the context itself has no corresponding PDF representation, and is not saved.
     * </p>
     */
    class OCGContext extends PDFNet.Destroyable {
        /**
         * create a context object that represents an optional-content state of the
         * document from a given configuration.
         * @param cfg - A configuration from which to take initial OCG states.
         * @returns A promise that resolves to an object of type: "PDFNet.OCGContext"
         */
        static createFromConfig(cfg: PDFNet.OCGConfig): Promise<PDFNet.OCGContext>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.OCGContext"
         */
        copy(): Promise<PDFNet.OCGContext>;
        /**
         * @param grp - The optional-content group (OCG) that is queried.
         * @returns A promise that resolves to the ON-OFF states (true or false) for the given optional-content
         * group (OCG) in this OC context.
         */
        getState(grp: PDFNet.OCG): Promise<boolean>;
        /**
         * sets the ON-OFF states for the given optional-content group (OCG) in this
         * context.
         * @param grp - The optional-content group (OCG) that is queried.
         * @param state - true for 'ON' and false for 'OFF'.
         */
        setState(grp: PDFNet.OCG, state: boolean): Promise<void>;
        /**
         * sets the sates of all OCGs in the context to ON or OFF.
         * @param all_on - A flag used to specify whether the OCG states should be set
         * to ON (if true), or OFF (if false).
         */
        resetStates(all_on: boolean): Promise<void>;
        /**
         * Sets the non-OC status for this context. Content that is not marked
         * as optional content is drawn (or element.IsOCVisible()) when 'draw_non_OC'
         * is true, and not drawn/visible otherwise.
         * @param draw_non_OC - A flag specifying whether the content that is not
         * marked as optional should be treated as visible.
         */
        setNonOCDrawing(draw_non_OC: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to the non-OC status for this context. The flag indicates whether the
         * content that is not marked as optional should be treated as visible.
         * For more information, please see SetNonOCDrawing().
         */
        getNonOCDrawing(): Promise<boolean>;
        /**
         * sets the drawing and enumeration type for this context. This type, together
         * with the visibility determined by the OCG and OCMD states, controls whether
         * content that is marked as optional content is drawn or enumerated.
         * @param oc_draw_mode - <pre>
         * PDFNet.OCGContext.OCDrawMode = {
         * 	e_VisibleOC : 0
         * 	e_AllOC : 1
         * 	e_NoOC : 2
         * }
         * </pre>
         * A flag specifying the visibility of optional content.
         */
        setOCDrawMode(oc_draw_mode: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.OCGContext.OCDrawMode = {
         * 	e_VisibleOC : 0
         * 	e_AllOC : 1
         * 	e_NoOC : 2
         * }
         * </pre>
         * @returns A promise that resolves to the drawing and enumeration type for this context.
         * For more information, please see SetOCMode() and OCG::Context::OCDrawMode.
         */
        getOCMode(): Promise<number>;
    }
    /**
     * The OCMD object represents an Optional Content Membership Dictionary (OCMD)
     * that allows the visibility of optional content to depend on the states in a
     * set of optional-content groups (OCG::Group). The object directly corresponds
     * to the OCMD dictionary (Section 4.10.1 'Optional Content Groups' in PDF
     * Reference).
     * <p>
     * An OCMD collects a set of OCGs. It sets a visibility policy, so that
     * content in the member groups is visible only when all groups are ON
     * or OFF, or when any of the groups is ON or OFF. This makes it possible
     * to set up complex dependencies among groups. For example, an object can be
     * visible only if some other conditions are met (such as if another layer is
     * visible).
     * </p>
     */
    class OCMD {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.OCMD"
         */
        static createFromObj(ocmd_dict: PDFNet.Obj): Promise<PDFNet.OCMD>;
        /**
         * Creates a new optional-content membership dictionary (OCMD) object in the
         * given document for the given groups and visibility policy.
         * @param pdfdoc - The document in which the new OCMD will be created.
         * @param ocgs - An array of optional-content groups (OCGs) to be members of the dictionary.
         * @param vis_policy - <pre>
         * PDFNet.OCMD.VisibilityPolicyType = {
         * 	e_AllOn : 0
         * 	e_AnyOn : 1
         * 	e_AnyOff : 2
         * 	e_AllOff : 3
         * }
         * </pre>
         * The visibility policy that determines the visibility of content with respect
         * to the ON-OFF state of OCGs listed in the dictionary.
         * @returns A promise that resolves to the newly created OCG::OCMD object.
         */
        static create(pdfdoc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, ocgs: PDFNet.Obj, vis_policy: number): Promise<PDFNet.OCMD>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.OCMD"
         */
        copy(): Promise<PDFNet.OCMD>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * Returns the optional-content groups listed under 'OCGs' entry in the object
         * dictionary.
         * @returns A promise that resolves to a dictionary (for a single OCG::Group object), an SDF::Obj array
         * (for multiple OCG::Group objects) or NULL (for an empty OCMD).
         */
        getOCGs(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to if the PDOCMD has a visibility expression entry, return the
         * SDF::Obj array object representing the expression, otherwise returns NULL.
         */
        getVisibilityExpression(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to true if this is a valid (non-null) OCMD, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @param ctx - The context in which the visibility of content is tested.
         * @returns A promise that resolves to true if content tagged with this OCMD is visible in the given
         * context, false if it is hidden.
         * <p>
         * Based on the optional-content groups listed in the dictionary, the current
         * ON-OFF state of those groups within the specified context, and the
         * dictionary's visibility policy, test whether the content tagged with
         * this dictionary would be visible.
         */
        isCurrentlyVisible(ctx: PDFNet.OCGContext): Promise<boolean>;
        /**
         * Returns the optional-content membership dictionary's visibility policy, which
         * determines the visibility of content with respect to the ON-OFF state of
         * OCGs listed in the dictionary.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.OCMD.VisibilityPolicyType = {
         * 	e_AllOn : 0
         * 	e_AnyOn : 1
         * 	e_AnyOff : 2
         * 	e_AllOff : 3
         * }
         * </pre>
         * @returns A promise that resolves to the visibility policy.
         */
        getVisibilityPolicy(): Promise<number>;
        /**
         * Sets the optional-content membership dictionary's visibility policy, which
         * determines the visibility of content with respect to the ON-OFF state of
         * OCGs listed in the dictionary.
         * @param vis_policy - <pre>
         * PDFNet.OCMD.VisibilityPolicyType = {
         * 	e_AllOn : 0
         * 	e_AnyOn : 1
         * 	e_AnyOff : 2
         * 	e_AllOff : 3
         * }
         * </pre>
         * New visibility policy.
         */
        setVisibilityPolicy(vis_policy: number): Promise<void>;
    }
    /**
     * The class OCRModule.
     * static interface to PDFTron SDKs OCR functionality
     */
    class OCRModule {
        /**
         * Method to create an OCROptions object
         * @returns A promise that resolves to a PDFNet.OCRModule.OCROptions.
         */
        static createOCROptions(): Promise<PDFNet.OCRModule.OCROptions>;
        /**
         * Find out whether the OCR module is available (and licensed).
         * @returns A promise that resolves to returns true if OCR operations can be performed.
         */
        static isModuleAvailable(): Promise<boolean>;
        /**
         * Convert an image to a PDF with searchable text.
         * @param dst - - The destination document.
         * @param src - - The path to the input image.
         * @param [options] - - OCR options (optional).
         */
        static imageToPDF(dst: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, src: string, options?: PDFNet.Obj | PDFNet.OCRModule.OCROptions): Promise<void>;
        /**
         * Add searchable and selectable text to raster images within a PDF.
         * @param dst - - The source and destination document.
         * @param [options] - - OCR options (optional).
         */
        static processPDF(dst: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, options?: PDFNet.Obj | PDFNet.OCRModule.OCROptions): Promise<void>;
        /**
         * Perform OCR on an image and return resulting JSON string. Side effect: source image is converted to PDF and stored in the destination document.
         * @param dst - - The destination document.
         * @param src - - The path to the input image.
         * @param [options] - - OCR options (optional).
         * @returns A promise that resolves to jSON string represeting OCR results.
         */
        static getOCRJsonFromImage(dst: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, src: string, options?: PDFNet.Obj | PDFNet.OCRModule.OCROptions): Promise<string>;
        /**
         * Perform OCR on raster images within a PDF and return resulting JSON string.
         * @param src - - The source document.
         * @param [options] - - OCR options (optional).
         * @returns A promise that resolves to jSON string represeting OCR results.
         */
        static getOCRJsonFromPDF(src: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, options?: PDFNet.Obj | PDFNet.OCRModule.OCROptions): Promise<string>;
        /**
         * Add hidden text layer to a PDF consisting of raster image(s).
         * @param dst - - The source and destination document.
         * @param json - - JSON representing OCR results.
         */
        static applyOCRJsonToPDF(dst: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, json: string): Promise<void>;
        /**
         * Perform OCR on an image and return resulting XML string. Side effect: source image is converted to PDF and stored in the destination document.
         * @param dst - - The destination document.
         * @param src - - The path to the input image.
         * @param [options] - - OCR options (optional).
         * @returns A promise that resolves to xML string represeting OCR results.
         */
        static getOCRXmlFromImage(dst: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, src: string, options?: PDFNet.Obj | PDFNet.OCRModule.OCROptions): Promise<string>;
        /**
         * Perform OCR on raster images within a PDF and return resulting XML string.
         * @param src - - The source document.
         * @param [options] - - OCR options (optional).
         * @returns A promise that resolves to xML string represeting OCR results.
         */
        static getOCRXmlFromPDF(src: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, options?: PDFNet.Obj | PDFNet.OCRModule.OCROptions): Promise<string>;
        /**
         * Add hidden text layer to a PDF consisting of raster image(s).
         * @param dst - - The source and destination document.
         * @param xml - - XML representing OCR results.
         */
        static applyOCRXmlToPDF(dst: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, xml: string): Promise<void>;
    }
    /**
     * Obj is a concrete class for all SDF/Cos objects. Obj hierarchy implements the
     * composite design pattern. As a result, you can invoke a member function of any
     * 'derived' object through Obj interface. If the member function is not supported
     * (e.g. if you invoke Obj::GetNumber() on a boolean object) an Exception will be
     * thrown.
     *
     * You can use GetType() or obl.Is???() member functions to find out type-information at
     * run time, however most of the time the type can be inferred from the PDF specification.
     * Therefore when you call Doc::GetTrailer() you can assume that returned object is
     * a dictionary. If there is any ambiguity use Is???() methods.
     *
     * Objects can't be shared across documents, however you can use Doc::ImportObj()
     * to copy objects from one document to another.
     *
     * Objects can be shared within a document provided that they are created as indirect.
     * Indirect objects are the ones that are referenced in cross-reference table.
     * To create an object as indirect use doc.CreateIndirect???() (where ? is the
     * Object type).
     */
    class Obj {
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Obj.Type = {
         * 	e_null : 0
         * 	e_bool : 1
         * 	e_number : 2
         * 	e_name : 3
         * 	e_string : 4
         * 	e_dict : 5
         * 	e_array : 6
         * 	e_stream : 7
         * }
         * </pre>
         * @returns A promise that resolves to the object type.
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the document to which this object belongs.
         */
        getDoc(): Promise<PDFNet.SDFDoc>;
        /**
         * The function writes the Obj to the output stream
         * @param stream - the input stream where the Obj will be written
         */
        write(stream: PDFNet.FilterWriter): Promise<void>;
        /**
         * @param to - Obj to compare to
         * @returns A promise that resolves to true if two Obj's point to the same object.
         * This method does not compare object content. For this operation use
         * IsEqualValue() instead.
         */
        isEqual(to: PDFNet.Obj): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if this is a Bool object, false otherwise.
         */
        isBool(): Promise<boolean>;
        /**
         * @returns A promise that resolves to bool value if this is Bool.
         */
        getBool(): Promise<boolean>;
        /**
         * @param b - bool value used to set Bool object.
         */
        setBool(b: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to true if this is a Number object, false otherwise.
         */
        isNumber(): Promise<boolean>;
        /**
         * @returns A promise that resolves to value, if this is Number.
         */
        getNumber(): Promise<number>;
        /**
         * @param n - value used to set Number object.
         */
        setNumber(n: number): Promise<void>;
        /**
         * @returns A promise that resolves to true if this is a Null object, false otherwise.
         */
        isNull(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if this is a Str (String) object, false otherwise.
         */
        isString(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a pointer to the string buffer. Please note that the string may not
         * be NULL terminated and that it may not be represented in ASCII or Unicode
         * encoding. For more information on SDF/Cos String objects, please refer to
         * section 3.2.3 'String Objects' in PDF Reference Manual.
         */
        getBuffer(): Promise<number>;
        setString(value: string): Promise<void>;
        /**
         * Sets the string object value.
         * @param value - A Unicode string value.
         */
        setUString(value: string): Promise<void>;
        /**
         * @returns A promise that resolves to true if this is Name, false otherwise.
         */
        isName(): Promise<boolean>;
        /**
         * @returns A promise that resolves to string representing the Name object.
         */
        getName(): Promise<string>;
        /**
         * @param name - value used to set Name object.
         */
        setName(name: string): Promise<void>;
        /**
         * @returns A promise that resolves to true if this is Indirect object (i.e. object referenced in the
         * cross-reference table), false otherwise.
         */
        isIndirect(): Promise<boolean>;
        /**
         * @returns A promise that resolves to object number. If this is not an Indirect object, object number of
         * a containing indirect object is returned.
         */
        getObjNum(): Promise<number>;
        /**
         * @returns A promise that resolves to generation number. If this is not an Indirect object, generation number of
         * a containing indirect object is returned.
         */
        getGenNum(): Promise<number>;
        /**
         * @returns A promise that resolves to object offset from the beginning of the file. If this is not an Indirect object,
         * offset of a containing indirect object is returned.
         */
        getOffset(): Promise<number>;
        /**
         * @returns A promise that resolves to true if the object is in use or is marked as free.
         */
        isFree(): Promise<boolean>;
        /**
         * Set the object mark. Mark is a boolean value that can be associated with every
         * indirect object. This is especially useful when an object graph should be
         * traversed and an operation should be performed on each node only once.
         * @param mark - boolean value that the object's mark should be set to.
         */
        setMark(mark: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to true if the object is marked.
         */
        isMarked(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the object is loaded in memory.
         */
        isLoaded(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if this is a Container (a dictionary, array, or a stream),
         * false otherwise.
         */
        isContainer(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the 'size' of the object. The definition of 'size' depends on
         * the object type. In particular:
         *  For a dictionary or a stream object, the method will return the
         *    number of key/value pairs in the dictionary.
         *  For an array object the method will return the number of Obj
         *    entries in the array.
         *  For a string object the method will return the number of bytes
         *    in the string buffer.
         *  For any other object the method will always return 1.
         */
        size(): Promise<number>;
        /**
         * @returns A promise that resolves to an iterator that addresses the first element in the dictionary.
         * <pre>
         *  DictIterator itr = dict.GetDictIterator();
         *  while (itr.HasNext()) {
         *      Obj key = itr.Key();
         *      Obj value = itr.Value();
         *      // ...
         *      itr.Next()
         *   }
         * </pre>
         */
        getDictIterator(): Promise<PDFNet.DictIterator>;
        /**
         * @returns A promise that resolves to true if this is a dictionary (i.e. Dict), false otherwise.
         */
        isDict(): Promise<boolean>;
        /**
         * Search the dictionary for a given key.
         * @param key - a key to search for in the dictionary
         * @returns A promise that resolves to the iterator to the matching key/value pair or invalid iterator
         * (i.e. itr.HasNext()==fase) if the if the dictionary does not contain the given key.
         */
        find(key: string): Promise<PDFNet.DictIterator>;
        /**
         * Search the dictionary for a given key.
         * @param key - a key to search for in the dictionary
         * @returns A promise that resolves to nULL if the dictionary does not contain the specified key.
         * Otherwise return the corresponding value.
         */
        findObj(key: string): Promise<PDFNet.Obj>;
        /**
         * Search the dictionary for a given key and throw an exception if the key is not found.
         * @param key - a key to search for in the dictionary
         * @returns A promise that resolves to obj::Null object if the value matching specified key is a Obj::Null object.
         * otherwise return the iterator to the matching key/value pair.
         */
        get(key: string): Promise<PDFNet.DictIterator>;
        /**
         * Inserts a <key, Obj::Type::e_name> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param name - The value of the Obj::Type::e_name object to be inserted into
         * the dictionary.
         * @returns A promise that resolves to a newly created name object.
         */
        putName(key: string, name: string): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_array> pair in the dictionary.
         * @param key - The key of the value to set.
         * @returns A promise that resolves to a newly created array object.
         */
        putArray(key: string): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_bool> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param value - The value of the Obj::Type::e_bool object to be inserted into
         * the dictionary.
         * @returns A promise that resolves to a newly created boolean object.
         */
        putBool(key: string, value: boolean): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_dict> pair in the dictionary.
         * @param key - The key of the value to set.
         * @returns A promise that resolves to a newly created dictionary.
         */
        putDict(key: string): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_number> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param value - The value of the Obj::Type::e_number object to be inserted into
         * the dictionary.
         * @returns A promise that resolves to a newly created number object.
         */
        putNumber(key: string, value: number): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_string> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param value - The value of the Obj::Type::e_string object to be inserted into
         * the dictionary.
         * @returns A promise that resolves to a newly created string object.
         */
        putString(key: string, value: string): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_string> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param value - The buffer used to set the value of the Obj::Type::e_string
         * object to be inserted into the dictionary.
         * @param size - The number of bytes to copy from the 'value' buffer parameter.
         * @returns A promise that resolves to a newly created string object.
         */
        putStringWithSize(key: string, value: string, size: number): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_string> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param t - The value of the Obj::Type::e_string object to be inserted into
         * the dictionary.
         * @returns A promise that resolves to a newly created string object.
         */
        putText(key: string, t: string): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, Obj::Type::e_null> pair in the dictionary.
         * @param key - The key of the value to set.
         */
        putNull(key: string): Promise<void>;
        /**
         * Inserts a <key, Obj> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param input_obj - The value to be inserted into the dictionary. If 'obj' is
         * indirect (i.e. is a shared) object it will be inserted by reference,
         * otherwise the object will be cloned and then inserted into the dictionary.
         * @returns A promise that resolves to a newly inserted object.
         */
        put(key: string, input_obj: PDFNet.Obj): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, [x1,y1,x2,y2]> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param x1 - The bottom left x value of the rect to be inserted
         * @param y1 - The bottom left y value of the rect to be inserted
         * @param x2 - The top right x value of the rect to be inserted
         * @param y2 - The top right y value of the rect to be inserted
         * @returns A promise that resolves to a newly created array object.
         */
        putRect(key: string, x1: number, y1: number, x2: number, y2: number): Promise<PDFNet.Obj>;
        /**
         * Inserts a <key, [a,b,c,d,h,v]> pair in the dictionary.
         * @param key - The key of the value to set.
         * @param mtx - A matrix used to set the values in an array of six numbers.
         * The resulting array will be inserted into the dictionary.
         * @returns A promise that resolves to a newly created array object.
         */
        putMatrix(key: string, mtx: PDFNet.Matrix2D): Promise<PDFNet.Obj>;
        /**
         * Removes an element in the dictionary that matches the given key.
         * @param key - A string representing the key value of the element to remove.
         */
        eraseFromKey(key: string): Promise<void>;
        /**
         * Removes an element in the dictionary from specified position.
         * @param pos - A dictionary iterator indicating the position of the element to remove.
         */
        erase(pos: PDFNet.DictIterator): Promise<void>;
        /**
         * Change the key value of a dictionary entry.
         * The key can't be renamed if another key with the same name already exists
         * in the dictionary. In this case Rename returns false.
         * @param old_key - A string representing the key value to be changed.
         * @param new_key - A string representing the key value that the old key is changed into.
         * @returns A promise that resolves to an object of type: "boolean"
         */
        rename(old_key: string, new_key: string): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if this is an Array, false otherwise.
         */
        isArray(): Promise<boolean>;
        /**
         * @param index - The array element to obtain. The first element in an array has an index of zero.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        getAt(index: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_name object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param name - The value of the Obj::Type::e_name object to be inserted.
         * @returns A promise that resolves to a newly created name object.
         */
        insertName(pos: number, name: string): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_array object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @returns A promise that resolves to a newly created array object.
         */
        insertArray(pos: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_bool object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param value - The value of the Obj::Type::e_bool object to be inserted.
         * @returns A promise that resolves to a newly created boolean object.
         */
        insertBool(pos: number, value: boolean): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_dict object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @returns A promise that resolves to a newly created dictionary object.
         */
        insertDict(pos: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_number object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param value - The value of the Obj::Type::e_number object to be inserted.
         * @returns A promise that resolves to a newly created number object.
         */
        insertNumber(pos: number, value: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_string object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param value - The value of the Obj::Type::e_string object to be inserted.
         * @returns A promise that resolves to a newly created string object.
         */
        insertString(pos: number, value: string): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_string object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param value - The buffer used to set the value of the Obj::Type::e_string
         * object to be inserted.
         * @param size - The number of bytes to copy from the 'value' buffer parameter.
         * @returns A promise that resolves to a newly created string object.
         */
        insertStringWithSize(pos: number, value: string, size: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_string object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param t - The value of the Obj::Type::e_string object to be inserted.
         * @returns A promise that resolves to a newly created string object.
         */
        insertText(pos: number, t: string): Promise<PDFNet.Obj>;
        /**
         * Inserts an Obj::Type::e_null object in the array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @returns A promise that resolves to a newly created null object.
         */
        insertNull(pos: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an existing Obj in this array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param input_obj - The value to be inserted into the dictionary. If 'obj' is
         * indirect (i.e. is a shared) object it will be inserted by reference,
         * otherwise the object will be cloned and then inserted.
         * @returns A promise that resolves to a newly inserted object.
         */
        insert(pos: number, input_obj: PDFNet.Obj): Promise<PDFNet.Obj>;
        /**
         * Inserts an array of 4 numbers in this array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param x1 - The bottom left x value of the rect to be inserted
         * @param y1 - The bottom left y value of the rect to be inserted
         * @param x2 - The top right x value of the rect to be inserted
         * @param y2 - The top right y value of the rect to be inserted
         * @returns A promise that resolves to a newly created array object.
         */
        insertRect(pos: number, x1: number, y1: number, x2: number, y2: number): Promise<PDFNet.Obj>;
        /**
         * Inserts an array of 6 numbers in this array.
         * @param pos - The location in the array to insert the object . The object is inserted
         *          before the specified location. The first element in an array has a pos of
         * 	        zero. If pos >= Array->Length(), appends obj to array.
         * @param mtx - A matrix used to set the values in an array of six numbers.
         * The resulting array will be then inserted in this array.
         * @returns A promise that resolves to a newly created array object.
         */
        insertMatrix(pos: number, mtx: PDFNet.Matrix2D): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_name object at the end of the array.
         * @param name - The value of the Obj::Type::e_name object.
         * @returns A promise that resolves to the new array object.
         */
        pushBackName(name: string): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_array object at the end of the array.
         * @returns A promise that resolves to the new array object.
         */
        pushBackArray(): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_bool object at the end of the array.
         * @param value - The value of the Obj::Type::e_bool object.
         * @returns A promise that resolves to the new boolean object.
         */
        pushBackBool(value: boolean): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_dict object at the end of the array.
         * @returns A promise that resolves to the new dictionary object.
         */
        pushBackDict(): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_number object at the end of the array.
         * @param value - The value of the Obj::Type::e_number object.
         * @returns A promise that resolves to the new number object.
         */
        pushBackNumber(value: number): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_string object at the end of the array.
         * @param value - The value of the Obj::Type::e_string object.
         * @returns A promise that resolves to the new string object.
         */
        pushBackString(value: string): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_string object at the end of the array.
         * @param value - The buffer used to set the value of the Obj::Type::e_string
         * object to be inserted.
         * @param size - The number of bytes to copy from the 'value' buffer parameter.
         * @returns A promise that resolves to the new string object.
         */
        pushBackStringWithSize(value: string, size: number): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_string object at the end of the array.
         * @param t - The value of the Obj::Type::e_string object to be inserted.
         * @returns A promise that resolves to the new string object.
         */
        pushBackText(t: string): Promise<PDFNet.Obj>;
        /**
         * Appends a new Obj::Type::e_null object at the end of the array.
         * @returns A promise that resolves to the new null object.
         */
        pushBackNull(): Promise<PDFNet.Obj>;
        /**
         * Appends an existing Obj at the end of the array.
         * @param input_obj - The value to be inserted into the dictionary. If 'obj' is
         * indirect (i.e. is a shared) object it will be inserted by reference,
         * otherwise the object will be cloned and then appended.
         * @returns A promise that resolves to a newly appended object.
         */
        pushBack(input_obj: PDFNet.Obj): Promise<PDFNet.Obj>;
        /**
         * Appends an array of 4 numbers at the end of the array.
         * @param x1 - The bottom left x value of the rect to be inserted
         * @param y1 - The bottom left y value of the rect to be inserted
         * @param x2 - The top right x value of the rect to be inserted
         * @param y2 - The top right y value of the rect to be inserted
         * @returns A promise that resolves to a newly appended array object.
         */
        pushBackRect(x1: number, y1: number, x2: number, y2: number): Promise<PDFNet.Obj>;
        /**
         * Appends an array of 6 numbers at the end of the array.
         * @param mtx - A matrix used to set the values in an array of six numbers.
         * The resulting array will be then inserted in this array.
         * @returns A promise that resolves to a newly appended array object.
         */
        pushBackMatrix(mtx: PDFNet.Matrix2D): Promise<PDFNet.Obj>;
        /**
         * Checks whether the position is within the array bounds and then removes it from the
         * array and moves each subsequent element to the slot with the next smaller index and
         * decrements the arrays length by 1.
         * @param pos - The index for the array member to remove. Array indexes start at 0.
         */
        eraseAt(pos: number): Promise<void>;
        /**
         * @returns A promise that resolves to true if this is a Stream, false otherwise.
         */
        isStream(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the length of the raw/encoded stream equal to the Length parameter
         */
        getRawStreamLength(): Promise<number>;
        setStreamData(data: string, data_size: number): Promise<void>;
        setStreamDataWithFilter(data: string, data_size: number, filter_chain?: PDFNet.Filter): Promise<void>;
        /**
         * @param decrypt - If true decrypt the stream if the stream is encrypted.
         * @returns A promise that resolves to A filter to the encoded stream
         */
        getRawStream(decrypt: boolean): Promise<PDFNet.Filter>;
        /**
         * @returns A promise that resolves to A filter to the decoded stream
         */
        getDecodedStream(): Promise<PDFNet.Filter>;
        /**
         * Convert the SDF/Cos String object to 'PDF Text String' (a Unicode string).
        
        PDF Text Strings are not used to represent page content, however they
        are used in text annotations, bookmark names, article names, document
        information etc. These strings are encoded in either PDFDocEncoding or
        Unicode character encoding. For more information on PDF Text Strings,
        please refer to section 3.8.1 'Text Strings' in PDF Reference.
         * @returns A promise that resolves to the Unicode string of the SDF/Cos String object
         */
        getAsPDFText(): Promise<string>;
    }
    /**
     * ObjSet is a lightweight container that can hold a collection of SDF objects.
     */
    class ObjSet extends PDFNet.Destroyable {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.ObjSet"
         */
        static create(): Promise<PDFNet.ObjSet>;
        /**
         * Create a new name object in this object set.
         * @param name - The name of the object to create
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createName(name: string): Promise<PDFNet.Obj>;
        /**
         * Create a new array object in this object set.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createArray(): Promise<PDFNet.Obj>;
        /**
         * Create a new boolean object in this object set.
         * @param value - The boolean value of the object to create
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createBool(value: boolean): Promise<PDFNet.Obj>;
        /**
         * Create a new dictionary object in this object set.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createDict(): Promise<PDFNet.Obj>;
        /**
         * Create a new null object in this object set.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createNull(): Promise<PDFNet.Obj>;
        /**
         * Create a new number object in this object set.
         * @param value - numeric value of the number object to create.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createNumber(value: number): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createString(value: string): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createFromJson(json: string): Promise<PDFNet.Obj>;
    }
    /**
     * This class represents an object identifier (OID), as defined by ITU and used in X.509.
     */
    class ObjectIdentifier extends PDFNet.Destroyable {
        /**
         * Constructs an ObjectIdentifier from an enum.
         * @param in_oid_enum - the enumerated value to use
         * @returns A promise that resolves to an object of type: "PDFNet.ObjectIdentifier"
         */
        static createFromPredefined(in_oid_enum: number): Promise<PDFNet.ObjectIdentifier>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ObjectIdentifier"
         */
        static createFromIntArray(in_list: number[]): Promise<PDFNet.ObjectIdentifier>;
        /**
         * Retrieves the value of the object identifier.
         * @returns A promise that resolves to the value of the object identifier, as a container of integer components.
         */
        getRawValue(): Promise<number[]>;
    }
    /**
     * [Missing documentation]
     */
    class OwnedBitmap {
    }
    /**
     * PDFACompliance class is used to validate PDF documents for PDF/A (ISO 19005:1/2/3)
     * compliance or to convert existing PDF files to PDF/A compliant documents.
     *
     * The conversion option analyzes the content of existing PDF files and performs
     * a sequence of modifications in order to produce a PDF/A compliant document.
     * Features that are not suitable for long-term archiving (such as encryption,
     * obsolete compression schemes, missing fonts, or device-dependent color) are
     * replaced with their PDF/A compliant equivalents. Because the conversion process
     * applies only necessary changes to the source file, the information loss is
     * minimal. Also, because the converter provides a detailed report for each change,
     * it is simple to inspect changes and to determine whether the conversion loss
     * is acceptable.
     *
     * The validation option in PDF/A Manager can be used to quickly determine whether
     * a PDF file fully complies with the PDF/A specification according to the
     * international standard ISO 19005:1/2/3. For files that are not compliant, the
     * validation option can be used to produce a detailed report of compliance
     * violations as well as a list of relevant error objects.
     *
     * Key Functions:
     * - Checks if a PDF file is compliant with PDF/A (ISO 19005:1/2/3) specification.
     * - Converts any PDF to a PDF/A compliant document.
     * - Supports PDF/A-1a, PDF/A-1b, PDF/A-2b
     * - Produces a detailed report of compliance violations and associated PDF objects.
     * - Keeps the required changes a minimum, preserving the consistency of the original.
     * - Tracks all changes to allow for automatic assessment of data loss.
     * - Allows user to customize compliance checks or omit specific changes.
     * - Preserves tags, logical structure, and color information in existing PDF documents.
     * - Offers automatic font substitution, embedding, and subsetting options.
     * - Supports automation and batch operation. PDF/A Converter is designed to be used
     *   in unattended mode in high throughput server or batch environments
     */
    class PDFACompliance extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to the number of compliance violations.
         */
        getErrorCount(): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFACompliance.ErrorCode = {
         * 	e_PDFA0_1_0 : 10
         * 	e_PDFA0_1_1 : 11
         * 	e_PDFA0_1_2 : 12
         * 	e_PDFA0_1_3 : 13
         * 	e_PDFA0_1_4 : 14
         * 	e_PDFA0_1_5 : 15
         * 	e_PDFA1_2_1 : 121
         * 	e_PDFA1_2_2 : 122
         * 	e_PDFA1_3_1 : 131
         * 	e_PDFA1_3_2 : 132
         * 	e_PDFA1_3_3 : 133
         * 	e_PDFA1_3_4 : 134
         * 	e_PDFA1_4_1 : 141
         * 	e_PDFA1_4_2 : 142
         * 	e_PDFA1_6_1 : 161
         * 	e_PDFA1_7_1 : 171
         * 	e_PDFA1_7_2 : 172
         * 	e_PDFA1_7_3 : 173
         * 	e_PDFA1_7_4 : 174
         * 	e_PDFA1_8_1 : 181
         * 	e_PDFA1_8_2 : 182
         * 	e_PDFA1_8_3 : 183
         * 	e_PDFA1_8_4 : 184
         * 	e_PDFA1_8_5 : 185
         * 	e_PDFA1_8_6 : 186
         * 	e_PDFA1_10_1 : 1101
         * 	e_PDFA1_11_1 : 1111
         * 	e_PDFA1_11_2 : 1112
         * 	e_PDFA1_12_1 : 1121
         * 	e_PDFA1_12_2 : 1122
         * 	e_PDFA1_12_3 : 1123
         * 	e_PDFA1_12_4 : 1124
         * 	e_PDFA1_12_5 : 1125
         * 	e_PDFA1_12_6 : 1126
         * 	e_PDFA1_13_1 : 1131
         * 	e_PDFA2_2_1 : 221
         * 	e_PDFA2_3_2 : 232
         * 	e_PDFA2_3_3 : 233
         * 	e_PDFA2_3_3_1 : 2331
         * 	e_PDFA2_3_3_2 : 2332
         * 	e_PDFA2_3_4_1 : 2341
         * 	e_PDFA2_4_1 : 241
         * 	e_PDFA2_4_2 : 242
         * 	e_PDFA2_4_3 : 243
         * 	e_PDFA2_4_4 : 244
         * 	e_PDFA2_5_1 : 251
         * 	e_PDFA2_5_2 : 252
         * 	e_PDFA2_6_1 : 261
         * 	e_PDFA2_7_1 : 271
         * 	e_PDFA2_8_1 : 281
         * 	e_PDFA2_9_1 : 291
         * 	e_PDFA2_10_1 : 2101
         * 	e_PDFA3_2_1 : 321
         * 	e_PDFA3_3_1 : 331
         * 	e_PDFA3_3_2 : 332
         * 	e_PDFA3_3_3_1 : 3331
         * 	e_PDFA3_3_3_2 : 3332
         * 	e_PDFA3_4_1 : 341
         * 	e_PDFA3_5_1 : 351
         * 	e_PDFA3_5_2 : 352
         * 	e_PDFA3_5_3 : 353
         * 	e_PDFA3_5_4 : 354
         * 	e_PDFA3_5_5 : 355
         * 	e_PDFA3_5_6 : 356
         * 	e_PDFA3_6_1 : 361
         * 	e_PDFA3_7_1 : 371
         * 	e_PDFA3_7_2 : 372
         * 	e_PDFA3_7_3 : 373
         * 	e_PDFA4_1 : 41
         * 	e_PDFA4_2 : 42
         * 	e_PDFA4_3 : 43
         * 	e_PDFA4_4 : 44
         * 	e_PDFA4_5 : 45
         * 	e_PDFA4_6 : 46
         * 	e_PDFA5_2_1 : 521
         * 	e_PDFA5_2_2 : 522
         * 	e_PDFA5_2_3 : 523
         * 	e_PDFA5_2_4 : 524
         * 	e_PDFA5_2_5 : 525
         * 	e_PDFA5_2_6 : 526
         * 	e_PDFA5_2_7 : 527
         * 	e_PDFA5_2_8 : 528
         * 	e_PDFA5_2_9 : 529
         * 	e_PDFA5_2_10 : 5210
         * 	e_PDFA5_2_11 : 5211
         * 	e_PDFA5_3_1 : 531
         * 	e_PDFA5_3_2_1 : 5321
         * 	e_PDFA5_3_2_2 : 5322
         * 	e_PDFA5_3_2_3 : 5323
         * 	e_PDFA5_3_2_4 : 5324
         * 	e_PDFA5_3_2_5 : 5325
         * 	e_PDFA5_3_3_1 : 5331
         * 	e_PDFA5_3_3_2 : 5332
         * 	e_PDFA5_3_3_3 : 5333
         * 	e_PDFA5_3_3_4 : 5334
         * 	e_PDFA5_3_4_0 : 5340
         * 	e_PDFA5_3_4_1 : 5341
         * 	e_PDFA5_3_4_2 : 5342
         * 	e_PDFA5_3_4_3 : 5343
         * 	e_PDFA6_1_1 : 611
         * 	e_PDFA6_1_2 : 612
         * 	e_PDFA6_2_1 : 621
         * 	e_PDFA6_2_2 : 622
         * 	e_PDFA6_2_3 : 623
         * 	e_PDFA7_2_1 : 721
         * 	e_PDFA7_2_2 : 722
         * 	e_PDFA7_2_3 : 723
         * 	e_PDFA7_2_4 : 724
         * 	e_PDFA7_2_5 : 725
         * 	e_PDFA7_3_1 : 731
         * 	e_PDFA7_3_2 : 732
         * 	e_PDFA7_3_3 : 733
         * 	e_PDFA7_3_4 : 734
         * 	e_PDFA7_3_5 : 735
         * 	e_PDFA7_3_6 : 736
         * 	e_PDFA7_3_7 : 737
         * 	e_PDFA7_3_8 : 738
         * 	e_PDFA7_3_9 : 739
         * 	e_PDFA7_5_1 : 751
         * 	e_PDFA7_8_1 : 781
         * 	e_PDFA7_8_2 : 782
         * 	e_PDFA7_8_3 : 783
         * 	e_PDFA7_8_4 : 784
         * 	e_PDFA7_8_5 : 785
         * 	e_PDFA7_8_6 : 786
         * 	e_PDFA7_8_7 : 787
         * 	e_PDFA7_8_8 : 788
         * 	e_PDFA7_8_9 : 789
         * 	e_PDFA7_8_10 : 7810
         * 	e_PDFA7_8_11 : 7811
         * 	e_PDFA7_8_12 : 7812
         * 	e_PDFA7_8_13 : 7813
         * 	e_PDFA7_8_14 : 7814
         * 	e_PDFA7_8_15 : 7815
         * 	e_PDFA7_8_16 : 7816
         * 	e_PDFA7_8_17 : 7817
         * 	e_PDFA7_8_18 : 7818
         * 	e_PDFA7_8_19 : 7819
         * 	e_PDFA7_8_20 : 7820
         * 	e_PDFA7_8_21 : 7821
         * 	e_PDFA7_8_22 : 7822
         * 	e_PDFA7_8_23 : 7823
         * 	e_PDFA7_8_24 : 7824
         * 	e_PDFA7_8_25 : 7825
         * 	e_PDFA7_8_26 : 7826
         * 	e_PDFA7_8_27 : 7827
         * 	e_PDFA7_8_28 : 7828
         * 	e_PDFA7_8_29 : 7829
         * 	e_PDFA7_8_30 : 7830
         * 	e_PDFA7_8_31 : 7831
         * 	e_PDFA7_11_1 : 7111
         * 	e_PDFA7_11_2 : 7112
         * 	e_PDFA7_11_3 : 7113
         * 	e_PDFA7_11_4 : 7114
         * 	e_PDFA7_11_5 : 7115
         * 	e_PDFA9_1 : 91
         * 	e_PDFA9_2 : 92
         * 	e_PDFA9_3 : 93
         * 	e_PDFA9_4 : 94
         * 	e_PDFA3_8_1 : 381
         * 	e_PDFA8_2_2 : 822
         * 	e_PDFA8_3_3_1 : 8331
         * 	e_PDFA8_3_3_2 : 8332
         * 	e_PDFA8_3_4_1 : 8341
         * 	e_PDFA1_2_3 : 123
         * 	e_PDFA1_10_2 : 1102
         * 	e_PDFA1_10_3 : 1103
         * 	e_PDFA1_12_10 : 11210
         * 	e_PDFA1_13_5 : 1135
         * 	e_PDFA2_3_10 : 2310
         * 	e_PDFA2_4_2_10 : 24220
         * 	e_PDFA2_4_2_11 : 24221
         * 	e_PDFA2_4_2_12 : 24222
         * 	e_PDFA2_4_2_13 : 24223
         * 	e_PDFA2_5_10 : 2510
         * 	e_PDFA2_5_11 : 2511
         * 	e_PDFA2_5_12 : 2512
         * 	e_PDFA2_8_3_1 : 2831
         * 	e_PDFA2_8_3_2 : 2832
         * 	e_PDFA2_8_3_3 : 2833
         * 	e_PDFA2_8_3_4 : 2834
         * 	e_PDFA2_8_3_5 : 2835
         * 	e_PDFA2_10_20 : 21020
         * 	e_PDFA2_10_21 : 21021
         * 	e_PDFA11_0_0 : 11000
         * 	e_PDFA6_2_11_8 : 62118
         * 	e_PDFA8_1 : 81
         * 	e_PDFA_3E1 : 1
         * 	e_PDFA_3E2 : 2
         * 	e_PDFA_3E3 : 3
         * 	e_PDFA_LAST : 4
         * }
         * </pre>
         * @param idx - The index in the array of error code identifiers.
         * The array is indexed starting from zero.
         * @returns A promise that resolves to the error identifier.
         */
        getError(idx: number): Promise<number>;
        /**
         * @param id - <pre>
         * PDFNet.PDFACompliance.ErrorCode = {
         * 	e_PDFA0_1_0 : 10
         * 	e_PDFA0_1_1 : 11
         * 	e_PDFA0_1_2 : 12
         * 	e_PDFA0_1_3 : 13
         * 	e_PDFA0_1_4 : 14
         * 	e_PDFA0_1_5 : 15
         * 	e_PDFA1_2_1 : 121
         * 	e_PDFA1_2_2 : 122
         * 	e_PDFA1_3_1 : 131
         * 	e_PDFA1_3_2 : 132
         * 	e_PDFA1_3_3 : 133
         * 	e_PDFA1_3_4 : 134
         * 	e_PDFA1_4_1 : 141
         * 	e_PDFA1_4_2 : 142
         * 	e_PDFA1_6_1 : 161
         * 	e_PDFA1_7_1 : 171
         * 	e_PDFA1_7_2 : 172
         * 	e_PDFA1_7_3 : 173
         * 	e_PDFA1_7_4 : 174
         * 	e_PDFA1_8_1 : 181
         * 	e_PDFA1_8_2 : 182
         * 	e_PDFA1_8_3 : 183
         * 	e_PDFA1_8_4 : 184
         * 	e_PDFA1_8_5 : 185
         * 	e_PDFA1_8_6 : 186
         * 	e_PDFA1_10_1 : 1101
         * 	e_PDFA1_11_1 : 1111
         * 	e_PDFA1_11_2 : 1112
         * 	e_PDFA1_12_1 : 1121
         * 	e_PDFA1_12_2 : 1122
         * 	e_PDFA1_12_3 : 1123
         * 	e_PDFA1_12_4 : 1124
         * 	e_PDFA1_12_5 : 1125
         * 	e_PDFA1_12_6 : 1126
         * 	e_PDFA1_13_1 : 1131
         * 	e_PDFA2_2_1 : 221
         * 	e_PDFA2_3_2 : 232
         * 	e_PDFA2_3_3 : 233
         * 	e_PDFA2_3_3_1 : 2331
         * 	e_PDFA2_3_3_2 : 2332
         * 	e_PDFA2_3_4_1 : 2341
         * 	e_PDFA2_4_1 : 241
         * 	e_PDFA2_4_2 : 242
         * 	e_PDFA2_4_3 : 243
         * 	e_PDFA2_4_4 : 244
         * 	e_PDFA2_5_1 : 251
         * 	e_PDFA2_5_2 : 252
         * 	e_PDFA2_6_1 : 261
         * 	e_PDFA2_7_1 : 271
         * 	e_PDFA2_8_1 : 281
         * 	e_PDFA2_9_1 : 291
         * 	e_PDFA2_10_1 : 2101
         * 	e_PDFA3_2_1 : 321
         * 	e_PDFA3_3_1 : 331
         * 	e_PDFA3_3_2 : 332
         * 	e_PDFA3_3_3_1 : 3331
         * 	e_PDFA3_3_3_2 : 3332
         * 	e_PDFA3_4_1 : 341
         * 	e_PDFA3_5_1 : 351
         * 	e_PDFA3_5_2 : 352
         * 	e_PDFA3_5_3 : 353
         * 	e_PDFA3_5_4 : 354
         * 	e_PDFA3_5_5 : 355
         * 	e_PDFA3_5_6 : 356
         * 	e_PDFA3_6_1 : 361
         * 	e_PDFA3_7_1 : 371
         * 	e_PDFA3_7_2 : 372
         * 	e_PDFA3_7_3 : 373
         * 	e_PDFA4_1 : 41
         * 	e_PDFA4_2 : 42
         * 	e_PDFA4_3 : 43
         * 	e_PDFA4_4 : 44
         * 	e_PDFA4_5 : 45
         * 	e_PDFA4_6 : 46
         * 	e_PDFA5_2_1 : 521
         * 	e_PDFA5_2_2 : 522
         * 	e_PDFA5_2_3 : 523
         * 	e_PDFA5_2_4 : 524
         * 	e_PDFA5_2_5 : 525
         * 	e_PDFA5_2_6 : 526
         * 	e_PDFA5_2_7 : 527
         * 	e_PDFA5_2_8 : 528
         * 	e_PDFA5_2_9 : 529
         * 	e_PDFA5_2_10 : 5210
         * 	e_PDFA5_2_11 : 5211
         * 	e_PDFA5_3_1 : 531
         * 	e_PDFA5_3_2_1 : 5321
         * 	e_PDFA5_3_2_2 : 5322
         * 	e_PDFA5_3_2_3 : 5323
         * 	e_PDFA5_3_2_4 : 5324
         * 	e_PDFA5_3_2_5 : 5325
         * 	e_PDFA5_3_3_1 : 5331
         * 	e_PDFA5_3_3_2 : 5332
         * 	e_PDFA5_3_3_3 : 5333
         * 	e_PDFA5_3_3_4 : 5334
         * 	e_PDFA5_3_4_0 : 5340
         * 	e_PDFA5_3_4_1 : 5341
         * 	e_PDFA5_3_4_2 : 5342
         * 	e_PDFA5_3_4_3 : 5343
         * 	e_PDFA6_1_1 : 611
         * 	e_PDFA6_1_2 : 612
         * 	e_PDFA6_2_1 : 621
         * 	e_PDFA6_2_2 : 622
         * 	e_PDFA6_2_3 : 623
         * 	e_PDFA7_2_1 : 721
         * 	e_PDFA7_2_2 : 722
         * 	e_PDFA7_2_3 : 723
         * 	e_PDFA7_2_4 : 724
         * 	e_PDFA7_2_5 : 725
         * 	e_PDFA7_3_1 : 731
         * 	e_PDFA7_3_2 : 732
         * 	e_PDFA7_3_3 : 733
         * 	e_PDFA7_3_4 : 734
         * 	e_PDFA7_3_5 : 735
         * 	e_PDFA7_3_6 : 736
         * 	e_PDFA7_3_7 : 737
         * 	e_PDFA7_3_8 : 738
         * 	e_PDFA7_3_9 : 739
         * 	e_PDFA7_5_1 : 751
         * 	e_PDFA7_8_1 : 781
         * 	e_PDFA7_8_2 : 782
         * 	e_PDFA7_8_3 : 783
         * 	e_PDFA7_8_4 : 784
         * 	e_PDFA7_8_5 : 785
         * 	e_PDFA7_8_6 : 786
         * 	e_PDFA7_8_7 : 787
         * 	e_PDFA7_8_8 : 788
         * 	e_PDFA7_8_9 : 789
         * 	e_PDFA7_8_10 : 7810
         * 	e_PDFA7_8_11 : 7811
         * 	e_PDFA7_8_12 : 7812
         * 	e_PDFA7_8_13 : 7813
         * 	e_PDFA7_8_14 : 7814
         * 	e_PDFA7_8_15 : 7815
         * 	e_PDFA7_8_16 : 7816
         * 	e_PDFA7_8_17 : 7817
         * 	e_PDFA7_8_18 : 7818
         * 	e_PDFA7_8_19 : 7819
         * 	e_PDFA7_8_20 : 7820
         * 	e_PDFA7_8_21 : 7821
         * 	e_PDFA7_8_22 : 7822
         * 	e_PDFA7_8_23 : 7823
         * 	e_PDFA7_8_24 : 7824
         * 	e_PDFA7_8_25 : 7825
         * 	e_PDFA7_8_26 : 7826
         * 	e_PDFA7_8_27 : 7827
         * 	e_PDFA7_8_28 : 7828
         * 	e_PDFA7_8_29 : 7829
         * 	e_PDFA7_8_30 : 7830
         * 	e_PDFA7_8_31 : 7831
         * 	e_PDFA7_11_1 : 7111
         * 	e_PDFA7_11_2 : 7112
         * 	e_PDFA7_11_3 : 7113
         * 	e_PDFA7_11_4 : 7114
         * 	e_PDFA7_11_5 : 7115
         * 	e_PDFA9_1 : 91
         * 	e_PDFA9_2 : 92
         * 	e_PDFA9_3 : 93
         * 	e_PDFA9_4 : 94
         * 	e_PDFA3_8_1 : 381
         * 	e_PDFA8_2_2 : 822
         * 	e_PDFA8_3_3_1 : 8331
         * 	e_PDFA8_3_3_2 : 8332
         * 	e_PDFA8_3_4_1 : 8341
         * 	e_PDFA1_2_3 : 123
         * 	e_PDFA1_10_2 : 1102
         * 	e_PDFA1_10_3 : 1103
         * 	e_PDFA1_12_10 : 11210
         * 	e_PDFA1_13_5 : 1135
         * 	e_PDFA2_3_10 : 2310
         * 	e_PDFA2_4_2_10 : 24220
         * 	e_PDFA2_4_2_11 : 24221
         * 	e_PDFA2_4_2_12 : 24222
         * 	e_PDFA2_4_2_13 : 24223
         * 	e_PDFA2_5_10 : 2510
         * 	e_PDFA2_5_11 : 2511
         * 	e_PDFA2_5_12 : 2512
         * 	e_PDFA2_8_3_1 : 2831
         * 	e_PDFA2_8_3_2 : 2832
         * 	e_PDFA2_8_3_3 : 2833
         * 	e_PDFA2_8_3_4 : 2834
         * 	e_PDFA2_8_3_5 : 2835
         * 	e_PDFA2_10_20 : 21020
         * 	e_PDFA2_10_21 : 21021
         * 	e_PDFA11_0_0 : 11000
         * 	e_PDFA6_2_11_8 : 62118
         * 	e_PDFA8_1 : 81
         * 	e_PDFA_3E1 : 1
         * 	e_PDFA_3E2 : 2
         * 	e_PDFA_3E3 : 3
         * 	e_PDFA_LAST : 4
         * }
         * </pre>
         * error code identifier (obtained using GetError() method).
         * @returns A promise that resolves to the number of object references associated with a given error.
         */
        getRefObjCount(id: number): Promise<number>;
        /**
         * @param id - <pre>
         * PDFNet.PDFACompliance.ErrorCode = {
         * 	e_PDFA0_1_0 : 10
         * 	e_PDFA0_1_1 : 11
         * 	e_PDFA0_1_2 : 12
         * 	e_PDFA0_1_3 : 13
         * 	e_PDFA0_1_4 : 14
         * 	e_PDFA0_1_5 : 15
         * 	e_PDFA1_2_1 : 121
         * 	e_PDFA1_2_2 : 122
         * 	e_PDFA1_3_1 : 131
         * 	e_PDFA1_3_2 : 132
         * 	e_PDFA1_3_3 : 133
         * 	e_PDFA1_3_4 : 134
         * 	e_PDFA1_4_1 : 141
         * 	e_PDFA1_4_2 : 142
         * 	e_PDFA1_6_1 : 161
         * 	e_PDFA1_7_1 : 171
         * 	e_PDFA1_7_2 : 172
         * 	e_PDFA1_7_3 : 173
         * 	e_PDFA1_7_4 : 174
         * 	e_PDFA1_8_1 : 181
         * 	e_PDFA1_8_2 : 182
         * 	e_PDFA1_8_3 : 183
         * 	e_PDFA1_8_4 : 184
         * 	e_PDFA1_8_5 : 185
         * 	e_PDFA1_8_6 : 186
         * 	e_PDFA1_10_1 : 1101
         * 	e_PDFA1_11_1 : 1111
         * 	e_PDFA1_11_2 : 1112
         * 	e_PDFA1_12_1 : 1121
         * 	e_PDFA1_12_2 : 1122
         * 	e_PDFA1_12_3 : 1123
         * 	e_PDFA1_12_4 : 1124
         * 	e_PDFA1_12_5 : 1125
         * 	e_PDFA1_12_6 : 1126
         * 	e_PDFA1_13_1 : 1131
         * 	e_PDFA2_2_1 : 221
         * 	e_PDFA2_3_2 : 232
         * 	e_PDFA2_3_3 : 233
         * 	e_PDFA2_3_3_1 : 2331
         * 	e_PDFA2_3_3_2 : 2332
         * 	e_PDFA2_3_4_1 : 2341
         * 	e_PDFA2_4_1 : 241
         * 	e_PDFA2_4_2 : 242
         * 	e_PDFA2_4_3 : 243
         * 	e_PDFA2_4_4 : 244
         * 	e_PDFA2_5_1 : 251
         * 	e_PDFA2_5_2 : 252
         * 	e_PDFA2_6_1 : 261
         * 	e_PDFA2_7_1 : 271
         * 	e_PDFA2_8_1 : 281
         * 	e_PDFA2_9_1 : 291
         * 	e_PDFA2_10_1 : 2101
         * 	e_PDFA3_2_1 : 321
         * 	e_PDFA3_3_1 : 331
         * 	e_PDFA3_3_2 : 332
         * 	e_PDFA3_3_3_1 : 3331
         * 	e_PDFA3_3_3_2 : 3332
         * 	e_PDFA3_4_1 : 341
         * 	e_PDFA3_5_1 : 351
         * 	e_PDFA3_5_2 : 352
         * 	e_PDFA3_5_3 : 353
         * 	e_PDFA3_5_4 : 354
         * 	e_PDFA3_5_5 : 355
         * 	e_PDFA3_5_6 : 356
         * 	e_PDFA3_6_1 : 361
         * 	e_PDFA3_7_1 : 371
         * 	e_PDFA3_7_2 : 372
         * 	e_PDFA3_7_3 : 373
         * 	e_PDFA4_1 : 41
         * 	e_PDFA4_2 : 42
         * 	e_PDFA4_3 : 43
         * 	e_PDFA4_4 : 44
         * 	e_PDFA4_5 : 45
         * 	e_PDFA4_6 : 46
         * 	e_PDFA5_2_1 : 521
         * 	e_PDFA5_2_2 : 522
         * 	e_PDFA5_2_3 : 523
         * 	e_PDFA5_2_4 : 524
         * 	e_PDFA5_2_5 : 525
         * 	e_PDFA5_2_6 : 526
         * 	e_PDFA5_2_7 : 527
         * 	e_PDFA5_2_8 : 528
         * 	e_PDFA5_2_9 : 529
         * 	e_PDFA5_2_10 : 5210
         * 	e_PDFA5_2_11 : 5211
         * 	e_PDFA5_3_1 : 531
         * 	e_PDFA5_3_2_1 : 5321
         * 	e_PDFA5_3_2_2 : 5322
         * 	e_PDFA5_3_2_3 : 5323
         * 	e_PDFA5_3_2_4 : 5324
         * 	e_PDFA5_3_2_5 : 5325
         * 	e_PDFA5_3_3_1 : 5331
         * 	e_PDFA5_3_3_2 : 5332
         * 	e_PDFA5_3_3_3 : 5333
         * 	e_PDFA5_3_3_4 : 5334
         * 	e_PDFA5_3_4_0 : 5340
         * 	e_PDFA5_3_4_1 : 5341
         * 	e_PDFA5_3_4_2 : 5342
         * 	e_PDFA5_3_4_3 : 5343
         * 	e_PDFA6_1_1 : 611
         * 	e_PDFA6_1_2 : 612
         * 	e_PDFA6_2_1 : 621
         * 	e_PDFA6_2_2 : 622
         * 	e_PDFA6_2_3 : 623
         * 	e_PDFA7_2_1 : 721
         * 	e_PDFA7_2_2 : 722
         * 	e_PDFA7_2_3 : 723
         * 	e_PDFA7_2_4 : 724
         * 	e_PDFA7_2_5 : 725
         * 	e_PDFA7_3_1 : 731
         * 	e_PDFA7_3_2 : 732
         * 	e_PDFA7_3_3 : 733
         * 	e_PDFA7_3_4 : 734
         * 	e_PDFA7_3_5 : 735
         * 	e_PDFA7_3_6 : 736
         * 	e_PDFA7_3_7 : 737
         * 	e_PDFA7_3_8 : 738
         * 	e_PDFA7_3_9 : 739
         * 	e_PDFA7_5_1 : 751
         * 	e_PDFA7_8_1 : 781
         * 	e_PDFA7_8_2 : 782
         * 	e_PDFA7_8_3 : 783
         * 	e_PDFA7_8_4 : 784
         * 	e_PDFA7_8_5 : 785
         * 	e_PDFA7_8_6 : 786
         * 	e_PDFA7_8_7 : 787
         * 	e_PDFA7_8_8 : 788
         * 	e_PDFA7_8_9 : 789
         * 	e_PDFA7_8_10 : 7810
         * 	e_PDFA7_8_11 : 7811
         * 	e_PDFA7_8_12 : 7812
         * 	e_PDFA7_8_13 : 7813
         * 	e_PDFA7_8_14 : 7814
         * 	e_PDFA7_8_15 : 7815
         * 	e_PDFA7_8_16 : 7816
         * 	e_PDFA7_8_17 : 7817
         * 	e_PDFA7_8_18 : 7818
         * 	e_PDFA7_8_19 : 7819
         * 	e_PDFA7_8_20 : 7820
         * 	e_PDFA7_8_21 : 7821
         * 	e_PDFA7_8_22 : 7822
         * 	e_PDFA7_8_23 : 7823
         * 	e_PDFA7_8_24 : 7824
         * 	e_PDFA7_8_25 : 7825
         * 	e_PDFA7_8_26 : 7826
         * 	e_PDFA7_8_27 : 7827
         * 	e_PDFA7_8_28 : 7828
         * 	e_PDFA7_8_29 : 7829
         * 	e_PDFA7_8_30 : 7830
         * 	e_PDFA7_8_31 : 7831
         * 	e_PDFA7_11_1 : 7111
         * 	e_PDFA7_11_2 : 7112
         * 	e_PDFA7_11_3 : 7113
         * 	e_PDFA7_11_4 : 7114
         * 	e_PDFA7_11_5 : 7115
         * 	e_PDFA9_1 : 91
         * 	e_PDFA9_2 : 92
         * 	e_PDFA9_3 : 93
         * 	e_PDFA9_4 : 94
         * 	e_PDFA3_8_1 : 381
         * 	e_PDFA8_2_2 : 822
         * 	e_PDFA8_3_3_1 : 8331
         * 	e_PDFA8_3_3_2 : 8332
         * 	e_PDFA8_3_4_1 : 8341
         * 	e_PDFA1_2_3 : 123
         * 	e_PDFA1_10_2 : 1102
         * 	e_PDFA1_10_3 : 1103
         * 	e_PDFA1_12_10 : 11210
         * 	e_PDFA1_13_5 : 1135
         * 	e_PDFA2_3_10 : 2310
         * 	e_PDFA2_4_2_10 : 24220
         * 	e_PDFA2_4_2_11 : 24221
         * 	e_PDFA2_4_2_12 : 24222
         * 	e_PDFA2_4_2_13 : 24223
         * 	e_PDFA2_5_10 : 2510
         * 	e_PDFA2_5_11 : 2511
         * 	e_PDFA2_5_12 : 2512
         * 	e_PDFA2_8_3_1 : 2831
         * 	e_PDFA2_8_3_2 : 2832
         * 	e_PDFA2_8_3_3 : 2833
         * 	e_PDFA2_8_3_4 : 2834
         * 	e_PDFA2_8_3_5 : 2835
         * 	e_PDFA2_10_20 : 21020
         * 	e_PDFA2_10_21 : 21021
         * 	e_PDFA11_0_0 : 11000
         * 	e_PDFA6_2_11_8 : 62118
         * 	e_PDFA8_1 : 81
         * 	e_PDFA_3E1 : 1
         * 	e_PDFA_3E2 : 2
         * 	e_PDFA_3E3 : 3
         * 	e_PDFA_LAST : 4
         * }
         * </pre>
         * error code identifier (obtained using GetError() method).
         * @param err_idx - The index in the array of object references.
         * The array is indexed starting from zero.
         * @returns A promise that resolves to a specific object reference associated with a given error type.
         * The return value is a PDF object identifier (i.e. object number for
         * 'pdftron.SDF.Obj)) for the that is associated with the error.
         */
        getRefObj(id: number, err_idx: number): Promise<number>;
        /**
         * @param id - <pre>
         * PDFNet.PDFACompliance.ErrorCode = {
         * 	e_PDFA0_1_0 : 10
         * 	e_PDFA0_1_1 : 11
         * 	e_PDFA0_1_2 : 12
         * 	e_PDFA0_1_3 : 13
         * 	e_PDFA0_1_4 : 14
         * 	e_PDFA0_1_5 : 15
         * 	e_PDFA1_2_1 : 121
         * 	e_PDFA1_2_2 : 122
         * 	e_PDFA1_3_1 : 131
         * 	e_PDFA1_3_2 : 132
         * 	e_PDFA1_3_3 : 133
         * 	e_PDFA1_3_4 : 134
         * 	e_PDFA1_4_1 : 141
         * 	e_PDFA1_4_2 : 142
         * 	e_PDFA1_6_1 : 161
         * 	e_PDFA1_7_1 : 171
         * 	e_PDFA1_7_2 : 172
         * 	e_PDFA1_7_3 : 173
         * 	e_PDFA1_7_4 : 174
         * 	e_PDFA1_8_1 : 181
         * 	e_PDFA1_8_2 : 182
         * 	e_PDFA1_8_3 : 183
         * 	e_PDFA1_8_4 : 184
         * 	e_PDFA1_8_5 : 185
         * 	e_PDFA1_8_6 : 186
         * 	e_PDFA1_10_1 : 1101
         * 	e_PDFA1_11_1 : 1111
         * 	e_PDFA1_11_2 : 1112
         * 	e_PDFA1_12_1 : 1121
         * 	e_PDFA1_12_2 : 1122
         * 	e_PDFA1_12_3 : 1123
         * 	e_PDFA1_12_4 : 1124
         * 	e_PDFA1_12_5 : 1125
         * 	e_PDFA1_12_6 : 1126
         * 	e_PDFA1_13_1 : 1131
         * 	e_PDFA2_2_1 : 221
         * 	e_PDFA2_3_2 : 232
         * 	e_PDFA2_3_3 : 233
         * 	e_PDFA2_3_3_1 : 2331
         * 	e_PDFA2_3_3_2 : 2332
         * 	e_PDFA2_3_4_1 : 2341
         * 	e_PDFA2_4_1 : 241
         * 	e_PDFA2_4_2 : 242
         * 	e_PDFA2_4_3 : 243
         * 	e_PDFA2_4_4 : 244
         * 	e_PDFA2_5_1 : 251
         * 	e_PDFA2_5_2 : 252
         * 	e_PDFA2_6_1 : 261
         * 	e_PDFA2_7_1 : 271
         * 	e_PDFA2_8_1 : 281
         * 	e_PDFA2_9_1 : 291
         * 	e_PDFA2_10_1 : 2101
         * 	e_PDFA3_2_1 : 321
         * 	e_PDFA3_3_1 : 331
         * 	e_PDFA3_3_2 : 332
         * 	e_PDFA3_3_3_1 : 3331
         * 	e_PDFA3_3_3_2 : 3332
         * 	e_PDFA3_4_1 : 341
         * 	e_PDFA3_5_1 : 351
         * 	e_PDFA3_5_2 : 352
         * 	e_PDFA3_5_3 : 353
         * 	e_PDFA3_5_4 : 354
         * 	e_PDFA3_5_5 : 355
         * 	e_PDFA3_5_6 : 356
         * 	e_PDFA3_6_1 : 361
         * 	e_PDFA3_7_1 : 371
         * 	e_PDFA3_7_2 : 372
         * 	e_PDFA3_7_3 : 373
         * 	e_PDFA4_1 : 41
         * 	e_PDFA4_2 : 42
         * 	e_PDFA4_3 : 43
         * 	e_PDFA4_4 : 44
         * 	e_PDFA4_5 : 45
         * 	e_PDFA4_6 : 46
         * 	e_PDFA5_2_1 : 521
         * 	e_PDFA5_2_2 : 522
         * 	e_PDFA5_2_3 : 523
         * 	e_PDFA5_2_4 : 524
         * 	e_PDFA5_2_5 : 525
         * 	e_PDFA5_2_6 : 526
         * 	e_PDFA5_2_7 : 527
         * 	e_PDFA5_2_8 : 528
         * 	e_PDFA5_2_9 : 529
         * 	e_PDFA5_2_10 : 5210
         * 	e_PDFA5_2_11 : 5211
         * 	e_PDFA5_3_1 : 531
         * 	e_PDFA5_3_2_1 : 5321
         * 	e_PDFA5_3_2_2 : 5322
         * 	e_PDFA5_3_2_3 : 5323
         * 	e_PDFA5_3_2_4 : 5324
         * 	e_PDFA5_3_2_5 : 5325
         * 	e_PDFA5_3_3_1 : 5331
         * 	e_PDFA5_3_3_2 : 5332
         * 	e_PDFA5_3_3_3 : 5333
         * 	e_PDFA5_3_3_4 : 5334
         * 	e_PDFA5_3_4_0 : 5340
         * 	e_PDFA5_3_4_1 : 5341
         * 	e_PDFA5_3_4_2 : 5342
         * 	e_PDFA5_3_4_3 : 5343
         * 	e_PDFA6_1_1 : 611
         * 	e_PDFA6_1_2 : 612
         * 	e_PDFA6_2_1 : 621
         * 	e_PDFA6_2_2 : 622
         * 	e_PDFA6_2_3 : 623
         * 	e_PDFA7_2_1 : 721
         * 	e_PDFA7_2_2 : 722
         * 	e_PDFA7_2_3 : 723
         * 	e_PDFA7_2_4 : 724
         * 	e_PDFA7_2_5 : 725
         * 	e_PDFA7_3_1 : 731
         * 	e_PDFA7_3_2 : 732
         * 	e_PDFA7_3_3 : 733
         * 	e_PDFA7_3_4 : 734
         * 	e_PDFA7_3_5 : 735
         * 	e_PDFA7_3_6 : 736
         * 	e_PDFA7_3_7 : 737
         * 	e_PDFA7_3_8 : 738
         * 	e_PDFA7_3_9 : 739
         * 	e_PDFA7_5_1 : 751
         * 	e_PDFA7_8_1 : 781
         * 	e_PDFA7_8_2 : 782
         * 	e_PDFA7_8_3 : 783
         * 	e_PDFA7_8_4 : 784
         * 	e_PDFA7_8_5 : 785
         * 	e_PDFA7_8_6 : 786
         * 	e_PDFA7_8_7 : 787
         * 	e_PDFA7_8_8 : 788
         * 	e_PDFA7_8_9 : 789
         * 	e_PDFA7_8_10 : 7810
         * 	e_PDFA7_8_11 : 7811
         * 	e_PDFA7_8_12 : 7812
         * 	e_PDFA7_8_13 : 7813
         * 	e_PDFA7_8_14 : 7814
         * 	e_PDFA7_8_15 : 7815
         * 	e_PDFA7_8_16 : 7816
         * 	e_PDFA7_8_17 : 7817
         * 	e_PDFA7_8_18 : 7818
         * 	e_PDFA7_8_19 : 7819
         * 	e_PDFA7_8_20 : 7820
         * 	e_PDFA7_8_21 : 7821
         * 	e_PDFA7_8_22 : 7822
         * 	e_PDFA7_8_23 : 7823
         * 	e_PDFA7_8_24 : 7824
         * 	e_PDFA7_8_25 : 7825
         * 	e_PDFA7_8_26 : 7826
         * 	e_PDFA7_8_27 : 7827
         * 	e_PDFA7_8_28 : 7828
         * 	e_PDFA7_8_29 : 7829
         * 	e_PDFA7_8_30 : 7830
         * 	e_PDFA7_8_31 : 7831
         * 	e_PDFA7_11_1 : 7111
         * 	e_PDFA7_11_2 : 7112
         * 	e_PDFA7_11_3 : 7113
         * 	e_PDFA7_11_4 : 7114
         * 	e_PDFA7_11_5 : 7115
         * 	e_PDFA9_1 : 91
         * 	e_PDFA9_2 : 92
         * 	e_PDFA9_3 : 93
         * 	e_PDFA9_4 : 94
         * 	e_PDFA3_8_1 : 381
         * 	e_PDFA8_2_2 : 822
         * 	e_PDFA8_3_3_1 : 8331
         * 	e_PDFA8_3_3_2 : 8332
         * 	e_PDFA8_3_4_1 : 8341
         * 	e_PDFA1_2_3 : 123
         * 	e_PDFA1_10_2 : 1102
         * 	e_PDFA1_10_3 : 1103
         * 	e_PDFA1_12_10 : 11210
         * 	e_PDFA1_13_5 : 1135
         * 	e_PDFA2_3_10 : 2310
         * 	e_PDFA2_4_2_10 : 24220
         * 	e_PDFA2_4_2_11 : 24221
         * 	e_PDFA2_4_2_12 : 24222
         * 	e_PDFA2_4_2_13 : 24223
         * 	e_PDFA2_5_10 : 2510
         * 	e_PDFA2_5_11 : 2511
         * 	e_PDFA2_5_12 : 2512
         * 	e_PDFA2_8_3_1 : 2831
         * 	e_PDFA2_8_3_2 : 2832
         * 	e_PDFA2_8_3_3 : 2833
         * 	e_PDFA2_8_3_4 : 2834
         * 	e_PDFA2_8_3_5 : 2835
         * 	e_PDFA2_10_20 : 21020
         * 	e_PDFA2_10_21 : 21021
         * 	e_PDFA11_0_0 : 11000
         * 	e_PDFA6_2_11_8 : 62118
         * 	e_PDFA8_1 : 81
         * 	e_PDFA_3E1 : 1
         * 	e_PDFA_3E2 : 2
         * 	e_PDFA_3E3 : 3
         * 	e_PDFA_LAST : 4
         * }
         * </pre>
         * error code identifier (obtained using GetError() method).
         * @returns A promise that resolves to a descriptive error message for the given error identifier.
         */
        static getPDFAErrorMessage(id: number): Promise<string>;
        /**
         * Retrieves whether document's XMP metadata claims PDF/A conformance and to what part and level.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFACompliance.Conformance = {
         * 	e_Level1A : 1
         * 	e_Level1B : 2
         * 	e_Level2A : 3
         * 	e_Level2B : 4
         * 	e_Level2U : 5
         * 	e_Level3A : 6
         * 	e_Level3B : 7
         * 	e_Level3U : 8
         * }
         * </pre>
         * @param doc - the document
         * @returns A promise that resolves to presumptive PDFA part number and conformance level, as an enumerated value.
         */
        static getDeclaredConformance(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<number>;
        /**
         * serializes the converted PDF/A document to a file on disk.
         * @param file_path - the output file name.
         * @param [linearized] - An optional flag used to specify whether the the resulting
         * PDF/A document should be web-optimized (linearized).
         */
        saveAsFromFileName(file_path: string, linearized?: boolean): Promise<void>;
        /**
         * Serializes the converted PDF/A document to a memory buffer.
         * @param [linearized] - An optional flag used to specify whether the the resulting
         * PDF/A document should be web-optimized (linearized).
         * @returns A promise that resolves to the converted document saved as a memory buffer.
         */
        saveAsFromBuffer(linearized?: boolean): Promise<Uint8Array>;
        /**
         * Perform PDF/A validation or PDF/A conversion on the input PDF document
        which is stored in a memory buffer.
         * @param convert - A flag used to instruct PDF/A processor to perform PDF/A
        conversion (if 'true') or PDF/A validation (if 'false'). After PDF/A conversion
        you can save the resulting document using Save() method(s).
         * @param url - A url from which the serialized PDF document can be downloaded.
         * @param [pwd = ""] - A parameter that can be used to specify the password for encrypted PDF documents (typically only useful in the conversion mode).
         * @param [conform = PDFNet.PDFACompliance.Conformance.e_Level1B] - <pre>
        PDFNet.PDFACompliance.Conformance = {
          e_Level1A : 1,
          e_Level1B : 2,
          e_Level2A : 3,
          e_Level2B : 4,
          e_Level2U : 5,
          e_Level3A : 6,
          e_Level3B : 7,
          e_Level3U : 8
        }
        </pre>
        The PDF conformance level defined in PDFNet.PDFACompliance.Conformance.
         * @param [excep = new Int32Array(0)] - Exceptions to ignore.
         * @param [max_ref_objs = 10] - The maximum number of object references per error condition.
         * @returns A promise that resolves to a PDFACompliance object
         */
        static createFromUrl(convert: boolean, url: string, pwd?: string, conform?: number, excep?: Int32Array, max_ref_objs?: number, first_stop?: boolean): Promise<PDFNet.PDFACompliance>;
        /**
         * Perform PDF/A validation or PDF/A conversion on the input PDF document
        which is stored in a memory buffer.
         * @param convert - A flag used to instruct PDF/A processor to perform PDF/A
        conversion (if 'true') or PDF/A validation (if 'false'). After PDF/A conversion
        you can save the resulting document using Save() method(s).
         * @param buf - A memory buffer containing the serialized PDF document.
         * @param [pwd = ""] - A parameter that can be used to specify the password for encrypted PDF documents (typically only useful in the conversion mode).
         * @param [conform = PDFNet.PDFACompliance.Conformance.e_Level1B] - <pre>
        PDFNet.PDFACompliance.Conformance = {
          e_Level1A : 1,
          e_Level1B : 2,
          e_Level2A : 3,
          e_Level2B : 4,
          e_Level2U : 5,
          e_Level3A : 6,
          e_Level3B : 7,
          e_Level3U : 8
        }
        </pre>
        The PDF conformance level defined in PDFNet.PDFACompliance.Conformance.
         * @param [excep = new Int32Array(0)] - Exceptions to ignore.
         * @param [max_ref_objs = 10] - The maximum number of object references per error condition.
         * @returns A promise that resolves to a PDFACompliance object
         */
        static createFromBuffer(convert: boolean, buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, pwd?: string, conform?: number, excep?: Int32Array, max_ref_objs?: number, first_stop?: boolean): Promise<PDFNet.PDFACompliance>;
        /**
         * Perform PDF/A validation or PDF/A conversion on the input PDF document
        which is stored in a file.
         * @param convert - A flag used to instruct PDF/A processor to perform PDF/A
        conversion (if 'true') or PDF/A validation (if 'false'). After PDF/A conversion
        you can save the resulting document using Save() method(s).
         * @param file_path - File name of the PDF document.
         * @param [pwd = ""] - A parameter that can be used to specify the password for encrypted PDF documents (typically only useful in the conversion mode).
         * @param [conform = PDFNet.PDFACompliance.Conformance.e_Level1B] - <pre>
        PDFNet.PDFACompliance.Conformance = {
          e_Level1A : 1,
          e_Level1B : 2,
          e_Level2A : 3,
          e_Level2B : 4,
          e_Level2U : 5,
          e_Level3A : 6,
          e_Level3B : 7,
          e_Level3U : 8
        }
        </pre>
        The PDF conformance level defined in PDFNet.PDFACompliance.Conformance.
         * @param [excep = new Int32Array(0)] - Exceptions to ignore.
         * @param [max_ref_objs = 10] - The maximum number of object references per error condition.
         * @returns A promise that resolves to a PDFACompliance object
         */
        static createFromFile(convert: boolean, file_path: string, pwd?: string, conform?: number, excep?: Int32Array, max_ref_objs?: number, first_stop?: boolean): Promise<PDFNet.PDFACompliance>;
    }
    /**
     * Note: PDFDC is deprecated. Please use PDFDCEx instead.
     * PDFDC is a utility class used to represent a PDF Device Context (DC).
     *
     * Windows developers can use standard GDI or GDI+ API-s to write on PDFDC
     * and to generate PDF documents based on their existing drawing functions.
     * PDFDC can also be used to implement file conversion from any printable
     * file format to PDF.
     *
     * PDFDC class can be used in many ways to translate from GDI to PDF:
     *  - To translate a single GDI drawing into a single page PDF document.
     *  - To translate a single GDI drawing into an object which can be reused
     *    many times throughout a PDF document (i.e. as a Form XObject).
     *  - To translate many GDI drawings into single page or multipage PDF document.
     *  ...
     *
     * Very few code changes are required to perform the translation from GDI to
     * PDF as PDFDC provides a GDI Device Context handle which can be passed to
     * all GDI function requiring an HDC.  PDFDC does not use a "Virtual Printer"
     * approach so the translation should be of both high quality and speed.
     * Unfortunately this also means that StartDoc, EndDoc, StartPage and EndPage
     * cannot be called with an HDC created with PDFDC::Begin.
     *
     * For more advanced translations or creations of PDF documents, such as security
     * handling, the use of other PDFNet classes will be required.
     *
     * An example use of PDFDC can be found in PDFDCTest.cpp:
     *
     * <pre>
     * // Start with a PDFDoc to put the picture into, and a PDFDC to translate GDI to PDF
     * PDFDoc pdfdoc;
     * PDFDC pdfDc;
     *
     * // Create a page to put the GDI content onto
     * Page page = pdfdoc.PageCreate();
     *
     * // Begin the translation from GDI to PDF.
     * // Provide the page to place the picture onto, and the bounding box for the content.
     * // We're going to scale the GDI content to fill the page while preserving the aspect
     * // ratio.
     * // Get back a GDI Device Context
     * HDC hDC = pdfDc.Begin( page, page.GetCropBox() );
     *
     *
     * ... perform GDI drawing ...
     *
     * // Complete the translation
     * pdfDc.End();
     *
     * // Add the page to the document
     * pdfdoc.PagePushBack(page);
     *
     * // Save the PDF document
     * pdfdoc.Save("PDFDC_is_cool.pdf", SDF::SDFDoc::e_remove_unused, NULL);
     * </pre>
     */
    class PDFDC {
    }
    /**
     * PDFDCEX is a utility class used to represent a PDF Device Context (DC).
     *
     * Windows developers can use standard GDI or GDI+ API-s to write on PDFDCEX
     * and to generate PDF documents based on their existing drawing functions.
     * PDFDCEX can also be used to implement file conversion from any printable
     * file format to PDF.
     *
     * PDFDCEX class can be used in many ways to translate from GDI to PDF:
     *  - To translate a single GDI drawing into a single page PDF document.
     *  - To translate a single GDI drawing into an object which can be reused
     *    many times throughout a PDF document (i.e. as a Form XObject).
     *  - To translate many GDI drawings into single page or multipage PDF document.
     *  ...
     *
     * Very few code changes are required to perform the translation from GDI to
     * PDF as PDFDCEX provides a GDI Device Context handle which can be passed to
     * all GDI function requiring an HDC.  PDFDCEX does use a "Virtual Printer"
     * approach so the translation should be of both high quality and speed.
     *
     * For more advanced translations or creations of PDF documents, such as security
     * handling, the use of other PDFNet classes will be required.
     *
     * An example use of PDFDCEX can be found in PDFDCTest.cpp:
     *
     * <pre>
     * // Start with a PDFDoc to put the picture into, and a PDFDCEX to translate GDI to PDF
     * PDFDoc pdfdoc;
     * PDFDCEX pdfdcex;
     *
     * // Begin the translation from GDI to PDF, provide the PDFDoc to append the translated
     * // GDI drawing to and get back a GDI Device Context
     * HDC hDC = pdfdcex.Begin(pdfdoc);
     * ::StartPage(hDC);
     *
     * ... perform GDI drawing ...
     *
     * ::EndPage(hDC);
     * // Complete the translation
     * pdfdcex.EndDoc();
     *
     * // Save the PDF document
     * pdfdoc.Save("PDFDCEX_is_cool.pdf", SDF::SDFDoc::e_remove_unused, NULL);
     * </pre>
     */
    class PDFDCEX {
    }
    /**
     * PDFDoc is a high-level class describing a single PDF (Portable Document
     * Format) document. Most applications using PDFNet will use this class to
     * open existing PDF documents, or to create new PDF documents from scratch.
     *
     * The class offers a number of entry points into the document. For example,
     *  - To access pages use pdfdoc.GetPageIterator() or pdfdoc.PageFind(page_num).
     *  - To access form fields use pdfdoc.GetFieldIterator() or pdfdoc.FieldFind(name).
     *  - To access document's meta-data use pdfdoc.GetDocInfo().
     *  - To access the outline tree use pdfdoc.GetFirstBookmark().
     *  - To access low-level Document Catalog use pdfdoc.GetRoot().
     *  ...
     *
     * The class also offers utility methods to slit and merge PDF pages,
     * to create new pages, to flatten forms, to change security settings, etc.
     */
    class PDFDoc extends PDFNet.Destroyable {
        /**
         * Method to create a ViewerOptimizedOptions object
         * @returns A promise that resolves to a PDFNet.PDFDoc.ViewerOptimizedOptions.
         */
        static createViewerOptimizedOptions(): Promise<PDFNet.PDFDoc.ViewerOptimizedOptions>;
        /**
         * Get the Action associated with the selected Doc Trigger event.
         * @param trigger - <pre>
         * PDFNet.PDFDoc.EventType = {
         * 	e_action_trigger_doc_will_close : 17
         * 	e_action_trigger_doc_will_save : 18
         * 	e_action_trigger_doc_did_save : 19
         * 	e_action_trigger_doc_will_print : 20
         * 	e_action_trigger_doc_did_print : 21
         * }
         * </pre>
         * the type of trigger event to get
         * @returns A promise that resolves to the Action Obj if present, otherwise NULL
         */
        getTriggerAction(trigger: number): Promise<PDFNet.Obj>;
        /**
         * Default constructor. Creates an empty new document.
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        static create(): Promise<PDFNet.PDFDoc>;
        /**
         * Open an existing PDF document
         * @param filepath - pathname to the file.
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        static createFromUFilePath(filepath: string): Promise<PDFNet.PDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        static createFromFilePath(filepath: string): Promise<PDFNet.PDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        static createFromFilter(stream: PDFNet.Filter): Promise<PDFNet.PDFDoc>;
        /**
         * Open a SDF/Cos document from a memory buffer.
         * @param buf - a memory buffer containing the serialized document
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        static createFromBuffer(buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.PDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        static createFromLayoutEls(buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.PDFDoc>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDoc"
         */
        createShallowCopy(): Promise<PDFNet.PDFDoc>;
        /**
         * @returns A promise that resolves to true if the document is/was originally encrypted false otherwise.
         */
        isEncrypted(): Promise<boolean>;
        /**
         * Initializes document's SecurityHandler using the supplied
         * password. This version of InitSecurityHandler() assumes that
         * document uses Standard security and that a password is specified
         * directly.
         *
         * This function should be called immediately after an encrypted
         * document is opened. The function does not have any side effects on
         * documents that are not encrypted.
         *
         * If the security handler was successfully initialized, it can be later
         * obtained using GetSecurityHandler() method.
         * @param password - Specifies the password used to open the document without
         * any user feedback. If you would like to dynamically obtain the password,
         * you need to derive a custom class from StdSecurityHandler() and use
         * InitSecurityHandler() without any parameters. See EncTest sample
         * for example code.
         * @param password_sz - An optional parameter used to specify the size of
         * the password buffer, in bytes. If the 'password_sz' is 0, or if the parameter
         * is not specified, the function assumes that the string is null terminated.
         * @returns A promise that resolves to true if the given password successfully unlocked the document,
         * false otherwise.
         */
        initStdSecurityHandler(password: string, password_sz: number): Promise<boolean>;
        /**
         * Initializes document's SecurityHandler using the supplied
         * password. This version of InitSecurityHandler() assumes that
         * document uses Standard security and that a password is specified
         * directly.
         *
         * This function should be called immediately after an encrypted
         * document is opened. The function does not have any side effects on
         * documents that are not encrypted.
         *
         * If the security handler was successfully initialized, it can be later
         * obtained using GetSecurityHandler() method.
         * @param password - Specifies the password used to open the document without
         * any user feedback. If you would like to dynamically obtain the password,
         * you need to derive a custom class from StdSecurityHandler() and use
         * InitSecurityHandler() without any parameters. See EncTest sample
         * for example code.
         * @returns A promise that resolves to true if the given password successfully unlocked the document,
         * false otherwise.
         */
        initStdSecurityHandlerUString(password: string): Promise<boolean>;
        /**
         * Initializes document's SecurityHandler using the supplied
         * password. This version of InitSecurityHandler() assumes that
         * document uses Standard security and that a password is specified
         * directly.
         *
         * This function should be called immediately after an encrypted
         * document is opened. The function does not have any side effects on
         * documents that are not encrypted.
         *
         * If the security handler was successfully initialized, it can be later
         * obtained using GetSecurityHandler() method.
         * @param password_buf - Specifies the password used to open the document without
         * any user feedback. If you would like to dynamically obtain the password,
         * you need to derive a custom class from StdSecurityHandler() and use
         * InitSecurityHandler() without any parameters. See EncTest sample
         * for example code.
         * @returns A promise that resolves to true if the given password successfully unlocked the document,
         * false otherwise.
         */
        initStdSecurityHandlerBuffer(password_buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<boolean>;
        /**
         * @returns A promise that resolves to currently selected SecurityHandler.
         */
        getSecurityHandler(): Promise<PDFNet.SecurityHandler>;
        setSecurityHandler(handler: PDFNet.SecurityHandler): Promise<void>;
        /**
         * This function removes document security.
         */
        removeSecurity(): Promise<void>;
        /**
         * @returns A promise that resolves to the class representing document information metadata.
         * (i.e. entries in the document information dictionary).
         */
        getDocInfo(): Promise<PDFNet.PDFDocInfo>;
        /**
         * @returns A promise that resolves to viewer preferences for this document.
         *
         * PDFDocViewPrefs is a high-level utility class that can be
         * used to control the way the document is to be presented on
         * the screen or in print.
         */
        getViewPrefs(): Promise<PDFNet.PDFDocViewPrefs>;
        /**
         * Call this function to determine whether the document has been modified since
         * it was last saved.
         * @returns A promise that resolves to true if document was modified, false otherwise
         */
        isModified(): Promise<boolean>;
        /**
         * Checks whether or not the underlying file has an XRef table that had to be repaired
         * when the file was opened. If the document had an invalid XRef table when opened,
         * PDFNet will have repaired the XRef table for its working representation of the document.
         * @returns A promise that resolves to true if document was found to be corrupted, and was repaired, during
         * opening and has not been saved since.
         */
        hasRepairedXRef(): Promise<boolean>;
        /**
         * Call this function to determine whether the document is represented in
         * linearized (fast web view) format.
         * @returns A promise that resolves to true if document is stored in fast web view format, false otherwise.
         */
        isLinearized(): Promise<boolean>;
        /**
         * Saves the document to a file.
         *
         * If a full save is requested to the original path, the file is saved to a file
         * system-determined temporary file, the old file is deleted, and the temporary file
         * is renamed to path.
         *
         * A full save with remove unused or linearization option may re-arrange object in
         * the cross reference table. Therefore all pointers and references to document objects
         * and resources should be re acquired in order to continue document editing.
         *
         * In order to use incremental save the specified path must match original path and
         * e_incremental flag bit should be set.
         * @param path - The full path name to which the file is saved.
         * @param flags - A bit field composed of an OR of SDFDoc::SaveOptions values.
         */
        save(path: string, flags: number): Promise<void>;
        /**
         * Saves the document to a memory buffer.
         * @param flags - A bit field composed of an OR of SDFDoc::SaveOptions values. Note that
         * this method ignores e_incremental flag.
         * @returns A promise that resolves to a Uint8Array containing file data.
         */
        saveMemoryBuffer(flags: number): Promise<Uint8Array>;
        /**
         * Saves the document to a stream.
         * @param stream - The output stream where to write data.
         * @param flags - A bit field composed of an OR of the SDFDoc::SaveOptions values.
         */
        saveStream(stream: PDFNet.Filter, flags: number): Promise<void>;
        /**
         * Use the Next() method on the returned iterator to traverse all pages in the document.
         * For example:
         * <pre>
         *   PageIterator itr = pdfdoc.GetPageIterator();
         *   while (itr.HasNext()) { //  Read every page
         *      Page page = itr.Current();
         *      // ...
         *      itr.Next()
         *   }
         * </pre>
         *
         * For full sample code, please take a look at ElementReader, PDFPageTest and PDFDraw sample projects.
         * @param [page_number] - page to set the iterator on. 1 corresponds to the first page.
         * @returns A promise that resolves to an iterator to the first page in the document.
         */
        getPageIterator(page_number?: number): Promise<PDFNet.Iterator<PDFNet.Page>>;
        /**
         * @param page_number - the page number in document's page sequence. Page numbers
         * in document's page sequence are indexed from 1.
         * @returns A promise that resolves to a Page corresponding to a given page number, or null (invalid page)
         * if the document does not contain the given page number.
         *
         * For example:
         * <pre>
         *   Page page = pdfdoc.GetPage(page_num);
         *   if (page == null) return; //  Page not found
         * </pre>
         */
        getPage(page_number: number): Promise<PDFNet.Page>;
        /**
         * @param page_itr - the PageIterator to the page that should be removed
         * A PageIterator for the given page can be obtained using PDFDoc::GetPageIterator(page_num)
         * or using direct iteration through document's page sequence.
         */
        pageRemove(page_itr: PDFNet.Iterator<PDFNet.Page>): Promise<void>;
        /**
         * Insert/Import a single page at a specific location in the page sequence.
         * @param where - The location in the page sequence indicating where to insert
         * the page. The page is inserted before the specified location.
         * @param page - A page to insert.
         */
        pageInsert(where: PDFNet.Iterator<PDFNet.Page>, page: PDFNet.Page): Promise<void>;
        /**
         * Inserts a range of pages from specified PDFDoc
         * @param insert_before_page_number - the destination of the insertion. If less than or equal to 1,
         * the pages are added to the beginning of the document. If larger than the number of pages
         * in the destination document, the pages are appended to the document.
         * @param src_doc - source PDFDoc to insert from
         * @param start_page - start of the page number to insert
         * @param end_page - end of the page number to insert
         * @param flag - <pre>
         * PDFNet.PDFDoc.InsertFlag = {
         * 	e_none : 0
         * 	e_insert_bookmark : 1
         * }
         * </pre>
         * specifies insert options
         */
        insertPages(insert_before_page_number: number, src_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, start_page: number, end_page: number, flag: number): Promise<void>;
        /**
         * Inserts a range of pages from specified PDFDoc using PageSet
         * @param insert_before_page_number - the destination of the insertion. If less than or equal to 1,
         * the pages are added to the beginning of the document. If larger than the number of pages
         * in the destination document, the pages are appended to the document.
         * @param src_doc - source PDFDoc to insert from
         * @param source_page_set - a collection of the page number to insert
         * @param flag - <pre>
         * PDFNet.PDFDoc.InsertFlag = {
         * 	e_none : 0
         * 	e_insert_bookmark : 1
         * }
         * </pre>
         * specifies insert options
         */
        insertPageSet(insert_before_page_number: number, src_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, source_page_set: PDFNet.PageSet, flag: number): Promise<void>;
        /**
         * Moves a range of pages from specified PDFDoc. Pages are deleted from source document after move.
         * @param move_before_page_number - the destination of the move. If less than or equal to 1,
         * the pages are moved to the beginning of the document. If larger than the number of pages
         * in the destination document, the pages are moved to the end of the document.
         * @param src_doc - source PDFDoc to move from
         * @param start_page - start of the page number to move
         * @param end_page - end of the page number to move
         * @param flag - <pre>
         * PDFNet.PDFDoc.InsertFlag = {
         * 	e_none : 0
         * 	e_insert_bookmark : 1
         * }
         * </pre>
         * specifies insert options
         */
        movePages(move_before_page_number: number, src_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, start_page: number, end_page: number, flag: number): Promise<void>;
        /**
         * Moves a range of pages from specified PDFDoc. Pages are deleted from source document after move.
         * @param move_before_page_number - the destination of the move. If less than or equal to 1,
         * the pages are moved to the beginning of the document. If larger than the number of pages
         * in the destination document, the pages are moved to the end of the document.
         * @param src_doc - source PDFDoc to move from
         * @param source_page_set - a collection of the page number to move
         * @param flag - <pre>
         * PDFNet.PDFDoc.InsertFlag = {
         * 	e_none : 0
         * 	e_insert_bookmark : 1
         * }
         * </pre>
         * specifies insert options
         */
        movePageSet(move_before_page_number: number, src_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, source_page_set: PDFNet.PageSet, flag: number): Promise<void>;
        /**
         * Adds a page to the beginning of a document's page sequence.
         * @param page - a page to prepend to the document
         * Invalidates all PageIterators pointing to the document.
         */
        pagePushFront(page: PDFNet.Page): Promise<void>;
        /**
         * Adds a page to the end of a document's page sequence.
         * @param page - a page to append to the document
         */
        pagePushBack(page: PDFNet.Page): Promise<void>;
        /**
         * Create a new, empty page in the document. You can use PageWriter to fill the
         * page with new content. Finally the page should be inserted at specific
         * place within document page sequence using PageInsert/PagePushFront/PagePushBack
         * methods.
         * @param [media_box] - A rectangle, expressed in default user space units, defining
         * the boundaries of the physical medium on which the page is intended to be
         * displayed or printed. A user space units is 1/72 of an inch. If media_box is
         * not specified the default dimensions of the page are 8.5 x 11 inches (or
         * 8.5*72, 11*72 units).
         *
         * <p>The following is a listing of some standard U.S. page sizes:</p>
         *  <ul>
         *    <li>Letter    = Rect(0, 0, 612, 792)
         *    <li>Legal	    = Rect(0, 0, 612, 1008)
         *    <li>Ledger	= Rect(0, 0, 1224, 792)
         *    <li>Tabloid   = Rect(0, 0, 792, 1224)
         *    <li>Executive	= Rect(0, 0, 522, 756)
         * </ul>
         *
         * <p>The following is a listing of ISO standard page sizes:</p>
         *  <ul>
         *    <li> 4A0 = Rect(0, 0, 4768, 6741)
         *    <li> 2A0 = Rect(0, 0, 3370, 4768)
         *    <li> A0 =  Rect(0, 0, 2384, 3370)
         *    <li> A1 =  Rect(0, 0, 1684, 2384)
         *    <li> A2 =  Rect(0, 0, 1191, 1684)
         *    <li> A3 =  Rect(0, 0, 842,  1191)
         *    <li> A4 =  Rect(0, 0, 595,  842)
         *    <li> A5 =  Rect(0, 0, 420,  595)
         *    <li> A6 =  Rect(0, 0, 298,  420)
         *    <li> A7 =  Rect(0, 0, 210,  298)
         *    <li> A8 =  Rect(0, 0, 147,  210)
         *    <li> A9 =  Rect(0, 0, 105,  147)
         *    <li> A10 = Rect(0, 0, 74,   105)
         *    <li> B0 =  Rect(0, 0, 2835, 4008)
         *    <li> B1 =  Rect(0, 0, 2004, 2835)
         *    <li> B2 =  Rect(0, 0, 1417, 2004)
         *    <li> B3 =  Rect(0, 0, 1001, 1417)
         *    <LI> B4 =  Rect(0, 0, 709,  1001)
         *    <LI> B5 =  Rect(0, 0, 499,  709)
         *    <LI> B6 =  Rect(0, 0, 354,  499)
         *    <LI> B7 =  Rect(0, 0, 249,  354)
         *    <LI> B8 =  Rect(0, 0, 176,  249)
         *    <LI> B9 =  Rect(0, 0, 125,  176)
         *    <li> B10 = Rect(0, 0, 88,   125)
         *    <li> C0 =  Rect(0, 0, 2599, 3677)
         *    <li> C1 =  Rect(0, 0, 1837, 2599)
         *    <li> C2 =  Rect(0, 0, 1298, 1837)
         *    <li> C3 =  Rect(0, 0, 918,  1298)
         *    <li> C4 =  Rect(0, 0, 649,  918)
         *    <li> C5 =  Rect(0, 0, 459,  649)
         *    <li> C6 =  Rect(0, 0, 323,  459)
         *    <li> C7 =  Rect(0, 0, 230,  323)
         *    <li> C8 =  Rect(0, 0, 162,  230)
         *    <li> C9 =  Rect(0, 0, 113,  162)
         *    <li>C10 =  Rect(0, 0, 79,   113)
         * </ul>
         * @returns A promise that resolves to a new, empty page.
         */
        pageCreate(media_box?: PDFNet.Rect): Promise<PDFNet.Page>;
        /**
         * @returns A promise that resolves to the first Bookmark from the document's outline tree. If the
         * Bookmark tree is empty the underlying SDF/Cos Object is null and returned
         * Bookmark is not valid (i.e. Bookmark::IsValid() returns false).
         */
        getFirstBookmark(): Promise<PDFNet.Bookmark>;
        /**
         * Adds/links the specified Bookmark to the root level of document's outline tree.
         * @param root_bookmark - Bookmark to Add/link
         */
        addRootBookmark(root_bookmark: PDFNet.Bookmark): Promise<void>;
        /**
         * @returns A promise that resolves to A dictionary representing the Cos root of the document (document's trailer)
         */
        getTrailer(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to A dictionary representing the Cos root of the document (/Root entry
         * within the trailer dictionary)
         */
        getRoot(): Promise<PDFNet.Obj>;
        jsContextInitialize(): Promise<void>;
        /**
         * @returns A promise that resolves to A dictionary representing the root of the low level page-tree
         */
        getPages(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the number of pages in the document.
         */
        getPageCount(): Promise<number>;
        /**
         * Returns the number of bytes that have been downloaded, when `HasDownloader()` is True.
         * @returns A promise that resolves to the number bytes downloaded.
         */
        getDownloadedByteCount(): Promise<number>;
        /**
         * Returns the document's total size in bytes, when `HasDownloader()` is True.
         * @returns A promise that resolves to the total number of bytes in the remote document.
         */
        getTotalRemoteByteCount(): Promise<number>;
        /**
         * An interactive form (sometimes referred to as an AcroForm) is a
         * collection of fields for gathering information interactively from
         * the user. A PDF document may contain any number of fields appearing
         * on any combination of pages, all of which make up a single, global
         * interactive form spanning the entire document.
         *
         * The following methods are used to access and manipulate Interactive form
         * fields (sometimes referred to as AcroForms).
         * @returns A promise that resolves to an iterator to the first Field in the document.
         *
         * The list of all Fields present in the document can be traversed as follows:
         * <pre>
         * FieldIterator itr = pdfdoc.GetFieldIterator();
         * for(; itr.HasNext(); itr.Next()) {
         *   Field field = itr.Current();
         *   Console.WriteLine("Field name: {0}", field.GetName());
         *  }
         * </pre>
         *
         * For a sample, please refer to 'InteractiveForms' sample project.
         */
        getFieldIteratorBegin(): Promise<PDFNet.Iterator<PDFNet.Field>>;
        /**
         * An interactive form (sometimes referred to as an AcroForm) is a
         * collection of fields for gathering information interactively from
         * the user. A PDF document may contain any number of fields appearing
         * on any combination of pages, all of which make up a single, global
         * interactive form spanning the entire document.
         *
         * The following methods are used to access and manipulate Interactive form
         * fields (sometimes referred to as AcroForms).
         * @param field_name - String representing the name of the field to get.
         * @returns A promise that resolves to an iterator to the Field in the document.
         *
         * For a sample, please refer to 'InteractiveForms' sample project.
         */
        getFieldIterator(field_name: string): Promise<PDFNet.Iterator<PDFNet.Field>>;
        /**
         * @param field_name - a string representing the fully qualified name of
         * the field (e.g. "employee.name.first").
         * @returns A promise that resolves to a FieldIterator referring to an interactive Field
         * or to invalid field if the field name was not found. If a given field name was
         * not found itr.HasNext() will return false. For example:
         *
         * <pre>
         * FieldIterator itr = pdfdoc.GetFieldIterator("name");
         * if (itr.HasNext()) {
         *   Console.WriteLine("Field name: {0}", itr.Current().GetName());
         * }
         * else { ...field was not found... }
         * </pre>
         */
        getField(field_name: string): Promise<PDFNet.Field>;
        /**
         * Create a new interactive form Field.
         * @param field_name - a string representing the fully qualified name of the
         * field (e.g. "employee.name.first"). field_name must be either a unique name or
         * equal to an existing terminal field name.
         * @param type - <pre>
         * PDFNet.Field.Type = {
         * 	e_button : 0
         * 	e_check : 1
         * 	e_radio : 2
         * 	e_text : 3
         * 	e_choice : 4
         * 	e_signature : 5
         * 	e_null : 6
         * }
         * </pre>
         * field type (e.g. Field::e_text, Field::e_button, etc.)
         * @returns A promise that resolves to the new form Field.
         */
        fieldCreate(field_name: string, type: number, field_value?: PDFNet.Obj, def_field_value?: PDFNet.Obj): Promise<PDFNet.Field>;
        /**
         * Create a new interactive form Field.
         * @param field_name - a string representing the fully qualified name of the
         * field (e.g. "employee.name.first"). field_name must be either a unique name or
         * equal to an existing terminal field name.
         * @param type - <pre>
         * PDFNet.Field.Type = {
         * 	e_button : 0
         * 	e_check : 1
         * 	e_radio : 2
         * 	e_text : 3
         * 	e_choice : 4
         * 	e_signature : 5
         * 	e_null : 6
         * }
         * </pre>
         * field type (e.g. Field::e_text, Field::e_button, etc.)
         * @returns A promise that resolves to the new form Field.
         */
        fieldCreateFromStrings(field_name: string, type: number, field_value: string, def_field_value?: string): Promise<PDFNet.Field>;
        /**
         * Regenerates the appearance stream for every widget annotation in the document
         * Call this method if you modified field's value and would like to update
         * field's appearances.
         */
        refreshFieldAppearances(): Promise<void>;
        /**
         * Generates the appearance stream for annotations in the document using the specified options. A common use case is to generate appearances
         * only for missing annotations, which can be accomplished using the default options.
         * @param [options] - Options that can be used to adjust this generation process.
         */
        refreshAnnotAppearances(options?: PDFNet.RefreshOptions): Promise<void>;
        /**
         * Flatten all annotations in the document.
         * @param [forms_only] - if false flatten all annotations, otherwise flatten
         * only form fields.
         */
        flattenAnnotations(forms_only?: boolean): Promise<void>;
        flattenAnnotationsAdvanced(flags: number): Promise<void>;
        /**
         * @returns A promise that resolves to the AcroForm dictionary located in "/Root" or NULL if dictionary is not present.
         */
        getAcroForm(): Promise<PDFNet.Obj>;
        /**
         * Extract form data and/or annotations to FDF
         * @param [flag] - <pre>
         * PDFNet.PDFDoc.ExtractFlag = {
         * 	e_forms_only : 0
         * 	e_annots_only : 1
         * 	e_both : 2
         * }
         * </pre>
         * specifies extract options
         * @returns A promise that resolves to a pointer to the newly created FDF file with an interactive data.
         */
        fdfExtract(flag?: number): Promise<PDFNet.FDFDoc>;
        /**
         * Extract form data and/or annotations to FDF
         * @param pages_to_extract - The set of pages for which to extract interactive data.
         * @param [flag] - <pre>
         * PDFNet.PDFDoc.ExtractFlag = {
         * 	e_forms_only : 0
         * 	e_annots_only : 1
         * 	e_both : 2
         * }
         * </pre>
         * specifies extract options
         * @returns A promise that resolves to a pointer to the newly created FDF file with an interactive data.
         */
        fdfExtractPageSet(pages_to_extract: PDFNet.PageSet, flag?: number): Promise<PDFNet.FDFDoc>;
        /**
         * Import form data from FDF file to PDF interactive form.
         * @param fdf_doc - a reference to the FDF file
         */
        fdfMerge(fdf_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<void>;
        /**
         * Replace existing form and annotation data with those imported from the FDF file.
         * Since this method avoids updating annotations unnecessarily it is ideal for incremental save.
         * @param fdf_doc - a reference to the FDF file
         */
        fdfUpdate(fdf_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<void>;
        /**
         * @returns A promise that resolves to action that is triggered when the document is opened.
         * The returned action can be either a destination or some other kind
         * of Action (see Section 8.5, 'Actions' in PDF Reference Manual).
         */
        getOpenAction(): Promise<PDFNet.Action>;
        /**
         * Sets the Action that will be triggered when the document is opened.
         * @param action - A new Action that will be triggered when the document is opened.
         * An example of such action is a GoTo Action that takes the user to a given
         * location in the document.
         */
        setOpenAction(action: PDFNet.Action): Promise<void>;
        /**
         * Associates a file attachment with the document.
         *
         * The file attachment will be displayed in the user interface of a viewer application
         * (in Acrobat this is File Attachment tab). The function differs from
         * Annot.CreateFileAttachment() because it associates the attachment with the
         * whole document instead of an annotation on a specific page.
         * @param file_key - A key/name under which the attachment will be stored.
         * @param embedded_file - Embedded file stream
         */
        addFileAttachment(file_key: string, embedded_file: PDFNet.FileSpec): Promise<void>;
        /**
         * @param page_num - The page number. Because PDFNet indexes pages
         * starting from 1, page_num must be larger than 0.
         * @returns A promise that resolves to the PageLabel that is in effect for the given page.
         * If there is no label object in effect, this method returns an
         * invalid page label object.
         */
        getPageLabel(page_num: number): Promise<PDFNet.PageLabel>;
        setPageLabel(page_num: number, label: PDFNet.PageLabel): Promise<void>;
        /**
         * removes the page label that is attached to the specified page,
         * effectively merging the specified range with the previous page
         * label sequence.
         * @param page_num - The page from which the page label is removed.
         * Because PDFNet indexes pages starting from 1, page_num must be
         * larger than 0.
         */
        removePageLabel(page_num: number): Promise<void>;
        /**
         * @returns A promise that resolves to the document's logical structure tree root.
         */
        getStructTree(): Promise<PDFNet.STree>;
        /**
         * @returns A promise that resolves to true if the optional content (OC) feature is associated with
         * the document. The document is considered to have optional content if
         * there is an OCProperties dictionary in the document's catalog, and
         * that dictionary has one or more entries in the OCGs array.
         */
        hasOC(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the Obj array that contains optional-content groups (OCGs) for
         * the document, or NULL if the document does not contain any OCGs. The
         * order of the groups is not guaranteed to be the creation order, and is
         * not the same as the display order.
         */
        getOCGs(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the default optional-content configuration for the document
         * from the OCProperties D entry.
         */
        getOCGConfig(): Promise<PDFNet.OCGConfig>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectName(name: string): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect array object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @returns A promise that resolves to returns a new indirect array object.
         */
        createIndirectArray(): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect boolean object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @param value - the value with which to create the boolean object.
         * @returns A promise that resolves to returns a new indirect boolean object.
         */
        createIndirectBool(value: boolean): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect dict object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @returns A promise that resolves to returns a new indirect dict object.
         */
        createIndirectDict(): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect null object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @returns A promise that resolves to returns a new indirect null object.
         */
        createIndirectNull(): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect number object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @param value - the value with which to create the number object.
         * @returns A promise that resolves to returns a new indirect number object.
         */
        createIndirectNumber(value: number): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect string object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @param value - Unsigned char pointer with which to create the string object.
         * @param buf_size - length of string.
         * @returns A promise that resolves to returns a new indirect string object.
         */
        createIndirectString(value: number, buf_size: number): Promise<PDFNet.Obj>;
        /**
         * This method creates an SDF/Cos indirect string object
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects can be shared).
         * @param str - reference to string with which to create the string object.
         * @returns A promise that resolves to returns a new indirect string object.
         */
        createIndirectStringFromUString(str: string): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectStreamFromFilter(data: PDFNet.FilterReader, filter_chain?: PDFNet.Filter): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectStream(data: string, data_size: number, filter_chain?: PDFNet.Filter): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to document's SDF/Cos document
         */
        getSDFDoc(): Promise<PDFNet.SDFDoc>;
        /**
         * Removes the lock from the document.
         */
        unlock(): Promise<void>;
        /**
         * Removes the read lock from the document.
         */
        unlockRead(): Promise<void>;
        /**
         * addHighlights is used to highlight text in a document using 'Adobe's Highlight
         * File Format' (Technical Note #5172 ). The method will parse the character offset data
         * and modify the current document by adding new highlight annotations.
         * @param hilite - a string representing the filename for the highlight file or
         * or a data buffer containing XML data.
         */
        addHighlights(hilite: string): Promise<void>;
        /**
         * @returns A promise that resolves to true if this document is marked as Tagged PDF, false otherwise.
         */
        isTagged(): Promise<boolean>;
        /**
         * Indicates whether this documents contains any digital signatures.
         * @returns A promise that resolves to true if a digital signature is found in this PDFDoc.
         */
        hasSignatures(): Promise<boolean>;
        /**
         * Adds a signature handler to the signature manager.
         * @param signature_handler - The signature handler instance to add to the signature manager.
         * @returns A promise that resolves to a unique ID representing the SignatureHandler within the SignatureManager.
         */
        addSignatureHandler(signature_handler: PDFNet.SignatureHandler): Promise<number>;
        /**
         * Adds a standard (built-in) signature handler to the signature manager. This method will use cryptographic
         * algorithm based on Adobe.PPKLite/adbe.pkcs7.detached filter to sign a PDF.
         * @param pkcs12_file - The private key certificate store to use.
         * @param pkcs12_pass - The passphrase for the provided private key.
         * @returns A promise that resolves to a unique ID representing the SignatureHandler within the SignatureManager.
         */
        addStdSignatureHandlerFromFile(pkcs12_file: string, pkcs12_pass: string): Promise<number>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        addStdSignatureHandlerFromBuffer(pkcs12_buffer: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray, pkcs12_pass: string): Promise<number>;
        /**
         * Removes a signature handler from the signature manager.
         * @param signature_handler_id - The unique id of the signature handler to remove.
         */
        removeSignatureHandler(signature_handler_id: number): Promise<void>;
        /**
         * Gets the associated signature handler instance from the signature manager by looking it up with the handler name.
         * @param signature_handler_id - The unique id of the signature handler to get.
         * @returns A promise that resolves to the signature handler instance if found, otherwise NULL.
         */
        getSignatureHandler(signature_handler_id: number): Promise<PDFNet.SignatureHandler>;
        /**
         * Generates thumbnail images for all the pages in this PDF document.
         * @param size - The maximum dimension (width or height) that thumbnails will have.
         */
        generateThumbnails(size: number): Promise<void>;
        /**
         * Add a page representing the difference between two pdf pages to this document.
         * @param p1 - one of the two pages for comparing
         * @param p2 - the other page for comparing
         * @param [opts] - options for comparison results. This parameter is supposed to be an object,
         * which can be generated by 'PDFNet.createDiffOptions()'. If this parameter is omitted or
         * set to null, default settings will be used.
         */
        appendVisualDiff(p1: PDFNet.Page, p2: PDFNet.Page, opts?: PDFNet.DiffOptions): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.GeometryCollection"
         */
        getGeometryCollectionForPage(page_num: number): Promise<PDFNet.GeometryCollection>;
        /**
         * @returns A promise that resolves to the UndoManager object (one-to-one mapped to document)
         */
        getUndoManager(): Promise<PDFNet.UndoManager>;
        /**
         * Creates an unsigned digital signature form field inside the document.
         * @param [in_sig_field_name] - The fully-qualified name to give the digital signature field. If one is not provided, a unique name is created automatically.
         * @returns A promise that resolves to a DigitalSignatureField object representing the created digital signature field.
         */
        createDigitalSignatureField(in_sig_field_name?: string): Promise<PDFNet.DigitalSignatureField>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.DigitalSignatureField"
         */
        getDigitalSignatureField(field_name: string): Promise<PDFNet.DigitalSignatureField>;
        /**
         * Retrieves an iterator that iterates over digital signature fields.
         * @returns A promise that resolves to an iterator that iterates over digital signature fields.
         */
        getDigitalSignatureFieldIteratorBegin(): Promise<PDFNet.Iterator<PDFNet.DigitalSignatureField>>;
        /**
         * Retrieves the most restrictive document permissions locking level from
         * all of the signed digital signatures in the document.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DigitalSignatureField.DocumentPermissions = {
         * 	e_no_changes_allowed : 1
         * 	e_formfilling_signing_allowed : 2
         * 	e_annotating_formfilling_signing_allowed : 3
         * 	e_unrestricted : 4
         * }
         * </pre>
         * @returns A promise that resolves to an enumerated value representing the most restrictive document permission level found in the document.
         */
        getDigitalSignaturePermissions(): Promise<number>;
        /**
         * Applies optimizations to improve viewing speed and saves the document to the specified file.
         * The main optimizations used are linearization and embedding thumbnails for the
         * first page and any complex pages.
         * @param path - The full path name to which the file is saved.
         * @param opts - The optimization options
         */
        saveViewerOptimized(path: string, opts: PDFNet.Obj | PDFNet.PDFDoc.ViewerOptimizedOptions): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.PDFDoc.SignaturesVerificationStatus = {
         * 	e_unsigned : 0
         * 	e_failure : 1
         * 	e_untrusted : 2
         * 	e_unsupported : 3
         * 	e_verified : 4
         * }
         * </pre>
         */
        verifySignedDigitalSignatures(opts: PDFNet.VerificationOptions): Promise<number>;
        /**
         * Open a PDF document from a url. This function will fully download the file as a memory buffer and create a PDFDoc object.
         * @param url - The url from which to download the file
         * @param [options] - Additional options
         * @param options.withCredentials - Whether to set the withCredentials property on the XMLHttpRequest
         * @param options.customHeaders - An object containing custom HTTP headers to be used when downloading the document
         * @returns A promise that resolves to an object of type: "PDFDoc".
         */
        static createFromURL(url: string, options?: {
            withCredentials: boolean;
            customHeaders: any;
        }): Promise<PDFNet.PDFDoc>;
        /**
         * Generate a stream that incrementally converts the pdf to XOD format.
         * @param [options] - the conversion options
         * @returns A filter from which the file can be read incrementally.
         */
        convertToXodStream(options?: PDFNet.Obj | PDFNet.Convert.XODOutputOptions): PDFNet.Filter;
        /**
         * Initializes document's SecurityHandler. This version of InitSecurityHandler()
        works with Standard and Custom PDF security and can be used in situations where
        the password is obtained dynamically via user feedback. See EncTest sample for
        example code.
        
        This function should be called immediately after an encrypted
        document is opened. The function does not have any side effects on
        documents that are not encrypted.
        
        If the security handler was successfully initialized it can be later obtained
        using GetSecurityHandler() method.
         * @returns A promise that resolves to true if the SecurityHandler was successfully initialized (this
        may include authentication data collection, verification etc.),
        false otherwise.
         */
        initSecurityHandler(): Promise<boolean>;
        /**
         * Adds a standard (built-in) signature handler to the signature manager. This method will use cryptographic
        algorithm based on Adobe.PPKLite/adbe.pkcs7.detached filter to sign a PDF.
         * @param pkcs12_file - The private key certificate store to use.
         * @param pkcs12_pass - The passphrase for the provided private key.
         * @returns A promise that resolves to a unique ID representing the SignatureHandler within the SignatureManager.
         */
        addStdSignatureHandlerFromURL(pkcs12_file: string, pkcs12_pass: string): Promise<number>;
        /**
         * The function imports a list of pages to this document. Although a list of pages
        can be imported using repeated calls to PageInsert(), PageImport will not import
        duplicate copies of resources that are shared across pages (such as fonts, images,
        colorspaces etc). Therefore this method is recommended when a page import list
        consists of several pages that share the same resources.
         * @param page_arr - A list of pages to import. All pages should belong to the same source document.
         * @param [import_bookmarks] - An optional flag specifying whether any bookmark items
        pointing to pages in the import list should be merged with the target (i.e. this)
        document.
         * @returns A promise that resolves to a list of imported pages. Note that imported pages are not placed in the
        document page sequence. This can be done using methods such as PageInsert(),
        PagePushBack(), etc.
         */
        importPages(page_arr: PDFNet.Page[], import_bookmarks?: number): Promise<PDFNet.Page>;
        /**
         * Locks the document to prevent competing threads from accessing the document
        at the same time. Threads attempting to access the document will wait in
        suspended state until the thread that owns the lock calls doc.Unlock().
         */
        lock(): Promise<void>;
        /**
         * Locks the document to prevent competing write threads (using Lock()) from accessing the document
        at the same time. Other reader threads however, will be allowed to access the document.
        Threads attempting to obtain write access to the document will wait in
        suspended state until the thread that owns the lock calls doc.UnlockRead().
        Note: To avoid deadlocks obtaining a write lock while holding
        a read lock is not permitted and will throw an exception. If this situation is encountered
        please either unlock the read lock before the write lock is obtained
        or acquire a write lock (rather than read lock) in the first place.
         */
        lockRead(): Promise<void>;
        /**
         * Try locking the document.
         * @returns A promise that resolves to true if the document is locked for multi-threaded access, false otherwise.
         */
        tryLock(): Promise<boolean>;
        /**
         * Try locking the document, waiting no longer than specified number of milliseconds.
         * @param milliseconds - max number of milliseconds to wait for the document to lock
         * @returns A promise that resolves to true if the document is locked for multi-threaded access, false otherwise.
         */
        timedLock(milliseconds: number): Promise<boolean>;
        /**
         * Tries to obtain a read lock the document and returns true if the lock was
        successfully acquired
         * @returns A promise that resolves to true if the document is locked for multi-threaded access, false otherwise.
         */
        tryLockRead(): Promise<boolean>;
        /**
         * Tries to obtain a read lock the document for <milliseconds> duration, and returns
        true if the lock was successfully acquired
         * @param milliseconds - duration to obtain a read lock for.
         * @returns A promise that resolves to true if the document is locked for multi-threaded access, false otherwise.
         */
        timedLockRead(milliseconds: number): Promise<boolean>;
    }
    /**
     * PDFDocInfo is a high-level utility class that can be used
     * to read and modify document's metadata.
     */
    class PDFDocInfo {
        /**
         * @returns A promise that resolves to the document's title.
         */
        getTitle(): Promise<string>;
        /**
         * @returns A promise that resolves to sDF/Cos string object representing document's title.
         */
        getTitleObj(): Promise<PDFNet.Obj>;
        /**
         * Set document's title.
         * @param title - New title of the document.
         */
        setTitle(title: string): Promise<void>;
        /**
         * @returns A promise that resolves to the name of the person who created the document.
         */
        getAuthor(): Promise<string>;
        /**
         * @returns A promise that resolves to sDF/Cos string object representing document's author.
         */
        getAuthorObj(): Promise<PDFNet.Obj>;
        /**
         * Set the author of the document.
         * @param author - The name of the person who created the document.
         */
        setAuthor(author: string): Promise<void>;
        /**
         * @returns A promise that resolves to the subject of the document.
         */
        getSubject(): Promise<string>;
        /**
         * @returns A promise that resolves to sDF/Cos string object representing document's subject.
         */
        getSubjectObj(): Promise<PDFNet.Obj>;
        /**
         * Set the subject of the document
         * @param subject - The subject of the document.
         */
        setSubject(subject: string): Promise<void>;
        /**
         * @returns A promise that resolves to keywords associated with the document.
         */
        getKeywords(): Promise<string>;
        /**
         * @returns A promise that resolves to sDF/Cos string object representing document's keywords.
         */
        getKeywordsObj(): Promise<PDFNet.Obj>;
        /**
         * Set keywords associated with the document.
         * @param Keywords - Keywords associated with the document.
         */
        setKeywords(Keywords: string): Promise<void>;
        /**
         * @returns A promise that resolves to if the document was converted to PDF from another
         * format, the name of the application that created the original
         * document from which it was converted.
         */
        getCreator(): Promise<string>;
        /**
         * @returns A promise that resolves to sDF/Cos string object representing document's creator.
         */
        getCreatorObj(): Promise<PDFNet.Obj>;
        /**
         * Set document's creator.
         * @param creator - The name of the application that created
         * the original document.
         */
        setCreator(creator: string): Promise<void>;
        /**
         * @returns A promise that resolves to if the document was converted to PDF from another format,
         * the name of the application (for example, Distiller) that
         * converted it to PDF.
         */
        getProducer(): Promise<string>;
        /**
         * @returns A promise that resolves to sDF/Cos string object representing document's producer.
         */
        getProducerObj(): Promise<PDFNet.Obj>;
        /**
         * Set document's producer.
         * @param producer - The name of the application that generated PDF.
         */
        setProducer(producer: string): Promise<void>;
        /**
         * @returns A promise that resolves to the date and time the document was created,
         * in human-readable form.
         */
        getCreationDate(): Promise<PDFNet.Date>;
        /**
         * Set document's creation date.
         * @param creation_date - The date and time the document was created.
         */
        setCreationDate(creation_date: PDFNet.Date): Promise<void>;
        /**
         * @returns A promise that resolves to the date and time the document was most recently
         * modified, in human-readable form.
         */
        getModDate(): Promise<PDFNet.Date>;
        /**
         * Set document's modification date.
         * @param mod_date - The date and time the document was most
         * recently modified.
         */
        setModDate(mod_date: PDFNet.Date): Promise<void>;
        /**
         * @returns A promise that resolves to document's SDF/Cos 'Info' dictionary or NULL if
         * the info dictionary is not available.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDocInfo"
         */
        static create(tr: PDFNet.Obj): Promise<PDFNet.PDFDocInfo>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDocInfo"
         */
        copy(): Promise<PDFNet.PDFDocInfo>;
    }
    /**
     * PDFDocViewPrefs is a high-level utility class that can be
     * used to control the way the document is to be presented on
     * the screen or in print.
     *
     * PDFDocViewPrefs class corresponds to PageMode, PageLayout, and
     * ViewerPreferences entries in the document's catalog. For more
     * details please refer to section 8.1 'Viewer Preferences' in
     * PDF Reference Manual.
     */
    class PDFDocViewPrefs {
        /**
         * A utility method used to set the fist page displayed after
         * the document is opened. This method is equivalent to
         * PDFDoc::SetOpenAction(goto_action).
         *
         * If OpenAction is not specified the document should be
         * opened to the top of the first page at the default magnification
         * factor.
         * @param dest - A value specifying the page destination to be
         * displayed when the document is opened.
         *
         * Example:
         * <pre>
         * Destination dest = Destination::CreateFit(page);
         * pdfdoc.GetViewPrefs().SetInitialPage(dest);
         * </pre>
         */
        setInitialPage(dest: PDFNet.Destination): Promise<void>;
        /**
         * Sets PageMode property and change the value of the
         * PageMode key in the Catalog dictionary.
         * @param mode - <pre>
         * PDFNet.PDFDocViewPrefs.PageMode = {
         * 	e_UseNone : 0
         * 	e_UseThumbs : 1
         * 	e_UseBookmarks : 2
         * 	e_FullScreen : 3
         * 	e_UseOC : 4
         * 	e_UseAttachments : 5
         * }
         * </pre>
         * New PageMode setting. Default value is e_UseNone.
         */
        setPageMode(mode: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFDocViewPrefs.PageMode = {
         * 	e_UseNone : 0
         * 	e_UseThumbs : 1
         * 	e_UseBookmarks : 2
         * 	e_FullScreen : 3
         * 	e_UseOC : 4
         * 	e_UseAttachments : 5
         * }
         * </pre>
         * @returns A promise that resolves to the value of currently selected PageMode property.
         */
        getPageMode(): Promise<number>;
        /**
         * Sets PageLayout property and change the value of the
         * PageLayout key in the Catalog dictionary.
         * @param mode - <pre>
         * PDFNet.PDFDocViewPrefs.PageLayout = {
         * 	e_Default : 0
         * 	e_SinglePage : 1
         * 	e_OneColumn : 2
         * 	e_TwoColumnLeft : 3
         * 	e_TwoColumnRight : 4
         * 	e_TwoPageLeft : 5
         * 	e_TwoPageRight : 6
         * }
         * </pre>
         * New PageLayout setting. Default value is
         * e_SinglePage.
         */
        setLayoutMode(mode: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFDocViewPrefs.PageLayout = {
         * 	e_Default : 0
         * 	e_SinglePage : 1
         * 	e_OneColumn : 2
         * 	e_TwoColumnLeft : 3
         * 	e_TwoColumnRight : 4
         * 	e_TwoPageLeft : 5
         * 	e_TwoPageRight : 6
         * }
         * </pre>
         * @returns A promise that resolves to the value of currently selected PageLayout property.
         */
        getLayoutMode(): Promise<number>;
        /**
         * sets the value of given ViewerPref property.
         * @param pref - <pre>
         * PDFNet.PDFDocViewPrefs.ViewerPref = {
         * 	e_HideToolbar : 0
         * 	e_HideMenubar : 1
         * 	e_HideWindowUI : 2
         * 	e_FitWindow : 3
         * 	e_CenterWindow : 4
         * 	e_DisplayDocTitle : 5
         * }
         * </pre>
         * the ViewerPref property type to modify.
         * @param value - The new value for the property.
         */
        setPref(pref: number, value: boolean): Promise<void>;
        /**
         * @param pref - <pre>
         * PDFNet.PDFDocViewPrefs.ViewerPref = {
         * 	e_HideToolbar : 0
         * 	e_HideMenubar : 1
         * 	e_HideWindowUI : 2
         * 	e_FitWindow : 3
         * 	e_CenterWindow : 4
         * 	e_DisplayDocTitle : 5
         * }
         * </pre>
         * the ViewerPref property type to query.
         * @returns A promise that resolves to the value of given ViewerPref property.
         */
        getPref(pref: number): Promise<boolean>;
        /**
         * set the document's page mode, specifying how to display the
         * document on exiting full-screen mode.
         * @param mode - <pre>
         * PDFNet.PDFDocViewPrefs.PageMode = {
         * 	e_UseNone : 0
         * 	e_UseThumbs : 1
         * 	e_UseBookmarks : 2
         * 	e_FullScreen : 3
         * 	e_UseOC : 4
         * 	e_UseAttachments : 5
         * }
         * </pre>
         * PageMode used after exiting full-screen mode.
         * Default value: e_UseNone.
         */
        setNonFullScreenPageMode(mode: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFDocViewPrefs.PageMode = {
         * 	e_UseNone : 0
         * 	e_UseThumbs : 1
         * 	e_UseBookmarks : 2
         * 	e_FullScreen : 3
         * 	e_UseOC : 4
         * 	e_UseAttachments : 5
         * }
         * </pre>
         * @returns A promise that resolves to the PageMode used after exiting full-screen mode.
         */
        getNonFullScreenPageMode(): Promise<number>;
        /**
         * sets the predominant reading order for text.
         *
         * This flag has no direct effect on the document's contents
         * or page numbering but can be used to determine the relative
         * positioning of pages when displayed side by side or
         * printed n-up.
         * @param left_to_right - true if the predominant reading
         * order for text is from left to right and false if it is
         * right to left (including vertical writing systems, such
         * as Chinese, Japanese, and Korean).
         * Default value: left_to_right is true.
         */
        setDirection(left_to_right: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to true is the predominant reading order for text
         * is left to right, false otherwise. See SetDirection() for
         * more information.
         */
        getDirection(): Promise<boolean>;
        /**
         * Sets the page boundary representing the area of a page
         * to be displayed when viewing the document on the screen.
         * @param box - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * page boundary displayed when viewing the document
         * on the screen. By default, PDF viewers will display the
         * crop-box.
         */
        setViewArea(box: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * @returns A promise that resolves to the page boundary representing the area of a page
         * to be displayed when viewing the document on the screen.
         */
        getViewArea(): Promise<number>;
        /**
         * sets the page boundary to which the contents of a page are
         * to be clipped when viewing the document on the screen.
         * @param box - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * screen clip region. The default value is
         * page crop-box.
         */
        setViewClip(box: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * @returns A promise that resolves to the page boundary to which the contents of a page
         * are to be clipped when viewing the document on the screen.
         */
        getViewClip(): Promise<number>;
        /**
         * sets the page boundary representing the area of a page to
         * be rendered when printing the document.
         * @param box - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * printing region. The default value is page
         * crop-box.
         */
        setPrintArea(box: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * @returns A promise that resolves to the page boundary representing the area of a page
         * to be rendered when printing the document.
         */
        getPrintArea(): Promise<number>;
        /**
         * sets the page boundary to which the contents of a page are
         * to be clipped when printing the document.
         * @param box - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * printing clip region. The default value is page
         * crop-box.
         */
        setPrintClip(box: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * @returns A promise that resolves to the page boundary to which the contents of a page
         * are to be clipped when printing the document.
         */
        getPrintClip(): Promise<number>;
        /**
         * @returns A promise that resolves to document's SDF/Cos 'ViewerPreferences' dictionary
         * or NULL if the object is not present.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDocViewPrefs"
         */
        static create(tr: PDFNet.Obj): Promise<PDFNet.PDFDocViewPrefs>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDocViewPrefs"
         */
        copy(): Promise<PDFNet.PDFDocViewPrefs>;
    }
    /**
     * PDFDraw contains methods for converting PDF pages to images and to Bitmap objects.
     * Utility methods are provided to export PDF pages to various raster formats as well
     * as to convert pages to GDI+ bitmaps for further manipulation or drawing.
     */
    class PDFDraw extends PDFNet.Destroyable {
        /**
         * sets the Optional Content Group (OCG) context that should be used when
         * rendering the page. This function can be used to selectively render optional
         * content (such as PDF layers) based on the states of optional content groups
         * in the given context.
         * @param ctx - Optional Content Group (OCG) context, or NULL if the rasterizer
         * should render all content on the page.
         */
        setOCGContext(ctx: PDFNet.OCGContext): Promise<void>;
        /**
         * PdFDraw constructor and destructor
         * @param [dpi] - Default resolution used to rasterize pages. If the parameter is not
         * specified, the initial resolution is 92 dots per inch. DPI parameter can be
         * modified at any time using PDFDraw::SetDPI() method.
         * @returns A promise that resolves to an object of type: "PDFNet.PDFDraw"
         */
        static create(dpi?: number): Promise<PDFNet.PDFDraw>;
        /**
         * Sets the core graphics library used for rasterization and
         * rendering. Using this method it is possible to quickly switch
         * between different implementations. By default, PDFDraw uses
         * the built-in, platform independent rasterizer.
         * @param type - <pre>
         * PDFNet.PDFRasterizer.Type = {
         * 	e_BuiltIn : 0
         * 	e_GDIPlus : 1
         * }
         * </pre>
         * Rasterizer type.
         */
        setRasterizerType(type: number): Promise<void>;
        /**
         * Sets the output image resolution.
         *
         * DPI stands for Dots Per Inch. This parameter is used to specify the output
         * image size and quality. A typical screen resolution for monitors these days is
         * 92 DPI, but printers could use 200 DPI or more.
         * @param dpi - value to set the image resolution to. Higher value = higher resolution.
         */
        setDPI(dpi: number): Promise<void>;
        /**
         * SetImageSize can be used instead of SetDPI() to adjust page  scaling so that
         * image fits into a buffer of given dimensions.
         *
         * If this function is used, DPI will be calculated dynamically for each
         * page so that every page fits into the buffer of given dimensions.
         * @param width - The width of the image, in pixels/samples.
         * @param height - The height of the image, in pixels/samples.
         * @param [preserve_aspect_ratio] - True to preserve the aspect ratio, false
         * otherwise. By default, preserve_aspect_ratio is true.
         */
        setImageSize(width: number, height: number, preserve_aspect_ratio?: boolean): Promise<void>;
        /**
         * Selects the page box/region to rasterize.
         * @param region - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * Page box to rasterize. By default, PDFDraw will rasterize
         * page crop box.
         */
        setPageBox(region: number): Promise<void>;
        /**
         * clip the render region to the provided rect (in page space)
         * @param rect - clipping rect. By default, PDFDraw will rasterize
         * the entire page box.
         */
        setClipRect(rect: PDFNet.Rect): Promise<void>;
        /**
         * Flips the vertical (i.e. Y) axis of the image.
         * @param flip_y - true to flip the Y axis, false otherwise. For compatibility with
         * most raster formats 'flip_y' is true by default.
         */
        setFlipYAxis(flip_y: boolean): Promise<void>;
        /**
         * Sets the rotation value for this page.
         * @param angle - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * Rotation value to be set for a given page. Must be one
         * of the Page::Rotate values.
         */
        setRotate(angle: number): Promise<void>;
        /**
         * Enable or disable annotation and forms rendering. By default, all annotations
         * and form fields are rendered.
         * @param render_annots - True to draw annotations, false otherwise.
         */
        setDrawAnnotations(render_annots: boolean): Promise<void>;
        /**
         * Enable or disable highlighting form fields. Default is disabled.
         * @param highlight - true to highlight, false otherwise.
         */
        setHighlightFields(highlight: boolean): Promise<void>;
        /**
         * Enable or disable anti-aliasing.
         *
         * Anti-Aliasing is a technique used to improve the visual
         * quality of images when displaying them on low resolution
         * devices (for example, low DPI computer monitors).
         * @param enable_aa - if true anti-aliasing will be enabled.
         */
        setAntiAliasing(enable_aa: boolean): Promise<void>;
        /**
         * Enable or disable path hinting.
         * @param enable_hinting - if true path hinting will be enabled. Path hinting is used
         * to slightly adjust paths in order to avoid or alleviate artifacts of hair line
         * cracks between certain graphical elements. This option is turned on by default.
         */
        setPathHinting(enable_hinting: boolean): Promise<void>;
        /**
         * Set thin line adjustment parameters.
         * @param grid_fit - if true (horizontal/vertical) thin lines will be snapped to
         * integer pixel positions. This helps make thin lines look sharper and clearer. This
         * option is turned off by default and it only works if path hinting is enabled.
         * @param stroke_adjust - if true auto stroke adjustment is enabled. Currently, this would
         * make lines with sub-pixel width to be one-pixel wide. This option is turned on by default.
         */
        setThinLineAdjustment(grid_fit: boolean, stroke_adjust: boolean): Promise<void>;
        /**
         * Sets the gamma factor used for anti-aliased rendering.
         * @param exp - is the exponent value of gamma function. Typical values
         * are in the range from 0.1 to 3.
         *
         * Gamma correction can be used to improve the quality of anti-aliased
         * image output and can (to some extent) decrease the appearance common
         * anti-aliasing artifacts (such as pixel width lines between polygons).
         */
        setGamma(exp: number): Promise<void>;
        /**
         * tells the rasterizer to render the page 'print' mode. Certain page elements
         * (such as annotations or OCG-s) are meant to be visible either on the screen or
         * on the printed paper but not both. A common example, is the "Submit" button on
         * electronic forms.
         * @param is_printing - set to true if the page should be rendered in print mode.
         */
        setPrintMode(is_printing: boolean): Promise<void>;
        /**
         * sets the page color to transparent.
         * @param is_transparent - If true, page's backdrop color will be transparent.
         * If false, the page's backdrop will be a opaque white.
         */
        setPageTransparent(is_transparent: boolean): Promise<void>;
        /**
         * sets the default color of the page backdrop.
         * @param r - the red component of the page backdrop color.
         * @param g - the green component of the page backdrop color.
         * @param b - the blue component of the page backdrop color.
         */
        setDefaultPageColor(r: number, g: number, b: number): Promise<void>;
        /**
         * enable or disable support for overprint and overprint simulation.
         * Overprint is a device dependent feature and the results will vary depending on
         * the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc).
         * @param op - <pre>
         * PDFNet.PDFRasterizer.OverprintPreviewMode = {
         * 	e_op_off : 0
         * 	e_op_on : 1
         * 	e_op_pdfx_on : 2
         * }
         * </pre>
         * e_op_on: always enabled; e_op_off: always disabled; e_op_pdfx_on: enabled for PDF/X files only.
         */
        setOverprint(op: number): Promise<void>;
        /**
         * Enable or disable image smoothing.
         *
         * The rasterizer allows a tradeoff between rendering quality and rendering speed.
         * This function can be used to indicate the preference between rendering speed and quality.
         * @param [smoothing_enabled] - True to enable image smoothing, false otherwise.
         * @param [hq_image_resampling] - True to use a higher quality (but slower) smoothing algorithm
         */
        setImageSmoothing(smoothing_enabled?: boolean, hq_image_resampling?: boolean): Promise<void>;
        /**
         * enables or disables caching. Caching can improve the rendering performance in cases
         * where the same page will be drawn multiple times.
         * @param [enabled] - if true PDFRasterizer will cache frequently used graphics objects.
         */
        setCaching(enabled?: boolean): Promise<void>;
        export(page: PDFNet.Page, filename: string, format?: string, encoder_params?: PDFNet.Obj): Promise<void>;
        /**
         * Set the color post processing transformation.
         * This transform is applied to the rasterized bitmap as the final step
         * in the rasterization process, and is applied directly to the resulting
         * bitmap (disregarding any color space information). Color post
         * processing only supported for RGBA output.
         * @param mode - <pre>
         * PDFNet.PDFRasterizer.ColorPostProcessMode = {
         * 	e_postprocess_none : 0
         * 	e_postprocess_invert : 1
         * }
         * </pre>
         * is the specific transform to be applied
         */
        setColorPostProcessMode(mode: number): Promise<void>;
        /**
         * Export the given PDF page to a buffer.
         * @param page - The source PDF page.
         * @param [format] - The file format of the output image. Currently supported formats
        are:
           - "RAW"   : RAW format. There are four possibilities:
                          e_rgba - if transparent and color page;
                          e_gray_alpha - if transparent and gray page;
                          e_rgb - if opaque and color page;
                          e_gray - if opaque and gray page.
                        NOTE that if page is set to be transparent (SetPageTransparent),
                        the output color channels are already multiplied by the alpha channel.
           - "BMP"   : Bitmap image format (BMP)
           - "JPEG"  : Joint Photographic Experts Group (JPEG) image format
           - "PNG"   : 24-bit W3C Portable Network Graphics (PNG) image format
           - "PNG8"  : 8-bit, palettized PNG format. The exported file size should be
                       smaller than the one generated using "PNG", possibly at the
                       expense of some image quality.
           - "TIFF"  : Tag Image File Format (TIFF) image format.
           - "TIFF8" : Tag Image File Format (TIFF) image format (with 8-bit palete).
        
        By default, the function exports to PNG.
         * @param [encoder_params] - An optional SDF dictionary object containing key/value
        pairs representing optional encoder parameters. The following table list possible
        parameters for corresponding export filters:
        
         <table border="1">
            <tr>
                <td>Parameter/Key</td>
                <td>Output Format</td>
                <td>Description/Value</td>
                <td>Example</td>
            </tr>
            <tr>
                <td>Quality</td>
                <td>JPEG</td>
                <td>The value for compression 'Quality' must be a number between 0 and 100
                    specifying the tradeoff between compression ratio and loss in image quality.
                    100 stands for best quality.</td>
                <td>
                    <P style="Z-INDEX: 0">hint.PutNumber("Quality", 60);</P>
                    <P>See the Example 2 in PDFDraw sample project.</P>
                </td>
            </tr>
            <tr>
                <td>Dither</td>
                <td>
                    PNG, PNG8, TIFF or TIFF8.</td>
                <td>A boolean used to enable or disable dithering. Relevent only for when the image
                    is exported in palettized or monochrome mode.</td>
                <td>hint.PutBool("Dither", true);</td>
            </tr>
            <tr>
                <td>ColorSpace</td>
                <td>PNG or TIFF for grayscale; TIFF for CMYK; PNG, BMP, JPEG, or TIFF for Separation.</td>
                <td>A name object used to select the rendering and export color space. Currently
                    supported values are "Gray", "RGB, "CMYK", and "Separation". The output image
                    format must support specified color space, otherwise the parameter will be ignored.
                    An example of image format that supports CMYK is TIFF. Image formats that support
                    grayscale are PNG and TIFF. Separation output is supported in either a single
                    N-Channel TIFF, or in separate single-channel files (either PNG, BMP, or JPEG).
                    Output in "Separation" space implies that overprint simulation is on.
                    By default, the image is rendered and exported in RGB color space.</td>
                <td>hint.PutName("ColorSpace", "CMYK");</td>
            </tr>
            <tr>
                <td>BPC</td>
                <td>PNG or TIFF.</td>
                <td>A number used to specify 'bits per pixel' in the output file. Currently
                    supported values are 1 and 8 (default is 8). To export monochrome (1 bit
                    per pixel) image, use 1 as the value of BPC parameter and use TIFF or PNG as the
                    export format for the image. By default, the image is not dithered when BPC is 1.
                    To enable dithering add 'Dither' option in the export hint.</td>
                <td>hint.PutNumber("BPC", 1);</td>
            </tr>
         </table>
         * @returns A promise that resolves to a Uint8Array containing the page data.
         */
        static exportBuffer(page: PDFNet.Page, format?: string, encoder_params?: PDFNet.Obj): Promise<Uint8Array>;
    }
    /**
     * PDFRasterizer is a low-level PDF rasterizer.
     *
     * The main purpose of this class is to convert PDF pages to raster
     * images (or bitmaps).
     */
    class PDFRasterizer extends PDFNet.Destroyable {
        /**
         * PdFRasterizer constructor and destructor
         * @param [type] - <pre>
         * PDFNet.PDFRasterizer.Type = {
         * 	e_BuiltIn : 0
         * 	e_GDIPlus : 1
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.PDFRasterizer"
         */
        static create(type?: number): Promise<PDFNet.PDFRasterizer>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ChunkRenderer"
         */
        getChunkRendererPath(page: PDFNet.Page, file_path: string, width: number, height: number, demult: boolean, device_mtx: PDFNet.Matrix2D, clip: PDFNet.Rect, scrl_clp_regions: PDFNet.Rect, cancel: boolean): Promise<PDFNet.ChunkRenderer>;
        /**
         * Enable or disable annotation and forms rendering. By default, annotations and forms
         * are rendered.
         * @param render_annots - True to draw annotations, false otherwise.
         */
        setDrawAnnotations(render_annots: boolean): Promise<void>;
        /**
         * Enable or disable highlighting form fields. Default is disabled.
         * @param highlight - true to highlight, false otherwise.
         */
        setHighlightFields(highlight: boolean): Promise<void>;
        setAntiAliasing(enable_aa: boolean): Promise<void>;
        /**
         * Enable or disable path hinting.
         * @param enable_hinting - if true path hinting is enabled. Path hinting is used to slightly
         * adjust paths in order to avoid or alleviate artifacts of hair line cracks between
         * certain graphical elements. This option is turned on by default.
         */
        setPathHinting(enable_hinting: boolean): Promise<void>;
        /**
         * Set thin line adjustment parameters.
         * @param grid_fit - if true (horizontal/vertical) thin lines will be snapped to
         * integer pixel positions. This helps make thin lines look sharper and clearer. This
         * option is turned off by default and it only works if path hinting is enabled.
         * @param stroke_adjust - if true auto stroke adjustment is enabled. Currently, this would
         * make lines with sub-pixel width to be one-pixel wide. This option is turned on by default.
         */
        setThinLineAdjustment(grid_fit: boolean, stroke_adjust: boolean): Promise<void>;
        /**
         * sets the gamma factor used for anti-aliased rendering.
         * @param expgamma - is the exponent value of gamma function. Typical values
         * are in the range from 0.1 to 3.
         *
         * Gamma correction can be used to improve the quality of anti-aliased
         * image output and can (to some extent) decrease the appearance common
         * anti-aliasing artifacts (such as pixel width lines between polygons).
         */
        setGamma(expgamma: number): Promise<void>;
        /**
         * sets the Optional Content Group (OCG) context that should be used when
         *  rendering the page. This function can be used to selectively render optional
         * content (such as PDF layers) based on the states of optional content groups
         * in the given context.
         * @param ctx - Optional Content Group (OCG) context, or NULL if the rasterizer
         * should render all content on the page.
         */
        setOCGContext(ctx: PDFNet.OCGContext): Promise<void>;
        /**
         * tells the rasterizer to render the page 'print' mode. Certain page elements
         * (such as annotations or OCG-s) are meant to be visible either on the screen or
         * on the printed paper but not both. A common example, is the "Submit" button on
         * electronic forms.
         * @param is_printing - set to true is the page should be rendered in print mode.
         */
        setPrintMode(is_printing: boolean): Promise<void>;
        /**
         * Enable or disable image smoothing.
         *
         * The rasterizer allows a tradeoff between rendering quality and rendering speed.
         * This function can be used to indicate the preference between rendering speed and quality.
         * @param [smoothing_enabled] - True to enable image smoothing, false otherwise.
         * @param [hq_image_resampling] - True to use a higher quality (but slower) smoothing algorithm
         */
        setImageSmoothing(smoothing_enabled?: boolean, hq_image_resampling?: boolean): Promise<void>;
        /**
         * enable or disable support for overprint and overprint simulation.
         * Overprint is a device dependent feature and the results will vary depending on
         * the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc).
         * @param op - <pre>
         * PDFNet.PDFRasterizer.OverprintPreviewMode = {
         * 	e_op_off : 0
         * 	e_op_on : 1
         * 	e_op_pdfx_on : 2
         * }
         * </pre>
         * e_op_on: always enabled; e_op_off: always disabled; e_op_pdfx_on: enabled for PDF/X files only.
         */
        setOverprint(op: number): Promise<void>;
        /**
         * enables or disables caching. Caching can improve the rendering performance in cases
         * where the same page will be drawn multiple times.
         * @param [enabled] - if true PDFRasterizer will cache frequently used graphics objects.
         */
        setCaching(enabled?: boolean): Promise<void>;
        /**
         * @param new_view_state - <pre>
         * PDFNet.Annot.State = {
         * 	e_normal : 0
         * 	e_rollover : 1
         * 	e_down : 2
         * }
         * </pre>
         */
        setAnnotationState(annot: PDFNet.Annot, new_view_state: number): Promise<void>;
        /**
         * Sets the core graphics library used for rasterization and
         * rendering. Using this method it is possible to quickly switch
         * between different implementations. By default, PDFNet uses a
         * built-in, high-quality, and platform independent rasterizer.
         * @param type - <pre>
         * PDFNet.PDFRasterizer.Type = {
         * 	e_BuiltIn : 0
         * 	e_GDIPlus : 1
         * }
         * </pre>
         * Rasterizer type.
         */
        setRasterizerType(type: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFRasterizer.Type = {
         * 	e_BuiltIn : 0
         * 	e_GDIPlus : 1
         * }
         * </pre>
         * @returns A promise that resolves to the type of current rasterizer.
         */
        getRasterizerType(): Promise<number>;
        /**
         * Set the color post processing transformation.
         * This transform is applied to the rasterized bitmap as the final step
         * in the rasterization process, and is applied directly to the resulting
         * bitmap (disregarding any color space information). Color post
         * processing only supported for RGBA output.
         * @param mode - <pre>
         * PDFNet.PDFRasterizer.ColorPostProcessMode = {
         * 	e_postprocess_none : 0
         * 	e_postprocess_invert : 1
         * }
         * </pre>
         * is the specific transform to be applied
         */
        setColorPostProcessMode(mode: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PDFRasterizer.ColorPostProcessMode = {
         * 	e_postprocess_none : 0
         * 	e_postprocess_invert : 1
         * }
         * </pre>
         * @returns A promise that resolves to the current color post processing mode.
         */
        getColorPostProcessMode(): Promise<number>;
        enableDisplayListCaching(enabled: boolean): Promise<void>;
        /**
         * this function is typically called for progressive rendering, in which
         *  we don't want to stop the main rendering thread. Since the rendering thread may
         *  modify separation channels, we don't consider separations in progressive rendering.
         */
        updateBuffer(): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.OwnedBitmap"
         */
        rasterizeAnnot(annot: PDFNet.Annot, page: PDFNet.Page, device_mtx: PDFNet.Matrix2D, demult: boolean, cancel: boolean): Promise<PDFNet.OwnedBitmap>;
    }
    /**
     * PDFView is a utility class that can be used for interactive rendering of PDF documents.
     *
     * In .NET environment PDFView is derived from System.Windows.Forms.Control and it can be
     * used like a regular form (see PDFViewForm.cs in PDFView sample for C# for a concrete
     * example).
     *
     * PDFView implements some essential features such as double-buffering, multi-threaded
     * rendering, scrolling, zooming, and page navigation that are essential in interactive
     * rendering applications (e.g. in client PDF viewing and editing applications).
     *
     * PDFView defines several coordinate spaces and it is important to understand their
     * differences:
     *
     * - Page Space refers to the space in which a PDF page is defined. It is determined by
     * a page itself and the origin is at the lower-left corner of the page. Note that Page
     * Space is independent of how a page is viewed in PDFView and each page has its own Page
     * space.
     *
     * - Canvas Space refers to the tightest axis-aligned bounding box of all the pages given
     * the current page presentation mode in PDFView. For example, if the page presentation
     * mode is e_single_continuous, all the pages are arranged vertically with one page in each
     * row, and therefore the Canvas Space is rectangle with possibly large height value. For
     * this reason, Canvas Space is also, like Page Space, independent of the zoom factor. Also
     * note that since PDFView adds gaps between adjacent pages, the Canvas Space is larger than
     * the space occupied by all the pages. The origin of the Canvas Space is located at the
     * upper-left corner.
     *
     * - Screen Space (or Client Space) is the space occupied by PDFView and its origin is at
     * the upper-left corner. Note that the virtual size of this space can extend beyond the
     * visible region.
     *
     * - Scrollable Space is the virtual space within which PDFView can scroll. It is determined
     * by the Canvas Space and the current zoom factor. Roughly speaking, the dimensions of the
     * Scrollable Space is the dimensions of the Canvas Space timed by the zoom. Therefore, a large
     * zoom factor will result in a larger Scrollable region given the same Canvas region. For this
     * reason, Scrollable Space might also be referred to as Zoomed Canvas Space. Note that since
     * PDFView adds gaps between pages in Canvas Space and these gaps are not scaled when rendered,
     * the scrollable range is not exactly what the zoom factor times the Canvas range. For
     * functions such as SetHScrollPos(), SetVScrollPos(), GetCanvasHeight(), and
     * GetCanvasWidth(), it is the Scrollable Space that is involved.
     */
    class PDFView {
    }
    /**
     * PDFViewCtrl is a utility class that can be used for interactive rendering of PDF documents.
     *
     * In .NET environment PDFViewCtrl is derived from System.Windows.Forms.Control and it can be
     * used like a regular form (see PDFViewForm.cs in PDFView sample for C# for a concrete
     * example).
     *
     * Unlike PDFView, PDFViewCtrl is a control that implements a number of tool modes,
     * dialog boxes like find and password, has some built-in form filling capabilities
     * and a navigation panel for bookmarks, thumbview and layer views.
     *
     * PDFViewCtrl defines several coordinate spaces and it is important to understand their
     * differences:
     *
     * - Page Space refers to the space in which a PDF page is defined. It is determined by
     * a page itself and the origin is at the lower-left corner of the page. Note that Page
     * Space is independent of how a page is viewed in PDFView and each page has its own Page
     * space.
     *
     * - Canvas Space refers to the tightest axis-aligned bounding box of all the pages given
     * the current page presentation mode in PDFView. For example, if the page presentation
     * mode is e_single_continuous, all the pages are arranged vertically with one page in each
     * row, and therefore the Canvas Space is rectangle with possibly large height value. For
     * this reason, Canvas Space is also, like Page Space, independent of the zoom factor. Also
     * note that since PDFView adds gaps between adjacent pages, the Canvas Space is larger than
     * the space occupied by all the pages. The origin of the Canvas Space is located at the
     * upper-left corner.
     *
     * - Screen Space (or Client Space) is the space occupied by PDFView and its origin is at
     * the upper-left corner. Note that the virtual size of this space can extend beyond the
     * visible region.
     *
     * - Scrollable Space is the virtual space within which PDFView can scroll. It is determined
     * by the Canvas Space and the current zoom factor. Roughly speaking, the dimensions of the
     * Scrollable Space is the dimensions of the Canvas Space timed by the zoom. Therefore, a large
     * zoom factor will result in a larger Scrollable region given the same Canvas region. For this
     * reason, Scrollable Space might also be referred to as Zoomed Canvas Space. Note that since
     * PDFView adds gaps between pages in Canvas Space and these gaps are not scaled when rendered,
     * the scrollable range is not exactly what the zoom factor times the Canvas range. For
     * functions such as SetHScrollPos(), SetVScrollPos(), GetCanvasHeight(), and
     * GetCanvasWidth(), it is the Scrollable Space that is involved.
     */
    class PDFViewCtrl {
    }
    /**
     * Page is a high-level class representing PDF page object (see 'Page Objects' in
     * Section 3.6.2, 'Page Tree,' in PDF Reference Manual).
     *
     * Among other associated objects, a page object contains:
     *  - A series of objects representing the objects drawn on the page (See Element and
     *    ElementReader class for examples of how to extract page content).
     *  - A list of resources used in drawing the page
     *  - Annotations
     *  - Beads, private metadata, optional thumbnail image, etc.
     */
    class Page {
        /**
         * Initialize a page using an existing low-level Cos/SDF page object.
         * @param [page_dict] - a low-level (SDF/Cos) page dictionary.
         * @returns A promise that resolves to an object of type: "PDFNet.Page"
         */
        static create(page_dict?: PDFNet.Obj): Promise<PDFNet.Page>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Page"
         */
        copy(): Promise<PDFNet.Page>;
        /**
         * @returns A promise that resolves to true if this is a valid (non-null) page, false otherwise.
         * If the function returns false the underlying SDF/Cos object is null
         * or is not valid.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the Page number indication the position of this Page in document's page sequence.
         * Document's page sequence is indexed from 1. Page number 0 means that the page is not part
         * of document's page sequence or that the page is not valid.
         */
        getIndex(): Promise<number>;
        /**
         * Get the Action associated with the selected Page Trigger event.
         * @param trigger - <pre>
         * PDFNet.Page.EventType = {
         * 	e_action_trigger_page_open : 11
         * 	e_action_trigger_page_close : 12
         * }
         * </pre>
         * the type of trigger event to get
         * @returns A promise that resolves to the Action Obj if present, otherwise NULL
         */
        getTriggerAction(trigger: number): Promise<PDFNet.Obj>;
        /**
         * @param type - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * The type of the page bounding box. Possible values are: e_media, e_crop,
         * e_bleed, e_trim, and e_art.
         *
         * If the value for 'type' is e_crop, this call is equivalent to GetCropBox().
         * If the value for 'type' is e_media, this call is equivalent to GetMediaBox().
         * @returns A promise that resolves to the box specified for the page object intersected with the media box.
         */
        getBox(type: number): Promise<PDFNet.Rect>;
        /**
         * Sets the page bounding box specified by 'page_region' for this page.
         * @param type - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * The type of the page bounding box. Possible values are: e_media, e_crop,
         * e_bleed, e_trim, and e_art.
         * @param box - A rectangle specifying the coordinates to set for the box. The coordinates are
         * specified in user space units.
         */
        setBox(type: number, box: PDFNet.Rect): Promise<void>;
        /**
         * @returns A promise that resolves to the crop box for this page. The page dimensions are specified in user space
         * coordinates.
         *
         * The crop box is the region of the page to display and print.
         */
        getCropBox(): Promise<PDFNet.Rect>;
        /**
         * Sets the crop box for this page. The crop box is the region of the page to
         * display and print.
         * @param box - the new crop box for this page. The page dimensions are specified in user space
         * coordinates.
         *
         * The crop box defines the region to which the contents of the page are to be clipped (cropped)
         * when displayed or printed.
         */
        setCropBox(box: PDFNet.Rect): Promise<void>;
        /**
         * @returns A promise that resolves to the media box for this page. The page dimensions are specified in user space
         * coordinates.
         *
         * The media box defines the boundaries of the physical medium on which the page is to
         * be printed. It may include any extended area surrounding the finished page for bleed,
         * printing marks, or other such purposes.
         */
        getMediaBox(): Promise<PDFNet.Rect>;
        /**
         * Sets the media box for this page.
         * @param box - the new media box for this page. The page dimensions are specified in user space
         * coordinates.
         *
         * The media box defines the boundaries of the physical medium on which the page is to
         * be printed. It may include any extended area surrounding the finished page for bleed,
         * printing marks, or other such purposes.
         */
        setMediaBox(box: PDFNet.Rect): Promise<void>;
        /**
         * @returns A promise that resolves to the bounding box for this page. The page dimensions are specified in user space
         * coordinates.
         *
         * The bounding box is defined as the smallest rectangle that includes all the visible content on the page.
         */
        getVisibleContentBox(): Promise<PDFNet.Rect>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * @returns A promise that resolves to the rotation value for this page.
         */
        getRotation(): Promise<number>;
        /**
         * Sets the rotation value for this page.
         * @param angle - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * Rotation value to be set for a given page. Must be one
         * of the Page::Rotate values.
         */
        setRotation(angle: number): Promise<void>;
        /**
         * Rotate r0 clockwise by r1
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * @param r0 - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * first rotation.
         * @param r1 - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * second rotation.
         * @returns A promise that resolves to returns r0 + r1
         */
        static addRotations(r0: number, r1: number): Promise<number>;
        /**
         * Rotate r0 counter clockwise by r1.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * @param r0 - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * first rotation.
         * @param r1 - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * second rotation.
         * @returns A promise that resolves to returns r0 r1
         */
        static subtractRotations(r0: number, r1: number): Promise<number>;
        /**
         * Convert a rotation to a number.
         * @param r - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * rotation to convert to number
         * @returns A promise that resolves to one of four numbers; 0, 90, 180 or 270.
         */
        static rotationToDegree(r: number): Promise<number>;
        /**
         * Convert a number that represents rotation in degrees to a rotation enum.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * @param r - degree to convert to rotation. Valid numbers are multiples of 90.
         * @returns A promise that resolves to one of four angles; e_0, e_90, e_180 or e_270. Returns e_0 if input is
         * not a multiple 90.
         */
        static degreeToRotation(r: number): Promise<number>;
        /**
         * @param [box_type] - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * indicates the page box/region to query for width.
         * @returns A promise that resolves to the width for the given page region/box taking into account page
         * rotation attribute (i.e. /Rotate entry in page dictionary).
         */
        getPageWidth(box_type?: number): Promise<number>;
        /**
         * @param [box_type] - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * indicates the page box/region to query for height.
         * @returns A promise that resolves to the height for the given page region/box taking into account page
         * rotation attribute (i.e. /Rotate entry in page dictionary).
         */
        getPageHeight(box_type?: number): Promise<number>;
        /**
         * @param [flip_y] - this parameter can be used to mirror the page. if 'flip_y' is true the Y
         * axis is not flipped and it is increasing, otherwise Y axis is decreasing.
         * @param [box_type] - <pre>
         * PDFNet.Page.Box = {
         * 	e_media : 0
         * 	e_crop : 1
         * 	e_bleed : 2
         * 	e_trim : 3
         * 	e_art : 4
         * }
         * </pre>
         * an optional parameter used to specify the page box/region that the matrix
         * should map to. By default, the function transforms user space coordinates to cropped
         * coordinates.
         * @param [angle] - <pre>
         * PDFNet.Page.Rotate = {
         * 	e_0 : 0
         * 	e_90 : 1
         * 	e_180 : 2
         * 	e_270 : 3
         * }
         * </pre>
         * an optional parameter used to specify page rotation in addition to the
         * rotation specified in the page dictionary. This parameter is usually used to rotate the
         * page without modifying the document itself.
         * @returns A promise that resolves to the matrix that transforms user space coordinates to rotated and cropped coordinates.
         * The origin of this space is the bottom-left of the rotated, cropped page.
         */
        getDefaultMatrix(flip_y?: boolean, box_type?: number, angle?: number): Promise<PDFNet.Matrix2D>;
        /**
         * Returns SDF/Cos array containing annotation dictionaries. See Section 8.4 in
         * the PDF Reference for a description of the annotation array.
         * @returns A promise that resolves to an array of annotation dictionaries representing annotations
         * associated with the page or NULL if page dictionary is not specified.
         */
        getAnnots(): Promise<PDFNet.Obj>;
        /**
         * Returns the number of annotations on a page. Widget annotations (form fields) are
         * included in the count.
         * @returns A promise that resolves to the number of annotations on a page.
         */
        getNumAnnots(): Promise<number>;
        /**
         * Returns the annotation on the page.
         * @param index - The index of the annotation to get on a page. The first annotation
         * on a page has an index of zero.
         * @returns A promise that resolves to annotation object. If the index is out of range returned Annot object
         * is not valid (i.e. annot.IsValid() returns false).
         */
        getAnnot(index: number): Promise<PDFNet.Annot>;
        /**
         * Adds an annotation at the specified location in a page's annotation array.
         * @param pos - The location in the array to insert the object. The object is inserted
         * before the specified location. The first element in an array has a pos of zero.
         * If pos >= GetNumAnnots(), the method appends the annotation to the array.
         * @param annot - The annotation to add.
         */
        annotInsert(pos: number, annot: PDFNet.Annot): Promise<void>;
        /**
         * Adds an annotation to the end of a page's annotation array.
         * @param annot - The annotation to prepend in a page's annotation array.
         */
        annotPushBack(annot: PDFNet.Annot): Promise<void>;
        /**
         * Adds an annotation to the beginning of a page's annotation array.
         * @param annot - The annotation to append in a page's annotation array.
         */
        annotPushFront(annot: PDFNet.Annot): Promise<void>;
        /**
         * Removes the given annotation from the page.
         * @param annot - The annotation to remove.
         */
        annotRemove(annot: PDFNet.Annot): Promise<void>;
        /**
         * Removes the annotation at a given location.
         * @param index - A zero based index of the annotation to remove.
         */
        annotRemoveByIndex(index: number): Promise<void>;
        /**
         * A utility method used to scale physical dimensions of the page including
         * all page content.
         * @param scale - A number greater than 0 which is used as a scale factor.
         *  For example, calling page.Scale(0.5) will reduce physical dimensions of the
         *  page to half its original size, whereas page.Scale(2) will double the physical
         * dimensions of the page and will rescale all page content appropriately.
         */
        scale(scale: number): Promise<void>;
        /**
         * Flatten/Merge existing form field appearances with the page content and
         * remove widget annotation.
         *
         * Form 'flattening' refers to the operation that changes active form fields
         * into a static area that is part of the PDF document, just like the other
         * text and images in the document. A completely flattened PDF form does not
         * have any widget annotations or interactive fields.
         * @param field_to_flatten - field to flatten
         */
        flattenField(field_to_flatten: PDFNet.Field): Promise<void>;
        /**
         * tests whether this page has a transition.
         * @returns A promise that resolves to an object of type: "boolean"
         */
        hasTransition(): Promise<boolean>;
        /**
         * Returns the UserUnit value for the page. A UserUnit is a positive number giving
         * the size of default user space units, in multiples of 1/72 inch.
         * @returns A promise that resolves to the UserUnit value for the page. If the key is not present in the
         * page dictionary the default of 1.0 is returned.
         */
        getUserUnitSize(): Promise<number>;
        /**
         * Sets the UserUnit value for a page.
         * @param unit_size - A positive number giving the size of default user space
         * units, in multiples of 1/72 inch.
         */
        setUserUnitSize(unit_size: number): Promise<void>;
        /**
         * @returns A promise that resolves to a pointer to the page resource dictionary.
         */
        getResourceDict(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to nULL if page is empty, otherwise a single stream or an array of streams.
         */
        getContents(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to object representing the Image thumbnail.
         */
        getThumb(): Promise<PDFNet.Obj>;
        /**
         * Returns the page dictionary.
         * @returns A promise that resolves to the object to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * Some of the page attributes are designated as inheritable.
         * If such an attribute is omitted from a page object, its value is inherited
         * from an ancestor node in the page tree. If the attribute is a required one,
         * a value must be supplied in an ancestor node; if it is optional and no
         * inherited value is specified, the default value should be used.
         *
         * The function walks up the page inheritance tree in search for specified
         * attribute.
         * @param attrib - A string representing the attribute to search for.
         * @returns A promise that resolves to if the attribute was found return a pointer to the value. Otherwise
         * the function return NULL.
         *
         * 	Resources dictionary (Required; inheritable)
         *  MediaBox rectangle (Required; inheritable)
         *  CropBox rectangle (Optional; inheritable)
         *  Rotate integer (Optional; inheritable)
         */
        findInheritedAttribute(attrib: string): Promise<PDFNet.Obj>;
    }
    /**
     * PDF page labels can be used to describe a page. This is used to
     * allow for non-sequential page numbering or the addition of arbitrary
     * labels for a page (such as the inclusion of Roman numerals at the
     * beginning of a book). PDFNet PageLabel object can be used to specify
     * the numbering style to use (for example, upper- or lower-case Roman,
     * decimal, and so forth), the starting number for the first page,
     * and an arbitrary prefix to be pre-appended to each number (for
     * example, "A-" to generate "A-1", "A-2", "A-3", and so forth.)
     *
     * PageLabel corresponds to the PDF Page Label object (Section 8.3.1,
     * 'Page Labels' in the PDF Reference Manual.
     *
     * Each page in a PDF document is identified by an integer page index
     * that expresses the page's relative position within the document.
     * In addition, a document may optionally define page labels to identify
     * each page visually on the screen or in print. Page labels and page
     * indices need not coincide: the indices are fixed, running consecutively
     * through the document starting from 1 for the first page, but the
     * labels can be specified in any way that is appropriate for the particular
     * document. For example, if the document begins with 12 pages of front
     * matter numbered in roman numerals and the remainder of the document is
     * numbered in Arabic, the first page would have a page index of 1 and a
     * page label of i, the twelfth page would have index 12 and label xii,
     * and the thirteenth page would have index 13 and label 1.
     *
     * For purposes of page labeling, a document can be divided into labeling
     * ranges, each of which is a series of consecutive pages using the same
     * numbering system. Pages within a range are numbered sequentially in
     * ascending order. A page's label consists of a numeric portion based
     * on its position within its labeling range, optionally preceded by a
     * label prefix denoting the range itself. For example, the pages in an
     * appendix might be labeled with decimal numeric portions prefixed with
     * the string "A-" and the resulting page labels would be "A-1", "A-2",
     */
    class PageLabel {
        constructor(mp_obj?: PDFNet.Obj, m_first_page?: number, m_last_page?: number);
        /**
         * Creates a new PageLabel.
         * @param doc - A document to which the page label is added.
         * @param style - <pre>
         * PDFNet.PageLabel.Style = {
         * 	e_decimal : 0
         * 	e_roman_uppercase : 1
         * 	e_roman_lowercase : 2
         * 	e_alphabetic_uppercase : 3
         * 	e_alphabetic_lowercase : 4
         * 	e_none : 5
         * }
         * </pre>
         * The numbering style for the label.
         * @param [prefix] - The string used to prefix the numeric portion of the
         * page label.
         * @param [start_at] - the value to use when generating the numeric portion of the first
         * label in this range; must be greater than or equal to 1.
         * @returns A promise that resolves to newly created PageLabel object.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, style: number, prefix?: string, start_at?: number): Promise<PDFNet.PageLabel>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PageLabel"
         */
        static createFromObj(l?: PDFNet.Obj, first_page?: number, last_page?: number): Promise<PDFNet.PageLabel>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        compare(d: PDFNet.PageLabel): Promise<boolean>;
        /**
         * @returns A promise that resolves to whether this is a valid (non-null) PageLabel. If the
         * function returns false the underlying SDF/Cos object is null or is
         * not valid and the PageLabel object should be treated as null as well.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "string"
         */
        getLabelTitle(page_num: number): Promise<string>;
        /**
         * Sets the numbering style for the label.
         * @param style - <pre>
         * PDFNet.PageLabel.Style = {
         * 	e_decimal : 0
         * 	e_roman_uppercase : 1
         * 	e_roman_lowercase : 2
         * 	e_alphabetic_uppercase : 3
         * 	e_alphabetic_lowercase : 4
         * 	e_none : 5
         * }
         * </pre>
         * the numbering style for the label.
         *
         * You may use label style to customize the page numbering schemes
         * used throughout a document. There are three numbering formats:
         *  decimal (often used for normal page ranges)
         *  roman (often used for front matter such as a preface)
         *  alphabetic (often used for back matter such as appendices)
         */
        setStyle(style: number): Promise<void>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PageLabel.Style = {
         * 	e_decimal : 0
         * 	e_roman_uppercase : 1
         * 	e_roman_lowercase : 2
         * 	e_alphabetic_uppercase : 3
         * 	e_alphabetic_lowercase : 4
         * 	e_none : 5
         * }
         * </pre>
         * @returns A promise that resolves to page numbering style.
         */
        getStyle(): Promise<number>;
        /**
         * @returns A promise that resolves to the string used to prefix the numeric portion of
         * the page label
         */
        getPrefix(): Promise<string>;
        /**
         * @param prefix - the string used to prefix the numeric portion of
         * the page label.
         */
        setPrefix(prefix: string): Promise<void>;
        /**
         * @returns A promise that resolves to the value to use when generating the numeric portion of
         * the first label in this range; must be greater than or equal to 1.
         */
        getStart(): Promise<number>;
        /**
         * @param start_at - the value to use when generating the numeric
         * portion of the first label in this range; must be greater than
         * or equal to 1.
         */
        setStart(start_at: number): Promise<void>;
        /**
         * @returns A promise that resolves to the first page in the range associated with this label
         * or -1 if the label is not associated with any page.
         */
        getFirstPageNum(): Promise<number>;
        /**
         * @returns A promise that resolves to the last page in the range associated with this label
         * or -1 if the label is not associated with any page.
         */
        getLastPageNum(): Promise<number>;
        /**
         * @returns A promise that resolves to the pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        mp_obj: PDFNet.Obj;
        m_first_page: number;
        m_last_page: number;
    }
    /**
     * PageSet is a container of page numbers ordered following a linear sequence.
     * The page numbers are integers and must be greater than zero. Duplicates are allowed.
     *
     * (NOTE: This is not a mathematical set)
     */
    class PageSet extends PDFNet.Destroyable {
        /**
         * Default constructor. Constructs 'PageSet' with no pages
         * @returns A promise that resolves to an object of type: "PDFNet.PageSet"
         */
        static create(): Promise<PDFNet.PageSet>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PageSet"
         */
        static createSinglePage(one_page: number): Promise<PDFNet.PageSet>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PageSet"
         */
        static createRange(range_start: number, range_end: number): Promise<PDFNet.PageSet>;
        /**
         * @param [filter] - <pre>
         * PDFNet.PageSet.Filter = {
         * 	e_all : 0
         * 	e_even : 1
         * 	e_odd : 2
         * }
         * </pre>
         * @returns A promise that resolves to an object of type: "PDFNet.PageSet"
         */
        static createFilteredRange(range_start: number, range_end: number, filter?: number): Promise<PDFNet.PageSet>;
        /**
         * Add a value to the sequence.
         * @param one_page - The page number being added
         */
        addPage(one_page: number): Promise<void>;
        /**
         * Add a range of values to the sequence. Reverse ordering is legal.
         * @param range_start - The starting value in the range
         * @param range_end - The ending value in the range
         * @param [filter] - <pre>
         * PDFNet.PageSet.Filter = {
         * 	e_all : 0
         * 	e_even : 1
         * 	e_odd : 2
         * }
         * </pre>
         *
         * 		-e_all: Default, includes all pages in the range
         * 		-e_odd: Includes odd numbers in the range (discards even numbers)
         * 		-e_even: Includes even numbers in the range (discards odd numbers)
         */
        addRange(range_start: number, range_end: number, filter?: number): Promise<void>;
    }
    /**
     * Patterns are quite general, and have many uses; for example, they can be used
     * to create various graphical textures, such as weaves, brick walls, sunbursts,
     * and similar geometrical and chromatic effects.
     * Patterns are specified in a special family of color spaces named Pattern, whose
     * 'color values' are PatternColor objects instead of the numeric component values
     * used with other spaces. Therefore PatternColor is to pattern color space what is
     * ColorPt to all other color spaces.
     *
     * A tiling pattern consists of a small graphical figure called a pattern cell.
     * Painting with the pattern replicates the cell at fixed horizontal and vertical
     * intervals to fill an area. The effect is as if the figure were painted on the
     * surface of a clear glass tile, identical copies of which were then laid down
     * in an array covering the area and trimmed to its boundaries. This is called
     * tiling the area.
     *
     * The pattern cell can include graphical elements such as filled areas, text,
     * and sampled images. Its shape need not be rectangular, and the spacing of
     * tiles can differ from the dimensions of the cell itself.
     *
     * The order in which individual tiles (instances of the cell) are painted is
     * unspecified and unpredictable; it is inadvisable for the figures on adjacent
     * tiles to overlap.
     */
    class PatternColor extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PatternColor"
         */
        static create(pattern: PDFNet.Obj): Promise<PDFNet.PatternColor>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.PatternColor.Type = {
         * 	e_uncolored_tiling_pattern : 0
         * 	e_colored_tiling_pattern : 1
         * 	e_shading : 2
         * 	e_null : 3
         * }
         * </pre>
         */
        static getTypeFromObj(pattern: PDFNet.Obj): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PatternColor.Type = {
         * 	e_uncolored_tiling_pattern : 0
         * 	e_colored_tiling_pattern : 1
         * 	e_shading : 2
         * 	e_null : 3
         * }
         * </pre>
         * @returns A promise that resolves to the pattern type
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to pattern matrix, a transformation matrix that maps the pattern's
         * internal coordinate system to the default coordinate system of the pattern's
         * parent content stream (the content stream in which the pattern is defined as
         * a resource). The concatenation of the pattern matrix with that of the parent content
         * stream establishes the pattern coordinate space, within which all graphics objects
         * in the pattern are interpreted.
         */
        getMatrix(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to the shading object defining the shading pattern's gradient fill.
         */
        getShading(): Promise<PDFNet.Shading>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PatternColor.TilingType = {
         * 	e_constant_spacing : 0
         * 	e_no_distortion : 1
         * 	e_constant_spacing_fast_fill : 2
         * }
         * </pre>
         * @returns A promise that resolves to the tiling type identifier that controls adjustments to the spacing
         * of tiles relative to the device pixel grid:
         */
        getTilingType(): Promise<number>;
        /**
         * @returns A promise that resolves to a rectangle in the pattern coordinate system giving the
         * coordinates of the left, bottom, right, and top edges, respectively,
         * of the pattern cell's bounding box. These boundaries are used to clip
         * the pattern cell.
         */
        getBBox(): Promise<PDFNet.Rect>;
        /**
         * @returns A promise that resolves to the desired horizontal spacing between pattern cells, measured
         * in the pattern coordinate system.
         */
        getXStep(): Promise<number>;
        /**
         * @returns A promise that resolves to the desired vertical spacing between pattern cells, measured in
         * the pattern coordinate system.
         */
        getYStep(): Promise<number>;
    }
    /**
     * This header defines classes for the Polygon and PolyLine annotations.
     * Polygon annotations (PDF 1.5) display closed polygons on the page.
     * Such polygons may have any number of vertices connected by straight lines.
     * Polyline annotations (PDF 1.5) are similar to polygons, except that the
     * first and last vertex are not implicitly connected.
     */
    class PolyLineAnnot extends PDFNet.LineAnnot {
        /**
         * creates a PolyLine annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.PolyLineAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.PolyLineAnnot>;
        /**
         * creates a PolyLine annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the PolyLine annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.PolyLineAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.PolyLineAnnot>;
        /**
         * Creates a new PolyLine annotation in the specified document.
         * @param doc - A document to which the PolyLine annotation is added.
         * @param pos - A rectangle specifying the PolyLine annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank PolyLine annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.PolyLineAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the number of vertices in the Vertices array.
         * @returns A promise that resolves to the number of vertices.
         */
        getVertexCount(): Promise<number>;
        /**
         * Returns the vertex(as a Point object) corresponding to the index
         * within the Vertices array.
         * @param idx - The index of the vertex, should be less than the value returned by GetVertexCount().
         * @returns A promise that resolves to a Point object corresponding to the vertex in the specified index position.
         */
        getVertex(idx: number): Promise<PDFNet.Point>;
        /**
         * sets the vertex(in Point object form) corresponding to the index
         * within the Vertices array.
         * @param idx - The index of the vertex.
         * @param pt - A Point object corresponding to the vertex to be added to the array.
         */
        setVertex(idx: number, pt: PDFNet.Point): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         */
        getStartStyle(): Promise<number>;
        /**
         * @param style - <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         */
        setStartStyle(style: number): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         * <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         */
        getEndStyle(): Promise<number>;
        /**
         * @param style - <pre>
         * PDFNet.LineAnnot.EndingStyle = {
         * 	e_Square : 0
         * 	e_Circle : 1
         * 	e_Diamond : 2
         * 	e_OpenArrow : 3
         * 	e_ClosedArrow : 4
         * 	e_Butt : 5
         * 	e_ROpenArrow : 6
         * 	e_RClosedArrow : 7
         * 	e_Slash : 8
         * 	e_None : 9
         * 	e_Unknown : 10
         * }
         * </pre>
         */
        setEndStyle(style: number): Promise<void>;
        /**
         * Returns the intent name as a value of the "IntentName" enumeration type.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.PolyLineAnnot.IntentType = {
         * 	e_PolygonCloud : 0
         * 	e_PolyLineDimension : 1
         * 	e_PolygonDimension : 2
         * 	e_Unknown : 3
         * }
         * </pre>
         * @returns A promise that resolves to the intent type of the annotation.
         */
        getIntentName(): Promise<number>;
        /**
         * Sets the Intent name as a value of the "IntentName" enumeration type.
         * (Optional; PDF 1.6 )
         * @param mode - <pre>
         * PDFNet.PolyLineAnnot.IntentType = {
         * 	e_PolygonCloud : 0
         * 	e_PolyLineDimension : 1
         * 	e_PolygonDimension : 2
         * 	e_Unknown : 3
         * }
         * </pre>
         * The intent name of the annotation.
         */
        setIntentName(mode: number): Promise<void>;
    }
    /**
     * A polygon annotation
     */
    class PolygonAnnot extends PDFNet.PolyLineAnnot {
        /**
         * creates a Polygon annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.PolygonAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.PolygonAnnot>;
        /**
         * creates a Polygon annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Polygon annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.PolygonAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.PolygonAnnot>;
        /**
         * Creates a new Polygon annotation in the specified document.
         * @param doc - A document to which the Polygon annotation is added.
         * @param pos - A rectangle specifying the Polygon annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Polygon annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.PolygonAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
    }
    /**
     * A pop-up annotation (PDF 1.3) displays text in a pop-up window for entry and
     * editing. It shall not appear alone but is associated with a markup annotation,
     * its parent annotation, and shall be used for editing the parent's text.
     * It shall have no appearance stream or associated actions of its own and
     * shall be identified by the Popup entry in the parent's annotation dictionary.
     */
    class PopupAnnot extends PDFNet.Annot {
        /**
         * creates a Popup annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.PopupAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.PopupAnnot>;
        /**
         * creates a Popup annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Popup annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.PopupAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.PopupAnnot>;
        /**
         * Creates a new Popup annotation in the specified document.
         * @param doc - A document to which the Popup annotation is added.
         * @param pos - A rectangle specifying the Popup annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Popup annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.PopupAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the parent annotation of the Popup annotation.
         * @returns A promise that resolves to an annot object which is the parent annotation of the Popup annotation.
         */
        getParent(): Promise<PDFNet.Annot>;
        /**
         * sets the Parent annotation of the Popup annotation.
         * (Optional)
         * @param parent - An annot object which is the parent annotation of the Popup annotation.
         */
        setParent(parent: PDFNet.Annot): Promise<void>;
        /**
         * Returns the initial opening condition of Popup.
         * @returns A promise that resolves to a bool indicating whether the Popup is initially open.
         */
        isOpen(): Promise<boolean>;
        /**
         * sets the initial opening condition of Popup.
         * (Optional)
         * @param isopen - A bool indicating whether the Popup is initially open.
         */
        setOpen(isopen: boolean): Promise<void>;
    }
    /**
     * [Missing documentation]
     */
    class PrinterMode {
    }
    /**
     * [Missing documentation]
     */
    class PushButtonWidget extends PDFNet.WidgetAnnot {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.PushButtonWidget"
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field_name?: string): Promise<PDFNet.PushButtonWidget>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PushButtonWidget"
         */
        static createWithField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.PushButtonWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PushButtonWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.PushButtonWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.PushButtonWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.PushButtonWidget>;
    }
    /**
     * An object representing a Group of Radio Buttons that can be used to create new Radio Buttons.
     * If a group contains multiple buttons they will be connected.
     */
    class RadioButtonGroup {
    }
    /**
     * An object representing a Radio Button used in a PDF Form.
     */
    class RadioButtonWidget extends PDFNet.WidgetAnnot {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.RadioButtonWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.RadioButtonWidget>;
        /**
         * Creates a Radio Button Widget annotation and initialize it using given annotation object.
         *
         * <p>
         * <b> Note: </b>  The constructor does not copy any data, but is instead the logical
         * equivalent of a type cast.
         * @param annot - The annotation to use.
         * @returns A promise that resolves to an object of type: "PDFNet.RadioButtonWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.RadioButtonWidget>;
        /**
         * Determines whether this button is enabled.
         * @returns A promise that resolves to a boolean value indicating whether the Radio Button is enabled.
         */
        isEnabled(): Promise<boolean>;
        /**
         * Enable the current radio button. Note that this may disable other Radio Buttons in the same group.
         */
        enableButton(): Promise<void>;
        /**
         * Gets the group to which the current button is connected.
         * @returns A promise that resolves to the group containing this Radio Button.
         */
        getGroup(): Promise<PDFNet.RadioButtonGroup>;
    }
    /**
     * Rect is a struct used to manipulate PDF rectangle objects
     */
    class Rect {
        constructor(x1?: number, y1?: number, x2?: number, y2?: number, mp_rect?: PDFNet.Obj);
        /**
         * Create a Rect and initialize it using specified parameters.
         * @param x1 - The left-most position of the rect.
         * @param y1 - The bottom-most position of the rect.
         * @param x2 - The right-most position of the rect.
         * @param y2 - The top-most position of the rect.
         * @returns A promise that resolves to a Rect (rectangle) object
         * The rect is not attached to any Cos/SDF object.
         */
        static init(x1: number, y1: number, x2: number, y2: number): Promise<PDFNet.Rect>;
        /**
         * attach the Cos/SDF object to the Rect.
         * @param obj - underlying Cos/SDF object. Must be an SDF::Array with four
         *  SDF::Number elements.
         */
        attach(obj: PDFNet.Obj): Promise<void>;
        /**
         * Saves changes made to the Rect object in the attached (or specified) SDF/Cos rectangle.
         * @param [obj] - an optional parameter indicating a SDF array that should be
         *  updated and attached to this Rect. If parameter rect is NULL or is omitted, update
         *  is performed on previously attached Cos/SDF rectangle.
         * @returns A promise that resolves to true if the attached Cos/SDF rectangle array was successfully updated,
         * false otherwise.
         */
        update(obj?: PDFNet.Obj): Promise<boolean>;
        /**
         * Get the coordinates of the rectangle
         * @returns A promise that resolves to an object of type: "Object"
         */
        get(): Promise<object>;
        /**
         * Set the coordinates of the rectangle
         * @param x1 - The left-most position of the rect.
         * @param y1 - The bottom-most position of the rect.
         * @param x2 - The right-most position of the rect.
         * @param y2 - The top-most position of the rect.
         * The rect is not attached to any Cos/SDF object.
         */
        set(x1: number, y1: number, x2: number, y2: number): Promise<void>;
        /**
         * @returns A promise that resolves to rectangle's width
         */
        width(): Promise<number>;
        /**
         * @returns A promise that resolves to rectangle's height
         */
        height(): Promise<number>;
        /**
         * Determines if the specified point is contained within the rectangular region
         * defined by this Rectangle
         * @param x - horizontal x value of the point to check
         * @param y - vertical y value of the point to check
         * @returns A promise that resolves to true is the point is in the rectangle, false otherwise.
         */
        contains(x: number, y: number): Promise<boolean>;
        /**
         * Makes a Rect equal to the intersection of two existing rectangles.
         * @param rect1 - A Rect object that contains a source rectangle.
         * @param rect2 - A Rect object that contains a source rectangle.
         * @returns A promise that resolves to true if the intersection is not empty; 0 if the intersection is empty.
         */
        intersectRect(rect1: PDFNet.Rect, rect2: PDFNet.Rect): Promise<boolean>;
        /**
         * Arrange the points in the rectangle so that the first point is the lower-left
         * corner and the second point is the upper-right corner of the rectangle.
         */
        normalize(): Promise<void>;
        /**
         * Expands the rectangle by the specified size, in all directions.
         * @param amount - Specifies the amount to increase the rectangle in all directions.
         */
        inflate1(amount: number): Promise<void>;
        /**
         * Expands the rectangle by the specified size, in all directions.
         * @param x - Specifies the amount to increase the rectangle's Left (x1) and Right (x2) properties.
         * @param y - Specifies the amount to increase the rectangle's Top (y1) and Bottom (y2) properties.
         */
        inflate2(x: number, y: number): Promise<void>;
        x1: number;
        y1: number;
        x2: number;
        y2: number;
        mp_rect: PDFNet.Obj;
    }
    /**
     * A redaction annotation (PDF 1.7) identifies content that is intended to
     * be removed from the document. The intent of redaction annotations is to
     * enable the following:
     * a)Content identification. A user applies redact annotations that specify
     * the pieces or regions of content that should be removed. Up until the
     * next step is performed, the user can see, move and redefine these
     * annotations.
     * b)Content removal. The user instructs the viewer application to apply
     * the redact annotations, after which the content in the area specified
     * by the redact annotations is removed. In the removed content's place,
     * some marking appears to indicate the area has been redacted. Also, the
     * redact annotations are removed from the PDF document.
     * Redaction annotations provide a mechanism for the first step in the
     * redaction process (content identification). This allows content to be
     * marked for redaction in a non-destructive way, thus enabling a review
     * process for evaluating potential redactions prior to removing the
     * specified content.
     * Redaction annotations shall provide enough information to be used
     * in the second phase of the redaction process (content removal).
     * This phase is application-specific and requires the conforming reader
     * to remove all content identified by the redaction annotation, as well
     * as the annotation itself.
     * Conforming readers that support redaction annotations shall provide
     * a mechanism for applying content removal, and they shall remove all
     * traces of the specified content. If a portion of an image is contained
     * in a redaction region, that portion of the image data shall be destroyed;
     * clipping or image masks shall not be used to hide that data.
     * Such conforming readers shall also be diligent in their consideration
     * of all content that can exist in a PDF document, including XML Forms
     * Architecture (XFA) content and Extensible Metadata Platform (XMP)
     * content.
     */
    class Redaction {
    }
    /**
     * A redaction annotation (PDF 1.7) identifies content that is intended to
     * be removed from the document. The intent of redaction annotations is to
     * enable the following:
     * a)Content identification. A user applies redact annotations that specify
     * the pieces or regions of content that should be removed. Up until the
     * next step is performed, the user can see, move and redefine these
     * annotations.
     * b)Content removal. The user instructs the viewer application to apply
     * the redact annotations, after which the content in the area specified
     * by the redact annotations is removed. In the removed content's place,
     * some marking appears to indicate the area has been redacted. Also, the
     * redact annotations are removed from the PDF document.
     * Redaction annotations provide a mechanism for the first step in the
     * redaction process (content identification). This allows content to be
     * marked for redaction in a non-destructive way, thus enabling a review
     * process for evaluating potential redactions prior to removing the
     * specified content.
     * Redaction annotations shall provide enough information to be used
     * in the second phase of the redaction process (content removal).
     * This phase is application-specific and requires the conforming reader
     * to remove all content identified by the redaction annotation, as well
     * as the annotation itself.
     * Conforming readers that support redaction annotations shall provide
     * a mechanism for applying content removal, and they shall remove all
     * traces of the specified content. If a portion of an image is contained
     * in a redaction region, that portion of the image data shall be destroyed;
     * clipping or image masks shall not be used to hide that data.
     * Such conforming readers shall also be diligent in their consideration
     * of all content that can exist in a PDF document, including XML Forms
     * Architecture (XFA) content and Extensible Metadata Platform (XMP)
     * content.
     */
    class RedactionAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates a Redaction annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.RedactionAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.RedactionAnnot>;
        /**
         * creates an Redaction annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Redaction annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.RedactionAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.RedactionAnnot>;
        /**
         * Creates a new Redaction annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, in user space coordinates.
         * @returns A promise that resolves to a newly created blank Circle annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.RedactionAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the number of QuadPoints in the QuadPoints array of the Redaction annotation.
         * @returns A promise that resolves to the number of QuadPoints.
         */
        getQuadPointCount(): Promise<number>;
        /**
         * Returns the QuadPoint located at a certain index of the QuadPoint array of the Redaction
         * annotation.
         * @param idx - The index of the QuadPoint, starts at zero and must be less than return value of GetQuadPointCount().
         * @returns A promise that resolves to the QuadPoint located at a certain index of the QuadPoint array of the Redaction annotation.
         */
        getQuadPoint(idx: number): Promise<PDFNet.QuadPoint>;
        /**
         * Set the QuadPoint to be located at a certain index of the QuadPoint array of the Redaction
         * annotation.
         * (Optional; PDF 1.6 )
         * @param idx - The index position where the QuadPoint of interest is to be inserted, starting at 0.
         * @param qp - The QuadPoint to be inserted at that position.
         */
        setQuadPoint(idx: number, qp: PDFNet.QuadPoint): Promise<void>;
        /**
         * sets Overlay appearance of the Redaction annotation.
         * (Optional)
         * @param formxo - An SDF object that represents the overlay appearance of the Redaction annotation.
         */
        setAppFormXO(formxo: PDFNet.Obj): Promise<void>;
        /**
         * Returns Overlay text of the Redaction annotation.
         * @returns A promise that resolves to a string containing the overlay text of the annotation.
         */
        getOverlayText(): Promise<string>;
        /**
         * sets Overlay text of the Redaction annotation.
         * @param title - A string containing the overlay text of the annotation.
         */
        setOverlayText(title: string): Promise<void>;
        /**
         * Returns the option of whether to use repeat for the Redaction annotation.
         * @returns A promise that resolves to a bool indicating whether to repeat for the Redaction annotation.
         */
        getUseRepeat(): Promise<boolean>;
        /**
         * sets the option of whether to use repeat for the Redaction annotation.
         * @param [userepeat] - A bool indicating whether to repeat for the Redaction annotation.
         */
        setUseRepeat(userepeat?: boolean): Promise<void>;
        /**
         * Returns Overlay text appearance of the Redaction annotation.
         * @returns A promise that resolves to a string containing the overlay text appearance of the annotation.
         */
        getOverlayTextAppearance(): Promise<string>;
        /**
         * sets Overlay text appearance of the Redaction annotation.
         * @param app - A string containing the overlay text appearance of the annotation.
         */
        setOverlayTextAppearance(app: string): Promise<void>;
        /**
         * Returns Overlay text quadding(justification) format of the Redaction annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.RedactionAnnot.QuadForm = {
         * 	e_LeftJustified : 0
         * 	e_Centered : 1
         * 	e_RightJustified : 2
         * 	e_None : 3
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "QuadForm" enum type, indicating the
         * overlay text quadding(justification) format of the Redaction annotation's overlay text.
         */
        getQuadForm(): Promise<number>;
        /**
         * sets Overlay text quadding (justification) format of the Redaction annotation.
         * @param [form] - <pre>
         * PDFNet.RedactionAnnot.QuadForm = {
         * 	e_LeftJustified : 0
         * 	e_Centered : 1
         * 	e_RightJustified : 2
         * 	e_None : 3
         * }
         * </pre>
         * A value of the "QuadForm" enum type, indicating the
         * overlay text quadding(justification) format of the Redaction annotation.
         */
        setQuadForm(form?: number): Promise<void>;
        /**
         * Returns Overlay appearance of the Redaction annotation.
         * @returns A promise that resolves to an SDF object that represents the overlay appearance of the Redaction annotation.
         */
        getAppFormXO(): Promise<PDFNet.Obj>;
    }
    /**
     * PDF Redactor is a separately licensable Add-on that offers options to remove
     * (not just covering or obscuring) content within a region of PDF.
     * With printed pages, redaction involves blacking-out or cutting-out areas of
     * the printed page. With electronic documents that use formats such as PDF,
     * redaction typically involves removing sensitive content within documents for
     * safe distribution to courts, patent and government institutions, the media,
     * customers, vendors or any other audience with restricted access to the content.
     *
     * The redaction process in PDFNet consists of two steps:
     *
     *  a) Content identification: A user applies redact annotations that specify the
     * pieces or regions of content that should be removed. The content for redaction
     * can be identified either interactively (e.g. using 'pdftron.PDF.PDFViewCtrl'
     * as shown in PDFView sample) or programmatically (e.g. using 'pdftron.PDF.TextSearch'
     * or 'pdftron.PDF.TextExtractor'). Up until the next step is performed, the user
     * can see, move and redefine these annotations.
     *  b) Content removal: Using 'pdftron.PDF.Redactor.Redact()' the user instructs
     * PDFNet to apply the redact regions, after which the content in the area specified
     * by the redact annotations is removed. The redaction function includes number of
     * options to control the style of the redaction overlay (including color, text,
     * font, border, transparency, etc.).
     *
     * PDFTron Redactor makes sure that if a portion of an image, text, or vector graphics
     * is contained in a redaction region, that portion of the image or path data is
     * destroyed and is not simply hidden with clipping or image masks. PDFNet API can also
     * be used to review and remove metadata and other content that can exist in a PDF
     * document, including XML Forms Architecture (XFA) content and Extensible Metadata
     * Platform (XMP) content.
     */
    class Redactor {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Redaction"
         */
        static redactionCreate(page_num: number, bbox: PDFNet.Rect, negative: boolean, text: string): Promise<PDFNet.Redaction>;
        /**
         * Destructor
         */
        static redactionDestroy(redaction: PDFNet.Redaction): Promise<void>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.Redaction"
         */
        static redactionCopy(other: PDFNet.Redaction): Promise<PDFNet.Redaction>;
        /**
         * Apply the redactions specified in the array red_arr to the PDFDoc doc.
         * @param doc - the document to redact
         * @param red_arr - an array of redaction objects specifying the regions to redact.
         * @param [appearance] - optional parameter used to customize the appearance of the redaction overlay.
        To create an appearance object, create an empty javascript object {} and add entries
        containing values to it. The following entries are handled by redact:
        
        <pre>
        app.redaction_overlay = boolean
        If redaction_overlay is set to true, redactor will draw an overlay
        covering all redacted regions. The rest of properties in the
        appearance object defines visual properties of the overlay.
        If false the overlay region will not be drawn.
        Defaults to true.
        
        app.positive_overlay_color = ColorPt object
        positive_overlay_color defines the overlay background color in RGB color space for positive redactions.
        Defaults to white (PDFNet.ColorPt(1, 1, 1, 0));
        
        app.negative_overlay_color = ColorPt object
        negative_overlay_color defines the overlay background color in RGB color space for negative redactions.
        Defaults to white (PDFNet.ColorPt(1, 1, 1, 0));
        
        app.border = boolean
        border specifies if the overlay will be surrounded by a border.
        Defaults to true.
        
        app.font = Font object
        font specifies the font used to represent the text in the overlay.
        Defaults to true.
        
        app.min_font_size = int
        min_font_size specifies the minimum font size used to represent the text in the overlay
        Defaults to 2.
        
        app.max_font_size = int
        max_font_size specifies the maximum font size used to represent the text in the overlay
        Defaults to 24.
        
        app.text_color = ColorPt object
        text_color specifies the color used to paint the text in the overlay (in RGB).
        Defaults to dark green (PDFNet.ColorPt(0, 0.5, 0, 0));
        
        app.horiz_text_alignment = int
        horiz_text_alignment specifies the horizontal text alignment in the overlay:
            align<0	 -> text will be left aligned.
            align==0 -> text will be center aligned.
            align>0	 -> text will be right aligned.
        Defaults to -1.
        
        app.vert_text_alignment = int
        vert_text_alignment specifies the vertical text alignment in the overlay:
            align<0	 -> text will be top aligned.
            align==0 -> text will be center aligned.
            align>0	 -> text will be bottom aligned.
        Defaults to 1.
        
        app.show_redacted_content_regions = boolean
        show_redacted_content_regions specifies whether an overlay should be drawn in place of
        the redacted content. This option can be used to indicate the areas where the content
        was removed from without revealing the content itself. Defaults to false. Uses
        RedactedContentColor as a fill color.
        Defaults to false.
        
        app.redacted_content_color = ColorPt object
        Specifies the color used to paint the regions where content was removed.
        Only useful when ShowRedactedContentRegions == true.
        Defaults to gray (PDFNet.ColorPt(0.3, 0.3, 0.3, 0)).
        </pre>
         * @param [ext_neg_mode = true] - if true, negative redactions expand beyond the page to remove
        content from other pages in the document. if false, the redaction will be localized
        to the given page.
         * @param [page_coord_sys = true] - if true, redaction coordinates are relative to the lower-left corner of the page,
        otherwise the redaction coordinates are defined in PDF user coordinate system (which may or may not coincide with
        page coordinates).
         */
        static redact(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, red_arr: PDFNet.Redactor[], appearance?: any, ext_neg_mode?: boolean, page_coord_sys?: boolean): Promise<void>;
    }
    /**
     * The class ResultSnapshot.
     * Represents a transition between two document states.
     */
    class ResultSnapshot extends PDFNet.Destroyable {
        /**
         * Retrieves the document state to which this transition has transitioned.
         * @returns A promise that resolves to the current document state.
         */
        currentState(): Promise<PDFNet.DocSnapshot>;
        /**
         * Retrieves the document state from which this transition has transitioned.
         * @returns A promise that resolves to the previous document state.
         */
        previousState(): Promise<PDFNet.DocSnapshot>;
        /**
         * Returns whether this transition is valid or a null transition.
         * @returns A promise that resolves to whether this transition is valid or a null transition.
         */
        isOk(): Promise<boolean>;
        /**
         * Returns whether this transition is a null transition.
         * @returns A promise that resolves to whether this transition is a null transition.
         */
        isNullTransition(): Promise<boolean>;
    }
    /**
     * RoleMap is a dictionary that maps the names of structure types used in the
     * document to their approximate equivalents in the set of standard structure
     * types.
     */
    class RoleMap {
        /**
         * Initialize a RoleMap using an existing low-level Cos/SDF role map dictionary.
         * @param dict - a low-level (SDF/Cos) RoleMap dictionary.
         * @returns A promise that resolves to an object of type: "PDFNet.RoleMap"
         */
        static create(dict: PDFNet.Obj): Promise<PDFNet.RoleMap>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.RoleMap"
         */
        copy(): Promise<PDFNet.RoleMap>;
        /**
         * @returns A promise that resolves to true if this is a valid (non-null) RoleMap, false otherwise.
         * If the function returns false the underlying SDF/Cos object is null or is
         * not valid and the RoleMap should be treated as NULL object.
         */
        isValid(): Promise<boolean>;
        /**
         * @param type - element type
         * @returns A promise that resolves to the direct mapping of given StructElement type, or NULL
         * if such mapping is not defined.
         */
        getDirectMap(type: string): Promise<string>;
        /**
         * Returns the RoleMap dictionary.
         * @returns A promise that resolves to the object to the underlying SDF/Cos dictionary.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * A RubberStamp annotation displays text or graphics intended
     * to look as if they were stamped on the page with a rubber stamp.
     */
    class RubberStampAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates a RubberStamp annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.RubberStampAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.RubberStampAnnot>;
        /**
         * creates a RubberStamp annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the RubberStamp annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.RubberStampAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.RubberStampAnnot>;
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.RubberStampAnnot"
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.RubberStampAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.RubberStampAnnot"
         */
        static createCustom(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, formxo: PDFNet.Obj): Promise<PDFNet.RubberStampAnnot>;
        /**
         * Returns the type of the icon associated with the RubberStamp annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.RubberStampAnnot.Icon = {
         * 	e_Approved : 0
         * 	e_Experimental : 1
         * 	e_NotApproved : 2
         * 	e_AsIs : 3
         * 	e_Expired : 4
         * 	e_NotForPublicRelease : 5
         * 	e_Confidential : 6
         * 	e_Final : 7
         * 	e_Sold : 8
         * 	e_Departmental : 9
         * 	e_ForComment : 10
         * 	e_TopSecret : 11
         * 	e_ForPublicRelease : 12
         * 	e_Draft : 13
         * 	e_Unknown : 14
         * }
         * </pre>
         * @returns A promise that resolves to a value of enum "Icon" that represents
         * the type of icon associated with the annotation.
         * Default value: e_Draft.
         */
        getIcon(): Promise<number>;
        /**
         * sets the type of the icon associated with the RubberStamp annotation.
         * @param [type] - <pre>
         * PDFNet.RubberStampAnnot.Icon = {
         * 	e_Approved : 0
         * 	e_Experimental : 1
         * 	e_NotApproved : 2
         * 	e_AsIs : 3
         * 	e_Expired : 4
         * 	e_NotForPublicRelease : 5
         * 	e_Confidential : 6
         * 	e_Final : 7
         * 	e_Sold : 8
         * 	e_Departmental : 9
         * 	e_ForComment : 10
         * 	e_TopSecret : 11
         * 	e_ForPublicRelease : 12
         * 	e_Draft : 13
         * 	e_Unknown : 14
         * }
         * </pre>
         * A value of enum "Icon" type that represents
         * the type of icon associated with the annotation.
         * Default value: e_Draft.
         */
        setIcon(type?: number): Promise<void>;
        setIconDefault(): Promise<void>;
        /**
         * Returns the name of the icon associated with the RubberStamp annotation.
         * @returns A promise that resolves to a string that is the name of the icon associated with
         * the RubberStamp annotation.
         */
        getIconName(): Promise<string>;
        /**
         * sets the name of the icon associated with the RubberStamp annotation.
         * @param iconstring - the name of the icon associated with
         * the RubberStamp annotation.
         */
        setIconName(iconstring: string): Promise<void>;
    }
    /**
     * SDFDoc is a low-level document representing a graph of SDF::Obj nodes that
     * can be used to build higher-level document models such as PDF (Portable Document
     * Format) or FDF (Forms Document Format).
     *
     * SDFDoc brings together document security, document utility methods, and all SDF
     * objects.
     *
     * A SDF document can be created from scratch using a default constructor:
     *
     * SDFDoc mydoc;
     * Obj trailer = mydoc.GetTrailer();
     *
     * SDF document can be also created from an existing file (e.g. an external PDF document):
     * <pre>
     * SDFDoc mydoc("in.pdf");
     * Obj trailer = mydoc.GetTrailer();
     * </pre>
     * or from a memory buffer or some other Filter/Stream such as a HTTP Filter connection:
     *
     * <pre>
     * MemoryFilter memory = ....
     * SDFDoc mydoc(memory);
     * Obj trailer = mydoc.GetTrailer();
     * </pre>
     *
     * Finally SDF document can be accessed from a high-level PDF document as follows:
     *
     * <pre>
     * PDFDoc doc("in.pdf");
     * SDFDoc& mydoc = doc.GetSDFDoc();
     * Obj trailer = mydoc.GetTrailer();
     * </pre>
     *
     * Note that the examples above used doc.GetTrailer() in order to access document
     * trailer, the starting SDF object (root node) in every document. Following the trailer
     * links, it is possible to visit all low-level objects in a document (e.g. all pages,
     * outlines, fonts, etc).
     *
     * SDFDoc also provides utility methods used to import objects and object collections
     * from one document to another. These methods can be useful for copy operations between
     * documents such as a high-level page merge and document assembly.
     */
    class SDFDoc {
        /**
         * Open a SDF/Cos document from a file.
         * @param filepath - path name to the file.
         * @returns A promise that resolves to an object of type: "PDFNet.SDFDoc"
         */
        static createFromFileUString(filepath: string): Promise<PDFNet.SDFDoc>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.SDFDoc"
         */
        static createFromFileString(filepath: string): Promise<PDFNet.SDFDoc>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.SDFDoc"
         */
        createShallowCopy(): Promise<PDFNet.SDFDoc>;
        releaseFileHandles(): Promise<void>;
        /**
         * @returns A promise that resolves to true if the document is/was originally encrypted false otherwise.
         */
        isEncrypted(): Promise<boolean>;
        /**
         * Initializes document's SecurityHandler using the supplied
         * password. This version of InitSecurityHandler() assumes that
         * document uses Standard security and that a password is specified
         * directly.
         *
         * This function should be called immediately after an encrypted
         * document is opened. The function does not have any side effects on
         * documents that are not encrypted.
         *
         * If the security handler was successfully initialized, it can be later
         * obtained using GetSecurityHandler() method.
         * @param password - Specifies the password used to open the document without
         * any user feedback. If you would like to dynamically obtain the password,
         * you need to derive a custom class from StdSecurityHandler() and use
         * InitSecurityHandler() without any parameters. See EncTest sample
         * for example code.
         * @param password_sz - An optional parameter used to specify the size of
         * the password buffer, in bytes. If the 'password_sz' is 0, or if the parameter
         * is not specified, the function assumes that the string is null terminated.
         * @returns A promise that resolves to true if the given password successfully unlocked the document,
         * false otherwise.
         */
        initStdSecurityHandler(password: string, password_sz: number): Promise<boolean>;
        /**
         * Initializes document's SecurityHandler using the supplied
         * password. This version of InitSecurityHandler() assumes that
         * document uses Standard security and that a password is specified
         * directly.
         *
         * This function should be called immediately after an encrypted
         * document is opened. The function does not have any side effects on
         * documents that are not encrypted.
         *
         * If the security handler was successfully initialized, it can be later
         * obtained using GetSecurityHandler() method.
         * @param password - Specifies the password used to open the document without
         * any user feedback. If you would like to dynamically obtain the password,
         * you need to derive a custom class from StdSecurityHandler() and use
         * InitSecurityHandler() without any parameters. See EncTest sample
         * for example code.
         * @returns A promise that resolves to true if the given password successfully unlocked the document,
         * false otherwise.
         */
        initStdSecurityHandlerUString(password: string): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if document was modified, false otherwise
         */
        isModified(): Promise<boolean>;
        /**
         * Checks whether or not the underlying file has an XRef table that had to be repaired
         * when the file was opened. If the document had an invalid XRef table when opened,
         * PDFNet will have repaired the XRef table for its working representation of the document.
         * @returns A promise that resolves to true if document was found to be corrupted, and was repaired, during
         * opening and has not been saved since.
         */
        hasRepairedXRef(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the document requires full save.
         */
        isFullSaveRequired(): Promise<boolean>;
        /**
         * @returns A promise that resolves to A dictionary representing the root of the document (i.e.
         * a document trailer dictionary)
         */
        getTrailer(): Promise<PDFNet.Obj>;
        /**
         * @param obj_num - object number of the object to retrieve.
         * @returns A promise that resolves to the latest version of the object matching specified object number.
         */
        getObj(obj_num: number): Promise<PDFNet.Obj>;
        /**
         * @param obj - an object to import.
         * @param deep_copy - a boolean indicating whether to perform a deep or shallow copy.
         * In case of shallow copy all indirect references will be set to null.
         *
         * If the object belongs to a document the function will perform deep or shallow
         * clone depending whether deep_copy flag was specified.
         *
         * If the object does not belong to any document ImportObj does not take the
         * object ownership. ImportObj copies the source object and it is users
         * responsibility to delete free objects.
         * @returns A promise that resolves to a pointer to the root indirect object in this document
         */
        importObj(obj: PDFNet.Obj, deep_copy: boolean): Promise<PDFNet.Obj>;
        /**
         * The function performs a deep copy of all objects specified in the 'obj_list'.
         * If objects in the list are directly or indirectly referring to the same object/s,
         * only one copy of the shared object/s will be copied. Therefore, unlike repeated calls
         * to ImportObj, this method will import only one copy of shared objects (objects
         * representing an intersection of graphs specified through 'obj_list' of root pointers.
         * @param obj_list - a list of root objects to import. All directly or indirectly objects will be
         * imported as well.
         * @param exclude_list - a list of objects to not include in the deep copy.
         * @returns A promise that resolves to a list of imported root objects in this document.
         */
        importObjsWithExcludeList(obj_list: PDFNet.Obj[], exclude_list: PDFNet.Obj[]): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to The size of cross reference table
         */
        xRefSize(): Promise<number>;
        /**
         * Removes 'marked' flag from all objects in cross reference table.
         */
        clearMarks(): Promise<void>;
        /**
         * Saves the document to a file.
         *
         * If a full save is requested to the original path, the file is saved to a file
         * system-determined temporary file, the old file is deleted, and the temporary file
         * is renamed to path.
         *
         * A full save with remove unused or linearization option may re-arrange object in
         * the cross reference table. Therefore all pointers and references to document objects
         * and resources should be re acquired in order to continue document editing.
         *
         * In order to use incremental save the specified path must match original path and
         * e_incremental flag bit should be set.
         * @param path - The full path name to which the file is saved.
         * @param flags - A bit field composed of an OR of the SDFDoc::SaveOptions values.
         * @param header - Fileheader header. A new file header is set only during full save.
         */
        save(path: string, flags: number, header: string): Promise<void>;
        /**
         * Saves the document to a memory buffer.
         * @param flags - A bit field composed of an OR of the SDFDoc::SaveOptions values.
         * Note that this method ignores e_incremental flag.
         * @param header - File header. A new file header is set only during full save.
         * @returns A promise that resolves to the buffer containing the serialized version of the document.
         */
        saveMemory(flags: number, header: string): Promise<Uint8Array>;
        /**
         * Saves the document to a stream.
         * @param stream - The output stream where to write data.
         * @param flags - A bit field composed of an OR of the SDFDoc::SaveOptions values.
         * @param header - Fileheader header. A new file header is set only during full save. See also GetHeader()
         */
        saveStream(stream: PDFNet.Filter, flags: number, header: string): Promise<void>;
        /**
         * @returns A promise that resolves to the header string identifying the document version to which the file conforms.
         * For a file conforming to PDF version 1.4, the header should be %PDF-1.4.
         * In general header strings have the following syntax: %AAA-N.n where AAA identifies
         * document specification (such as PDF, FDF, PJTF etc), N is the major version and
         * n is the minor version. The new header string can be set during a full save (see SDFDoc::Save()).
         * For a document that is not serialized the function returns an empty string.
         */
        getHeader(): Promise<string>;
        /**
         * @returns A promise that resolves to currently selected SecurityHandler.
         */
        getSecurityHandler(): Promise<PDFNet.SecurityHandler>;
        setSecurityHandler(handler: PDFNet.SecurityHandler): Promise<void>;
        /**
         * This function removes document security.
         */
        removeSecurity(): Promise<void>;
        /**
         * Sometimes it is desirable to modify all indirect references to a given
         * indirect object. It would be inefficient to manually search for all
         * indirect references to a given indirect object.
         *
         * A more efficient and less error prone method is to replace the indirect
         * object in the cross reference table with a new object. This way the object
         * that is referred to is modified (or replaced) and indirect references do
         * not have to be changed.
         * @param obj_num1 - object number of first object to be swapped.
         * @param obj_num2 - object number of second object to be swapped.
         */
        swap(obj_num1: number, obj_num2: number): Promise<void>;
        /**
         * Call this function to determine whether the document is represented in
         * linearized (fast web view) format.
         * @returns A promise that resolves to true if document is stored in fast web view format, false otherwise.
         */
        isLinearized(): Promise<boolean>;
        /**
         * Returns document's initial linearization dictionary if it is available.
         * @returns A promise that resolves to the linearization dictionary of the original document or NULL
         * if the dictionary is not available.
         */
        getLinearizationDict(): Promise<PDFNet.Obj>;
        /**
         * Returns document's initial linearization hint stream if it is available.
         * @returns A promise that resolves to the linearization hint stream of the original document or NULL
         * if the hint stream is not available.
         */
        getHintStream(): Promise<PDFNet.Obj>;
        /**
         * A document uses a temporary file which is used to cache the contents
         * of any new stream object created in the document (that is the default behavior).
         * @param use_cache_flag - true to enable caching, false to disable caching.
         * Use this function to enable or disable this feature dynamically.
         */
        enableDiskCaching(use_cache_flag: boolean): Promise<void>;
        /**
         * Locks the document to prevent competing threads from accessing the document
         * at the same time. Threads attempting to access the document will wait in
         * suspended state until the thread that owns the lock calls doc.Unlock().
         */
        lock(): Promise<void>;
        /**
         * Removes the lock from the document.
         */
        unlock(): Promise<void>;
        /**
         * Locks the document to prevent competing write threads (using Lock()) from accessing the document
         * at the same time. Other reader threads however, will be allowed to access the document.
         * Threads attempting to obtain write access to the document will wait in
         * suspended state until the thread that owns the lock calls doc.UnlockRead().
         * Note: To avoid deadlocks obtaining a write lock while holding
         * a read lock is not permitted and will throw an exception. If this situation is encountered
         * please either unlock the read lock before the write lock is obtained
         * or acquire a write lock (rather than read lock) in the first place.
         */
        lockRead(): Promise<void>;
        /**
         * Removes the lock from the document.
         */
        unlockRead(): Promise<void>;
        /**
         * Try locking the document.
         * @returns A promise that resolves to true if the document is locked for multi-threaded access, false otherwise.
         */
        tryLock(): Promise<boolean>;
        /**
         * Tries to obtain a read lock the document and returns true if the lock was
         * successfully acquired
         * @returns A promise that resolves to true if the document is locked for multi-threaded access, false otherwise.
         */
        tryLockRead(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the filename of the document if the document is loaded from disk,
         * or empty string if the document is not yet saved or is loaded from a memory
         * buffer.
         */
        getFileName(): Promise<string>;
        /**
         * Used to create SDF/Cos indirect object.
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects they can be shared).
         * @param name - indirect const char* object to create.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectName(name: string): Promise<PDFNet.Obj>;
        /**
         * Used to create SDF/Cos indirect object.
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects they can be shared).
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectArray(): Promise<PDFNet.Obj>;
        /**
         * Used to create SDF/Cos indirect object.
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects they can be shared).
         * @param value - indirect boolean to create.
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectBool(value: boolean): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectDict(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectNull(): Promise<PDFNet.Obj>;
        /**
         * Used to create SDF/Cos indirect object.
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects they can be shared).
         * @param value - indirect boolean to create.
         * @returns A promise that resolves to the created indirect object.
         */
        createIndirectNumber(value: number): Promise<PDFNet.Obj>;
        /**
         * Used to create SDF/Cos indirect object.
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects they can be shared).
         * @param buf_value - indirect boolean to create.
         * @returns A promise that resolves to the created indirect object.
         */
        createIndirectString(buf_value: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<PDFNet.Obj>;
        /**
         * Used to create SDF/Cos indirect object.
         *
         * Unlike direct objects, indirect objects can be referenced by more than one
         * object (i.e. indirect objects they can be shared).
         * @param str - indirect boolean to create.
         * @returns A promise that resolves to the created indirect object.
         */
        createIndirectStringFromUString(str: string): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectStreamFromFilter(data: PDFNet.FilterReader, filter_chain?: PDFNet.Filter): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Obj"
         */
        createIndirectStream(data: string, data_size: number, filter_chain?: PDFNet.Filter): Promise<PDFNet.Obj>;
        /**
         * Initializes document's SecurityHandler. This version of InitSecurityHandler()
        works with Standard and Custom PDF security and can be used in situations where
        the password is obtained dynamically via user feedback. See EncTest sample for
        example code.
        
        This function should be called immediately after an encrypted
        document is opened. The function does not have any side effects on
        documents that are not encrypted.
        
        If the security handler was successfully initialized it can be later obtained
        using GetSecurityHandler() method.
         * @returns A promise that resolves to true if the SecurityHandler was successfully initialized (this
        may include authentication data collection, verification etc.),
        false otherwise.
         */
        initSecurityHandler(): Promise<boolean>;
        applyCustomQuery(query: any): Promise<any>;
    }
    /**
     * SElement represents PDF structural elements, which are nodes in a tree
     * structure, defining a PDF document's logical structure.
     *
     * Unlike the StructTree, SElement can have two different kinds
     * of children: another SElement or a ContentItem (which can be marked
     * content (MC), or a PDF object reference (OBJR)).
     */
    class SElement {
        constructor(obj?: PDFNet.Obj, k?: PDFNet.Obj);
        /**
         * Initialize a SElement using an existing low-level Cos/SDF object.
         * @param [dict] - a low-level (SDF/Cos) dictionary representing the structural element.
         * @returns A promise that resolves to an object of type: "PDFNet.SElement"
         */
        static create(dict?: PDFNet.Obj): Promise<PDFNet.SElement>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.SElement"
         */
        static createFromPDFDoc(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, struct_type: string): Promise<PDFNet.SElement>;
        insert(kid: PDFNet.SElement, insert_before: number): Promise<void>;
        /**
         * @param doc - The document in which the new ContentItem will be created in.
         * @param page - The page object to insert the ContentItem in.
         * @param [insert_before] - The position after which the kid is inserted. If
         * element currently has no kids, insert_before is ignored.
         * @returns A promise that resolves to an object of type: "number"
         */
        createContentItem(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, page: PDFNet.Page, insert_before?: number): Promise<number>;
        /**
         * @returns A promise that resolves to true if this is a valid structure element object, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the element's structural element type. The type corresponds to
         * the 'S' (i.e. subtype) key in the structure element dictionary.
         *
         * The type identifies the nature of the structure element and its role
         * within the document (such as a chapter, paragraph, or footnote).
         */
        getType(): Promise<string>;
        /**
         * @returns A promise that resolves to the number of direct kids.
         */
        getNumKids(): Promise<number>;
        /**
         * @param index - The index of the kid type to obtain.
         *
         * To retrieve a content item at a given array index use GetAsContentItem(index),
         * otherwise use GetAsStructElem(index)
         * @returns A promise that resolves to true if the kid at a given array index is a content item,
         * false otherwise.
         */
        isContentItem(index: number): Promise<boolean>;
        /**
         * @param index - The index of the kid to obtain.
         * @returns A promise that resolves to the kid at a given array index assuming that the kid is a ContentItem.
         */
        getAsContentItem(index: number): Promise<PDFNet.ContentItem>;
        /**
         * @param index - The index of the kid to obtain.
         * @returns A promise that resolves to the kid at a given array index assuming that the kid is a SElement.
         */
        getAsStructElem(index: number): Promise<PDFNet.SElement>;
        /**
         * @returns A promise that resolves to the immediate ancestor element of the specified element in
         * the structure tree.
         */
        getParent(): Promise<PDFNet.SElement>;
        /**
         * @returns A promise that resolves to the structure tree root of the document that directly or
         * indirectly contains this element.
         */
        getStructTreeRoot(): Promise<PDFNet.STree>;
        /**
         * @returns A promise that resolves to if this SElement has title.
         *
         * The title of the structure element, a text string representing it in
         * human-readable form.
         */
        hasTitle(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the title of this structure element.
         */
        getTitle(): Promise<string>;
        /**
         * @returns A promise that resolves to the ID of an element, or NULL if the ID is not defined.
         */
        getID(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to if this structure element defines ActualText.
         *
         * ActualText is an exact replacement for the structure element and its children.
         * This replacement text is useful when extracting the document's contents in
         * support of accessibility to users with disabilities or for other purposes.
         */
        hasActualText(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the ActualText associated with this element.
         */
        getActualText(): Promise<string>;
        /**
         * @returns A promise that resolves to if this structure element defines Alt text.
         *
         * Alt text is an alternate description of the structure element and
         * its children in human-readable form, which is useful when extracting
         * the document's contents in support of accessibility.
         */
        hasAlt(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the alternate text associated with this element.
         */
        getAlt(): Promise<string>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        obj: PDFNet.Obj;
        k: PDFNet.Obj;
    }
    /**
     * STree is the root of the structure tree, which is a central repository
     * for information related to a PDF document's logical structure. There is at most
     * one structure tree in each document.
     */
    class STree {
        /**
         * Initialize a STree using an existing low-level Cos/SDF object.
         * @param struct_dict - a low-level (SDF/Cos) dictionary representing the .
         * @returns A promise that resolves to an object of type: "PDFNet.STree"
         */
        static create(struct_dict: PDFNet.Obj): Promise<PDFNet.STree>;
        /**
         * Create a structure tree if it is missing, else return the existing
         * structure tree
         * @param doc - the document in which to create or get the structure tree from
         * @returns A promise that resolves to structure tree of the document
         */
        static createFromPDFDoc(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<PDFNet.STree>;
        /**
         * Inserts the specified kid element after the given position as a kid of
         * the specified structure tree root.
         * @param kid - The kid element to insert.
         * @param insert_before - The position after which the kid is inserted. If
         * element currently has no kids, insert_before is ignored.
         */
        insert(kid: PDFNet.SElement, insert_before: number): Promise<void>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.STree"
         */
        copy(): Promise<PDFNet.STree>;
        /**
         * @returns A promise that resolves to true if this is a valid STree object, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the number of kids of the structure tree root.
         */
        getNumKids(): Promise<number>;
        /**
         * @param index - The index of the kid to obtain.
         * @returns A promise that resolves to the kid at an array index in the structure tree root.
         */
        getKid(index: number): Promise<PDFNet.SElement>;
        /**
         * @returns A promise that resolves to the RoleMap object from the structure tree root.
         */
        getRoleMap(): Promise<PDFNet.RoleMap>;
        /**
         * @returns A promise that resolves to the ClassMap object from the structure tree root.
         */
        getClassMap(): Promise<PDFNet.ClassMap>;
        /**
         * @returns A promise that resolves to pointer to the underlying SDF/Cos object.
         */
        getSDFObj(): Promise<PDFNet.Obj>;
    }
    /**
     * A screen annotation (PDF 1.5) specifies a region of a page upon which
     * media clips may be played. It also serves as an object from which
     * actions can be triggered.
     */
    class ScreenAnnot extends PDFNet.Annot {
        /**
         * creates a Screen annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.ScreenAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.ScreenAnnot>;
        /**
         * creates a Screen annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Screen annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.ScreenAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.ScreenAnnot>;
        /**
         * Returns the title of the annotation.
         * @returns A promise that resolves to a string representing the title of the annotation.
         */
        getTitle(): Promise<string>;
        /**
         * sets the title of the Annotation.
         * (Optional)
         * @param title - A string representing the title of the annotation.
         */
        setTitle(title: string): Promise<void>;
        /**
         * Creates a new Screen annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Screen annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.ScreenAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the action of the Screen annotation
         * @returns A promise that resolves to an action object representing the action of the annotation.
         */
        getAction(): Promise<PDFNet.Action>;
        /**
         * sets the action of the Screen annotation
         * (Optional; PDF 1.1 )
         * @param action - An action object representing the action of the annotation.
         */
        setAction(action: PDFNet.Action): Promise<void>;
        /**
         * Returns the border color of the annotation.
         * @returns A promise that resolves to a color object that denotes the color of the Screen border.
         */
        getBorderColor(): Promise<PDFNet.ColorPt>;
        /**
         * sets the border color of the annotation.
         * (Optional)
         * @param col - A color object that denotes the color of the screen border.
         * @param numcomp - An integer which value indicates the color space used for the parameter c.
         */
        setBorderColor(col: PDFNet.ColorPt, numcomp: number): Promise<void>;
        /**
         * Returns the number indicating border color space of the annotation.
         * @returns A promise that resolves to an integer indicating a color space value from the ColorSpace::Type enum.
         * That is, 1 corresponding to "e_device_gray",
         * 3 corresponding to "e_device_rgb", and 4 corresponding to "e_device_cmyk".
         * 0 means this annotation had no color assigned.
         */
        getBorderColorCompNum(): Promise<number>;
        /**
         * Returns the number indicating background color space of the annotation.
         * @returns A promise that resolves to an integer indicating a color space value from the ColorSpace::Type enum.
         * That is, 1 corresponding to "e_device_gray",
         * 3 corresponding to "e_device_rgb", and 4 corresponding to "e_device_cmyk" if color space is applicable,
         * 0 means no background color was assigned.
         */
        getBackgroundColorCompNum(): Promise<number>;
        /**
         * Returns the background color of the annotation.
         * @returns A promise that resolves to a color object that denotes the color of the Screen background.
         */
        getBackgroundColor(): Promise<PDFNet.ColorPt>;
        /**
         * sets the background color of the annotation.
         * (Optional)
         * @param col - A color point that denotes the color of the screen background.
         * @param numcomp - An integer which value indicates the color space used for the parameter c.
         */
        setBackgroundColor(col: PDFNet.ColorPt, numcomp: number): Promise<void>;
        /**
         * Returns static caption text of the annotation.
         * @returns A promise that resolves to a string containing the static caption text of the annotation.
         */
        getStaticCaptionText(): Promise<string>;
        /**
         * sets static caption text of the annotation.
         * (Optional; button fields only)
         * @param contents - A string containing the static caption text of the annotation.
         */
        setStaticCaptionText(contents: string): Promise<void>;
        /**
         * Returns the rollover caption text of the annotation.
         * @returns A promise that resolves to a string containing the rollover caption text of the annotation.
         */
        getRolloverCaptionText(): Promise<string>;
        /**
         * sets the roll over caption text of the annotation.
         * (Optional; button fields only)
         * @param contents - A string containing the roll over caption text of the annotation.
         */
        setRolloverCaptionText(contents: string): Promise<void>;
        /**
         * Returns the button down caption text of the annotation.
         * @returns A promise that resolves to a string containing the button down text of the annotation.
         */
        getMouseDownCaptionText(): Promise<string>;
        /**
         * sets the button down caption text of the annotation.
         * (Optional; button fields only)
         * @param contents - A string containing the button down text of the annotation.
         */
        setMouseDownCaptionText(contents: string): Promise<void>;
        /**
         * Returns the static icon associated with the annotation.
         * @returns A promise that resolves to an SDF object that represents the static icon
         * associated with the annotation.
         */
        getStaticIcon(): Promise<PDFNet.Obj>;
        /**
         * sets the static icon associated with the annotation.
         * (Optional; button fields only)
         * @param icon - An SDF object that represents the static icon
         * associated with the annotation.
         */
        setStaticIcon(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the rollover icon associated with the annotation.
         * @returns A promise that resolves to an SDF object that represents the rollover icon
         * associated with the annotation.
         */
        getRolloverIcon(): Promise<PDFNet.Obj>;
        /**
         * sets the rollover icon associated with the annotation.
         * (Optional; button fields only)
         * @param icon - An SDF object that represents the rollover icon
         * associated with the annotation.
         */
        setRolloverIcon(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the Mouse Down icon associated with the annotation.
         * @returns A promise that resolves to an SDF object that represents the Mouse Down icon
         * associated with the annotation.
         */
        getMouseDownIcon(): Promise<PDFNet.Obj>;
        /**
         * sets the Mouse Down icon associated with the annotation.
         * (Optional; button fields only)
         * @param icon - An SDF object that represents the Mouse Down icon
         * associated with the annotation.
         */
        setMouseDownIcon(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the Scale Type of the annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ScreenAnnot.ScaleType = {
         * 	e_Anamorphic : 0
         * 	e_Proportional : 1
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "ScaleType" enum which represents the Scale Type of the annotation.
         * Default value: P.
         */
        getScaleType(): Promise<number>;
        /**
         * sets the Scale Type of the annotation.
         * (Optional)
         * @param st - <pre>
         * PDFNet.ScreenAnnot.ScaleType = {
         * 	e_Anamorphic : 0
         * 	e_Proportional : 1
         * }
         * </pre>
         * An entry of the "ScaleType" enum which represents the Scale Type of the annotation.
         * Default value: P.
         */
        setScaleType(st: number): Promise<void>;
        /**
         * Returns the Icon and caption relationship of the  annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ScreenAnnot.IconCaptionRelation = {
         * 	e_NoIcon : 0
         * 	e_NoCaption : 1
         * 	e_CBelowI : 2
         * 	e_CAboveI : 3
         * 	e_CRightILeft : 4
         * 	e_CLeftIRight : 5
         * 	e_COverlayI : 6
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "IconCaptionRelation" enum type.
         * Default value: e_NoIcon.
         */
        getIconCaptionRelation(): Promise<number>;
        /**
         * sets the Icon and caption relationship of the  annotation.
         * (Optional; pushbutton fields only)
         * @param icr - <pre>
         * PDFNet.ScreenAnnot.IconCaptionRelation = {
         * 	e_NoIcon : 0
         * 	e_NoCaption : 1
         * 	e_CBelowI : 2
         * 	e_CAboveI : 3
         * 	e_CRightILeft : 4
         * 	e_CLeftIRight : 5
         * 	e_COverlayI : 6
         * }
         * </pre>
         * A value of the "IconCaptionRelation" enum type.
         * Default value: e_NoIcon.
         */
        setIconCaptionRelation(icr: number): Promise<void>;
        /**
         * Returns the condition under which the icon should be scaled.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ScreenAnnot.ScaleCondition = {
         * 	e_Always : 0
         * 	e_WhenBigger : 1
         * 	e_WhenSmaller : 2
         * 	e_Never : 3
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "ScaleCondition" enum type.
         * Default value: e_Always.
         */
        getScaleCondition(): Promise<number>;
        /**
         * sets the condition under which the icon should be scaled.
         * (Optional)
         * @param sc - <pre>
         * PDFNet.ScreenAnnot.ScaleCondition = {
         * 	e_Always : 0
         * 	e_WhenBigger : 1
         * 	e_WhenSmaller : 2
         * 	e_Never : 3
         * }
         * </pre>
         * A value of the "ScaleCondition" enum type.
         * Default value: e_Always.
         */
        setScaleCondition(sc: number): Promise<void>;
        /**
         * Returns the "fit full" flag.
         * @returns A promise that resolves to a boolean value indicating the "fit full" flag value.
         */
        getFitFull(): Promise<boolean>;
        /**
         * sets the "fit full" flag.
         * (Optional)
         * @param ff - A boolean value indicating the "fit full" flag value.
         */
        setFitFull(ff: boolean): Promise<void>;
        /**
         * Returns the horizontal leftover space of the icon within the annotation.
         * @returns A promise that resolves to a number indicating the horizontal
         * leftover space of the icon within the annotation.
         */
        getHIconLeftOver(): Promise<number>;
        /**
         * sets the horizontal leftover space of the icon within the annotation.
         * (Optional)
         * @param hl - A number indicating the horizontal
         * leftover space of the icon within the annotation.
         */
        setHIconLeftOver(hl: number): Promise<void>;
        /**
         * Returns the vertical leftover space of the icon within the annotation.
         * @returns A promise that resolves to a number indicating the vertical
         * leftover space of the icon within the annotation.
         */
        getVIconLeftOver(): Promise<number>;
        /**
         * sets the vertical leftover space of the icon within the annotation.
         * (Optional)
         * @param vl - A number indicating the vertical
         * leftover space of the icon within the annotation.
         */
        setVIconLeftOver(vl: number): Promise<void>;
    }
    /**
     * Standard Security Handler is a standard password-based security handler.
     */
    class SecurityHandler extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to a new, cloned instance of SecurityHandler.
         */
        clone(): Promise<PDFNet.SecurityHandler>;
        /**
         * @param p - <pre>
         * PDFNet.SecurityHandler.Permission = {
         * 	e_owner : 1
         * 	e_doc_open : 2
         * 	e_doc_modify : 3
         * 	e_print : 4
         * 	e_print_high : 5
         * 	e_extract_content : 6
         * 	e_mod_annot : 7
         * 	e_fill_forms : 8
         * 	e_access_support : 9
         * 	e_assemble_doc : 10
         * }
         * </pre>
         * A Permission to be granted.
         * @returns A promise that resolves to true if the SecurityHandler permits the specified action (Permission p)
         * on the document, or false if the permission was not granted.
         */
        getPermission(p: number): Promise<boolean>;
        /**
         * @returns A promise that resolves to the length of the encryption key in bytes.
         */
        getKeyLength(): Promise<number>;
        /**
         * @returns A promise that resolves to the encryption algorithm identifier. A code specifying the algorithm
         * to be used in encrypting and decrypting the document. Returned number corresponds
         * to V entry in encryption dictionary. Currently allowed values are from 0-4.
         * See PDF Reference Manual for more details.
         */
        getEncryptionAlgorithmID(): Promise<number>;
        /**
         * @returns A promise that resolves to the name of the security handler as it appears in the serialized file
         * as the value of /Filter key in /Encrypt dictionary.
         */
        getHandlerDocName(): Promise<string>;
        /**
         * @returns A promise that resolves to true if the SecurityHandler was modified (by calling SetModified())
         * or false otherwise.
         *
         * If the user changes SecurityHandler's settings (e.g. by changing a password),
         * IsModified() should return true.
         */
        isModified(): Promise<boolean>;
        /**
         * The method allows derived classes to set SecurityHandler is modified flag.
         * This method should be called whenever there are changes (e.g. a password change)
         * to the SecurityHandler
         * @param [is_modified] - Value to set the SecurityHandler's is modified flag to
         */
        setModified(is_modified?: boolean): Promise<void>;
        /**
         * create a Standard Security Handler.
         * @param crypt_type - <pre>
         * PDFNet.SecurityHandler.AlgorithmType = {
         * 	e_RC4_40 : 1
         * 	e_RC4_128 : 2
         * 	e_AES : 3
         * 	e_AES_256 : 4
         * }
         * </pre>
         * The encryption algorithm identifier.
         * @returns A promise that resolves to an object of type: "PDFNet.SecurityHandler"
         */
        static create(crypt_type: number): Promise<PDFNet.SecurityHandler>;
        /**
         * create a Standard Security Handler.
         * @param name - The name of the Standard Security Handler.
         * @param key_len - The bit length of the encryption key (40 or 128 bit).
         * @param enc_code - The encryption algorithm identifier. The number corresponds
         * to the V entry in encryption dictionary. Currently allowed values are (see
         * Table 3.18 in PDF Reference Manual v1.6 for more details):
         *   1 : Encryption using 40-bit RC4 algorithm.
         *   2 : Encryption using 128-bit RC4 algorithm. Available in PDF 1.4 and above.
         *   3 : This algorithm was deprecated by PDF standard and is not supported.
         *   4 : Encryption using Crypt filters and 128-bit AES (Advanced Encryption
         *         Standard) algorithm. Available in PDF 1.6 and above.
         * @returns A promise that resolves to an object of type: "PDFNet.SecurityHandler"
         */
        static createFromEncCode(name: string, key_len: number, enc_code: number): Promise<PDFNet.SecurityHandler>;
        /**
         * Create a default Security Handler.
         * @returns A promise that resolves to a SecurityHandler object.
         */
        static createDefault(): Promise<PDFNet.SecurityHandler>;
        /**
         * Set the permission setting of the StdSecurityHandler.
         * @param perm - <pre>
         * PDFNet.SecurityHandler.Permission = {
         * 	e_owner : 1
         * 	e_doc_open : 2
         * 	e_doc_modify : 3
         * 	e_print : 4
         * 	e_print_high : 5
         * 	e_extract_content : 6
         * 	e_mod_annot : 7
         * 	e_fill_forms : 8
         * 	e_access_support : 9
         * 	e_assemble_doc : 10
         * }
         * </pre>
         * indicates a permission to set or clear. It can be any of the
         * following values:
         *
         * 	e_print				// print the document.
         * 	e_doc_modify		// edit the document more than adding or modifying text notes.
         * 	e_extract_content	// enable content extraction
         * 	e_mod_annot			// allow modifications to annotations
         * 	e_fill_forms		// allow changes to fill in forms
         * 	e_access_support	// content access for the visually impaired.
         * 	e_assemble_doc		// allow document assembly
         * 	e_print_high		// high resolution print.
         * @param value - true if the permission/s should be granted, false otherwise.
         */
        setPermission(perm: number, value: boolean): Promise<void>;
        /**
         * Change the revision number and the encryption algorithm of the
         * standard security handler.
         * @param rev_num - the new revision number of the standard security
         * algorithm. Currently allowed values for the revision number are
         * (see Table 3.18 in PDF Reference Manual v1.6 for more details):
         *   2 : Encryption using 40-bit RC4 algorithm.
         *   3 : Encryption using 128-bit RC4 algorithm. Available in PDF 1.4 and above.
         *   4 : Encryption using Crypt filters and 128-bit AES (Advanced Encryption
         *         Standard) algorithm. Available in PDF 1.6 and above.
         */
        changeRevisionNumber(rev_num: number): Promise<void>;
        /**
         * Indicates whether the document-level metadata stream is to
         * be encrypted.
         * @param encrypt_metadata - true if metadata stream should be
         * encrypted, false otherwise.
         */
        setEncryptMetadata(encrypt_metadata: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to the revision number of the standard security algorithm.
         */
        getRevisionNumber(): Promise<number>;
        /**
         * @returns A promise that resolves to true if the SecurityHandler requires a user password.
         */
        isUserPasswordRequired(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if the SecurityHandler requires a master (owner) password.
         */
        isMasterPasswordRequired(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true is this security handler uses 128 bit AES (Advanced Encryption Standard)
         * algorithm to encrypt strings or streams.
         */
        isAES(): Promise<boolean>;
        /**
         * The following function can be used to verify whether a given stream is
         * encrypted using AES.
         * @param stream - A pointer to an SDF::Stream object
         * @returns A promise that resolves to true if the given stream is encrypted using AES encryption.
         */
        isAESObj(stream: PDFNet.Obj): Promise<boolean>;
        /**
         * @returns A promise that resolves to true is this security handler uses RC4 algorithm to encrypt strings or streams.
         */
        isRC4(): Promise<boolean>;
        /**
         * Sets the new user password.
         * @param password - The new user password.
         */
        changeUserPasswordUString(password: string): Promise<void>;
        changeUserPasswordBuffer(password_buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<void>;
        /**
         * Sets the new master/owner password.
         * @param password - The new master/owner password.
         */
        changeMasterPasswordUString(password: string): Promise<void>;
        changeMasterPasswordBuffer(password_buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<void>;
        initPasswordUString(password: string): Promise<void>;
        initPasswordBuffer(password_buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<void>;
        /**
         * The method is called when a user tries to set security for an encrypted
         * document and when a user tries to open a file. It must decide, based on
         * the contents of the authorization data structure, whether or not the
         * user is permitted to open the file, and what permissions the user has
         * for this file.
         * @param p - <pre>
         * PDFNet.SecurityHandler.Permission = {
         * 	e_owner : 1
         * 	e_doc_open : 2
         * 	e_doc_modify : 3
         * 	e_print : 4
         * 	e_print_high : 5
         * 	e_extract_content : 6
         * 	e_mod_annot : 7
         * 	e_fill_forms : 8
         * 	e_access_support : 9
         * 	e_assemble_doc : 10
         * }
         * </pre>
         * permission to authorize
         * @returns A promise that resolves to an object of type: "boolean"
         */
        authorize(p: number): Promise<boolean>;
        /**
         * A callback method indicating repeated failed authorization.
         * Override this callback in order to provide a UI feedback for failed
         * authorization. Default implementation returns immediately.
         */
        authorizeFailed(): Promise<void>;
        /**
         * This method is invoked in case Authorize() failed. The callback must
         * determine the user's authorization properties for the document by
         * obtaining authorization data (e.g. a password through a GUI dialog).
         *
         * The authorization data is subsequently used by the security handler's Authorize()
         * to determine whether or not the user is authorized to open the file.
         * @param req_opr - <pre>
         * PDFNet.SecurityHandler.Permission = {
         * 	e_owner : 1
         * 	e_doc_open : 2
         * 	e_doc_modify : 3
         * 	e_print : 4
         * 	e_print_high : 5
         * 	e_extract_content : 6
         * 	e_mod_annot : 7
         * 	e_fill_forms : 8
         * 	e_access_support : 9
         * 	e_assemble_doc : 10
         * }
         * </pre>
         * the permission for which authorization data is requested.
         * @returns A promise that resolves to false if the operation was canceled, true otherwise.
         */
        getAuthorizationData(req_opr: number): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        editSecurityData(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<boolean>;
        /**
         * Called when an encrypted document is saved. Fills the document's Encryption
         * dictionary with whatever information the security handler wants to store in
         * the document.
         *
         * The sequence of events during creation of the encrypt_dict is as follows:
         *  encrypt_dict is created (if it does not exist)
         *  Filter attribute is added to the dictionary
         *  call this method to allow the security handler to add its own attributes
         *  call the GetCryptKey to get the algorithm version, key, and key length
         *  checks if the V attribute has been added to the dictionary and, if not,
         *    then sets V to the algorithm version
         *  set the Length attribute if V is 2 or greater
         *  add the encrypt_dict to the document
         * @param doc - The document to save.
         * @returns A promise that resolves to encrypt_dict
         */
        fillEncryptDict(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc): Promise<PDFNet.Obj>;
        /**
         * Set the new user password to a binary string
         * @param password - the new user password
         */
        changeUserPasswordNonAscii(password: string): Promise<void>;
        /**
         * Set the new master password to a binary string
         * @param password - the new user password
         */
        changeMasterPasswordNonAscii(password: string): Promise<void>;
        /**
         * the method can be called in GetAuthorizationData() callback to
        specify user supplied ASCII password.
         * @param password - user supplied ASCII password
         */
        initPassword(password: string): Promise<void>;
        /**
         * the method can be called in GetAuthorizationData() callback to
        specify user supplied non-ASCII password.
         * @param password - user supplied non-ASCII password
         */
        initPasswordNonAscii(password: string): Promise<void>;
    }
    /**
     * Shading is a class that represents a flat interface around all PDF shading types:
     *
     *  - In Function-based (type 1) shadings, the color at every point in
     *    the domain is defined by a specified mathematical function. The function need
     *    not be smooth or continuous. This is the most general of the available shading
     *    types, and is useful for shadings that cannot be adequately described with any
     *    of the other types.
     *
     *  - Axial shadings (type 2) define a color blend along a line between two points,
     *    optionally extended beyond the boundary points by continuing the boundary
     *    colors.
     *
     *  - Radial shadings (type 3) define a color blend that varies between two circles.
     *    Shadings of this type are commonly used to depict three-dimensional spheres
     *    and cones.
     *
     *  - Free-form Gouraud-shaded triangle mesh shadings (type 4) and lattice Gouraud
     *    shadings (type 5) are commonly used to represent complex colored and shaded
     *    three-dimensional shapes. The area to be shaded is defined by a path composed entirely
     *    of triangles. The color at each vertex of the triangles is specified, and a technique
     *    known as Gouraud interpolation is used to color the interiors. The interpolation
     *    functions defining the shading may be linear or nonlinear.
     *
     *  - Coons patch mesh shadings (type 6) are constructed from one or more color
     *    patches, each bounded by four cubic Bezier curves.
     *
     *    A Coons patch generally has two independent aspects:
     *    - Colors are specified for each corner of the unit square, and bilinear
     *      interpolation is used to fill in colors over the entire unit square<BR>
     *    - Coordinates are mapped from the unit square into a four-sided patch whose
     *      sides are not necessarily linear. The mapping is continuous: the corners
     *      of the unit square map to corners of the patch and the sides of the unit
     *      square map to sides of the patch.
     *
     *  - Tensor-product patch mesh shadings (type 7) are identical to type 6
     *    (Coons mesh), except that they are based on a bicubic tensor-product
     *    patch defined by 16 control points, instead of the 12 control points
     *    that define a Coons patch. The shading Patterns dictionaries representing
     *    the two patch types differ only in the value of the Type entry and
     *    in the number of control points specified for each patch in the data stream.
     *    Although the Coons patch is more concise and easier to use, the tensor-
     *    product patch affords greater control over color mapping.
     */
    class Shading extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Shading"
         */
        static create(shading_dict?: PDFNet.Obj): Promise<PDFNet.Shading>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Shading.Type = {
         * 	e_function_shading : 0
         * 	e_axial_shading : 1
         * 	e_radial_shading : 2
         * 	e_free_gouraud_shading : 3
         * 	e_lattice_gouraud_shading : 4
         * 	e_coons_shading : 5
         * 	e_tensor_shading : 6
         * 	e_null : 7
         * }
         * </pre>
         * @param shading_dict - SDF/Cos shading dictionary to get the Type from
         * @returns A promise that resolves to the Type of a given SDF/Cos shading dictionary, or e_null for if
         * SDF object is not a valid shading object
         */
        static getTypeFromObj(shading_dict: PDFNet.Obj): Promise<number>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.Shading.Type = {
         * 	e_function_shading : 0
         * 	e_axial_shading : 1
         * 	e_radial_shading : 2
         * 	e_free_gouraud_shading : 3
         * 	e_lattice_gouraud_shading : 4
         * 	e_coons_shading : 5
         * 	e_tensor_shading : 6
         * 	e_null : 7
         * }
         * </pre>
         * @returns A promise that resolves to the shading type
         */
        getType(): Promise<number>;
        /**
         * @returns A promise that resolves to the underlying SDF/Cos object
         */
        getSDFObj(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the color space in which color values are expressed.
         * This may be any device, CIE-based, or special color space
         * except a Pattern space.
         */
        getBaseColorSpace(): Promise<PDFNet.ColorSpace>;
        /**
         * @returns A promise that resolves to true if shading has a bounding box, false otherwise.
         */
        hasBBox(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a rectangle giving the left, bottom, right, and top
         * coordinates, respectively, of the shading's bounding box. The coordinates
         * are interpreted in the shading's target coordinate space. If present, this
         * bounding box is applied as a temporary clipping boundary when the shading
         * is painted, in addition to the current clipping path and any other clipping
         * boundaries in effect at that time.
         */
        getBBox(): Promise<PDFNet.Rect>;
        /**
         * @returns A promise that resolves to true if the shading has a background color or false otherwise.
         */
        hasBackground(): Promise<boolean>;
        /**
         * An color point represented in base color space specifying a single
         * background color value. If present, this color is used before
         * any painting operation involving the shading, to fill those portions of the
         * area to be painted that lie outside the bounds of the shading object itself.
         * In the opaque imaging model, the effect is as if the painting operation were
         * performed twice: first with the background color and then again with the
         * shading.
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        getBackground(): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to a flag indicating whether to filter the shading function to prevent
         * aliasing artifacts. See Table 4.25
         */
        getAntialias(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a number specifying the limiting value of a parametric variable t.
         * The variable is considered to vary linearly between GetParamStart() and
         * GetParamEnd() as the color gradient varies between the starting and ending points
         * of the axis for Axial shading or circles for Radial shading.
         * The variable t becomes the input argument to the color function(s).
         */
        getParamStart(): Promise<number>;
        /**
         * @returns A promise that resolves to a number specifying the limiting value of a parametric variable t.
         * The variable is considered to vary linearly between GetParamStart() and
         * GetParamEnd() as the color gradient varies between the starting and ending points
         * of the axis for Axial shading or circles for Radial shading.
         * The variable t becomes the input argument to the color function(s).
         */
        getParamEnd(): Promise<number>;
        /**
         * @returns A promise that resolves to a flag specifying whether to extend the shading beyond the starting
         * point of the axis for Axial shading or starting circle for Radial shading.
         */
        isExtendStart(): Promise<boolean>;
        /**
         * @returns A promise that resolves to a flag specifying whether to extend the shading beyond the ending
         * point of the axis for Axial shading or ending circle for Radial shading.
         */
        isExtendEnd(): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        getColor(t: number): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to an object of type: "Object"
         */
        getCoords(): Promise<object>;
        /**
         * @returns A promise that resolves to for Radial shading returns six numbers (x0 y0 r0 x1 y1 r1) specifying
         * the centers and radii of the starting and ending circles, expressed in the
         * shading's target coordinate space. The radii r0 and r1 must both be greater
         * than or equal to 0. If one radius is 0, the corresponding circle is treated
         * as a point; if both are 0, nothing is painted.
         */
        getCoordsRadial(): Promise<object>;
        /**
         * @returns A promise that resolves to an array of four numbers [xmin xmax ymin ymax] specifying the rectangular
         * domain of coordinates over which the color function(s) are defined.
         * If the function does not contain /Domain entry the function returns: [0 1 0 1].
         */
        getDomain(): Promise<object>;
        /**
         * @returns A promise that resolves to a matrix specifying a mapping from the coordinate space specified
         * by the Domain entry into the shading's target coordinate space.
         */
        getMatrix(): Promise<PDFNet.Matrix2D>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        getColorForFunction(t1: number, t2: number): Promise<PDFNet.ColorPt>;
    }
    /**
     * The class ShapedText.
     * A sequence of positioned glyphs -- the visual representation of a
     * given text string
     */
    class ShapedText extends PDFNet.Destroyable {
        /**
         * Scaling factor of this shaped text relative to the em size. A scaling factor of 1 means that all units are relative to the em size. PDF scaling is typically 1000 units per em.
         * @returns A promise that resolves to returns the scaling factor for the glyph positions.
         */
        getScale(): Promise<number>;
        /**
         * Get the state of the shaping operation. Even if the shaping did not fully succeed, this object can be added to an elementbuilder, and will fallback to placing unshped text. See GetFailureReason() in the case this method returns something other than FullShaping.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ShapedText.ShapingStatus = {
         * 	e_FullShaping : 0
         * 	e_PartialShaping : 1
         * 	e_NoShaping : 2
         * }
         * </pre>
         * @returns A promise that resolves to .
         */
        getShapingStatus(): Promise<number>;
        /**
         * In the case where GetShapingStatus() returns something other than FullShaping, this method will return a more detailed reason behind the failure.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.ShapedText.FailureReason = {
         * 	e_NoFailure : 0
         * 	e_UnsupportedFontType : 1
         * 	e_NotIndexEncoded : 2
         * 	e_FontDataNotFound : 3
         * }
         * </pre>
         * @returns A promise that resolves to .
         */
        getFailureReason(): Promise<number>;
        /**
         * The original source text string.
         * @returns A promise that resolves to returns the source text string.
         */
        getText(): Promise<string>;
        /**
         * Number of glyphs present in the shaped text. Might be different from the .
         * @returns A promise that resolves to returns the the number of utf32 codepoints in this shaped text.
         */
        getNumGlyphs(): Promise<number>;
        /**
         * Get the glyph ID at the indicated place in the shaped sequence. This number is specific to the font file used to generate the shaping results, and does not always have a clean mapping to a particular Unicode codepoint in the original string.
         * @param index - - the index of the glyph to be retrieved. Must be less than GetNumGlyphs().
         * @returns A promise that resolves to returns the glyph ID for the indicated place in the shaped result.
         */
        getGlyph(index: number): Promise<number>;
        /**
         * The X position of the glyph at the requested index. This number has been scaled by GetScale().
         * @param index - - the index of the glyph position to be retrieved. Must be less than GetNumGlyphs().
         * @returns A promise that resolves to returns the X position for the glyph at the specified index.
         */
        getGlyphXPos(index: number): Promise<number>;
        /**
         * The Y position of the glyph at the requested index. This number has been scaled by GetScale().
         * @param index - - the index of the glyph position to be retrieved. Must be less than GetNumGlyphs().
         * @returns A promise that resolves to returns the Y position for the glyph at the specified index.
         */
        getGlyphYPos(index: number): Promise<number>;
    }
    /**
     * SignatureHandler instances are responsible for defining the digest and cipher algorithms to create
     * and/or validate a signed PDF document. SignatureHandlers are added to PDFDoc instances by
     * calling the [PDFDoc].addSignatureHandler method.
     */
    class SignatureHandler {
        /**
         * Gets the name of this SignatureHandler. The name of the SignatureHandler is what identifies this SignatureHandler
         * from all others. This name is also added to the PDF as the value of /Filter entry in the signature dictionary.
         * @returns A promise that resolves to the name of this SignatureHandler.
         */
        getName(): Promise<string>;
        /**
         * Resets any data appending and signature calculations done so far. This method should allow PDFNet to restart the
         * whole signature calculation process. It is important that when this method is invoked, any data processed with
         * the AppendData method should be discarded.
         * @returns A promise that resolves to true if there are no errors, otherwise false.
         */
        reset(): Promise<boolean>;
        /**
         * Destructor
         */
        destructor(): Promise<void>;
    }
    /**
     * An object representing a Signature used in a PDF Form. These Widgets can be signed directly, or signed using a DigitalSignatureField.
     */
    class SignatureWidget extends PDFNet.WidgetAnnot {
        /**
         * Creates a new SignatureWidget annotation in the specified document, and adds an associated signature form field to the document.
         * @param doc - The document to which the widget is to be added.
         * @param pos - A rectangle specifying the widget's bounds in default user space units.
         * @param [field_name] - The name of the digital signature field to create. Optional autogenerated by default.
         * @returns A promise that resolves to a newly-created blank SignatureWidget annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field_name?: string): Promise<PDFNet.SignatureWidget>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Creates a new SignatureWidget annotation associated with a particular form field in the specified document.
         * @param doc - The document to which the widget is to be added.
         * @param pos - A rectangle specifying the widget's bounds in default user space units.
         * @param field - The digital signature field for which to create a signature widget.
         * @returns A promise that resolves to a newly-created blank SignatureWidget annotation.
         */
        static createWithField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.SignatureWidget>;
        /**
         * Creates a new SignatureWidget annotation associated with a particular DigitalSignatureField object (representing a signature-type form field) in the specified document.
         * @param doc - The document to which the widget is to be added.
         * @param pos - A rectangle specifying the widget's bounds in default user space units.
         * @param field - The digital signature field for which to create a signature widget.
         * @returns A promise that resolves to a newly-created blank SignatureWidget annotation.
         */
        static createWithDigitalSignatureField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.DigitalSignatureField): Promise<PDFNet.SignatureWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.SignatureWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.SignatureWidget>;
        /**
         * Creates a Widget annotation and initializes it using given annotation object.
         * @param annot - the annot
         * @returns A promise that resolves to an object of type: "PDFNet.SignatureWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.SignatureWidget>;
        /**
         * a function that will create and add an appearance to this widget
         * by centering an image within it.
         * @param img - A PDF::Image object representing the image to use.
         */
        createSignatureAppearance(img: PDFNet.Image): Promise<void>;
        /**
         * Retrieves the DigitalSignatureField associated with this SignatureWidget.
         * @returns A promise that resolves to a DigitalSignatureField object representing the digital signature form field associated with this signature widget annotation.
         */
        getDigitalSignatureField(): Promise<PDFNet.DigitalSignatureField>;
    }
    /**
     * A Sound annotation represents a sound recording attached to a point in
     * the PDF document. When closed, this annotation appear as an icon; when open
     * and activated, a sound record from the computer's microphone or imported from a file
     * associated with this annotation is played.The icon of this annotation by default
     * is a speaker.
     */
    class SoundAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates a Sound annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.SoundAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.SoundAnnot>;
        /**
         * creates a Sound annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Sound annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.SoundAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.SoundAnnot>;
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.SoundAnnot"
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.SoundAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.SoundAnnot"
         */
        static createWithData(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, stream: PDFNet.Filter, sample_bits: number, sample_freq: number, num_channels: number): Promise<PDFNet.SoundAnnot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.SoundAnnot"
         */
        static createAtPoint(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Point): Promise<PDFNet.SoundAnnot>;
        /**
         * Returns the sound object of the Sound annotation.
         * @returns A promise that resolves to an SDF object representing a sound stream.
         */
        getSoundStream(): Promise<PDFNet.Obj>;
        /**
         * sets the sound object of the Sound annotation.
         * @param icon - An SDF object representing a sound stream.
         */
        setSoundStream(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the Icon of the Sound annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.SoundAnnot.Icon = {
         * 	e_Speaker : 0
         * 	e_Mic : 1
         * 	e_Unknown : 2
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "Icon" enum.
         * Default value: e_Speaker.
         */
        getIcon(): Promise<number>;
        /**
         * sets the Icon of the Sound annotation.
         * (Optional)
         * @param [type] - <pre>
         * PDFNet.SoundAnnot.Icon = {
         * 	e_Speaker : 0
         * 	e_Mic : 1
         * 	e_Unknown : 2
         * }
         * </pre>
         * A value of the "Icon" enumeration type specifying the icon to display.
         * Default value: e_Speaker.
         */
        setIcon(type?: number): Promise<void>;
        /**
         * Returns the Icon name of the Sound annotation.
         * @returns A promise that resolves to a string denoting the Icon name of the Sound annotation.
         */
        getIconName(): Promise<string>;
        /**
         * sets the Icon name of the Sound annotation.
         * (Optional)
         * @param type - A string denoting the Icon name of the Sound annotation.
         */
        setIconName(type: string): Promise<void>;
    }
    /**
     * Square annotation is used to display a rectangle on the page. When opened,
     * a square annotation can display a pop-up window containing the text of the
     * associated note. The rectangle may be inscribed and possibly padded within the
     * annotation rectangle defined by the annotation dictionary's Rect entry.
     */
    class SquareAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates an Square annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.SquareAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.SquareAnnot>;
        /**
         * creates a Square annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Square annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.SquareAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.SquareAnnot>;
        /**
         * Creates a new Square annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, in user space coordinates.
         * @returns A promise that resolves to a newly created blank Square annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.SquareAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.ColorPt"
         */
        getInteriorColor(): Promise<PDFNet.ColorPt>;
        /**
         * @returns A promise that resolves to an object of type: "number"
         */
        getInteriorColorCompNum(): Promise<number>;
        setInteriorColorDefault(col: PDFNet.ColorPt): Promise<void>;
        setInteriorColor(col: PDFNet.ColorPt, numcomp: number): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Rect"
         */
        getContentRect(): Promise<PDFNet.Rect>;
        setContentRect(cr: PDFNet.Rect): Promise<void>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.Rect"
         */
        getPadding(): Promise<PDFNet.Rect>;
        setPadding(cr: PDFNet.Rect): Promise<void>;
    }
    /**
     * A Squiggly annotation shows as a wavy line segment across the bottom
     * of a word or a group of contiguous words.
     */
    class SquigglyAnnot extends PDFNet.TextMarkupAnnot {
        /**
         * creates a Squiggly annotation and initializes it using given Cos/SDF object.
         * @param d - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.SquigglyAnnot"
         */
        static createFromObj(d: PDFNet.Obj): Promise<PDFNet.SquigglyAnnot>;
        /**
         * creates a Squiggly annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Squiggly annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.SquigglyAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.SquigglyAnnot>;
        /**
         * Creates a new Squiggly annotation in the specified document.
         * @param doc - A document to which the Popup annotation is added.
         * @param pos - A rectangle specifying the Popup annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Squiggly annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.SquigglyAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
    }
    /**
     * Stamper is a utility class that can be used to stamp PDF pages with text, images,
     * or vector art (including another PDF page) in only a few lines of code.
     *
     * Although Stamper is very simple to use compared to ElementBuilder/ElementWriter
     * it is not as powerful or flexible. In case you need full control over PDF creation
     * use ElementBuilder/ElementWriter to add new content to existing PDF pages as
     * shown in the ElementBuilder sample project.
     */
    class Stamper extends PDFNet.Destroyable {
        /**
         * Stamper constructor
         * @param size_type - <pre>
         * PDFNet.Stamper.SizeType = {
         * 	e_relative_scale : 1
         * 	e_absolute_size : 2
         * 	e_font_size : 3
         * }
         * </pre>
         * Specifies how the stamp will be sized
         * 		-e_relative_scale: Stamp size is relative to the size of the crop box of
         * 		the destination page. 'a' is a percentage of the width of the crop box
         * 		(e.g.: 0.5 is 50% of the width of the crop box) 'b' is a percentage of the
         * 		height of the crop box. If 'a' <= 0 then only b will be used. If 'b' <= 0
         * 		then only 'a' will be used.
         *
         * 		-e_absolute_scale: Stamp size is explicitly set. 'a' sets the width of
         * 		of the stamp's bounding box. 'b' sets the height of the stamp's bounding
         * 		box. The width and height are constant, regardless of the size of the
         * 		destination page's bounding box.
         *
         * 		-e_font_size: This type only applies to text stamps. 'a' sets the font
         * 		size. 'b' is ignored.
         * @param a - @see size_type
         * @param b - @see size_type
         * @returns A promise that resolves to an object of type: "PDFNet.Stamper"
         */
        static create(size_type: number, a: number, b: number): Promise<PDFNet.Stamper>;
        /**
         * Stamps an image to the given destination document at the set of page numbers
         * @param dest_doc - The document being stamped
         * @param img - The image that is being stamped to the document
         * @param dest_pages - The set of pages in the document being stamped
         */
        stampImage(dest_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, img: PDFNet.Image, dest_pages: PDFNet.PageSet): Promise<void>;
        /**
         * Stamps a PDF page to the given destination document at the set of page numbers
         * @param dest_doc - The document being stamped
         * @param page - The page that is being stamped to the document
         * @param dest_pages - The set of pages in the document being stamped
         */
        stampPage(dest_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, page: PDFNet.Page, dest_pages: PDFNet.PageSet): Promise<void>;
        /**
         * Stamps text to the given destination document at the set of page numbers
         * @param dest_doc - The document being stamped
         * @param txt - The image that is being stamped to the document
         * @param dest_pages - The set of pages in the document being stamped
         */
        stampText(dest_doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, txt: string, dest_pages: PDFNet.PageSet): Promise<void>;
        /**
         * Defines the font of the stamp. (This only applies to text-based stamps)
         * @param font - The font of the text stamp
         */
        setFont(font: PDFNet.Font): Promise<void>;
        /**
         * Sets the font color (This only effects text-based stamps)
         * @param font_color - The color of the font
         */
        setFontColor(font_color: PDFNet.ColorPt): Promise<void>;
        /**
         * Sets the text alignment (note: this only applies to text watermarks)
         * @param text_alignment - <pre>
         * PDFNet.Stamper.TextAlignment = {
         * 	e_align_left : -1
         * 	e_align_center : 0
         * 	e_align_right : 1
         * }
         * </pre>
         * Enumerator for text alignment (e_left, e_center, e_right)
         */
        setTextAlignment(text_alignment: number): Promise<void>;
        /**
         * Sets the opacity value for the stamp
         * @param opacity - The opacity value of the stamp
         */
        setOpacity(opacity: number): Promise<void>;
        /**
         * Rotates the stamp by the given number of degrees
         * @param rotation - Rotation in degrees
         */
        setRotation(rotation: number): Promise<void>;
        /**
         * Specifies if the stamp is to be stamped in the background or the foreground.
         * @param background - A flag specifying if the stamp should be added
         * as a background layer to the destination page
         */
        setAsBackground(background: boolean): Promise<void>;
        /**
         * Specifies if the stamp is to be stamped as an annotation.
         * @param annotation - A flag specifying if the stamp should be added
         * as an annotation or not
         */
        setAsAnnotation(annotation: boolean): Promise<void>;
        /**
         * @param on_screen - Specifies if the watermark will be displayed on screen
         */
        showsOnScreen(on_screen: boolean): Promise<void>;
        /**
         * @param on_print - Specifies if the watermark will be displayed when printed
         */
        showsOnPrint(on_print: boolean): Promise<void>;
        /**
         * Sets the alignment for the x and y variables.
         * @param horizontal_alignment - <pre>
         * PDFNet.Stamper.HorizontalAlignment = {
         * 	e_horizontal_left : -1
         * 	e_horizontal_center : 0
         * 	e_horizontal_right : 1
         * }
         * </pre>
         * Can be set to e_left, e_center or e_right
         * 		e_left:	horizontal_distance measures the distance between the left edge of the stamp's
         * 			bounding box and the left edge of the crop box
         * 		e_center: horizontal_distance measures the distance between the horizontal bisector of the
         * 			stamp's bounding box and the horizontal bisector of the crop box
         * 		e_right: horizontal_distance measures the distance between the right edge of the stamp's
         * 			bounding box and the right edge of the crop box
         * @param vertical_alignment - <pre>
         * PDFNet.Stamper.VerticalAlignment = {
         * 	e_vertical_bottom : -1
         * 	e_vertical_center : 0
         * 	e_vertical_top : 1
         * }
         * </pre>
         * Can be set to e_top, e_center or e_bottom
         * 		e_bottom: vertical_distance measures the distance between the bottom edge of the stamp's
         * 			bounding box and the bottom edge of the crop box
         * 		e_center: vertical_distance measures the distance between the vertical bisector of the
         * 			stamp's bounding box and the vertical bisector of the crop box
         * 		e_top: vertical_distance measures the distance between the top edge of the stamp's
         * 			bounding box and the top edge of the crop box
         */
        setAlignment(horizontal_alignment: number, vertical_alignment: number): Promise<void>;
        setPosition(x: number, y: number, use_percentage?: boolean): Promise<void>;
        /**
         * sets the size of the stamp
         * @param size_type - <pre>
         * PDFNet.Stamper.SizeType = {
         * 	e_relative_scale : 1
         * 	e_absolute_size : 2
         * 	e_font_size : 3
         * }
         * </pre>
         * Specifies how the stamp will be sized
         * 		-e_relative_scale: Stamp size is relative to the size of the crop box of
         * 		the destination page. 'a' is a percentage of the width of the crop box
         * 		(e.g.: 0.5 is 50% of the width of the crop box) 'b' is a percentage of the
         * 		height of the crop box. If 'a' <= 0 then only b will be used. If 'b' <= 0
         * 		then only 'a' will be used.
         *
         * 		-e_absolute_scale: Stamp size is explicitly set. 'a' sets the width of
         * 		of the stamp's bounding box. 'b' sets the height of the stamp's bounding
         * 		box. The width and height are constant, regardless of the size of the
         * 		destination page's bounding box.
         *
         * 		-e_font_size: This type only applies to text stamps. 'a' sets the font
         * 		size. 'b' is ignored.
         * @param a - Generally the horizontal component of the size. See size_type for more details.
         * @param b - Generally the vertical component of the size. See size_type for more details.
         */
        setSize(size_type: number, a: number, b: number): Promise<void>;
        /**
         * Deletes PDFTron stamps from document at given page numbers
         * @param doc - The document to delete stamps from
         * @param page_set - The set of pages to delete stamps from
         */
        static deleteStamps(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, page_set: PDFNet.PageSet): Promise<void>;
        /**
         * Returns true if the given set of pages has at least one stamp
         * @param doc - The document that's being checked
         * @param page_set - The set of page that's being checked
         * @returns A promise that resolves to an object of type: "boolean"
         */
        static hasStamps(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, page_set: PDFNet.PageSet): Promise<boolean>;
    }
    /**
     * A StrikeOut annotation shows as a line segment crossing out
     * a word or a group of contiguous words.
     */
    class StrikeOutAnnot extends PDFNet.TextMarkupAnnot {
        /**
         * creates a StrikeOut annotation and initializes it using given Cos/SDF object.
         * @param d - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.StrikeOutAnnot"
         */
        static createFromObj(d: PDFNet.Obj): Promise<PDFNet.StrikeOutAnnot>;
        /**
         * creates a StrikeOut annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the StrikeOut annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.StrikeOutAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.StrikeOutAnnot>;
        /**
         * Creates a new StrikeOut annotation in the specified document.
         * @param doc - A document to which the Popup annotation is added.
         * @param pos - A rectangle specifying the Popup annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank StrikeOut annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.StrikeOutAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
    }
    /**
     * A text annotation represents a "sticky note" attached to a point in
     * the PDF document. When closed, the annotation shall appear as an icon;
     * when open, it shall display a pop-up window containing the text of
     * the note in a font and size chosen by the conforming reader.
     * Text annotations do not scale and rotate with the page (i.e. they should
     * behave as if the NoZoom and NoRotate annotation flags).
     */
    class TextAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates a Text annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.TextAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.TextAnnot>;
        /**
         * creates a Text annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Text annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.TextAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.TextAnnot>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.TextAnnot"
         */
        static createAtPoint(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Point): Promise<PDFNet.TextAnnot>;
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.TextAnnot"
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.TextAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Returns the initial status of the Text annotation.
         * @returns A promise that resolves to a boolean value that specifies whether the annotation shall
         * initially be displayed as opened.
         * Default value: false.
         */
        isOpen(): Promise<boolean>;
        /**
         * sets the initial status of the Text annotation.
         * (Optional)
         * @param isopen - A boolean value that specifies whether the annotation shall
         * initially be displayed as opened.
         * Default value: false.
         */
        setOpen(isopen: boolean): Promise<void>;
        /**
         * Returns the type of the icon associated with the Text annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.TextAnnot.Icon = {
         * 	e_Comment : 0
         * 	e_Key : 1
         * 	e_Help : 2
         * 	e_NewParagraph : 3
         * 	e_Paragraph : 4
         * 	e_Insert : 5
         * 	e_Note : 6
         * 	e_Unknown : 7
         * }
         * </pre>
         * @returns A promise that resolves to a value of the enumeration type "Icon".
         * Default value: e_Note.
         */
        getIcon(): Promise<number>;
        /**
         * sets the type of the icon associated with the Text annotation.
         * (Optional)
         * @param [icon] - <pre>
         * PDFNet.TextAnnot.Icon = {
         * 	e_Comment : 0
         * 	e_Key : 1
         * 	e_Help : 2
         * 	e_NewParagraph : 3
         * 	e_Paragraph : 4
         * 	e_Insert : 5
         * 	e_Note : 6
         * 	e_Unknown : 7
         * }
         * </pre>
         * A value of the enum "Icon" type.
         * Default value: e_Note.
         */
        setIcon(icon?: number): Promise<void>;
        setIconDefault(): Promise<void>;
        /**
         * Returns the name of the icon associated with the Text annotation.
         * @returns A promise that resolves to a string denoting the name of the icon.
         */
        getIconName(): Promise<string>;
        /**
         * sets the name of the icon associated with the Text annotation.
         * (Optional)
         * @param icon - A string denoting the name of the icon.
         */
        setIconName(icon: string): Promise<void>;
        /**
         * Returns the string indicating the state of the Text annotation.
         * (PDF 1.5)
         * @returns A promise that resolves to a string that indicates the state of the Text annotation when first loaded.
         * Default: "Unmarked" if StateModel is "Marked"; "None" if StateModel is "Review".
         */
        getState(): Promise<string>;
        /**
         * Sets the string indicating the state of the Text annotation.
         * (Optional; PDF 1.5 )
         * @param [state] - A string that indicates the state of the Text annotation when first loaded.
         * Default: "Unmarked" if StateModel is "Marked"; "None" if StateModel is "Review".
         */
        setState(state?: string): Promise<void>;
        /**
         * Returns the string indicating the state model of the Text annotation.
         * (PDF 1.5)
         * @returns A promise that resolves to a string containing the state model name either "Marked" or "Review".
         */
        getStateModel(): Promise<string>;
        /**
         * Sets the string indicating the state model of the Text annotation.
         * (Required if State is present, otherwise optional; PDF 1.5 )
         * @param sm - A string containing the state model name either "Marked" or "Review".
         */
        setStateModel(sm: string): Promise<void>;
    }
    /**
     * TextExtractor is used to analyze a PDF page and extract words and logical
     * structure within a given region. The resulting list of lines and words can
     * be traversed element by element or accessed as a string buffer. The class
     * also includes utility methods to extract PDF text as HTML or XML.
     *
     * Possible use case scenarios for TextExtractor include:
     *  - Converting PDF pages to text or XML for content repurposing.
     *  - Searching PDF pages for specific words or keywords.
     *  - Indexing large PDF repositories for indexing or content
     *    retrieval purposes (i.e. implementing a PDF search engine).
     *  - Classifying or summarizing PDF documents based on their text content.
     *  - Finding specific words for content editing purposes (such as splitting pages
     *    based on keywords etc).
     *
     * The main task of TextExtractor is to interpret PDF pages and offer a
     * simple to use API to:
     *  - Normalize all text content to Unicode.
     *  - Extract inferred logical structure (word by word, line by line,
     *    or paragraph by paragraph).
     *  - Extract positioning information for every line, word, or a glyph.
     *  - Extract style information (such as information about the font, font size,
     *    font styles, etc) for every line, word, or a glyph.
     *  - Control the content analysis process. A number of options (such as
     *    removal of text obscured by images) is available to let the user
     *    direct the flow of content recognition algorithms that will meet their
     *    requirements.
     *  - Offer utility methods to convert PDF page content to text, XML, or HTML.
     *
     * Note: TextExtractor is analyzing only textual content of the page.
     * This means that the rasterized (e.g. in scanned pages) or vectorized
     * text (where glyphs are converted to path outlines) will not be recognized
     * as text. Please note that it is still possible to extract this content
     * using pdftron.PDF.ElementReader interface.
     *
     * In some cases TextExtractor may extract text that does not appear to
     * be on the visible page (e.g. when text is obscured by an image or a
     * rectangle). In these situations it is possible to use processing flags
     * such as 'e_remove_hidden_text' and 'e_no_invisible_text' to remove
     * hidden text.
     *
     * A sample use case (in C++):
     *
     * <pre>
     * ... Initialize PDFNet ...
     * PDFDoc doc(filein);
     * doc.InitSecurityHandler();
     * Page page = *doc.PageBegin();
     * TextExtractor txt;
     * txt.Begin(page, 0, TextExtractor::e_remove_hidden_text);
     * UString text;
     * txt.GetAsText(text);
     * // or traverse words one by one...
     * TextExtractor::Line line = txt.GetFirstLine(), lend;
     * TextExtractor::Word word, wend;
     * for (; line!=lend; line=line.GetNextLine()) {
     *  for (word=line.GetFirstWord(); word!=wend; word=word.GetNextWord()) {
     *    text.Assign(word.GetString(), word.GetStringLen());
     *    cout << text << '\n';
     *  }
     * }
     * </pre>
     *
     * A sample use case (in C#):
     *
     * <pre>
     * ... Initialize PDFNet ...
     * PDFDoc doc = new PDFDoc(filein);
     * doc.InitSecurityHandler();
     * Page page = doc.PageBegin().Current();
     * TextExtractor txt = new TextExtractor();
     * txt.Begin(page, 0, TextExtractor.ProcessingFlags.e_remove_hidden_text);
     * string text = txt.GetAsText();
     * // or traverse words one by one...
     * TextExtractor.Word word;
     * for (TextExtractor.Line line = txt.GetFirstLine(); line.IsValid(); line=line.GetNextLine()) {
     *   for (word=line.GetFirstWord(); word.IsValid(); word=word.GetNextWord()) {
     *     Console.WriteLine(word.GetString());
     *   }
     * }
     * </pre>
     *
     * For full sample code, please take a look at TextExtract sample project.
     */
    class TextExtractor extends PDFNet.Destroyable {
        /**
         * Constructor and destructor
         * @returns A promise that resolves to an object of type: "PDFNet.TextExtractor"
         */
        static create(): Promise<PDFNet.TextExtractor>;
        /**
         * Sets the Optional Content Group (OCG) context that should be used when
         * processing the document. This function can be used to change the current
         * OCG context. Optional content (such as PDF layers) will be selectively
         * processed based on the states of optional content groups in the given
         * context.
         * @param ctx - Optional Content Group (OCG) context, or NULL if TextExtractor
         * should process all content on the page.
         */
        setOCGContext(ctx: PDFNet.OCGContext): Promise<void>;
        /**
         * Start reading the page.
         * @param page - Page to read.
         * @param [clip_ptr] - An optional clipping rectangle. This
         * parameter can be used to selectively read text from a given rectangle.
         * @param [flags] - A list of ProcessingFlags used to control text extraction
         * algorithm.
         */
        begin(page: PDFNet.Page, clip_ptr?: PDFNet.Rect, flags?: number): Promise<void>;
        /**
         * @returns A promise that resolves to the number of words on the page.
         */
        getWordCount(): Promise<number>;
        /**
         * Sets the directionality of text extractor.
         * Must be called before the processing of a page started.
         * @param rtl - mode reverses the directionality of TextExtractor algorithm.
         */
        setRightToLeftLanguage(rtl: boolean): Promise<void>;
        /**
         * @returns A promise that resolves to the directionality of text extractor.
         */
        getRightToLeftLanguage(): Promise<boolean>;
        /**
         * get all words in the current selection as a single string.
         * @param [dehyphen] - If true, finds and removes hyphens that split words
         * across two lines. Hyphens are often used a the end of lines as an
         * indicator that a word spans two lines. Hyphen detection enables removal
         * of hyphen character and merging of text runs to form a single word.
         * This option has no effect on Tagged PDF files.
         * @returns A promise that resolves to an object of type: "string"
         */
        getAsText(dehyphen?: boolean): Promise<string>;
        /**
         * Get all the characters that intersect an annotation.
         * @param annot - The annotation to intersect with.
         * @returns A promise that resolves to an object of type: "string"
         */
        getTextUnderAnnot(annot: PDFNet.Annot): Promise<string>;
        /**
         * get text content in a form of an XML string.
         * @param [xml_output_flags] - flags controlling XML output. For more
         * information, please see TextExtract::XMLOutputFlags.
         *
         * XML output will be encoded in UTF-8 and will have the following
         * structure:
         * <pre>
         * <Page num="1 crop_box="0, 0, 612, 792" media_box="0, 0, 612, 792" rotate="0">
         *  <Flow id="1">
         *   <Para id="1">
         *    <Line box="72, 708.075, 467.895, 10.02" style="font-family:Calibri; font-size:10.02; color: #000000;">
         *      <Word box="72, 708.075, 30.7614, 10.02">PDFNet</Word>
         *      <Word box="106.188, 708.075, 15.9318, 10.02">SDK</Word>
         *      <Word box="125.617, 708.075, 6.22242, 10.02">is</Word>
         *      ...
         *    </Line>
         *   </Para>
         *  </Flow>
         * </Page>
         * </pre>
         *
         * The above XML output was generated by passing the following union of
         * flags in the call to GetAsXML():
         *   (TextExtractor::e_words_as_elements | TextExtractor::e_output_bbox | TextExtractor::e_output_style_info)
         *
         * In case 'xml_output_flags' was not specified, the default XML output
         * would look as follows:
         *
         * <Page num="1 crop_box="0, 0, 612, 792" media_box="0, 0, 612, 792" rotate="0">
         * <Flow id="1">
         * <Para id="1">
         *     <Line>PDFNet SDK is an amazingly comprehensive, high-quality PDF developer toolkit...</Line>
         *     <Line>levels. Using the PDFNet PDF library, ...</Line>
         *     ...
         *   </Para>
         *  </Flow>
         * </Page>
         * </pre>
         * @returns A promise that resolves to an object of type: "string"
         */
        getAsXML(xml_output_flags?: number): Promise<string>;
        /**
         * @returns A promise that resolves to the number of lines of text on the selected page.
         */
        getNumLines(): Promise<number>;
        /**
         * @returns A promise that resolves to the first line of text on the selected page.
         */
        getFirstLine(): Promise<PDFNet.TextExtractorLine>;
        /**
         * [CURRENTLY BUGGED]
         * @param mtx - The quadrilateral representing a tight bounding box
         * @param quads - n
         * @param quads_size - n
         * for this word (in unrotated page coordinates).
         */
        getQuads(mtx: PDFNet.Matrix2D, quads: number, quads_size: number): Promise<void>;
    }
    /**
     * TextExtractor::Line object represents a line of text on a PDF page.
     * Each line consists of a sequence of words, and each words in one or
     * more styles.
     */
    class TextExtractorLine {
        constructor(line?: number, uni?: number, num?: number, cur_num?: number, m_direction?: number, mp_bld?: any);
        /**
         * @returns A promise that resolves to the number of words in this line.
         */
        getNumWords(): Promise<number>;
        /**
         * @returns A promise that resolves to true is this line is not rotated (i.e. if the
         * quadrilaterals returned by GetBBox() and GetQuad() coincide).
         */
        isSimpleLine(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the first word in the line.
         */
        getFirstWord(): Promise<PDFNet.TextExtractorWord>;
        /**
         * @param word_idx - A integer representing the index of the word to get.
         * @returns A promise that resolves to the i-th word in this line.
         */
        getWord(word_idx: number): Promise<PDFNet.TextExtractorWord>;
        /**
         * @returns A promise that resolves to the next line on the page.
         */
        getNextLine(): Promise<PDFNet.TextExtractorLine>;
        /**
         * @returns A promise that resolves to the index of this line of the current page.
         */
        getCurrentNum(): Promise<number>;
        /**
         * @returns A promise that resolves to predominant style for this line.
         */
        getStyle(): Promise<PDFNet.TextExtractorStyle>;
        /**
         * @returns A promise that resolves to the unique identifier for a paragraph or column
         * that this line belongs to. This information can be used to
         * identify which lines belong to which paragraphs.
         */
        getParagraphID(): Promise<number>;
        /**
         * @returns A promise that resolves to the unique identifier for a paragraph or column
         * that this line belongs to. This information can be used to
         * identify which lines/paragraphs belong to which flows.
         */
        getFlowID(): Promise<number>;
        /**
         * @returns A promise that resolves to true is this line of text ends with a hyphen (i.e. '-'),
         * false otherwise.
         */
        endsWithHyphen(): Promise<boolean>;
        /**
         * Comparison function.
         * Determines if parameter object is equal to current object.
         * @returns A promise that resolves to True if the two objects are equivalent, False otherwise
         */
        compare(line2: PDFNet.TextExtractorLine): Promise<boolean>;
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.TextExtractorLine"
         */
        static create(): Promise<PDFNet.TextExtractorLine>;
        /**
         * @returns A promise that resolves to true if this is a valid line, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the bounding box for this line (in unrotated page
        coordinates).
         */
        getBBox(): Promise<PDFNet.Rect>;
        /**
         * Gets the quadrilateral bounding box for the line (in unrotated page coordinates)
         * @returns A promise that resolves to an object of type: "PDFNet.QuadPoint"
         */
        getQuad(): Promise<PDFNet.QuadPoint>;
        line: number;
        uni: number;
        num: number;
        cur_num: number;
        m_direction: number;
        mp_bld: any;
    }
    /**
     * A class representing predominant text style associated with a
     * given Line, a Word, or a Glyph. The class includes information about
     * the font, font size, font styles, text color, etc.
     */
    class TextExtractorStyle {
        constructor(mp_imp?: any);
        /**
         * @returns A promise that resolves to low-level PDF font object. A high level font object can
         * be instantiated as follows:
         * In C++: pdftron.PDF.Font f(style.GetFont())
         * In C#: pdftron.PDF.Font f = new pdftron.PDF.Font(style.GetFont());
         */
        getFont(): Promise<PDFNet.Obj>;
        /**
         * @returns A promise that resolves to the font name used to draw the selected text.
         */
        getFontName(): Promise<string>;
        /**
         * @returns A promise that resolves to the font size used to draw the selected text as it
         * appears on the output page.
         */
        getFontSize(): Promise<number>;
        /**
         * @returns A promise that resolves to the weight (thickness) component of the fully-qualified font name
         * or font specifier. The possible values are 100, 200, 300, 400, 500, 600, 700,
         * 800, or 900, where each number indicates a weight that is at least as dark as
         * its predecessor. A value of 400 indicates a normal weight; 700 indicates bold.
         * Note: The specific interpretation of these values varies from font to font.
         * For example, 300 in one font may appear most similar to 500 in another.
         */
        getWeight(): Promise<number>;
        /**
         * @returns A promise that resolves to true if glyphs have dominant vertical strokes that are slanted.
         */
        isItalic(): Promise<boolean>;
        /**
         * @returns A promise that resolves to true if glyphs have serifs, which are short strokes drawn at an angle on the top
         * and bottom of glyph stems.
         */
        isSerif(): Promise<boolean>;
        /**
         * Comparison function.
         * Determines if parameter object is equal to current object.
         * @returns A promise that resolves to True if the two objects are equivalent, False otherwise
         */
        compare(s: PDFNet.TextExtractorStyle): Promise<boolean>;
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.TextExtractorStyle"
         */
        static create(): Promise<PDFNet.TextExtractorStyle>;
        /**
         * Copy Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.TextExtractorStyle"
         */
        copy(): Promise<PDFNet.TextExtractorStyle>;
        /**
         * @returns A promise that resolves to text color in RGB color space.
         */
        getColor(): Promise<ColorPt>;
        mp_imp: any;
    }
    /**
     * TextExtractor::Word object represents a word on a PDF page.
     * Each word contains a sequence of characters in one or more styles
     * (see TextExtractor::Style).
     */
    class TextExtractorWord {
        constructor(line?: number, word?: number, end?: number, uni?: number, num?: number, cur_num?: number, mp_bld?: any);
        /**
         * @returns A promise that resolves to the number of glyphs in this word.
         */
        getNumGlyphs(): Promise<number>;
        /**
         * @param char_idx - The index of a character in this word.
         * @returns A promise that resolves to the style associated with a given character.
         */
        getCharStyle(char_idx: number): Promise<PDFNet.TextExtractorStyle>;
        /**
         * @returns A promise that resolves to predominant style for this word.
         */
        getStyle(): Promise<PDFNet.TextExtractorStyle>;
        /**
         * @returns A promise that resolves to the number of characters in this word.
         */
        getStringLen(): Promise<number>;
        /**
         * @returns A promise that resolves to the next word on the current line.
         */
        getNextWord(): Promise<PDFNet.TextExtractorWord>;
        /**
         * @returns A promise that resolves to the index of this word of the current line. A word that
         * starts the line will return 0, whereas the last word in the line
         * will return (line.GetNumWords()-1).
         */
        getCurrentNum(): Promise<number>;
        /**
         * Comparison function.
         * Determines if parameter object is equal to current object.
         * @returns A promise that resolves to True if the two objects are equivalent, False otherwise
         */
        compare(word: PDFNet.TextExtractorWord): Promise<boolean>;
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.TextExtractorWord"
         */
        static create(): Promise<PDFNet.TextExtractorWord>;
        /**
         * @returns A promise that resolves to true if this is a valid word, false otherwise.
         */
        isValid(): Promise<boolean>;
        /**
         * @returns A promise that resolves to the bounding box for this word (in unrotated page
        coordinates).
         */
        getBBox(): Promise<PDFNet.Rect>;
        /**
         * @returns A promise that resolves to the quadrilateral representing a tight
        bounding box for this word (in unrotated page coordinates).
         */
        getQuad(): Promise<PDFNet.QuadPoint>;
        /**
         * @returns A promise that resolves to the content of this word represented as a string.
        coordinates).
         */
        getString(): Promise<string>;
        line: number;
        word: number;
        end: number;
        uni: number;
        num: number;
        cur_num: number;
        mp_bld: any;
    }
    /**
     * A TextMarkup is a base class for highlight, underline,
     * strikeout, and squiggly annotations.
     */
    class TextMarkupAnnot extends PDFNet.MarkupAnnot {
        /**
         * creates a TextMarkup annotation and initializes it using given Cos/SDF object.
         * @param d - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.TextMarkupAnnot"
         */
        static createFromObj(d: PDFNet.Obj): Promise<PDFNet.TextMarkupAnnot>;
        /**
         * creates a TextMarkup annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the TextMarkup annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.TextMarkupAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.TextMarkupAnnot>;
        /**
         * Returns the number of QuadPoints in the QuadPoints array of the TextMarkup annotation
         * (PDF 1.6)
         * @returns A promise that resolves to the number of QuadPoints.
         */
        getQuadPointCount(): Promise<number>;
        /**
         * Returns the QuadPoint located at a certain index of the QuadPoint array.
         * (PDF 1.6)
         * @param idx - The index where the QuadPoint is located. The index starts at zero and must be less than return value of GetQuadPointCount().
         * @returns A promise that resolves to the QuadPoint located at a certain index of the QuadPoint array of the TextMarkup annotation.
         */
        getQuadPoint(idx: number): Promise<PDFNet.QuadPoint>;
        /**
         * sets the QuadPoint to be located at a certain index of the QuadPoint array.
         * (Optional; PDF 1.6 )
         * @param idx - The index where the QuadPoint is to be located (the index is counted from 0).
         * @param qp - The QuadPoint to be located at a certain index of the QuadPoint array of the TextMarkup annotation.
         */
        setQuadPoint(idx: number, qp: PDFNet.QuadPoint): Promise<void>;
    }
    /**
     * TextSearch searches through a PDF document for a user-given search pattern.
     * The current implementation supports both verbatim search and the search
     * using regular expressions, whose detailed syntax can be found at:
     *
     * http://www.boost.org/doc/libs/release/libs/regex/doc/html/boost_regex/syntax/perl_syntax.html
     *
     * TextSearch also provides users with several useful search modes and extra
     * information besides the found string that matches the pattern. TextSearch
     * can either keep running until a matched string is found or be set to return
     * periodically in order for the caller to perform any necessary updates
     * (e.g., UI updates). It is also worth mentioning that the search modes can be
     * changed on the fly while searching through a document.
     *
     * Possible use case scenarios for TextSearch include:
     *  - Guide users of a PDF viewer (e.g. implemented by PDFViewCtrl) to places
     *    where they are intersted in;
     *  - Find interested PDF documents which contain certain patterns;
     *  - Extract interested information (e.g., credit card numbers) from a set of files;
     *  - Extract Highlight information (refer to the Highlights class for details) from
     *    files for external use.
     *
     * Note:
     *  - Since hyphens ('-') are frequently used in PDF documents to concatenate the two
     *    broken pieces of a word at the end of a line, for example
     *
     * 	  "TextSearch is powerful for finding patterns in PDF files; yes, it is really pow-
     *     erful."
     *
     *    a search for "powerful" should return both instances. However, not all end-of-line
     *    hyphens are hyphens added to connect a broken word; some of them could be "real"
     *    hyphens. In addition, an input search pattern may also contain hyphens that complicate
     *    the situation. To tackle this problem, the following conventions are adopted:
     *
     *    a)When in the verbatim search mode and the pattern contains no hyphen, a matching
     *      string is returned if it is exactly the same or it contains end-of-line
     *      or start-of-line hyphens. For example, as mentioned above, a search for "powerful"
     *      would return both instances.
     *    b)When in verbatim search mode and the pattern contains one or multiple hyphens, a
     *      matching string is returned only if the string matches the pattern exactly. For
     *      example, a search for "pow-erful" will only return the second instance, and a search
     *      for "power-ful" will return nothing.
     *    c)When searching using regular expressions, hyphens are not taken care implicitly.
     *      Users should take care of it themselves. For example, in order to find both the
     *      "powerful" instances, the input pattern can be "pow-{0,1}erful".
     *
     * A sample use case (in C++):
     *
     * <pre>
     * //... Initialize PDFNet ...
     * PDFDoc doc(filein);
     * doc.InitSecurityHandler();
     * int page_num;
     * char buf[32];
     * UString result_str, ambient_string;
     * Highlights hlts;
     * TextSearch txt_search;
     * TextSearch::Mode mode = TextSearch::e_whole_word | TextSearch::e_page_stop;
     * UString pattern( "joHn sMiTh" );
     *
     * //PDFDoc doesn't allow simultaneous access from different threads. If this
     * //document could be used from other threads (e.g., the rendering thread inside
     * //PDFView/PDFViewCtrl, if used), it is good practice to lock it.
     * //Notice: don't forget to call doc.Unlock() to avoid deadlock.
     * doc.Lock();
     *
     * txt_search.Begin( doc, pattern, mode );
     * while ( true )
     * {
     * 	   SearchResult result = code = txt_search.Run(page_num, result_str, ambient_string, hlts );
     * 	   if ( code == TextSearch::e_found )
     * 	   {
     * 		   result_str.ConvertToAscii(buf, 32, true);
     * 		   cout << "found one instance: " << char_buf << endl;
     * 	   }
     * 	   else
     * 	   {
     * 		   break;
     * 	   }
     * }
     *
     * //unlock the document to avoid deadlock.
     * doc.UnLock();
     * </pre>
     *
     *
     * For a full sample, please take a look at the TextSearch sample project.
     */
    class TextSearch extends PDFNet.Destroyable {
        /**
         * Constructor and destructor.
         * @returns A promise that resolves to an object of type: "PDFNet.TextSearch"
         */
        static create(): Promise<PDFNet.TextSearch>;
        /**
         * @returns A promise that resolves to an object of type: "boolean"
         */
        begin(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pattern: string, mode: number, start_page?: number, end_page?: number): Promise<boolean>;
        /**
         * Set the current search pattern. Note that it is not necessary to call this method since
         * the search pattern is already set when calling the Begin() method. This method is provided
         * for users to change the search pattern while searching through a document.
         * @param pattern - the search pattern to set.
         * @returns A promise that resolves to true if the setting has succeeded.
         */
        setPattern(pattern: string): Promise<boolean>;
        /**
         * Retrieve the current search mode.
         * @returns A promise that resolves to the current search mode.
         */
        getMode(): Promise<number>;
        /**
         * set the current search mode. For example, the following code turns on the regular
         * expressions:
         *
         * 	TextSearch ts;
         *   ...
         *   TextSearch::Mode mode = ts.GetMode();
         *   mode |= TextSearch::e_reg_expression;
         *   ts.SetMode(mode);
         *   ...
         * @param mode - the search mode to set.
         */
        setMode(mode: number): Promise<void>;
        /**
         * Tells TextSearch that language is from right to left.
         * @param flag - Set to true if the language is right to left.
         */
        setRightToLeftLanguage(flag: boolean): Promise<void>;
        /**
         * Retrieve the number of the current page that is searched in.
         * If the returned value is -1, it indicates the search process has not been initialized
         * (e.g., Begin() is not called yet); if the returned value is 0, it indicates the search
         * process has finished, and if the returned value is positive, it is a valid page number.
         * @returns A promise that resolves to the current page number.
         */
        getCurrentPage(): Promise<number>;
        /**
         * Sets the Optional Content Group (OCG) context that should be used when
         * processing the document. This function can be used to change the current
         * OCG context. Optional content (such as PDF layers) will be selectively
         * processed based on the states of optional content groups in the given
         * context.
         * @param ctx - Optional Content Group (OCG) context, or NULL if TextSearch
         * should process all content on the page.
         */
        setOCGContext(ctx: PDFNet.OCGContext): Promise<void>;
        /**
         * Runs a search on the document for a certain string. Make sure to call
        TextSearch.begin(doc, pattern, mode) with the proper parameters
        before calling TextSearch.run()
        
        The resolved object that TextSearch.run() returns contains the following objects:
        page_num - The number of the page with the match
        out_str - The string that matches the search parameter
        ambient_str - The ambient string of the found string (computed only if e_ambient_string is set)
        highlights - The Highlights info associated with the match (computed only if 'e_highlight' is set)
        code - Number representing the status of the search.
            - 0 - e_done, reached end of document.
            - 1 - e_page, reached end of page. (if set to return by specifying mode 'e_page_stop')
            - 2 - e_found, found an instance matching the search pattern
         * @returns A promise that resolves to an object containing the page_num, out_str
        ambient_str, highlights, and result code.
         */
        run(): Promise<any>;
    }
    /**
     * An object representing a Text Box used in a PDF Form.
     */
    class TextWidget extends PDFNet.WidgetAnnot {
        /**
         * Creates a new Text Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param [field_name] - The name of the field for which to create a Text widget.
         * @returns A promise that resolves to a newly created blank Text Widget annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field_name?: string): Promise<PDFNet.TextWidget>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * Creates a new Text Widget annotation, in the specified document.
         * @param doc - The document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds, specified in
         * user space coordinates.
         * @param field - the field for which to create a Text Widget.
         * @returns A promise that resolves to a newly created blank Widget annotation.
         */
        static createWithField(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.TextWidget>;
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.TextWidget"
         */
        static createFromObj(obj?: PDFNet.Obj): Promise<PDFNet.TextWidget>;
        /**
         * Creates a Text Widget annotation and initialize it using given annotation object.
         *
         * <b> Note: </b>  The constructor does not copy any data, but is instead the logical
         * equivalent of a type cast.
         * @param annot - The annotation object to use.
         * @returns A promise that resolves to an object of type: "PDFNet.TextWidget"
         */
        static createFromAnnot(annot: PDFNet.Annot): Promise<PDFNet.TextWidget>;
        /**
         * Sets the text content of the Text Widget.
         * @param text - The text to be displayed in the Text Widget.
         */
        setText(text: string): Promise<void>;
        /**
         * Retrieves the text content of the Text Widget.
         * @returns A promise that resolves to  The Text Widget contents.
         */
        getText(): Promise<string>;
    }
    /**
     * A class representing a set of options for timestamping a document.
     */
    class TimestampingConfiguration extends PDFNet.Destroyable {
        /**
         * Constructor.
         * @param in_url - - a string representing a URL of a timestamp authority (TSA) to use during timestamping
         * @returns A promise that resolves to an object of type: "PDFNet.TimestampingConfiguration"
         */
        static createFromURL(in_url: string): Promise<PDFNet.TimestampingConfiguration>;
        /**
         * Setter to change the timestamp authority (TSA) URL to use during timestamping.
         * @param in_url - - a string representing a URL of a timestamp authority.
         */
        setTimestampAuthorityServerURL(in_url: string): Promise<void>;
        /**
         * Setter for a username to use during timestamping.
         * @param in_username - - a string representing a username..
         */
        setTimestampAuthorityServerUsername(in_username: string): Promise<void>;
        /**
         * Setter for a username to use during timestamping.
         * @param in_password - - a string representing a password..
         */
        setTimestampAuthorityServerPassword(in_password: string): Promise<void>;
        /**
         * Setter for whether to use a nonce (unique random number) during timestamping.
         * This is on by default, and should be on whenever a timestamp authority
         * supports it, because it helps prevent replay attacks.
         * @param in_use_nonce - - a boolean value representing whether or not to use a nonce
         */
        setUseNonce(in_use_nonce: boolean): Promise<void>;
        /**
         * Tests a timestamping configuration for usability and reports any failures. This function
         * does not throw on many common failures, unlike the real signing, thereby allowing early
         * diagnosis of things like connectivity issues with timestamp authorities.
         * @param in_opts - - a set of verification options to try to use
         * @returns A promise that resolves to a result object containing information about the status of the configuration test
         * and any errors that occurred during it.
         */
        testConfiguration(in_opts: PDFNet.VerificationOptions): Promise<PDFNet.TimestampingTestResult>;
    }
    /**
     * A class representing the result of testing a timestamping configuration.
     */
    class TimestampingTestResult extends PDFNet.Destroyable {
        /**
         * Retrieves the overall status of the timestamping configuration testing operation.
         * @returns A promise that resolves to a boolean value representing the status.
         */
        getStatus(): Promise<boolean>;
        /**
         * Retrieves a result message regarding the timestamping configuration testing operation.
         * @returns A promise that resolves to a string result message.
         */
        getString(): Promise<string>;
        /**
         * Returns whether a timestamp response verification result is available. This means
         * that false will be returned when a timestamp response was not received or was empty
         * (e.g. network failures, improper server configuration, bad URL, etc.). This function
         * should be called to check for the availability of a verification result before actually
         * attempting to retrieve one using GetResponseVerificationResult (which throws if a result is not
         * available).
         * @returns A promise that resolves to whether a timestamp response verification result is available
         */
        hasResponseVerificationResult(): Promise<boolean>;
        /**
         * If a timestamp response was successfully retrieved from a timestamp authority, returns
         * the result of verifying it. If a timestamp response was not received, throws. One should
         * call HasResponseVerificationResult first to see if a detailed result is available before calling
         * this function.
         * @returns A promise that resolves to a timestamp response verification result
         */
        getResponseVerificationResult(): Promise<PDFNet.EmbeddedTimestampVerificationResult>;
    }
    /**
     * The detailed result of a trust verification step of a verification
     * operation performed on a digital signature.
     */
    class TrustVerificationResult extends PDFNet.Destroyable {
        /**
         * Retrieves the trust verification status.
         * @returns A promise that resolves to a boolean representing whether or not the trust verification operation was successful. Whether trust-related warnings are treated as errors or not depend on the VerificationOptions used for the verification operation.
         */
        wasSuccessful(): Promise<boolean>;
        /**
         * Retrieves a string representation of the details of the trust verification status.
         * @returns A promise that resolves to a string.
         */
        getResultString(): Promise<string>;
        /**
         * Retrieves the reference-time used for trust verification as an epoch time.
         * @returns A promise that resolves to an integral value representing an epoch time.
         */
        getTimeOfTrustVerification(): Promise<number>;
        /**
         * Retrieves the type of reference-time used for trust verification.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationOptions.TimeMode = {
         * 	e_signing : 0
         * 	e_timestamp : 1
         * 	e_current : 2
         * }
         * </pre>
         * @returns A promise that resolves to an enumerated value representing the type of reference-time used for trust verification.
         */
        getTimeOfTrustVerificationEnum(): Promise<number>;
        /**
         * Returns whether this TrustVerificationResult has a detailed timestamp result inside.
         * @returns A promise that resolves to a boolean value representing whether this TrustVerificationResult has a detailed timestamp result inside.
         */
        hasEmbeddedTimestampVerificationResult(): Promise<boolean>;
        /**
         * Returns the detailed timestamp result inside this TrustVerificationResult. One must call HasEmbeddedTimestampVerificationResult first to check whether the result is available.
         * @returns A promise that resolves to an EmbeddedTimestampVerificationResult object.
         */
        getEmbeddedTimestampVerificationResult(): Promise<PDFNet.EmbeddedTimestampVerificationResult>;
        /**
         * Retrieves the certificate path that was used for verification.
         * @returns A promise that resolves to a container of X509Certificate objects
         */
        getCertPath(): Promise<PDFNet.X509Certificate[]>;
    }
    /**
     * An Underline annotation shows as a line segment across the bottom
     * of a word or a group of contiguous words.
     */
    class UnderlineAnnot extends PDFNet.TextMarkupAnnot {
        /**
         * creates an Underline annotation and initializes it using given Cos/SDF object.
         * @param d - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.UnderlineAnnot"
         */
        static createFromObj(d: PDFNet.Obj): Promise<PDFNet.UnderlineAnnot>;
        /**
         * creates an Underline annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Underline annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.UnderlineAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.UnderlineAnnot>;
        /**
         * Creates a new Underline annotation in the specified document.
         * @param doc - A document to which the Underline annotation is added.
         * @param pos - A rectangle specifying the Underline annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Underline annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.UnderlineAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
    }
    /**
     * The class UndoManager.
     * Undo-redo interface; one-to-one mapped to document
     */
    class UndoManager extends PDFNet.Destroyable {
        /**
         * Forget all changes in this manager (without changing the document).
         * @returns A promise that resolves to an invalid DocSnapshot.
         */
        discardAllSnapshots(): Promise<PDFNet.DocSnapshot>;
        /**
         * Restores to the previous snapshot point, if there is one.
         * @returns A promise that resolves to the resulting snapshot id.
         */
        undo(): Promise<PDFNet.ResultSnapshot>;
        /**
         * Returns whether it is possible to undo from the current snapshot.
         * @returns A promise that resolves to whether it is possible to undo from the current snapshot.
         */
        canUndo(): Promise<boolean>;
        /**
         * Gets the previous state of the document. This state may be invalid if it is impossible to undo.
         * @returns A promise that resolves to the previous state of the document. This state may be invalid if it is impossible to undo.
         */
        getNextUndoSnapshot(): Promise<PDFNet.DocSnapshot>;
        /**
         * Restores to the next snapshot, if there is one.
         * @returns A promise that resolves to a representation of the transition to the next snapshot, if there is one.
         */
        redo(): Promise<PDFNet.ResultSnapshot>;
        /**
         * Returns a boolean indicating whether it is possible to redo from the current snapshot.
         * @returns A promise that resolves to a boolean indicating whether it is possible to redo from the current snapshot.
         */
        canRedo(): Promise<boolean>;
        /**
         * Gets the next state of the document. This state may be invalid if it is impossible to redo.
         * @returns A promise that resolves to the next state of the document. This state may be invalid if it is impossible to redo.
         */
        getNextRedoSnapshot(): Promise<PDFNet.DocSnapshot>;
        /**
         * Creates a snapshot of document state, transitions to the new snapshot.
         * @returns A promise that resolves to a representation of the transition.
         */
        takeSnapshot(): Promise<PDFNet.ResultSnapshot>;
    }
    /**
     * Options pertaining to digital signature verification.
     */
    class VerificationOptions extends PDFNet.Destroyable {
        /**
         * Constructs a set of options for digital signature verification.
         * @param in_level - <pre>
         * PDFNet.VerificationOptions.SecurityLevel = {
         * 	e_compatibility_and_archiving : 0
         * 	e_maximum : 1
         * }
         * </pre>
         * -- the general security level to use. Sets other security settings internally.
         * @returns A promise that resolves to an object of type: "PDFNet.VerificationOptions"
         */
        static create(in_level: number): Promise<PDFNet.VerificationOptions>;
        /**
         * Adds a certificate to the store of trusted certificates inside this options object.
         * @param in_certificate_buf - - a buffer consisting of the data of an X.509 public-key
         * certificate encoded in binary DER (Distinguished Encoding Rules) format, or in PEM
         * (appropriate Privacy-Enhanced Mail header+Base64 encoded DER+appropriate footer) format
         */
        addTrustedCertificate(in_certificate_buf: ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray): Promise<void>;
        /**
         * Adds a certificate to the store of trusted certificates inside this options object, by loading it from a file.
         * @param in_filepath - - a path to a file containing the data of an X.509 public-key certificate encoded in binary DER (Distinguished Encoding Rules) format, or in PEM (appropriate Privacy-Enhanced Mail header+Base64 encoded DER+appropriate footer) format.
         */
        addTrustedCertificateUString(in_filepath: string): Promise<void>;
        /**
         * Bulk trust list loading API. Attempts to decode the input data as binary DER and trust multiple trusted
         * root certificates from it. Compatible with Acrobat's .P7C format, which is a variation on PKCS #7/CMS that only
         * contains certificates (i.e. no CRLs, no signature, etc.). If a certificate cannot be decoded, this is ignored
         * and an attempt is made to decode the next certificate.
         * @param in_P7C_binary_DER_certificates_file_data - - the P7C-format bulk certificate data, encoded in binary DER (Distinguished Encoding Rules).
         * @param in_size - - the size of the data, in bytes.
         */
        addTrustedCertificates(in_P7C_binary_DER_certificates_file_data: number, in_size: number): Promise<void>;
        /**
         * Sets a flag that can turn on or off the verification of the permissibility of any modifications made to the document after the signing of the digital signature being verified, in terms of the document and field permissions specified by the digital signature being verified.
         * @param in_on_or_off - - A boolean.
         */
        enableModificationVerification(in_on_or_off: boolean): Promise<void>;
        /**
         * Sets a flag that can turn on or off the verification of the digest (cryptographic hash) component of a digital signature.
         * @param in_on_or_off - - A boolean.
         */
        enableDigestVerification(in_on_or_off: boolean): Promise<void>;
        /**
         * Sets a flag that can turn on or off the verification of the trust status of a digital signature.
         * @param in_on_or_off - - A boolean.
         */
        enableTrustVerification(in_on_or_off: boolean): Promise<void>;
        /**
         * Sets a URI prefix to use for online revocation requests during digital signature verification.
         * Useful for Emscripten platform -- used to avoid CORS-related errors. The default value is https://proxy.pdftron.com
         * @param in_str - the proxy prefix URL string to use for revocation requests
         */
        setRevocationProxyPrefix(in_str: string): Promise<void>;
        /**
         * Enables/disables online CRL revocation checking. The default setting is
         * for it to be turned off, but this may change in future versions.
         * @param in_on_or_off - - what setting to use
         */
        enableOnlineCRLRevocationChecking(in_on_or_off: boolean): Promise<void>;
        /**
         * Enables/disables online OCSP revocation checking. The default setting is for it to be turned on.
         * @param in_on_or_off - - what setting to use.
         */
        enableOnlineOCSPRevocationChecking(in_on_or_off: boolean): Promise<void>;
        /**
         * enables/disables all online revocation checking modes. The default settings are that
         * online OCSP is turned on and online CRL is turned off, but the default CRL setting may change in
         * future versions.
         * @param in_on_or_off - - what setting to use
         */
        enableOnlineRevocationChecking(in_on_or_off: boolean): Promise<void>;
        /**
         * Adds a certificate from a url to the store of trusted certificates inside this options object.
         * @param url - The url from which to download the file
         * @param [options] - Additional options
         * @param options.withCredentials - Whether to set the withCredentials property on the XMLHttpRequest
         * @param options.customHeaders - An object containing custom HTTP headers to be used when downloading the document
         */
        static addTrustedCertificateFromURL(url: string, options?: {
            withCredentials: boolean;
            customHeaders: any;
        }): Promise<void>;
    }
    /**
     * The result of a verification operation performed on a digital signature.
     */
    class VerificationResult extends PDFNet.Destroyable {
        /**
         * @returns A promise that resolves to an object of type: "PDFNet.DigitalSignatureField"
         */
        getDigitalSignatureField(): Promise<PDFNet.DigitalSignatureField>;
        /**
         * Retrieves the main verification status. The main status is determined based on the other statuses.
         * Verification may fail for many reasons; some of these reasons are the presence of features that are
         * not supported yet. It may be desirable for you to report unsupported signatures differently (for example,
         * using a question mark rather than an X mark). Any unsupported features encountered can be retrieved
         * by the use of the function GetUnsupportedFeatures on this class.
         * @returns A promise that resolves to a boolean representing whether or not the verification operation was completely successful.
         */
        getVerificationStatus(): Promise<boolean>;
        /**
         * Retrieves the document-related result condition associated with a digital signature verification operation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.DocumentStatus = {
         * 	e_no_error : 0
         * 	e_corrupt_file : 1
         * 	e_unsigned : 2
         * 	e_bad_byteranges : 3
         * 	e_corrupt_cryptographic_contents : 4
         * }
         * </pre>
         * @returns A promise that resolves to a DocumentStatus-type enumeration value.
         */
        getDocumentStatus(): Promise<number>;
        /**
         * Retrieves the digest-related result condition associated with a digital signature verification operation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.DigestStatus = {
         * 	e_digest_invalid : 0
         * 	e_digest_verified : 1
         * 	e_digest_verification_disabled : 2
         * 	e_weak_digest_algorithm_but_digest_verifiable : 3
         * 	e_no_digest_status : 4
         * 	e_unsupported_encoding : 5
         * }
         * </pre>
         * @returns A promise that resolves to a DigestStatus-type enumeration value.
         */
        getDigestStatus(): Promise<number>;
        /**
         * Retrieves the trust-related result condition associated with a digital signature verification operation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.TrustStatus = {
         * 	e_trust_verified : 0
         * 	e_untrusted : 1
         * 	e_trust_verification_disabled : 2
         * 	e_no_trust_status : 3
         * }
         * </pre>
         * @returns A promise that resolves to a TrustStatus-type enumeration value.
         */
        getTrustStatus(): Promise<number>;
        /**
         * Retrieves the result condition about permissions checks performed on any unsigned modifications associated with a digital signature verification operation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.VerificationResult.ModificationPermissionsStatus = {
         * 	e_invalidated_by_disallowed_changes : 0
         * 	e_has_allowed_changes : 1
         * 	e_unmodified : 2
         * 	e_permissions_verification_disabled : 3
         * 	e_no_permissions_status : 4
         * }
         * </pre>
         * @returns A promise that resolves to a ModificationPermissionsStatus-type enumeration value.
         */
        getPermissionsStatus(): Promise<number>;
        /**
         * Retrieves the detailed result associated with the trust step of the verification operation that returned this VerificationResult, if such a detailed trust result is available. Must call HasTrustVerificationResult first and check for a true result.
         * @returns A promise that resolves to a TrustVerificationResult object
         */
        getTrustVerificationResult(): Promise<PDFNet.TrustVerificationResult>;
        /**
         * Returns whether there is a detailed TrustVerificationResult in this VerificationResult or not.
         * @returns A promise that resolves to a boolean.
         */
        hasTrustVerificationResult(): Promise<boolean>;
        /**
         * Retrieves a list of informational structures regarding any disallowed changes that have been made to the document since the signature associated with this verification result was signed.
         * @returns A promise that resolves to a collection of DisallowedChange objects.
         */
        getDisallowedChanges(): Promise<PDFNet.DisallowedChange[]>;
        /**
         * Retrieves an enumeration value representing the digest algorithm used to sign the signature that is associated with this verification result.
         * For DocTimeStamp signatures, returns the weakest algorithm found (between the CMS and message imprint digests).
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.DigestAlgorithm.Type = {
         * 	e_SHA1 : 0
         * 	e_SHA256 : 1
         * 	e_SHA384 : 2
         * 	e_SHA512 : 3
         * 	e_RIPEMD160 : 4
         * 	e_unknown_digest_algorithm : 5
         * }
         * </pre>
         * @returns A promise that resolves to a DigestAlgorithm enumeration value.
         */
        getDigestAlgorithm(): Promise<number>;
        /**
         * Retrieves the document-related result condition associated with a digital signature verification operation, as a descriptive string.
         * @returns A promise that resolves to a string.
         */
        getDocumentStatusAsString(): Promise<string>;
        /**
         * Retrieves the digest-related result condition associated with a digital signature verification operation, as a descriptive string.
         * @returns A promise that resolves to a string.
         */
        getDigestStatusAsString(): Promise<string>;
        /**
         * Retrieves the trust-related result condition associated with a digital signature verification operation, as a descriptive string.
         * @returns A promise that resolves to a string.
         */
        getTrustStatusAsString(): Promise<string>;
        /**
         * Retrieves the result condition about permissions checks performed on any unsigned modifications associated with a digital signature verification operation, as a descriptive string.
         * @returns A promise that resolves to a string.
         */
        getPermissionsStatusAsString(): Promise<string>;
        /**
         * Retrieves reports about unsupported features encountered during verification.
         * This function is the canonical source of information about all unsupported features encountered.
         * Current possible values:
         * 	"unsupported signature encoding",
         * 	"usage rights signature",
         * 	"legal content attestations",
         * 	"unsupported digest algorithm"
         * @returns A promise that resolves to a container of strings representing unsupported features encountered during verification
         */
        getUnsupportedFeatures(): Promise<string[]>;
    }
    /**
     * [Missing documentation]
     */
    class ViewChangeCollection extends PDFNet.Destroyable {
        /**
         * Constructor
         * @returns A promise that resolves to an object of type: "PDFNet.ViewChangeCollection"
         */
        static create(): Promise<PDFNet.ViewChangeCollection>;
    }
    /**
     * A Watermark annotation is an annotation that is printed at a fixed
     * size and position on a page, regardless of the dimensions of the printed page.
     */
    class WatermarkAnnot extends PDFNet.Annot {
        /**
         * creates a Watermark annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.WatermarkAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.WatermarkAnnot>;
        /**
         * creates a Watermark annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Watermark annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.WatermarkAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.WatermarkAnnot>;
        /**
         * Creates a new Watermark annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds in default user space units.
         * @returns A promise that resolves to a newly created blank Watermark annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect): Promise<PDFNet.WatermarkAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
    }
    /**
     * Interactive forms use widget annotations (PDF 1.2) to represent the appearance
     * of fields and to manage user interactions. As a convenience, when a field has
     * only a single associated widget annotation, the contents of the field dictionary
     * and the annotation dictionary may be merged into a single dictionary containing
     * entries that pertain to both a field and an annotation.
     * NOTE This presents no ambiguity, since the contents of the two kinds of
     * dictionaries do not conflict.
     */
    class WidgetAnnot extends PDFNet.Annot {
        /**
         * Creates a new widget annotation in the specified document.
         * @param doc - A document to which the annotation is added.
         * @param pos - A rectangle specifying the annotation's bounds in default user space units.
         * @param field - A form field associated with this widget.
         * @returns A promise that resolves to a newly created blank widget annotation.
         */
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, pos: PDFNet.Rect, field: PDFNet.Field): Promise<PDFNet.WidgetAnnot>;
        static create(doc: PDFNet.PDFDoc | PDFNet.SDFDoc | PDFNet.FDFDoc, type: number, pos: PDFNet.Rect): Promise<PDFNet.Annot>;
        /**
         * creates a widget annotation and initializes it using given Cos/SDF object.
         * @param [d] - The Cos/SDF object to initialze the annotation with.
         * @returns A promise that resolves to an object of type: "PDFNet.WidgetAnnot"
         */
        static createFromObj(d?: PDFNet.Obj): Promise<PDFNet.WidgetAnnot>;
        /**
         * creates a widget annotation and initializes it using given annotation object.
         * @param ann - Annot object used to initialize the Widget annotation.
         * @returns A promise that resolves to an object of type: "PDFNet.WidgetAnnot"
         */
        static createFromAnnot(ann: PDFNet.Annot): Promise<PDFNet.WidgetAnnot>;
        /**
         * Returns the field associated with the Widget.
         * @returns A promise that resolves to a Field object.
         */
        getField(): Promise<PDFNet.Field>;
        /**
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.WidgetAnnot.HighlightingMode = {
         * 	e_none : 0
         * 	e_invert : 1
         * 	e_outline : 2
         * 	e_push : 3
         * 	e_toggle : 4
         * }
         * </pre>
         * @returns A promise that resolves to the HighlightingMode of the widget annotation.
         */
        getHighlightingMode(): Promise<number>;
        /**
         * sets the HighlightingMode for the widget annotation.
         * @param [value] - <pre>
         * PDFNet.WidgetAnnot.HighlightingMode = {
         * 	e_none : 0
         * 	e_invert : 1
         * 	e_outline : 2
         * 	e_push : 3
         * 	e_toggle : 4
         * }
         * </pre>
         * New highlighting mode for the widget.
         */
        setHighlightingMode(value?: number): Promise<void>;
        /**
         * Returns the action of the widget annotation
         * @returns A promise that resolves to an action object representing the action of the widget annotation
         * that should be performed when the annotation is activated.
         */
        getAction(): Promise<PDFNet.Action>;
        /**
         * Sets the action of the widget annotation
         * (Optional; PDF 1.2 )
         * @param action - An action object representing the action of the widget annotation
         * that should be performed when the annotation is activated.
         */
        setAction(action: PDFNet.Action): Promise<void>;
        /**
         * Returns the border color of the annotation.
         * @returns A promise that resolves to a color object that denotes the color of the Screen border.
         */
        getBorderColor(): Promise<PDFNet.ColorPt>;
        /**
         * sets the border color of the annotation.
         * (Optional)
         * @param col - A color object that denotes the color of the screen border.
         * @param compnum - An integer which value indicates the color space used for the parameter c.
         */
        setBorderColor(col: PDFNet.ColorPt, compnum: number): Promise<void>;
        /**
         * Returns the number indicating border color space of the annotation.
         * @returns A promise that resolves to an integer indicating a color space value from the ColorSpace::Type enum.
         * That is, 1 corresponding to "e_device_gray",
         * 3 corresponding to "e_device_rgb", and 4 corresponding to "e_device_cmyk".
         * 0 means this annotation had no color assigned.
         */
        getBorderColorCompNum(): Promise<number>;
        /**
         * Returns the number indicating background color space of the annotation.
         * @returns A promise that resolves to an integer indicating a color space value from the ColorSpace::Type enum.
         * That is, 1 corresponding to "e_device_gray",
         * 3 corresponding to "e_device_rgb", and 4 corresponding to "e_device_cmyk" if color space is applicable,
         * 0 means no background color was assigned.
         */
        getBackgroundColorCompNum(): Promise<number>;
        /**
         * Returns the background color of the annotation.
         * @returns A promise that resolves to a color object that denotes the color of the Screen background.
         */
        getBackgroundColor(): Promise<PDFNet.ColorPt>;
        /**
         * sets the background color of the annotation.
         * (Optional)
         * @param col - A color point that denotes the color of the screen background.
         * @param compnum - An integer which value indicates the color space used for the parameter c.
         */
        setBackgroundColor(col: PDFNet.ColorPt, compnum: number): Promise<void>;
        /**
         * Returns static caption text of the annotation.
         * @returns A promise that resolves to a string containing the static caption text of the annotation.
         */
        getStaticCaptionText(): Promise<string>;
        /**
         * sets static caption text of the annotation.
         * (Optional; button fields only)
         * @param contents - A string containing the static caption text of the annotation.
         */
        setStaticCaptionText(contents: string): Promise<void>;
        /**
         * Returns the rollover caption text of the annotation.
         * @returns A promise that resolves to a string containing the rollover caption text of the annotation.
         */
        getRolloverCaptionText(): Promise<string>;
        /**
         * sets the roll over caption text of the annotation.
         * (Optional; button fields only)
         * @param contents - A string containing the roll over caption text of the annotation.
         */
        setRolloverCaptionText(contents: string): Promise<void>;
        /**
         * Returns the button down caption text of the annotation.
         * @returns A promise that resolves to a string containing the button down text of the annotation.
         */
        getMouseDownCaptionText(): Promise<string>;
        /**
         * sets the button down caption text of the annotation.
         * (Optional; button fields only)
         * @param contents - A string containing the button down text of the annotation.
         */
        setMouseDownCaptionText(contents: string): Promise<void>;
        /**
         * Returns the static icon associated with the annotation.
         * @returns A promise that resolves to an SDF object that represents the static icon
         * associated with the annotation.
         */
        getStaticIcon(): Promise<PDFNet.Obj>;
        /**
         * sets the static icon associated with the annotation.
         * (Optional; button fields only)
         * @param icon - An SDF object that represents the static icon
         * associated with the annotation.
         */
        setStaticIcon(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the rollover icon associated with the annotation.
         * @returns A promise that resolves to an SDF object that represents the rollover icon
         * associated with the annotation.
         */
        getRolloverIcon(): Promise<PDFNet.Obj>;
        /**
         * sets the rollover icon associated with the annotation.
         * (Optional; button fields only)
         * @param icon - An SDF object that represents the rollover icon
         * associated with the annotation.
         */
        setRolloverIcon(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the Mouse Down icon associated with the annotation.
         * @returns A promise that resolves to an SDF object that represents the Mouse Down icon
         * associated with the annotation.
         */
        getMouseDownIcon(): Promise<PDFNet.Obj>;
        /**
         * sets the Mouse Down icon associated with the annotation.
         * (Optional; button fields only)
         * @param icon - An SDF object that represents the Mouse Down icon
         * associated with the annotation.
         */
        setMouseDownIcon(icon: PDFNet.Obj): Promise<void>;
        /**
         * Returns the Scale Type of the annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.WidgetAnnot.ScaleType = {
         * 	e_Anamorphic : 0
         * 	e_Proportional : 1
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "ScaleType" enum which represents the Scale Type of the annotation.
         * Default value: P.
         */
        getScaleType(): Promise<number>;
        /**
         * sets the Scale Type of the annotation.
         * (Optional)
         * @param st - <pre>
         * PDFNet.WidgetAnnot.ScaleType = {
         * 	e_Anamorphic : 0
         * 	e_Proportional : 1
         * }
         * </pre>
         * An entry of the "ScaleType" enum which represents the Scale Type of the annotation.
         * Default value: P.
         */
        setScaleType(st: number): Promise<void>;
        /**
         * Returns the Icon and caption relationship of the  annotation.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.WidgetAnnot.IconCaptionRelation = {
         * 	e_NoIcon : 0
         * 	e_NoCaption : 1
         * 	e_CBelowI : 2
         * 	e_CAboveI : 3
         * 	e_CRightILeft : 4
         * 	e_CLeftIRight : 5
         * 	e_COverlayI : 6
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "IconCaptionRelation" enum type.
         * Default value: e_NoIcon.
         */
        getIconCaptionRelation(): Promise<number>;
        /**
         * sets the Icon and caption relationship of the  annotation.
         * (Optional; pushbutton fields only)
         * @param icr - <pre>
         * PDFNet.WidgetAnnot.IconCaptionRelation = {
         * 	e_NoIcon : 0
         * 	e_NoCaption : 1
         * 	e_CBelowI : 2
         * 	e_CAboveI : 3
         * 	e_CRightILeft : 4
         * 	e_CLeftIRight : 5
         * 	e_COverlayI : 6
         * }
         * </pre>
         * A value of the "IconCaptionRelation" enum type.
         * Default value: e_NoIcon.
         */
        setIconCaptionRelation(icr: number): Promise<void>;
        /**
         * Returns the condition under which the icon should be scaled.
         * @example
         * Return value enum:
         * <pre>
         * PDFNet.WidgetAnnot.ScaleCondition = {
         * 	e_Always : 0
         * 	e_WhenBigger : 1
         * 	e_WhenSmaller : 2
         * 	e_Never : 3
         * }
         * </pre>
         * @returns A promise that resolves to a value of the "ScaleCondition" enum type.
         * Default value: e_Always.
         */
        getScaleCondition(): Promise<number>;
        /**
         * sets the condition under which the icon should be scaled.
         * (Optional)
         * @param sd - <pre>
         * PDFNet.WidgetAnnot.ScaleCondition = {
         * 	e_Always : 0
         * 	e_WhenBigger : 1
         * 	e_WhenSmaller : 2
         * 	e_Never : 3
         * }
         * </pre>
         * A value of the "ScaleCondition" enum type.
         * Default value: e_Always.
         */
        setScaleCondition(sd: number): Promise<void>;
        /**
         * Returns the "fit full" flag.
         * @returns A promise that resolves to a boolean value indicating the "fit full" flag value.
         */
        getFitFull(): Promise<boolean>;
        /**
         * sets the "fit full" flag.
         * (Optional)
         * @param ff - A boolean value indicating the "fit full" flag value.
         */
        setFitFull(ff: boolean): Promise<void>;
        /**
         * Returns the horizontal leftover space of the icon within the annotation.
         * @returns A promise that resolves to a number indicating the horizontal
         * leftover space of the icon within the annotation.
         */
        getHIconLeftOver(): Promise<number>;
        /**
         * sets the horizontal leftover space of the icon within the annotation.
         * (Optional)
         * @param hl - A number indicating the horizontal
         * leftover space of the icon within the annotation.
         */
        setHIconLeftOver(hl: number): Promise<void>;
        /**
         * Returns the vertical leftover space of the icon within the annotation.
         * @returns A promise that resolves to a number indicating the vertical
         * leftover space of the icon within the annotation.
         */
        getVIconLeftOver(): Promise<number>;
        /**
         * sets the vertical leftover space of the icon within the annotation.
         * (Optional)
         * @param vl - A number indicating the vertical
         * leftover space of the icon within the annotation.
         */
        setVIconLeftOver(vl: number): Promise<void>;
        /**
         * Sets the font size of the Widget Annotation.
         * @param font_size - the new font size
         */
        setFontSize(font_size: number): Promise<void>;
        setTextColor(col: PDFNet.ColorPt, col_comp: number): Promise<void>;
        /**
         * Specifies a font to be used for text in this Widget.
         * @param font - the font to use.
         */
        setFont(font: PDFNet.Font): Promise<void>;
        /**
         * Returns the font size used in this Widget Annotation.
         * @returns A promise that resolves to the font size
         */
        getFontSize(): Promise<number>;
        /**
         * Returns the text color of the Widget Annotation.
         * @returns A promise that resolves to the text color.
         */
        getTextColor(): Promise<object>;
        /**
         * Retrieves the font used for displaying text in this Widget.
         * @returns A promise that resolves to the font used by this Widget.
         */
        getFont(): Promise<PDFNet.Font>;
    }
    /**
     * This class represents an AttributeTypeAndValue as mentioned in RFC 5280 in the context of DistinguishedNames and as defined in ITU X.501.
     */
    class X501AttributeTypeAndValue extends PDFNet.Destroyable {
        /**
         * Retrieves the OID (i.e. one of the object identifiers from the X.500 attribute types) in the form of integer components in a container.
         * @returns A promise that resolves to an ObjectIdentifier object.
         */
        getAttributeTypeOID(): Promise<PDFNet.ObjectIdentifier>;
        /**
         * Retrieves the value associated with the contained OID (object identifier) as a string, if the value is defined by the OID to be of a string type.
         * @returns A promise that resolves to a string containing the string value.
         */
        getStringValue(): Promise<string>;
    }
    /**
     * This class represents a distinguished name (DN) as defined in X.501.
     * See the X.500 standards, RFC 5280, and an OID repository for more information.
     */
    class X501DistinguishedName extends PDFNet.Destroyable {
        /**
         * Returns whether this distinguished name contains a particular attribute, identified by its object identifier (OID).
         * @param in_oid - the object identifier representing the sought attribute.
         * @returns A promise that resolves to a boolean value representing whether this distinguished name contains the supplied attribute.
         */
        hasAttribute(in_oid: PDFNet.ObjectIdentifier): Promise<boolean>;
        /**
         * @returns A promise that resolves to an object of type: "Array<string>"
         */
        getStringValuesForAttribute(obj: PDFNet.ObjectIdentifier): Promise<string[]>;
        /**
         * Retrieves all of the attribute-type-to-value pairs in this distinguished name.
         * @returns A promise that resolves to a container of X501AttributeTypeAndValue objects
         */
        getAllAttributesAndValues(): Promise<PDFNet.X501AttributeTypeAndValue[]>;
    }
    /**
     * This class represents an X509 public-key certificate, as specified in RFC 5280.
     */
    class X509Certificate extends PDFNet.Destroyable {
        /**
         * Retrieves the names of the certificate issuer as a map of OIDs (i.e. one of the
         * object identifiers from the X.500 attribute types) to string values. The issuer field
         * identifies the entity that has signed and issued the certificate. The returned value
         * will contain all of the AttributeTypeAndValue items from all of the
         * RelativeDistinguishedNames in the Name of the issuer. See RFC 5280, section 4.1.2.4
         * and Appendix A.1 (page 116).
         * @returns A promise that resolves to an X501DistinguishedName object
         */
        getIssuerField(): Promise<PDFNet.X501DistinguishedName>;
        /**
         * Retrieves the names of the certificate subject as a map of OIDs (i.e. one of the
         * object identifiers from the X.500 attribute types) to string values. The subject field
         * represents the identity of the entity associated with the certificate's public key. The
         * returned value will contain all of the AttributeTypeAndValue items from all of the
         * RelativeDistinguishedNames in the Name of the subject. See RFC 5280, section 4.1.2.6
         * and Appendix A.1 (page 116).
         * @returns A promise that resolves to an X501DistinguishedName object
         */
        getSubjectField(): Promise<PDFNet.X501DistinguishedName>;
        /**
         * Retrieves the notBefore time from the certificate's Validity entry in the form of an
         * integral value representing an epoch time.
         * @returns A promise that resolves to an integer containing an epoch time
         */
        getNotBeforeEpochTime(): Promise<number>;
        /**
         * Retrieves the notAfter time from the certificate's Validity entry in the form of an
         * integral value representing an epoch time.
         * @returns A promise that resolves to an integer containing an epoch time
         */
        getNotAfterEpochTime(): Promise<number>;
        /**
         * Retrieves the version number representing which version of X509 the certificate corresponds to, from the certificate.
         * @returns A promise that resolves to an integer containing the version number
         */
        getRawX509VersionNumber(): Promise<number>;
        /**
         * Returns a string representation of the certificate.
         * @returns A promise that resolves to a string representation of the certificate.
         */
        toString(): Promise<string>;
        /**
         * Retrieves, in a string, a text representation of a cryptographically-secure digest of the certificate that can be used to identify it uniquely.
         * @param [in_digest_algorithm] - <pre>
         * PDFNet.DigestAlgorithm.Type = {
         * 	e_SHA1 : 0
         * 	e_SHA256 : 1
         * 	e_SHA384 : 2
         * 	e_SHA512 : 3
         * 	e_RIPEMD160 : 4
         * 	e_unknown_digest_algorithm : 5
         * }
         * </pre>
         * An enumeration representing the digest algorithm to use. Currently supported are SHA-1 (SHA-160), SHA-256, SHA-384, and SHA-512.
         * @returns A promise that resolves to a string representation of the fingerprint, in the form of double ASCII characters representing hex bytes, separated by colons
         */
        getFingerprint(in_digest_algorithm?: number): Promise<string>;
        /**
         * Retrieves the serialNumber entry from the certificate.
         * @returns A promise that resolves to a big-integer-style container holding bytes representing the components of an integral serial number in big-endian order.
         */
        getSerialNumber(): Promise<Uint8Array>;
        /**
         * Retrieves all extensions (as first specified in V3 of X509, see RFC 5280) from the certificate.
         * @returns A promise that resolves to a container of X509Extension objects.
         */
        getExtensions(): Promise<PDFNet.X509Extension[]>;
        /**
         * Retrieves the certificate as binary DER-encoded data. (DER is short for Distinguished Encoding Rules.).
         * @returns A promise that resolves to a container of bytes representing the certificate, encoded as binary DER.
         */
        getData(): Promise<Uint8Array>;
    }
    /**
     * This class represents an X509v3 certificate extension. See RFC 5280 as a specification.
     */
    class X509Extension extends PDFNet.Destroyable {
        /**
         * Retrieves whether the extension is 'critical'. See RFC 5280 for an explanation of what this means.
         * @returns A promise that resolves to a boolean representing the criticality flag of the extension.
         */
        isCritical(): Promise<boolean>;
        /**
         * Retrieves the OID (object identifier) of the extension in the form of integer components in a container. The meaning of an OID can be determined from an OID repository.
         * @returns A promise that resolves to an ObjectIdentifier object.
         */
        getObjectIdentifier(): Promise<PDFNet.ObjectIdentifier>;
        /**
         * Returns a string representation of the extension.
         * @returns A promise that resolves to a string representation of the extension.
         */
        toString(): Promise<string>;
        /**
         * Retrieves the raw binary DER-encoded data of the extension. (DER is short for Distinguished Encoding Rules.)
         * @returns A promise that resolves to a container holding the bytes of the extension in the form of binary DER-encoded data
         */
        getData(): Promise<Uint8Array>;
    }
    /**
     * QuadPoint
    
    A QuadpPoint struct contains 8 values representing the (x,y) coordinates of four points in a rectangle..
    
    --------------------
    Since QuadPoint is a struct, it can be created manually by calling "new PDFNet.QuadPoint(p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y)"
    eg. var myfoo = new PDFNet.QuadPoint(1, 2, 3, 4, 5, 6, 7, 8)
    
    Default values for a Point struct are:
    p1x = 0
    p1y = 0
    p2x = 0
    p2y = 0
    p3x = 0
    p3y = 0
    p4x = 0
    p4y = 0
     */
    class QuadPoint {
        constructor(p1x?: number, p1y?: number, p2x?: number, p2y?: number, p3x?: number, p3y?: number, p4x?: number, p4y?: number);
        p1x: number;
        p1y: number;
        p2x: number;
        p2y: number;
        p3x: number;
        p3y: number;
        p4x: number;
        p4y: number;
    }
    /**
     * 2D Point
    
    A Point represents an (x,y) coordinate point.
    
    --------------------
    Since Point is a struct, it can be created manually by calling "new PDFNet.Point(x, y)"
    eg. var myfoo = new PDFNet.Point(1,2);
    
    Default values for a Point struct are:
    x = 0
    y = 0
     */
    class Point {
        constructor(x?: number, y?: number);
        x: number;
        y: number;
    }
    namespace Optimizer {
        /**
         * An object that stores downsampling/recompression settings for color and grayscale images.
         */
        class ImageSettings {
            /**
             * Sets the maximum and resampling dpi for images.
            By default these are set to 225 and 150 respectively.
             * @param maximum - the highest dpi of an image before it will be resampled
             * @param resampling - the image dpi to resample to if an image is encountered over the maximum dpi
             * @returns this object, for call chaining
             */
            setImageDPI(maximum: number, resampling: number): PDFNet.Optimizer.ImageSettings;
            /**
             * Sets the output compression mode for this type of image
            The default value is e_retain
             * @param mode - the compression mode to set
            <pre>
            PDFNet.Optimizer.ImageSettings.CompressionMode = {
             e_retain : 0,
             e_flate : 1,
             e_jpeg : 2,
             e_jpeg2000 : 3,
             e_none : 4
            }
            </pre>
             * @returns this object, for call chaining
             */
            setCompressionMode(mode: number): PDFNet.Optimizer.ImageSettings;
            /**
             * Sets the downsample mode for this type of image
            The default value is e_default which will allow downsampling of images
             * @param mode - the compression mode to set
            <pre>
            PDFNet.Optimizer.ImageSettings.DownsampleMode = {
             e_off : 0,
                e_default : 1
            }
            </pre>
             * @returns this object, for call chaining
             */
            setDownsampleMode(mode: number): PDFNet.Optimizer.ImageSettings;
            /**
             * Sets the quality for lossy compression modes.
            from 1 to 10 where 10 is lossless (if possible)
            the default value is 5
             * @param quality - the quality for lossy compression modes.
             * @returns this object, for call chaining
             */
            setQuality(quality: number): PDFNet.Optimizer.ImageSettings;
            /**
             * Sets whether recompression to the specified compression method, should be forced when the image is not downsampled.
            By default the compression method for these images will not be changed.
             * @param force - if true the compression method for all
             * @returns this object, for call chaining
             */
            forceRecompression(force: boolean): PDFNet.Optimizer.ImageSettings;
            /**
             * Sets whether image changes that grow the PDF file should be kept.
            This is off by default.
             * @param force - if true all image changes will be kept.
             * @returns this object, for call chaining
             */
            forceChanges(force: boolean): PDFNet.Optimizer.ImageSettings;
        }
        /**
         * An object that stores downsampling/recompression settings for monochrome images.
         */
        class MonoImageSettings {
            /**
             * Sets the maximum and resampling dpi for monochrome images.
            By default these are set to 450 and 300 respectively.
             * @param maximum - the highest dpi of an image before it will be resampled
             * @param resampling - the image dpi to resample to if an image is encountered over the maximum dpi
             * @returns this object, for call chaining
             */
            setImageDPI(maximum: number, resampling: number): PDFNet.Optimizer.MonoImageSettings;
            /**
             * Sets the output compression mode for monochrome images
            The default value is e_jbig2
             * @param mode - the compression mode to set
            <pre>
            PDFNet.Optimizer.MonoImageSettings.CompressionMode = {
                e_jbig2 : 0,
                e_flate : 1,
                e_none : 2
            }
            </pre>
             * @returns this object, for call chaining
             */
            setCompressionMode(mode: number): PDFNet.Optimizer.MonoImageSettings;
            /**
             * Sets the downsample mode for monochrome images
            The default value is e_default
             * @param mode - the compression mode to set
            <pre>
            PDFNet.Optimizer.MonoImageSettings.DownsampleMode = {
             e_off : 0,
                e_default : 1
            }
            </pre>
             * @returns this object, for call chaining
             */
            setDownsampleMode(mode: number): PDFNet.Optimizer.MonoImageSettings;
            /**
             * Sets the quality for lossy JBIG2Decode compression mode.
            The threshold is a floating point number in the range from 4 to 9
            The default value for JBIG2 is 8.5.  The setting is ignored for FLATE.
             * @param jbig2_threshold - the quality for lossy compression modes.
             * @returns this object, for call chaining
             */
            setJBIG2Threshold(jbig2_threshold: number): PDFNet.Optimizer.MonoImageSettings;
            /**
             * Sets whether recompression to the specified compression method, should be forced when the image is not downsampled.
            By default the compression method for these images will not be changed.
             * @param force - if true the compression method for all
             * @returns this object, for call chaining
             */
            forceRecompression(force: boolean): PDFNet.Optimizer.MonoImageSettings;
            /**
             * Sets whether image changes that grow the PDF file should be kept.
            This is off by default.
             * @param force - if true all image changes will be kept.
             * @returns this object, for call chaining
             */
            forceChanges(force: boolean): PDFNet.Optimizer.MonoImageSettings;
        }
        /**
         * An object that stores image text optimization settings.
         */
        class TextSettings {
            /**
             * Sets whether embedded fonts will be subset. This will generally reduce the size of fonts,
            but will strip font hinting.
            Subsetting is off by default.
             * @param subset - if true all embedded fonts will be subsetted.
             * @returns this object, for call chaining
             */
            subsetFonts(subset: boolean): PDFNet.Optimizer.TextSettings;
            /**
             * Sets whether fonts should be embedded. This will generally increase the size of the file,
            but will make the file appear the same on different machines.
            Font embedding is off by default.
             * @param embed - if true all fonts will be embedded.
             * @returns this object, for call chaining
             */
            embedFonts(embed: boolean): PDFNet.Optimizer.TextSettings;
        }
        /**
         * An object that stores settings for the optimizer
         */
        class OptimizerSettings {
            /**
             * updates the settings for color image processing
             * @param settings - settings for color image processing
             * @returns this object, for call chaining
             */
            setColorImageSettings(settings: PDFNet.Optimizer.ImageSettings): PDFNet.Optimizer.OptimizerSettings;
            /**
             * updates the settings for grayscale image processing
             * @param settings - settings for grayscale image processing
             * @returns this object, for call chaining
             */
            setGrayscaleImageSettings(settings: PDFNet.Optimizer.ImageSettings): PDFNet.Optimizer.OptimizerSettings;
            /**
             * updates the settings for monochrome image processing
             * @param settings - settings for monochrome image processing
             * @returns this object, for call chaining
             */
            setMonoImageSettings(settings: PDFNet.Optimizer.MonoImageSettings): PDFNet.Optimizer.OptimizerSettings;
            /**
             * updates the settings for text processing
             * @param settings - settings for text processing
             * @returns this object, for call chaining
             */
            setTextSettings(settings: PDFNet.Optimizer.TextSettings): PDFNet.Optimizer.OptimizerSettings;
            /**
             * Enable or disable removal of custom entries in the PDF.
            By default custom entries are removed.
             * @param should_remove - if true custom entries will be removed.
             * @returns this object, for call chaining
             */
            removeCustomEntries(should_remove: boolean): PDFNet.Optimizer.OptimizerSettings;
        }
        namespace ImageSettings {
            enum CompressionMode {
                e_retain,
                e_flate,
                e_jpeg,
                e_jpeg2000,
                e_none
            }
            enum DownsampleMode {
                e_off,
                e_default
            }
        }
        namespace MonoImageSettings {
            enum CompressionMode {
                e_jbig2,
                e_flate,
                e_none
            }
            enum DownsampleMode {
                e_off,
                e_default
            }
        }
    }
    namespace Convert {
        /**
         * An object containing options for wordToPdf functions
         */
        interface ConversionOptions {
        }
        interface OfficeToPDFOptions extends ConversionOptions {
        }
        /**
         * An object containing options for wordToPdf functions
         */
        class OfficeToPDFOptions implements ConversionOptions {
            /**
             * Sets the value for LayoutResourcesPluginPath in the options object
            The path at which the pdftron-provided font resource plugin resides
             * @param value - The path at which the pdftron-provided font resource plugin resides
             * @returns this object, for call chaining
             */
            setLayoutResourcesPluginPath(value: string): PDFNet.Convert.OfficeToPDFOptions;
            /**
             * Sets the value for ResourceDocPath in the options object
            The path at which the a .docx resource document resides
             * @param value - The path at which the a .docx resource document resides
             * @returns this object, for call chaining
             */
            setResourceDocPath(value: string): PDFNet.Convert.OfficeToPDFOptions;
            /**
             * Sets the value for SmartSubstitutionPluginPath in the options object
            The path at which the pdftron-provided font resource plugin resides
             * @param value - The path at which the pdftron-provided font resource plugin resides
             * @returns this object, for call chaining
             */
            setSmartSubstitutionPluginPath(value: string): PDFNet.Convert.OfficeToPDFOptions;
            /**
             * @returns this object, for call chaining
             */
            setExcelDefaultCellBorderWidth(width: number): PDFNet.Convert.OfficeToPDFOptions;
        }
        enum OverprintPreviewMode {
            e_op_off,
            e_op_on,
            e_op_pdfx_on
        }
        /**
         * An object containing options common to ToXps and ToXod functions
         */
        class XPSOutputCommonOptions {
            /**
             * Sets whether ToXps should be run in print mode.
            Default is false.
             * @param print_mode - if true print mode is enabled
             * @returns this object, for call chaining
             */
            setPrintMode(print_mode: boolean): PDFNet.Convert.XPSOutputCommonOptions;
            /**
             * The output resolution, from 1 to 1000, in Dots Per Inch (DPI) at which to
            render elements which cannot be directly converted.
            Default is 140.
             * @param dpi - the resolution in Dots Per Inch
             * @returns this object, for call chaining
             */
            setDPI(dpi: number): PDFNet.Convert.XPSOutputCommonOptions;
            /**
             * Sets whether rendering of pages should be permitted when necessary to guarantee output.
            Default is true.
             * @param render - if false rendering is not permitted under any circumstance
             * @returns this object, for call chaining
             */
            setRenderPages(render: boolean): PDFNet.Convert.XPSOutputCommonOptions;
            /**
             * Sets whether thin lines should be thickened.
            Default is true for XPS and false for XOD.
             * @param thicken - if true then thin lines will be thickened
             * @returns this object, for call chaining
             */
            setThickenLines(thicken: boolean): PDFNet.Convert.XPSOutputCommonOptions;
            /**
             * Sets whether links should be generated from urls found in the document.
            Default is false.
             * @param generate - if true links will be generated from urls
             * @returns this object, for call chaining
             */
            generateURLLinks(generate: boolean): PDFNet.Convert.XPSOutputCommonOptions;
            /**
             * Enable or disable support for overprint and overprint simulation.
            Overprint is a device dependent feature and the results will vary depending on
            the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc).
            Default is e_op_pdfx_on.
             * @param mode - <pre>
            PDFNet.Convert.OverprintPreviewMode = {
                e_op_off : 0
                e_op_on : 1
                e_op_pdfx_on : 2
            }
            </pre>
            e_op_on: always enabled; e_op_off: always disabled; e_op_pdfx_on: enabled for PDF/X files only.
             * @returns this object, for call chaining
             */
            setOverprint(mode: number): PDFNet.Convert.XPSOutputCommonOptions;
        }
        /**
         * An object containing options for ToXps functions
         */
        class XPSOutputOptions extends PDFNet.Convert.XPSOutputCommonOptions {
            /**
             * Sets whether the output format should be open xps.
            Default is Microsoft xps output.
             * @param openxps - if true open xps output is used
             * @returns this object, for call chaining
             */
            setOpenXps(openxps: boolean): PDFNet.Convert.XPSOutputOptions;
        }
        enum FlattenFlag {
            e_off,
            e_simple,
            e_fast,
            e_high_quality
        }
        enum FlattenThresholdFlag {
            e_very_strict,
            e_strict,
            e_default,
            e_keep_most,
            e_keep_all
        }
        enum AnnotationOutputFlag {
            e_internal_xfdf,
            e_external_xfdf,
            e_flatten
        }
        /**
         * An object containing options for ToXod functions
         */
        class XODOutputOptions extends PDFNet.Convert.XPSOutputCommonOptions {
            /**
             * Sets whether per page thumbnails should be included in the file.
            Default is true.
             * @param include_thumbs - if true thumbnails will be included
             * @returns this object, for call chaining
             */
            setOutputThumbnails(include_thumbs: boolean): PDFNet.Convert.XODOutputOptions;
            /**
             * Sets whether per page thumbnails should be included in the file.
            Default is true.
             * @param include_thumbs - if true thumbnails will be included
             * @returns this object, for call chaining
             */
            setOutputThumbnails(include_thumbs: boolean): PDFNet.Convert.XODOutputOptions;
            /**
             * The width and height of a square in which thumbnails will be contained.
            Default is 400 for normal pages and 1500 for large pages.
             * @param regular_size - the maximum dimension that thumbnails for regular size pages will have.
             * @param [large_size = regular_size] - the maximum dimension that thumbnails for large pages will have.
             * @returns this object, for call chaining
             */
            setThumbnailSize(regular_size: number, large_size?: number): PDFNet.Convert.XODOutputOptions;
            /**
             * If rendering is permitted, sets the maximum number of page elements before that page will be rendered.
            Default is 2000000000 which will never cause pages to be completely rendered in this manner.
             * @param element_limit - the maximum number of elements before a given page will be rendered
             * @returns this object, for call chaining
             */
            setElementLimit(element_limit: number): PDFNet.Convert.XODOutputOptions;
            /**
             * If rendering is permitted, sets whether pages containing opacity masks should be rendered.
            This option is used as a workaround to a bug in Silverlight where opacity masks are transformed incorrectly.
            Default is false.
             * @param opacity_render - if true pages with opacity masks will be rendered
             * @returns this object, for call chaining
             */
            setOpacityMaskWorkaround(opacity_render: boolean): PDFNet.Convert.XODOutputOptions;
            /**
             * Specifies the maximum image slice size in pixels.
            Default is 2000000.
             * @param max_pixels - the maximum number of pixels an image can have
             * @returns this object, for call chaining
             */
            setMaximumImagePixels(max_pixels: number): PDFNet.Convert.XODOutputOptions;
            /**
             * Flatten images and paths into a single background image overlaid with
            vector text. This option can be used to improve speed on devices with
            little processing power such as iPads. Default is e_high_quality.
             * @param flatten - <pre>
            PDFNet.Convert.FlattenFlag = {
                e_off : 0
                e_simple : 1
                e_fast : 2
                e_high_quality : 3
            }
            </pre>
             * @returns this object, for call chaining
             */
            setFlattenContent(flatten: number): PDFNet.Convert.XODOutputOptions;
            /**
             * Used to control how precise or relaxed text flattening is. When some text is
            preserved (not flattened to image) the visual appearance of the document may be altered.
             * @param threshold - <pre>
            PDFNet.PDFNet.Convert.FlattenThresholdFlag = {
                e_very_strict : 0
                e_strict : 1
                e_default : 2
                e_keep_most : 3
                e_keep_all : 4
            }
            </pre>
             * @returns this object, for call chaining
             */
            setFlattenThreshold(threshold: number): PDFNet.Convert.XODOutputOptions;
            /**
             * Where possible output JPG files rather than PNG. This will apply to both thumbnails and document images.
            Default is true.
             * @param prefer_jpg - if true JPG images will be used whenever possible.
             * @returns this object, for call chaining
             */
            setPreferJPG(prefer_jpg: boolean): PDFNet.Convert.XODOutputOptions;
            /**
             * Specifies the compression quality to use when generating JPEG images.
             * @param quality - the JPEG compression quality, from 0(highest compression) to 100(best quality).
             * @returns this object, for call chaining
             */
            setJPGQuality(quality: number): PDFNet.Convert.XODOutputOptions;
            /**
             * Outputs rotated text as paths. This option is used as a workaround to a bug in Silverlight
            where pages with rotated text could cause the plugin to crash.
            Default is false.
             * @param workaround - if true rotated text will be changed to paths
             * @returns this object, for call chaining
             */
            setSilverlightTextWorkaround(workaround: boolean): PDFNet.Convert.XODOutputOptions;
            /**
             * Choose how to output annotations.
            Default is e_internal_xfdf.
             * @param annot_output - <pre>
            PDFNet.PDFNet.Convert.AnnotationOutputFlag = {
                e_internal_xfdf : 0
                e_external_xfdf : 1
                e_flatten : 2
            }
            </pre>
             * @returns this object, for call chaining
             */
            setAnnotationOutput(annot_output: number): PDFNet.Convert.XODOutputOptions;
            /**
             * Output XOD as a collection of loose files rather than a zip archive.
            This option should be used when using the external part retriever in Webviewer.
            Default is false.
             * @param generate - if true XOD is output as a collection of loose files
             * @returns this object, for call chaining
             */
            setExternalParts(generate: boolean): PDFNet.Convert.XODOutputOptions;
            /**
             * Encrypt XOD parts with AES 128 encryption using the supplied password.
            This option is not available when using setExternalParts(true)
             * @param pass - the encryption password
             * @returns this object, for call chaining
             */
            setEncryptPassword(pass: string): PDFNet.Convert.XODOutputOptions;
            /**
             * The latest XOD format is only partially supported in Silverlight and Flash
            due to various optimizations in the text format and the addition of blend mode support.
            This option forces the converter to use an older version of XOD that is Silverlight/Flash
            compatible but does not have these improvements.
            By default the latest XOD format is generated.
             * @param compatible - if true will use the older XOD format which is not compatible with Silverlight/Flash
             * @returns this object, for call chaining
             */
            useSilverlightFlashCompatible(compatible: boolean): PDFNet.Convert.XODOutputOptions;
        }
        /**
         * An object containing options for ToTiff functions
         */
        class TiffOutputOptions {
            /**
             * Specifies the page box/region to rasterize.
            By default, page crop region will be rasterized.
             * @param type - <pre>
            PDFNet.Page.Box = {
                e_media : 0
                e_crop : 1
                e_bleed : 2
                e_trim : 3
                e_art : 4
            }
            </pre>
             * @returns this object, for call chaining
             */
            setBox(type: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * Rotates all pages by a given number of degrees counterclockwise. The allowed
            values are 0, 90, 180, and 270. The default value is 0.
             * @param rotation - <pre>
            PDFNet.Page.Rotate = {
                e_0 : 0
                e_90 : 1
                e_180 : 2
                e_270 : 3
            }
            </pre>
             * @returns this object, for call chaining
             */
            setRotate(rotation: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * User definable clip box.
            By default, the clip region is identical to current page 'box'.
             * @returns this object, for call chaining
             */
            setClip(x1: number, y1: number, x2: number, y2: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * Specifies the list of pages to convert.
            By default, all pages are converted.
             * @returns this object, for call chaining
             */
            setPages(page_desc: string): PDFNet.Convert.TiffOutputOptions;
            /**
             * Enable or disable support for overprint and overprint simulation.
            Overprint is a device dependent feature and the results will vary depending on
            the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc).
            Default is e_op_pdfx_on.
             * @param mode - <pre>
            PDFNet.PDFRasterizer.OverprintPreviewMode = {
                e_op_off : 0
                e_op_on : 1
                e_op_pdfx_on : 2
            }
            </pre>
             * @returns this object, for call chaining
             */
            setOverprint(mode: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * Render and export the image in CMYK mode.
            By default, the image is rendered and exported in RGB color space.
             * @returns this object, for call chaining
             */
            setCMYK(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Enables dithering when the image is exported in palletized or monochrome mode.
            This option is disabled by default.
             * @returns this object, for call chaining
             */
            setDither(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Render and export the image in grayscale mode. Sets pixel format to 8 bits per pixel grayscale.
            By default, the image is rendered and exported in RGB color space.
             * @returns this object, for call chaining
             */
            setGray(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Export the rendered image as 1 bit per pixel (monochrome) image. The image will be
            compressed using G4 CCITT compression algorithm. By default, the image is not dithered.
            To enable dithering use 'SetDither' option.
            This option is disabled by default.
             * @returns this object, for call chaining
             */
            setMono(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Enables or disables drawing of annotations.
            This option is enabled by default.
             * @returns this object, for call chaining
             */
            setAnnots(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Enables or disables image smoothing (default: enabled).
             * @returns this object, for call chaining
             */
            setSmooth(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Renders annotations in the print mode. This option can be used to render 'Print Only'
            annotations and to hide 'Screen Only' annotations.
            This option is disabled by default.
             * @returns this object, for call chaining
             */
            setPrintmode(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Sets the page color to transparent. By default, Convert assumes that the page
            is drawn directly on an opaque white surface. Some applications may need to
            draw the page on a different backdrop. In this case any pixels that are not
            covered during rendering will be transparent.
            This option is disabled by default.
             * @returns this object, for call chaining
             */
            setTransparentPage(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * Enabled the output of palettized TIFFs.
            This option is disabled by default.
             * @returns this object, for call chaining
             */
            setPalettized(enable: boolean): PDFNet.Convert.TiffOutputOptions;
            /**
             * The output resolution, from 1 to 1000, in Dots Per Inch (DPI). The higher
            the DPI, the larger the image. Resolutions larger than 1000 DPI can be
            achieved by rendering image in tiles or stripes. The default resolution is 92 DPI.
             * @returns this object, for call chaining
             */
            setDPI(dpi: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * Sets the gamma factor used for anti-aliased rendering. Typical values are in
            the range from 0.1 to 3. Gamma correction can be used to improve the quality
            of anti-aliased image output and can (to some extent) decrease the appearance
            common anti-aliasing artifacts (such as pixel width lines between polygons).
            The default gamma is 0.
             * @returns this object, for call chaining
             */
            setGamma(gamma: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * Sets the width of the output image, in pixels.
             * @returns this object, for call chaining
             */
            setHRes(hres: number): PDFNet.Convert.TiffOutputOptions;
            /**
             * Sets the height of the output image, in pixels.
             * @returns this object, for call chaining
             */
            setVRes(vres: number): PDFNet.Convert.TiffOutputOptions;
        }
        /**
         * An object containing options common to ToHtml and ToEpub functions
         */
        class HTMLOutputOptions {
            /**
             * Use JPG files rather than PNG. This will apply to all generated images.
            Default is true.
             * @param prefer_jpg - if true JPG images will be used whenever possible.
             * @returns this object, for call chaining
             */
            setPreferJPG(prefer_jpg: boolean): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Specifies the compression quality to use when generating JPEG images.
             * @param quality - the JPEG compression quality, from 0(highest compression) to 100(best quality).
             * @returns this object, for call chaining
             */
            setJPGQuality(quality: number): PDFNet.Convert.HTMLOutputOptions;
            /**
             * The output resolution, from 1 to 1000, in Dots Per Inch (DPI) at which to render elements which cannot be directly converted.
            Default is 140.
             * @param dpi - the resolution in Dots Per Inch
             * @returns this object, for call chaining
             */
            setDPI(dpi: number): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Specifies the maximum image slice size in pixels. Default is 2000000.
             * @param max_pixels - the maximum number of pixels an image can have
             * @returns this object, for call chaining
             */
            setMaximumImagePixels(max_pixels: number): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Switch between fixed (pre-paginated) and reflowable HTML generation.
            Default is false.
             * @param reflow - if true, generated HTML will be reflowable, otherwise, fixed positioning will be used
             * @returns this object, for call chaining
             */
            setReflow(reflow: boolean): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Set an overall scaling of the generated HTML pages. Default is 1.0.
             * @param scale - a number greater than 0 which is used as a scale factor.
            For example, calling SetScale(0.5) will reduce the HTML body of the page to half
            its original size, whereas SetScale(2) will double the HTML body dimensions of
            the page and will rescale all page content appropriately.
             * @returns this object, for call chaining
             */
            setScale(scale: number): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Enable the conversion of external URL navigation.
            Default is false.
             * @param enable - if true, links that specify external URL's are converted into HTML.
             * @returns this object, for call chaining
             */
            setExternalLinks(enable: boolean): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Enable the conversion of internal document navigation.
            Default is false.
             * @param enable - if true, links that specify page jumps are converted into HTML.
             * @returns this object, for call chaining
             */
            setInternalLinks(enable: boolean): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Controls whether converter optimizes DOM or preserves text placement accuracy.
            Default is false.
             * @param enable - If true, converter will try to reduce DOM complexity at the expense of text placement accuracy.
             * @returns this object, for call chaining
             */
            setSimplifyText(enable: boolean): PDFNet.Convert.HTMLOutputOptions;
            /**
             * Generate a XML file that contains additional information about the conversion process.
            By default no report is generated.
             * @param path - The file path to which the XML report is written to.
             * @returns this object, for call chaining
             */
            setReportFile(path: string): PDFNet.Convert.HTMLOutputOptions;
        }
        /**
         * An object containing options common to ToEpub functions
         */
        class EPUBOutputOptions {
            /**
             * Create the EPUB in expanded format.
            Default is false.
             * @param expanded - if false a single EPUB file will be generated,
            otherwise, the generated EPUB will be in unzipped (expanded) format
             * @returns this object, for call chaining
             */
            setExpanded(expanded: boolean): PDFNet.Convert.EPUBOutputOptions;
            /**
             * Set whether the first content page in the EPUB uses the cover image or not. If this
            is set to true, then the first content page will simply wrap the cover image in HTML.
            Otherwise, the page will be converted the same as all other pages in the EPUB.
            Default is false.
             * @param reuse - if true the first page will simply be EPUB cover image,
            otherwise, the first page will be converted the same as the other pages
             * @returns this object, for call chaining
             */
            setReuseCover(reuse: boolean): PDFNet.Convert.EPUBOutputOptions;
        }
        /**
         * An object containing options for ToSvg functions
         */
        class SVGOutputOptions {
            /**
             * Sets whether to embed all images
             * @param embed_images - if true, images will be embedded.
            Default is false.
             * @returns this object, for call chaining
             */
            setEmbedImages(embed_images: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Sets whether to disable conversion of font data to SVG
             * @param no_fonts - if true, font data conversion is disabled.
            Default is false.
             * @returns this object, for call chaining
             */
            setNoFonts(no_fonts: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Sets whether to convert all fonts to SVG or not.
             * @param svg_fonts - if true, fonts are converted to SVG. Otherwise they are converted to OpenType.
            Default is false.
             * @returns this object, for call chaining
             */
            setSvgFonts(svg_fonts: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Sets whether to embed fonts into each SVG page file, or to have them shared.
             * @param embed_fonts - if true, fonts are injected into each SVG page.
            Otherwise they are created as separate files that are shared between SVG pages.
            Default is false.
             * @returns this object, for call chaining
             */
            setEmbedFonts(embed_fonts: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Sets whether to disable mapping of text to public Unicode region.
            Instead text will be converted using a custom encoding
             * @param no_unicode - if true, mapping of text to public Unicode region is disabled
             * @returns this object, for call chaining
             */
            setNoUnicode(no_unicode: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Some viewers do not support the default text positioning correctly.
            This option works around this issue to place text correctly,
            but produces verbose output. This option will override SetRemoveCharPlacement
             * @param individual_char_placement - if true, text will be positioned correctly
             * @returns this object, for call chaining
             */
            setIndividualCharPlacement(individual_char_placement: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Sets whether to disable the output of character positions.
            This will produce slightly smaller output files than the default
            setting, but many viewers do not support the output correctly
             * @param remove_char_placement - if true, the output of character positions is disabled
             * @returns this object, for call chaining
             */
            setRemoveCharPlacement(remove_char_placement: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Flatten images and paths into a single background image overlaid with
            vector text. This option can be used to improve speed on devices with
            little processing power such as iPads. Default is e_fast.
             * @param flatten - <pre>
            PDFNet.Convert.FlattenFlag = {
                e_off : 0
                e_simple : 1
                e_fast : 2
                e_high_quality : 3
            }
            </pre>
             * @returns this object, for call chaining
             */
            setFlattenContent(flatten: number): PDFNet.Convert.SVGOutputOptions;
            /**
             * Used to control how precise or relaxed text flattening is. When some text is
            preserved (not flattened to image) the visual appearance of the document may be altered.
             * @param threshold - <pre>
            PDFNet.PDFNet.Convert.FlattenThresholdFlag = {
                e_very_strict : 0
                e_strict : 1
                e_default : 2
                e_keep_most : 3
                e_keep_all : 4
            }
            </pre>
             * @returns this object, for call chaining
             */
            setFlattenThreshold(threshold: number): PDFNet.Convert.SVGOutputOptions;
            /**
             * The output resolution, from 1 to 1000, in Dots Per Inch (DPI) at which to render elements which cannot be directly converted.
            Default is 140.
             * @param dpi - the resolution in Dots Per Inch
             * @returns this object, for call chaining
             */
            setFlattenDPI(dpi: number): PDFNet.Convert.SVGOutputOptions;
            /**
             * Specifies the maximum image slice size in pixels. Default is 2000000.
             * @param max_pixels - the maximum number of pixels an image can have
             * @returns this object, for call chaining
             */
            setFlattenMaximumImagePixels(max_pixels: number): PDFNet.Convert.SVGOutputOptions;
            /**
             * Compress output SVG files using SVGZ.
             * @param svgz - if true, SVG files are written in compressed format.
            Default is false.
             * @returns this object, for call chaining
             */
            setCompress(svgz: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Sets whether per page thumbnails should be included in the file.
            Default is true.
             * @param include_thumbs - if true thumbnails will be included
             * @returns this object, for call chaining
             */
            setOutputThumbnails(include_thumbs: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * The maximum dimension for thumbnails.
             * @param size - the maximum dimension (width or height) that thumbnails will have.
            Default is 400.
             * @returns this object, for call chaining
             */
            setThumbnailSize(size: number): PDFNet.Convert.SVGOutputOptions;
            /**
             * Create a XML document that contains metadata of the SVG document created.
             * @param xml - if true, XML wrapper is created. Default is true.
             * @returns this object, for call chaining
             */
            setCreateXmlWrapper(xml: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Set whether the DTD declaration is included in the SVG files.
             * @param dtd - if false, no DTD is added to SVG files.
            Default is true.
             * @returns this object, for call chaining
             */
            setDtd(dtd: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Control generation of form fields and annotations in SVG.
             * @param annots - if false, no form fields or annotations are converted.
            Default is true
             * @returns this object, for call chaining
             */
            setAnnots(annots: boolean): PDFNet.Convert.SVGOutputOptions;
            /**
             * Enable or disable support for overprint and overprint simulation.
            Overprint is a device dependent feature and the results will vary depending on
            the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc).
            Default is e_op_pdfx_on.
             * @param op - <pre>
            PDFNet.PDFRasterizer.OverprintPreviewMode = {
                e_op_off : 0
                e_op_on : 1
                e_op_pdfx_on : 2
            }
            </pre>
            e_op_on: always enabled; e_op_off: always disabled; e_op_pdfx_on: enabled for PDF/X files only.
             * @returns this object, for call chaining
             */
            setOverprint(op: number): PDFNet.Convert.SVGOutputOptions;
        }
        /**
         * An object containing options for fromCAD functions
         */
        class CADConvertOptions {
            /**
             * Sets the value for AutoRotate in the options object
            Maps the orientation to the sheet dimensions
             * @param value - the new value for AutoRotate
             * @returns this object, for call chaining
             */
            setAutoRotate(value: boolean): PDFNet.Convert.CADConvertOptions;
            /**
             * Sets the value for LineWeight in the options object
            Sets line weight option for cad conversion to true
             * @param value - the new value for LineWeight
             * @returns this object, for call chaining
             */
            setLineWeight(value: boolean): PDFNet.Convert.CADConvertOptions;
            /**
             * Sets the value for PageHeight in the options object
            The height of the output pdf, in millimeters
             * @param value - the new value for PageHeight
             * @returns this object, for call chaining
             */
            setPageHeight(value: number): PDFNet.Convert.CADConvertOptions;
            /**
             * Sets the value for PageWidth in the options object
            The width of the output pdf, in millimeters
             * @param value - the new value for PageWidth
             * @returns this object, for call chaining
             */
            setPageWidth(value: number): PDFNet.Convert.CADConvertOptions;
            /**
             * Sets the value for RasterDPI in the options object
            Rasterization dpi used when rendering 3D content. Currently only applies to .rvt conversions
             * @param value - the new value for RasterDPI
             * @returns this object, for call chaining
             */
            setRasterDPI(value: number): PDFNet.Convert.CADConvertOptions;
            /**
             * Adds new sheets to the Sheets array
            The list of sheets to be converted -- only applies to .rvt conversions
             * @param value - the sheets name to be added to Sheets
             * @returns this object, for call chaining
             */
            addSheets(value: string): PDFNet.Convert.CADConvertOptions;
        }
        enum PrinterMode {
            e_auto,
            e_interop_only,
            e_printer_only,
            e_prefer_builtin_converter
        }
    }
    namespace OCRModule {
        /**
         * An object containing options for OCRModule functions
         */
        class OCROptions {
            /**
             * Adds a collection of ignorable regions for the given page,
            an optional list of page areas not to be included in analysis
             * @param regions - the zones to be added to the ignore list
             * @param page_num - the page number the added regions belong to
             * @returns this object, for call chaining
             */
            addIgnoreZonesForPage(regions: PDFNet.Rect[], page_num: number): PDFNet.OCRModule.OCROptions;
            /**
             * Adds a language to the list of to be considered when
            processing this document
             * @param lang - the new language to be added to the language list
             * @returns this object, for call chaining
             */
            addLang(lang: string): PDFNet.OCRModule.OCROptions;
            /**
             * Adds a collection of text regions of interest for the given page,
            an optional list of known text zones that will be used to improve OCR quality
             * @param regions - the zones to be added to the text region list
             * @param page_num - the page number the added regions belong to
             * @returns this object, for call chaining
             */
            addTextZonesForPage(regions: PDFNet.Rect[], page_num: number): PDFNet.OCRModule.OCROptions;
            /**
             * Knowing proper image resolution is important,
             * as it enables the OCR engine to translate pixel heights of characters to their respective font sizes.
             * We do our best to retrieve resolution information from the input's metadata, however it occasionally can be corrupt or missing.
             * Hence we allow manual override of source's resolution, which supersedes any metadata found (both explicit as in image metadata and implicit as in PDF).
             * @param value - image resolution
             * @returns this object, for call chaining
             */
            addDPI(value: number): PDFNet.OCRModule.OCROptions;
        }
    }
    namespace PDFDoc {
        /**
         * An object containing options for PDFDoc.saveViewerOptimized function
         */
        class ViewerOptimizedOptions {
            /**
             * For any pages that are not forced to include thumbnails this
            function adjusts whether we should include them depending on the
            complexity of the page. This can be used to include fewer or more thumbnails
            as required by the use case. In particular reducing this value
            will tend to increase the number of page thumbnails included and vice versa.
             * @param threshold - A number from 0 (include all thumbnails) to 100
            (include only the first thumbnail) representing the complexity at which
            SaveViewerOptimized would include the thumbnail. The default value is 50.
             * @returns this object, for call chaining
             */
            setThumbnailRenderingThreshold(threshold: number): PDFNet.PDFDoc.ViewerOptimizedOptions;
            /**
             * The maximum allowed length for the thumbnail's height/width.
            The default thumbnail size is 1024.
             * @param size - the maximum dimension (width or height) that thumbnails will have.
             * @returns this object, for call chaining
             */
            setThumbnailSize(size: number): PDFNet.PDFDoc.ViewerOptimizedOptions;
            /**
             * Enable or disable support for overprint and overprint simulation in generated thumbnails.
            Overprint is a device dependent feature and the results will vary depending on
            the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc).
            Default is e_op_pdfx_on.
             * @param mode - <pre>
            PDFNet.PDFRasterizer.OverprintPreviewMode = {
                e_op_off : 0
                e_op_on : 1
                e_op_pdfx_on : 2
            }
            </pre>
            e_op_on: always enabled; e_op_off: always disabled; e_op_pdfx_on: enabled for PDF/X files only.
             * @returns this object, for call chaining
             */
            setOverprint(mode: number): PDFNet.PDFDoc.ViewerOptimizedOptions;
        }
        enum EventType {
            e_action_trigger_doc_will_close,
            e_action_trigger_doc_will_save,
            e_action_trigger_doc_did_save,
            e_action_trigger_doc_will_print,
            e_action_trigger_doc_did_print
        }
        enum InsertFlag {
            e_none,
            e_insert_bookmark
        }
        enum ExtractFlag {
            e_forms_only,
            e_annots_only,
            e_both
        }
        enum SignaturesVerificationStatus {
            e_unsigned,
            e_failure,
            e_untrusted,
            e_unsupported,
            e_verified
        }
    }
    namespace PrinterMode {
        enum PaperSize {
            e_custom,
            e_letter,
            e_letter_small,
            e_tabloid,
            e_ledger,
            e_legal,
            e_statement,
            e_executive,
            e_a3,
            e_a4,
            e_a4_mall,
            e_a5,
            e_b4_jis,
            e_b5_jis,
            e_folio,
            e_quarto,
            e_10x14,
            e_11x17,
            e_note,
            e_envelope_9,
            e_envelope_10,
            e_envelope_11,
            e_envelope_12,
            e_envelope_14,
            e_c_size_sheet,
            e_d_size_sheet,
            e_e_size_sheet,
            e_envelope_dl,
            e_envelope_c5,
            e_envelope_c3,
            e_envelope_c4,
            e_envelope_c6,
            e_envelope_c65,
            e_envelope_b4,
            e_envelope_b5,
            e_envelope_b6,
            e_envelope_italy,
            e_envelope_monarch,
            e_6_3_quarters_envelope,
            e_us_std_fanfold,
            e_german_std_fanfold,
            e_german_legal_fanfold,
            e_b4_iso,
            e_japanese_postcard,
            e_9x11,
            e_10x11,
            e_15x11,
            e_envelope_invite,
            e_reserved_48,
            e_reserved_49,
            e_letter_extra,
            e_legal_extra,
            e_tabloid_extra,
            e_a4_extra,
            e_letter_transverse,
            e_a4_transverse,
            e_letter_extra_transverse,
            e_supera_supera_a4,
            e_Superb_Superb_a3,
            e_letter_plus,
            e_a4_plus,
            e_a5_transverse,
            e_b5_jis_transverse,
            e_a3_extra,
            e_a5_extra,
            e_b5_iso_extra,
            e_a2,
            e_a3_transverse,
            e_a3_extra_transverse,
            e_japanese_double_postcard,
            e_a6,
            e_japanese_envelope_kaku_2,
            e_japanese_envelope_kaku_3,
            e_japanese_envelope_chou_3,
            e_japanese_envelope_chou_4,
            e_letter_rotated,
            e_a3_rotated,
            e_a4_rotated,
            e_a5_rotated,
            e_b4_jis_rotated,
            e_b5_jis_rotated,
            e_japanese_postcard_rotated,
            e_double_japanese_postcard_rotated,
            e_a6_rotated,
            e_japanese_envelope_kaku_2_rotated,
            e_japanese_envelope_kaku_3_rotated,
            e_japanese_envelope_chou_3_rotated,
            e_japanese_envelope_chou_4_rotated,
            e_b6_jis,
            e_b6_jis_rotated,
            e_12x11,
            e_japanese_envelope_you_4,
            e_japanese_envelope_you_4_rotated,
            e_PrinterMode_prc_16k,
            e_prc_32k,
            e_prc_32k_big,
            e_prc_envelop_1,
            e_prc_envelop_2,
            e_prc_envelop_3,
            e_prc_envelop_4,
            e_prc_envelop_5,
            e_prc_envelop_6,
            e_prc_envelop_7,
            e_prc_envelop_8,
            e_prc_envelop_9,
            e_prc_envelop_10,
            e_prc_16k_rotated,
            e_prc_32k_rotated,
            e_prc_32k_big__rotated,
            e_prc_envelop_1_rotated,
            e_prc_envelop_2_rotated,
            e_prc_envelop_3_rotated,
            e_prc_envelop_4_rotated,
            e_prc_envelop_5_rotated,
            e_prc_envelop_6_rotated,
            e_prc_envelop_7_rotated,
            e_prc_envelop_8_rotated,
            e_prc_envelop_9_rotated,
            e_prc_envelop_10_rotated
        }
    }
    namespace Field {
        enum EventType {
            e_action_trigger_keystroke,
            e_action_trigger_format,
            e_action_trigger_validate,
            e_action_trigger_calculate
        }
        enum Type {
            e_button,
            e_check,
            e_radio,
            e_text,
            e_choice,
            e_signature,
            e_null
        }
        enum Flag {
            e_read_only,
            e_required,
            e_no_export,
            e_pushbutton_flag,
            e_radio_flag,
            e_toggle_to_off,
            e_radios_in_unison,
            e_multiline,
            e_password,
            e_file_select,
            e_no_spellcheck,
            e_no_scroll,
            e_comb,
            e_rich_text,
            e_combo,
            e_edit,
            e_sort,
            e_multiselect,
            e_commit_on_sel_change
        }
        enum TextJustification {
            e_left_justified,
            e_centered,
            e_right_justified
        }
    }
    namespace Filter {
        enum StdFileOpenMode {
            e_read_mode,
            e_write_mode,
            e_append_mode
        }
        enum ReferencePos {
            e_begin,
            e_end,
            e_cur
        }
    }
    namespace OCGContext {
        enum OCDrawMode {
            e_VisibleOC,
            e_AllOC,
            e_NoOC
        }
    }
    namespace OCMD {
        enum VisibilityPolicyType {
            e_AllOn,
            e_AnyOn,
            e_AnyOff,
            e_AllOff
        }
    }
    namespace PDFACompliance {
        enum Conformance {
            e_Level1A,
            e_Level1B,
            e_Level2A,
            e_Level2B,
            e_Level2U,
            e_Level3A,
            e_Level3B,
            e_Level3U
        }
        enum ErrorCode {
            e_PDFA0_1_0,
            e_PDFA0_1_1,
            e_PDFA0_1_2,
            e_PDFA0_1_3,
            e_PDFA0_1_4,
            e_PDFA0_1_5,
            e_PDFA1_2_1,
            e_PDFA1_2_2,
            e_PDFA1_3_1,
            e_PDFA1_3_2,
            e_PDFA1_3_3,
            e_PDFA1_3_4,
            e_PDFA1_4_1,
            e_PDFA1_4_2,
            e_PDFA1_6_1,
            e_PDFA1_7_1,
            e_PDFA1_7_2,
            e_PDFA1_7_3,
            e_PDFA1_7_4,
            e_PDFA1_8_1,
            e_PDFA1_8_2,
            e_PDFA1_8_3,
            e_PDFA1_8_4,
            e_PDFA1_8_5,
            e_PDFA1_8_6,
            e_PDFA1_10_1,
            e_PDFA1_11_1,
            e_PDFA1_11_2,
            e_PDFA1_12_1,
            e_PDFA1_12_2,
            e_PDFA1_12_3,
            e_PDFA1_12_4,
            e_PDFA1_12_5,
            e_PDFA1_12_6,
            e_PDFA1_13_1,
            e_PDFA2_2_1,
            e_PDFA2_3_2,
            e_PDFA2_3_3,
            e_PDFA2_3_3_1,
            e_PDFA2_3_3_2,
            e_PDFA2_3_4_1,
            e_PDFA2_4_1,
            e_PDFA2_4_2,
            e_PDFA2_4_3,
            e_PDFA2_4_4,
            e_PDFA2_5_1,
            e_PDFA2_5_2,
            e_PDFA2_6_1,
            e_PDFA2_7_1,
            e_PDFA2_8_1,
            e_PDFA2_9_1,
            e_PDFA2_10_1,
            e_PDFA3_2_1,
            e_PDFA3_3_1,
            e_PDFA3_3_2,
            e_PDFA3_3_3_1,
            e_PDFA3_3_3_2,
            e_PDFA3_4_1,
            e_PDFA3_5_1,
            e_PDFA3_5_2,
            e_PDFA3_5_3,
            e_PDFA3_5_4,
            e_PDFA3_5_5,
            e_PDFA3_5_6,
            e_PDFA3_6_1,
            e_PDFA3_7_1,
            e_PDFA3_7_2,
            e_PDFA3_7_3,
            e_PDFA4_1,
            e_PDFA4_2,
            e_PDFA4_3,
            e_PDFA4_4,
            e_PDFA4_5,
            e_PDFA4_6,
            e_PDFA5_2_1,
            e_PDFA5_2_2,
            e_PDFA5_2_3,
            e_PDFA5_2_4,
            e_PDFA5_2_5,
            e_PDFA5_2_6,
            e_PDFA5_2_7,
            e_PDFA5_2_8,
            e_PDFA5_2_9,
            e_PDFA5_2_10,
            e_PDFA5_2_11,
            e_PDFA5_3_1,
            e_PDFA5_3_2_1,
            e_PDFA5_3_2_2,
            e_PDFA5_3_2_3,
            e_PDFA5_3_2_4,
            e_PDFA5_3_2_5,
            e_PDFA5_3_3_1,
            e_PDFA5_3_3_2,
            e_PDFA5_3_3_3,
            e_PDFA5_3_3_4,
            e_PDFA5_3_4_0,
            e_PDFA5_3_4_1,
            e_PDFA5_3_4_2,
            e_PDFA5_3_4_3,
            e_PDFA6_1_1,
            e_PDFA6_1_2,
            e_PDFA6_2_1,
            e_PDFA6_2_2,
            e_PDFA6_2_3,
            e_PDFA7_2_1,
            e_PDFA7_2_2,
            e_PDFA7_2_3,
            e_PDFA7_2_4,
            e_PDFA7_2_5,
            e_PDFA7_3_1,
            e_PDFA7_3_2,
            e_PDFA7_3_3,
            e_PDFA7_3_4,
            e_PDFA7_3_5,
            e_PDFA7_3_6,
            e_PDFA7_3_7,
            e_PDFA7_3_8,
            e_PDFA7_3_9,
            e_PDFA7_5_1,
            e_PDFA7_8_1,
            e_PDFA7_8_2,
            e_PDFA7_8_3,
            e_PDFA7_8_4,
            e_PDFA7_8_5,
            e_PDFA7_8_6,
            e_PDFA7_8_7,
            e_PDFA7_8_8,
            e_PDFA7_8_9,
            e_PDFA7_8_10,
            e_PDFA7_8_11,
            e_PDFA7_8_12,
            e_PDFA7_8_13,
            e_PDFA7_8_14,
            e_PDFA7_8_15,
            e_PDFA7_8_16,
            e_PDFA7_8_17,
            e_PDFA7_8_18,
            e_PDFA7_8_19,
            e_PDFA7_8_20,
            e_PDFA7_8_21,
            e_PDFA7_8_22,
            e_PDFA7_8_23,
            e_PDFA7_8_24,
            e_PDFA7_8_25,
            e_PDFA7_8_26,
            e_PDFA7_8_27,
            e_PDFA7_8_28,
            e_PDFA7_8_29,
            e_PDFA7_8_30,
            e_PDFA7_8_31,
            e_PDFA7_11_1,
            e_PDFA7_11_2,
            e_PDFA7_11_3,
            e_PDFA7_11_4,
            e_PDFA7_11_5,
            e_PDFA9_1,
            e_PDFA9_2,
            e_PDFA9_3,
            e_PDFA9_4,
            e_PDFA3_8_1,
            e_PDFA8_2_2,
            e_PDFA8_3_3_1,
            e_PDFA8_3_3_2,
            e_PDFA8_3_4_1,
            e_PDFA1_2_3,
            e_PDFA1_10_2,
            e_PDFA1_10_3,
            e_PDFA1_12_10,
            e_PDFA1_13_5,
            e_PDFA2_3_10,
            e_PDFA2_4_2_10,
            e_PDFA2_4_2_11,
            e_PDFA2_4_2_12,
            e_PDFA2_4_2_13,
            e_PDFA2_5_10,
            e_PDFA2_5_11,
            e_PDFA2_5_12,
            e_PDFA2_8_3_1,
            e_PDFA2_8_3_2,
            e_PDFA2_8_3_3,
            e_PDFA2_8_3_4,
            e_PDFA2_8_3_5,
            e_PDFA2_10_20,
            e_PDFA2_10_21,
            e_PDFA11_0_0,
            e_PDFA6_2_11_8,
            e_PDFA8_1,
            e_PDFA_3E1,
            e_PDFA_3E2,
            e_PDFA_3E3,
            e_PDFA_LAST
        }
    }
    namespace ContentItem {
        enum Type {
            e_MCR,
            e_MCID,
            e_OBJR,
            e_Unknown
        }
    }
    namespace Action {
        enum Type {
            e_GoTo,
            e_GoToR,
            e_GoToE,
            e_Launch,
            e_Thread,
            e_URI,
            e_Sound,
            e_Movie,
            e_Hide,
            e_Named,
            e_SubmitForm,
            e_ResetForm,
            e_ImportData,
            e_JavaScript,
            e_SetOCGState,
            e_Rendition,
            e_Trans,
            e_GoTo3DView,
            e_RichMediaExecute,
            e_Unknown
        }
        enum FormActionFlag {
            e_exclude,
            e_include_no_value_fields,
            e_export_format,
            e_get_method,
            e_submit_coordinates,
            e_xfdf,
            e_include_append_saves,
            e_include_annotations,
            e_submit_pdf,
            e_canonical_format,
            e_excl_non_user_annots,
            e_excl_F_key,
            e_embed_form
        }
    }
    namespace Page {
        enum EventType {
            e_action_trigger_page_open,
            e_action_trigger_page_close
        }
        enum Box {
            e_media,
            e_crop,
            e_bleed,
            e_trim,
            e_art
        }
        enum Rotate {
            e_0,
            e_90,
            e_180,
            e_270
        }
    }
    namespace Annot {
        enum EventType {
            e_action_trigger_activate,
            e_action_trigger_annot_enter,
            e_action_trigger_annot_exit,
            e_action_trigger_annot_down,
            e_action_trigger_annot_up,
            e_action_trigger_annot_focus,
            e_action_trigger_annot_blur,
            e_action_trigger_annot_page_open,
            e_action_trigger_annot_page_close,
            e_action_trigger_annot_page_visible,
            e_action_trigger_annot_page_invisible
        }
        enum Type {
            e_Text,
            e_Link,
            e_FreeText,
            e_Line,
            e_Square,
            e_Circle,
            e_Polygon,
            e_Polyline,
            e_Highlight,
            e_Underline,
            e_Squiggly,
            e_StrikeOut,
            e_Stamp,
            e_Caret,
            e_Ink,
            e_Popup,
            e_FileAttachment,
            e_Sound,
            e_Movie,
            e_Widget,
            e_Screen,
            e_PrinterMark,
            e_TrapNet,
            e_Watermark,
            e_3D,
            e_Redact,
            e_Projection,
            e_RichMedia,
            e_Unknown
        }
        enum Flag {
            e_invisible,
            e_hidden,
            e_print,
            e_no_zoom,
            e_no_rotate,
            e_no_view,
            e_annot_read_only,
            e_locked,
            e_toggle_no_view,
            e_locked_contents
        }
        enum State {
            e_normal,
            e_rollover,
            e_down
        }
    }
    namespace AnnotBorderStyle {
        enum Style {
            e_solid,
            e_dashed,
            e_beveled,
            e_inset,
            e_underline
        }
    }
    namespace LineAnnot {
        enum EndingStyle {
            e_Square,
            e_Circle,
            e_Diamond,
            e_OpenArrow,
            e_ClosedArrow,
            e_Butt,
            e_ROpenArrow,
            e_RClosedArrow,
            e_Slash,
            e_None,
            e_Unknown
        }
        enum IntentType {
            e_LineArrow,
            e_LineDimension,
            e_null,
            e_PolygonCloud,
            e_PolyLineDimension,
            e_PolygonDimension,
            e_Unknown
        }
        enum CapPos {
            e_Inline,
            e_Top
        }
    }
    namespace FileAttachmentAnnot {
        enum Icon {
            e_Graph,
            e_PushPin,
            e_Paperclip,
            e_Tag,
            e_Unknown
        }
    }
    namespace FreeTextAnnot {
        enum IntentName {
            e_FreeText,
            e_FreeTextCallout,
            e_FreeTextTypeWriter,
            e_Unknown
        }
    }
    namespace LinkAnnot {
        enum HighlightingMode {
            e_none,
            e_invert,
            e_outline,
            e_push
        }
    }
    namespace MarkupAnnot {
        enum BorderEffect {
            e_None,
            e_Cloudy
        }
    }
    namespace RedactionAnnot {
        enum QuadForm {
            e_LeftJustified,
            e_Centered,
            e_RightJustified,
            e_None
        }
    }
    namespace RubberStampAnnot {
        enum Icon {
            e_Approved,
            e_Experimental,
            e_NotApproved,
            e_AsIs,
            e_Expired,
            e_NotForPublicRelease,
            e_Confidential,
            e_Final,
            e_Sold,
            e_Departmental,
            e_ForComment,
            e_TopSecret,
            e_ForPublicRelease,
            e_Draft,
            e_Unknown
        }
    }
    namespace ScreenAnnot {
        enum ScaleType {
            e_Anamorphic,
            e_Proportional
        }
        enum ScaleCondition {
            e_Always,
            e_WhenBigger,
            e_WhenSmaller,
            e_Never
        }
        enum IconCaptionRelation {
            e_NoIcon,
            e_NoCaption,
            e_CBelowI,
            e_CAboveI,
            e_CRightILeft,
            e_CLeftIRight,
            e_COverlayI
        }
    }
    namespace SoundAnnot {
        enum Icon {
            e_Speaker,
            e_Mic,
            e_Unknown
        }
    }
    namespace TextAnnot {
        enum Icon {
            e_Comment,
            e_Key,
            e_Help,
            e_NewParagraph,
            e_Paragraph,
            e_Insert,
            e_Note,
            e_Unknown
        }
    }
    namespace WidgetAnnot {
        enum HighlightingMode {
            e_none,
            e_invert,
            e_outline,
            e_push,
            e_toggle
        }
        enum ScaleType {
            e_Anamorphic,
            e_Proportional
        }
        enum IconCaptionRelation {
            e_NoIcon,
            e_NoCaption,
            e_CBelowI,
            e_CAboveI,
            e_CRightILeft,
            e_CLeftIRight,
            e_COverlayI
        }
        enum ScaleCondition {
            e_Always,
            e_WhenBigger,
            e_WhenSmaller,
            e_Never
        }
    }
    namespace ColorSpace {
        enum Type {
            e_device_gray,
            e_device_rgb,
            e_device_cmyk,
            e_cal_gray,
            e_cal_rgb,
            e_lab,
            e_icc,
            e_indexed,
            e_pattern,
            e_separation,
            e_device_n,
            e_null
        }
    }
    namespace DocumentConversion {
        enum Result {
            e_Success,
            e_Incomplete,
            e_Failure
        }
    }
    namespace Destination {
        enum FitType {
            e_XYZ,
            e_Fit,
            e_FitH,
            e_FitV,
            e_FitR,
            e_FitB,
            e_FitBH,
            e_FitBV
        }
    }
    namespace GState {
        enum Attribute {
            e_transform,
            e_rendering_intent,
            e_stroke_cs,
            e_stroke_color,
            e_fill_cs,
            e_fill_color,
            e_line_width,
            e_line_cap,
            e_line_join,
            e_flatness,
            e_miter_limit,
            e_dash_pattern,
            e_char_spacing,
            e_word_spacing,
            e_horizontal_scale,
            e_leading,
            e_font,
            e_font_size,
            e_text_render_mode,
            e_text_rise,
            e_text_knockout,
            e_text_pos_offset,
            e_blend_mode,
            e_opacity_fill,
            e_opacity_stroke,
            e_alpha_is_shape,
            e_soft_mask,
            e_smoothnes,
            e_auto_stoke_adjust,
            e_stroke_overprint,
            e_fill_overprint,
            e_overprint_mode,
            e_transfer_funct,
            e_BG_funct,
            e_UCR_funct,
            e_halftone,
            e_null
        }
        enum LineCap {
            e_butt_cap,
            e_round_cap,
            e_square_cap
        }
        enum LineJoin {
            e_miter_join,
            e_round_join,
            e_bevel_join
        }
        enum TextRenderingMode {
            e_fill_text,
            e_stroke_text,
            e_fill_stroke_text,
            e_invisible_text,
            e_fill_clip_text,
            e_stroke_clip_text,
            e_fill_stroke_clip_text,
            e_clip_text
        }
        enum RenderingIntent {
            e_absolute_colorimetric,
            e_relative_colorimetric,
            e_saturation,
            e_perceptual
        }
        enum BlendMode {
            e_bl_compatible,
            e_bl_normal,
            e_bl_multiply,
            e_bl_screen,
            e_bl_difference,
            e_bl_darken,
            e_bl_lighten,
            e_bl_color_dodge,
            e_bl_color_burn,
            e_bl_exclusion,
            e_bl_hard_light,
            e_bl_overlay,
            e_bl_soft_light,
            e_bl_luminosity,
            e_bl_hue,
            e_bl_saturation,
            e_bl_color
        }
    }
    namespace Element {
        enum Type {
            e_null,
            e_path,
            e_text_begin,
            e_text,
            e_text_new_line,
            e_text_end,
            e_image,
            e_inline_image,
            e_shading,
            e_form,
            e_group_begin,
            e_group_end,
            e_marked_content_begin,
            e_marked_content_end,
            e_marked_content_point
        }
        enum PathSegmentType {
            e_moveto,
            e_lineto,
            e_cubicto,
            e_conicto,
            e_rect,
            e_closepath
        }
    }
    namespace ShapedText {
        enum ShapingStatus {
            e_FullShaping,
            e_PartialShaping,
            e_NoShaping
        }
        enum FailureReason {
            e_NoFailure,
            e_UnsupportedFontType,
            e_NotIndexEncoded,
            e_FontDataNotFound
        }
    }
    namespace ElementWriter {
        enum WriteMode {
            e_underlay,
            e_overlay,
            e_replacement
        }
    }
    namespace Flattener {
        enum Threshold {
            e_very_strict,
            e_strict,
            e_default,
            e_keep_most,
            e_keep_all
        }
        enum Mode {
            e_simple,
            e_fast
        }
    }
    namespace Font {
        enum StandardType1Font {
            e_times_roman,
            e_times_bold,
            e_times_italic,
            e_times_bold_italic,
            e_helvetica,
            e_helvetica_bold,
            e_helvetica_oblique,
            e_helvetica_bold_oblique,
            e_courier,
            e_courier_bold,
            e_courier_oblique,
            e_courier_bold_oblique,
            e_symbol,
            e_zapf_dingbats,
            e_null
        }
        enum Encoding {
            e_IdentityH,
            e_Indices
        }
        enum Type {
            e_Type1,
            e_TrueType,
            e_MMType1,
            e_Type3,
            e_Type0,
            e_CIDType0,
            e_CIDType2
        }
    }
    namespace Function {
        enum Type {
            e_sampled,
            e_exponential,
            e_stitching,
            e_postscript
        }
    }
    namespace Image {
        enum InputFilter {
            e_none,
            e_jpeg,
            e_jp2,
            e_flate,
            e_g3,
            e_g4,
            e_ascii_hex
        }
    }
    namespace PageLabel {
        enum Style {
            e_decimal,
            e_roman_uppercase,
            e_roman_lowercase,
            e_alphabetic_uppercase,
            e_alphabetic_lowercase,
            e_none
        }
    }
    namespace PageSet {
        enum Filter {
            e_all,
            e_even,
            e_odd
        }
    }
    namespace PatternColor {
        enum Type {
            e_uncolored_tiling_pattern,
            e_colored_tiling_pattern,
            e_shading,
            e_null
        }
        enum TilingType {
            e_constant_spacing,
            e_no_distortion,
            e_constant_spacing_fast_fill
        }
    }
    namespace GeometryCollection {
        enum SnappingMode {
            e_DefaultSnapMode,
            e_PointOnLine,
            e_LineMidpoint,
            e_LineIntersection,
            e_PathEndpoint
        }
    }
    namespace ObjectIdentifier {
        enum Predefined {
            e_commonName,
            e_surname,
            e_countryName,
            e_localityName,
            e_stateOrProvinceName,
            e_streetAddress,
            e_organizationName,
            e_organizationalUnitName
        }
    }
    namespace DigestAlgorithm {
        enum Type {
            e_SHA1,
            e_SHA256,
            e_SHA384,
            e_SHA512,
            e_RIPEMD160,
            e_unknown_digest_algorithm
        }
    }
    namespace DigitalSignatureField {
        enum SubFilterType {
            e_adbe_x509_rsa_sha1,
            e_adbe_pkcs7_detached,
            e_adbe_pkcs7_sha1,
            e_ETSI_CAdES_detached,
            e_ETSI_RFC3161,
            e_unknown,
            e_absent
        }
        enum DocumentPermissions {
            e_no_changes_allowed,
            e_formfilling_signing_allowed,
            e_annotating_formfilling_signing_allowed,
            e_unrestricted
        }
        enum FieldPermissions {
            e_lock_all,
            e_include,
            e_exclude
        }
    }
    namespace PDFDocViewPrefs {
        enum PageMode {
            e_UseNone,
            e_UseThumbs,
            e_UseBookmarks,
            e_FullScreen,
            e_UseOC,
            e_UseAttachments
        }
        enum PageLayout {
            e_Default,
            e_SinglePage,
            e_OneColumn,
            e_TwoColumnLeft,
            e_TwoColumnRight,
            e_TwoPageLeft,
            e_TwoPageRight
        }
        enum ViewerPref {
            e_HideToolbar,
            e_HideMenubar,
            e_HideWindowUI,
            e_FitWindow,
            e_CenterWindow,
            e_DisplayDocTitle
        }
    }
    namespace PDFRasterizer {
        enum Type {
            e_BuiltIn,
            e_GDIPlus
        }
        enum OverprintPreviewMode {
            e_op_off,
            e_op_on,
            e_op_pdfx_on
        }
        enum ColorPostProcessMode {
            e_postprocess_none,
            e_postprocess_invert
        }
    }
    namespace PDFDraw {
        enum PixelFormat {
            e_rgba,
            e_bgra,
            e_rgb,
            e_bgr,
            e_gray,
            e_gray_alpha,
            e_cmyk
        }
    }
    enum CMSType {
        e_lcms,
        e_icm,
        e_no_cms
    }
    enum CharacterOrdering {
        e_Identity,
        e_Japan1,
        e_Japan2,
        e_GB1,
        e_CNS1,
        e_Korea1
    }
    enum LogLevel {
        e_LogLevel_Off,
        e_LogLevel_Fatal,
        e_LogLevel_Error,
        e_LogLevel_Warning,
        e_LogLevel_Info,
        e_LogLevel_Trace,
        e_LogLevel_Debug
    }
    namespace Shading {
        enum Type {
            e_function_shading,
            e_axial_shading,
            e_radial_shading,
            e_free_gouraud_shading,
            e_lattice_gouraud_shading,
            e_coons_shading,
            e_tensor_shading,
            e_null
        }
    }
    namespace Stamper {
        enum SizeType {
            e_relative_scale,
            e_absolute_size,
            e_font_size
        }
        enum TextAlignment {
            e_align_left,
            e_align_center,
            e_align_right
        }
        enum HorizontalAlignment {
            e_horizontal_left,
            e_horizontal_center,
            e_horizontal_right
        }
        enum VerticalAlignment {
            e_vertical_bottom,
            e_vertical_center,
            e_vertical_top
        }
    }
    namespace TextExtractor {
        enum ProcessingFlags {
            e_no_ligature_exp,
            e_no_dup_remove,
            e_punct_break,
            e_remove_hidden_text,
            e_no_invisible_text
        }
        enum XMLOutputFlags {
            e_words_as_elements,
            e_output_bbox,
            e_output_style_info
        }
    }
    namespace TextSearch {
        enum ResultCode {
            e_done,
            e_page,
            e_found
        }
        enum Mode {
            e_reg_expression,
            e_case_sensitive,
            e_whole_word,
            e_search_up,
            e_page_stop,
            e_highlight,
            e_ambient_string
        }
    }
    namespace Obj {
        enum Type {
            e_null,
            e_bool,
            e_number,
            e_name,
            e_string,
            e_dict,
            e_array,
            e_stream
        }
    }
    namespace SDFDoc {
        enum SaveOptions {
            e_incremental,
            e_remove_unused,
            e_hex_strings,
            e_omit_xref,
            e_linearized,
            e_compatibility
        }
    }
    namespace SecurityHandler {
        enum Permission {
            e_owner,
            e_doc_open,
            e_doc_modify,
            e_print,
            e_print_high,
            e_extract_content,
            e_mod_annot,
            e_fill_forms,
            e_access_support,
            e_assemble_doc
        }
        enum AlgorithmType {
            e_RC4_40,
            e_RC4_128,
            e_AES,
            e_AES_256
        }
    }
    namespace VerificationOptions {
        enum SecurityLevel {
            e_compatibility_and_archiving,
            e_maximum
        }
        enum TimeMode {
            e_signing,
            e_timestamp,
            e_current
        }
    }
    namespace VerificationResult {
        enum DocumentStatus {
            e_no_error,
            e_corrupt_file,
            e_unsigned,
            e_bad_byteranges,
            e_corrupt_cryptographic_contents
        }
        enum DigestStatus {
            e_digest_invalid,
            e_digest_verified,
            e_digest_verification_disabled,
            e_weak_digest_algorithm_but_digest_verifiable,
            e_no_digest_status,
            e_unsupported_encoding
        }
        enum TrustStatus {
            e_trust_verified,
            e_untrusted,
            e_trust_verification_disabled,
            e_no_trust_status
        }
        enum ModificationPermissionsStatus {
            e_invalidated_by_disallowed_changes,
            e_has_allowed_changes,
            e_unmodified,
            e_permissions_verification_disabled,
            e_no_permissions_status
        }
    }
    namespace DisallowedChange {
        enum Type {
            e_form_filled,
            e_digital_signature_signed,
            e_page_template_instantiated,
            e_annotation_created_or_updated_or_deleted,
            e_other,
            e_unknown
        }
    }
    /**
     * @returns A promise that resolves to an object of type: "string"
     */
    function getNormalizedUrl(url: string): Promise<string>;
    /**
     * A switch that can be used to turn on/off JavaScript engine
     * @param enable - true to enable JavaScript engine, false to disable.
     */
    function enableJavaScript(enable: boolean): Promise<void>;
    /**
     * Test whether JavaScript is enabled
     * @returns A promise that resolves to true if it is enabled, false otherwise
     */
    function isJavaScriptEnabled(): Promise<boolean>;
    /**
     * Sets the location of PDFNet resource file.
     * @param path - The resource directory path to add to the search list.
     */
    function addResourceSearchPath(path: string): Promise<void>;
    /**
     * used to set a specific Color Management System (CMS) for
     * use during color conversion operators, image rendering, etc.
     * @param [t] - <pre>
     * PDFNet.CMSType = {
     * 	e_lcms : 0
     * 	e_icm : 1
     * 	e_no_cms : 2
     * }
     * </pre>
     * identifies the type of color management to use.
     */
    function setColorManagement(t?: number): Promise<void>;
    function setDefaultDeviceRGBProfile(icc_filename: string): Promise<void>;
    function setDefaultDeviceCMYKProfileFromFilter(stream: PDFNet.Filter): Promise<void>;
    function setDefaultDeviceRGBProfileFromFilter(stream: PDFNet.Filter): Promise<void>;
    /**
     * sets the default compression level for Flate (ZLib).
     * @param level - An integer in range 0-9 representing the compression value to use as
     *  a default for any Flate streams (e.g used to compress content streams, PNG images, etc).
     *  The library normally uses the default compression level (Z_DEFAULT_COMPRESSION).
     *  For most images, compression values in the range 3-6 compress nearly as well as higher
     *  levels, and do so much faster. For on-line applications it may be desirable to have
     *  maximum speed Z_BEST_SPEED = 1). You can also specify no compression (Z_NO_COMPRESSION = 0).
     */
    function setDefaultFlateCompressionLevel(level: number): Promise<void>;
    /**
     * @returns A promise that resolves to an object of type: "boolean"
     */
    function addFontSubstFromName(fontname: string, fontpath: string): Promise<boolean>;
    /**
     * @returns A promise that resolves to pDFNet version number.
     */
    function getVersion(): Promise<number>;
    /**
     * @param [level] - <pre>
     * PDFNet.LogLevel = {
     * 	e_LogLevel_Off : -1
     * 	e_LogLevel_Fatal : 5
     * 	e_LogLevel_Error : 4
     * 	e_LogLevel_Warning : 3
     * 	e_LogLevel_Info : 2
     * 	e_LogLevel_Trace : 1
     * 	e_LogLevel_Debug : 0
     * }
     * </pre>
     */
    function setLogLevel(level?: number): Promise<void>;
    /**
     * Get available fonts on the system.
     * @returns A promise that resolves to a JSON list of fonts accessible to PDFNet
     */
    function getSystemFontList(): Promise<string>;
    /**
     * Initializes PDFNet library.
    Initialize() is usually called once, during initialization.
     * @param [licenseKey] - Optional license key used to activate the product.
     If the licenseKey is not specified or is null, the product will work in the
     demo mode.
     * @param [pdfBackendType] - A string representing the "backend type" for
     rendering PDF documents. Pass "ems" to force the use of the ASM.js/WebAssembly
     worker and "wasm-threads" to use threaded WebAssembly
     */
    function initialize(licenseKey?: string, pdfBackendType?: string): Promise<void>;
    /**
     * This function is a utility method which will initialize PDFNet and
    execute an action defined by the first parameter generator. Unlike
    runGeneratorWithCleanup this method will not clean up PDFNet resources
    which can be useful when the user wishes to keep all of the objects alive.
     * @param generator - The generator object function to execute.
     * @param [license_key] - the license key used to initialize PDFNet.
     * @returns a promise that resolves to the return value of the generator.
     */
    function runGeneratorWithoutCleanup(generator: any, license_key?: string): Promise<any>;
    /**
     * This function is a utility method which will initialize PDFNet, begin a PDFNet operation
    execute an action defined by the first parameter generator and then finish this operation.
    This method will also clean up all PDFNet resources that are allocated within the
    generator which can greatly simplify user code. In some cases the user may wish to keep
    some PDFNet objects alive out of the scope of this generator function in which case
    takeOwnership should called on these objects. In case the user wishes to keep all the
    PDFNet objects alive, runGeneratorWithoutCleanup should be used instead.
     * @param generator - The generator object function to execute.
     * @param [license_key] - the license key used to initialize PDFNet.
     * @returns a promise that resolves to the return value of the generator.
     */
    function runGeneratorWithCleanup(generator: any, license_key?: string): Promise<any>;
    /**
     * Displays in the console a list of objects still in memory in the form of their type and ID.
     */
    function displayAllocatedObjects(): void;
    /**
     * startDeallocateStack initializes a deallocation point. All functions which create objects that take up
    memory after the most recent startDeallocateStack call will be deallocated upon calling
    PDFNet.endDeallocateStack().
     */
    function startDeallocateStack(): Promise<void>;
    /**
     * Deallocates all objects in memory that were created after the most recent startDeallocateStack call.
     */
    function endDeallocateStack(): Promise<void>;
    /**
     * Gets the number of PDFNet.startDeallocateStack() calls made that have not yet been ended.
     * @returns A promise that resolves to the number of PDFNet.startDeallocateStack() calls
    not yet ended.
     */
    function getStackCount(): Promise<number>;
    /**
     * Removes all PDFNetJS objects from memory.
     */
    function deallocateAllObjects(): Promise<void>;
    /**
     * This function shuts down the native backend. It will ensure the program can terminate when it is finished.
     */
    function shutdown(): void;
    /**
     * beginOperation locks all Emscripten worker operations on PDFNet so as to avoid
    potential editing conflicts. Calling beginOperation a second time before
    finishOperation is called will result in an exception being thrown. This can be
    disabled allowing multiple beginOperations to be called by passing in an options
    object (a default javascript object {}) with its parameter "allowMultipleInstances"
    set to "true".
    Will do nothing for Node.js
     * @param [optionsObj.allowMultipleInstances = false] - If allowMultipleInstances
    set to true, multiple instances of beginOperation will be allowed.
     */
    function beginOperation(optionsObj?: {
        allowMultipleInstances?: boolean;
    }): Promise<void>;
    /**
     * finishOperation releases the lock on all Emscripten worker operations on PDFNet.
    Will do nothing if PDFNet.beginOperation has not been called earlier.
    Will do nothing for Node.js
     */
    function finishOperation(): Promise<void>;
    /**
     * This function is a utility method which will initialize PDFNet, begin a PDFNet operation
    and run a callback function passed in. This method will also clean up all PDFNet resources
    that are allocated within the function which can greatly simplify user code. In some cases
    the user may wish to keep some PDFNet objects alive out of the scope of this function in
    which case takeOwnership should called on these objects. In case the user wishes to keep
    all the PDFNet objects alive, runWithoutCleanup should be used instead.
     * @param callback - A callback function to execute
     * @param [license_key] - the license key used to initialize PDFNet.
     * @returns a promise that resolves to the return value (if there is one) of the input function
     */
    function runWithCleanup(callback: (...params: any[]) => any, license_key?: string): Promise<any>;
    /**
     * This function is a utility method which will initialize PDFNet and
    execute an input. Unlike runWithCleanup this method will not clean up PDFNet resources
    which can be useful if the user wishes to keep all of the objects alive.
     * @param callback - A callback function to execute
     * @param [license_key] - the license key used to initialize PDFNet.
     * @returns a promise that resolves to the return value (if there is one) of the input function
     */
    function runWithoutCleanup(callback: (...params: any[]) => any, license_key?: string): Promise<any>;
}

