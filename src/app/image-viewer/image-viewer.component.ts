import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Rectangle } from '../draw.util';
import { DrawUtil } from './../draw.util';

declare var ImageZoom: any;

@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.scss']
})
export class ImageViewerComponent implements OnInit, AfterViewInit, OnChanges {

  blowupClass: string = 'image-viewer-blowup';
  containerClass: string = 'img-container';

  @Input() imageSrc: string;
  @Input() rectangles: Array<Rectangle>;
  @Input() width: number;
  @Input() height: number;
  @Input() imageContainerClass: string;
  @Input() imageInCanvas: boolean = false;
  @Input() zoomEnabled: boolean;
  @Input() zoomScale: number;
  @ViewChild('imageCanvas')
  canvas: ElementRef<HTMLCanvasElement>;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    if (this.imageInCanvas) {
      const ctx: CanvasRenderingContext2D = this.canvas.nativeElement.getContext('2d');
      DrawUtil.drawImage(ctx, this.imageSrc, this.width, this.height).subscribe(() => {
        this.drawRectangles(ctx);
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.zoomEnabled?.currentValue || changes.zoomScale) {
      $('.' + this.blowupClass)?.remove();
      this.initBlowup();
    } else if (changes.zoomEnabled?.currentValue === false) {
      $('.' + this.blowupClass)?.remove();
    }
  }

  onImageLoad(): void {
    if (!this.imageInCanvas) {
      this.drawRectangles();
      this.initBlowup();
    }
  }

  clearCanvas(): void {
    DrawUtil.clearCanvas(this.canvas.nativeElement.getContext('2d'), this.width, this.height);
  }

  private drawRectangles(ctx?: CanvasRenderingContext2D): void {
    ctx = ctx ? ctx : this.canvas.nativeElement.getContext('2d');
    this.rectangles.forEach(rect => {
      DrawUtil.drawRectangle(ctx, rect, this.width, this.height);
    });
  }


  private initBlowup(): void {
    if (this.zoomEnabled) {
      setTimeout(() => {
        const imageUrl: string = this.canvas.nativeElement.toDataURL();
        $('.' + this.containerClass).prepend(`<img src="${imageUrl}" class="${this.blowupClass}" style="position: absolute;"/>`);
        ($('.' + this.blowupClass) as any).blowup({ scale: this.zoomScale, imgUrl: imageUrl, id: 'abcd' });
      }, 300);
    }
  }

  private destroyBlowUp(): void {
    $('.' + this.blowupClass)?.remove();
  }

  // funzt nicht
  private initZoomJs(): void {
    setTimeout(() => {
      const imageUrl: string = this.canvas.nativeElement.toDataURL();
      const options: object = {
        width: 800,
        height: 450,
        img: imageUrl,
        zoomPosition: 'right',
        zoomWidth: 1600,
        offset: { vertical: 0, horizontal: 10 },
        scale: 0.2
      };
      // tslint:disable-next-line: no-unused-expression
      const iz: any = new ImageZoom(document.getElementById('img-container'), options);
      console.log(iz);
    }, 1000);
  }

  // funzt nicht
  private initZoomple(): void {
    setTimeout(() => {
      const imageUrl: string = this.canvas.nativeElement.toDataURL();
      $('.' + this.containerClass).attr('href', imageUrl);
      $('.' + this.containerClass).prepend(`<img id="theImg" src="${imageUrl}" style="position: absolute;"/>`);
      ($('.' + this.containerClass) as any).zoomple({
        zoomWidth: 300,
        zoomHeight: 300,
        roundedCorners: true,
        // source: 'rel',
        loaderUrl: 'assets/zoomple/loader.gif',
        blankURL: 'assets/zoomple/blank.gif',
        attachWindowToMouse: false
      });

    }, 100);
  }

}
