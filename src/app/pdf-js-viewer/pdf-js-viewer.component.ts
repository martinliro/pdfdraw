import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { getDocument, PDFPageViewport, PDFRenderParams, PDFRenderTask } from 'pdfjs-dist';
import { DrawUtil, Rectangle } from './../draw.util';


declare const WebViewer: any;


@Component({
  selector: 'app-pdf-js-viewer',
  templateUrl: './pdf-js-viewer.component.html',
  styleUrls: ['./pdf-js-viewer.component.scss']
})
export class PdfJsViewerComponent implements OnInit, OnChanges, AfterViewInit {

  blowupClass: string = 'pdf-viewer-blowup';
  containerClass: string = 'pdf-image-container';

  @Input() pdfSrc: string;
  @Input() rectangles: Array<Rectangle>;
  @Input() width: number;
  @Input() height: number;
  @Input() imageContainerClass: string;
  @Input() zoomEnabled: boolean;
  @Input() zoomScale: number;
  @Input() tronMode: boolean = false; // use PDFTron instead of pdfJS
  @ViewChild('pdfCanvas')
  canvas: ElementRef<HTMLCanvasElement>;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
    if (!this.tronMode) { // PDFJS
      getDocument(this.pdfSrc).promise.then(pdf => {
        pdf.getPage(2).then(page => { // Zweite Seite!

          // Skalierung auf Wunschbreite
          const desiredWidth: number = this.width;
          const viewport: PDFPageViewport = page.getViewport({ scale: 1, });
          const scale: number = desiredWidth / viewport.width;
          const scaledViewport: PDFPageViewport = page.getViewport({ scale: scale });

          // Canvas-Dimensionen setzen
          const canvas: HTMLCanvasElement = this.canvas.nativeElement;
          const context: CanvasRenderingContext2D = canvas.getContext('2d');
          canvas.height = scaledViewport.height;
          canvas.width = scaledViewport.width;

          // PDF-Seite in Canvas einzeichen
          const renderContext: PDFRenderParams = {
            canvasContext: context,
            viewport: scaledViewport
          };
          const renderTask: PDFRenderTask = page.render(renderContext);
          renderTask.promise.then(() => {
            // Rechtecke einzeichnen
            this.onPdfDrawn(context, canvas.width, canvas.height);
          });
        });
      });
    }
  }

  async ngAfterViewInit(): Promise<void> {
    if (this.tronMode) { // PDFTron
      // const doc: PDFNet.PDFDoc = await PDFNet.PDFDoc.createFromURL(this.pdfSrc);
      // await PDFNet.Optimizer.optimize(doc);
      WebViewer({
        path: '../../assets/pdftron',
        initialDoc: this.pdfSrc
      }, document.getElementById('pdfTronViewer'))
        .then(instance => {
          const docViewer: any = instance.docViewer;
          const annotManager: any = instance.annotManager;
          instance.disableElements(['ribbons']);
          instance.disableElements(['toolbarGroup-Shapes']);
          instance.disableElements(['toolbarGroup-Edit']);
          instance.disableElements(['toolbarGroup-Insert']);
          instance.docViewer.on('annotationsLoaded', () => {
            console.log('annotations loaded');
          });

          docViewer.on('documentLoaded', () => {
            console.log('PDFTRON: Doc loaded.');
            const { Annotations } = instance;
            this.rectangles.map(r => DrawUtil.apiRecToCanvasRec(r, this.width, this.height)).forEach(r => {
              console.log(r);
              const rectangleAnnot: Annotations.RectangleAnnotation = new Annotations.RectangleAnnotation();
              rectangleAnnot.PageNumber = 1;
              // values are in page coordinates with (0, 0) in the top left
              rectangleAnnot.X = r.x;
              rectangleAnnot.Y = r.y;
              rectangleAnnot.Width = r.width;
              rectangleAnnot.Height = r.height;
              rectangleAnnot.StrokeColor = new Annotations.Color(255, 0, 0);
              rectangleAnnot.Author = annotManager.getCurrentUser();
              annotManager.addAnnotation(rectangleAnnot);
              // need to draw the annotation otherwise it won't show up until the page is refreshed
              annotManager.redrawAnnotation(rectangleAnnot);
            });
          });
        });
    }
  }

  private optimizePDF(): void {
    // TODO:
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.zoomEnabled?.currentValue || changes.zoomScale) {
      $('.' + this.blowupClass)?.remove();
      this.initBlowup();
    } else if (changes.zoomEnabled?.currentValue === false) {
      $('.' + this.blowupClass)?.remove();
    }
  }

  onPdfDrawn(ctx: CanvasRenderingContext2D, width: number, height: number): void {
    this.rectangles.forEach(rect => {
      DrawUtil.drawRectangle(ctx, rect, width, height);
    });
  }

  private initBlowup(): void {
    if (this.zoomEnabled) {
      setTimeout(() => {
        const imageUrl: string = this.canvas.nativeElement.toDataURL();
        $('.' + this.containerClass).prepend(`<img src="${imageUrl}" class="${this.blowupClass}" style="position: absolute;"/>`);
        ($('.' + this.blowupClass) as any).blowup({ scale: this.zoomScale, imgUrl: imageUrl, id: 'ddabcd' });
      }, 100);
    }
  }

  private destroyBlowUp(): void {
    $('.' + this.blowupClass)?.remove();
  }

}
